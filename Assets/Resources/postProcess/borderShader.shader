Shader "Sprites/BorderShader"
{
	Properties
	{
		_Color ("Tint", Color) = (0.051,0.031,0.024,1)
		_Ratio ("Ratio 3:4 usually", float) = 0.75
		_Distance ("Dist to full in h", float) = 1.0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		//ZTest Always
		Fog { Mode Off }
		
		Blend DstColor Zero
		//Blend OneMinusSrcAlpha SrcAlpha
		Blend SrcAlpha	OneMinusSrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float2 texcoord : TEXCOORD;// base texture uv coordinates of the vertex

			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float2 texcoord  : TEXCOORD;
			};


			v2f vert(appdata_t IN)
			{
				v2f OUT;
				float4 v = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.vertex = v;
				OUT.texcoord = IN.texcoord;
				
				
				

				return OUT;
			}

			fixed4 _Color;
			float _Distance;
			float _Ratio;
			float4 frag(v2f IN) : COLOR
			{
			
				float2 tex = IN.texcoord.xy;
				float2 one = float2(1,1);
				float4 c = _Color;
				
				tex -= 0.5;
				//tex *= 2;
				tex = abs(tex);

				
	
				//tex.y *= _Ratio;
				float distSqrd = length(tex);
				
				distSqrd -= _Distance;
				distSqrd /= 0.3;
				distSqrd = clamp(distSqrd, 0.0, 1.0);
				
				
				
				float d = distSqrd*distSqrd;

				distSqrd = 3*d-2*d*distSqrd;
				
				
				
				
				//clip (distSqrd - 0.01);
				
				
				
				
				return float4(c.r, c.g, c.b, distSqrd);
			}
		ENDCG
		}
	}
}
