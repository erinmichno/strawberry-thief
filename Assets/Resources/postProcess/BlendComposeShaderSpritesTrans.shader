Shader "Sprites/BlendTrans"
{
	Properties
	{
		_BlendMap ("blend map tex", 2D) = "red" {}
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (0.5,0.5,1,1)
		
		_MapWidth ("Map Width Coordinate", float) = 2816
		_MapHeight ("Map Height Coordinate", float) = 3072
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		//ZTest Always
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		//BlendOp Add
		//Blend OneMinusSrcAlpha SrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
				float4 texcoord1 : TEXCOORD1;// second texture uv2 coordinates of the vertex
				
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float4 texcoord  : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 screenPos : TEXCOORD2;
			};
			
			
			float _MapWidth;
			float _MapHeight;
			fixed4 _Color;
			
			uniform float2 _ScreenCentreOffset;
			uniform float _TransitionBlendRadius;
			uniform sampler2D _BrushTex;
			uniform float _Ratio;

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				float4 v = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.vertex = v;
				OUT.screenPos = v;
				OUT.texcoord = IN.texcoord;
				
				float4 o = mul(_Object2World, IN.vertex);
				//o /= o.w;
				o.x *= 768;
				o.y *= 768;
				//o.xyz -= _WorldSpaceCameraPos;
		
		
				o.x /= _MapWidth;
				o.y /= _MapHeight;
				
				OUT.texcoord1 = o;
				
				
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _BlendMap;

			float4 frag(v2f IN) : COLOR
			{
				float4 sourceColor = tex2D(_MainTex, IN.texcoord.xy);
				float4 c = sourceColor;
			
				float2 d = (_ScreenCentreOffset - IN.screenPos.xy) * float2(_Ratio, 1.0);
				
				float dist = length(d);
				dist = min(dist / _TransitionBlendRadius, 1.0);
								
				float a = max(tex2D(_BlendMap, IN.texcoord1.xy).r, tex2D(_BrushTex, float2(0.5 + (dist / 2.0), 0.5)).r);
				
				return float4(c.r, c.g, c.b, a);
			}
		ENDCG
		}
	}
}
