Shader "Post/RedShader"
{
	SubShader
	{
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }
		
		Pass
		{

		    BindChannels
		    { 
				Bind "vertex", vertex 
				Bind "texcoord", texcoord0
			}
			
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "UnityCG.cginc"

			struct appdata_t {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
			};
	
			struct v2f {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
			};
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = v.texcoord;
				return o;
			}
			
			float4 frag (v2f i) : COLOR
			{
				return float4(1.0, 0.0, 0.0, 0.0);
			}
			ENDCG
		}
	}
	
	Fallback off
}