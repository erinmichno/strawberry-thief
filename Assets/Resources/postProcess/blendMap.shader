Shader "Post/blendMap"
{
	Properties
	{
		_MainTex ("Input", 2D) = "black" {}
		_BrushTex("brush texture", 2D) = "white" {}
		_ScorchColor ("Scorch Color", color) = (1, 1, 1, 1)
		_ScorchUCoord ("Scorch U Coordinate", float) = 0
		_ScorchVCoord ("Scorch V Coordinate", float) = 0
		_HalfTexelWidth ("Half Texel Width", float) = 0
		_PollenAmount("Pollen Percentage", float) = 1
		_Ratio("texture ratio", float) = 1
		_Hardness("Hardness", float) = 64
	}
	
	SubShader
	{
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }
		
		Pass
		{

		    BindChannels
		    { 
				Bind "vertex", vertex 
				Bind "texcoord", texcoord0
				Bind "texcoord1", texcoord1
			}
			
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "UnityCG.cginc"
			
			struct appdata_t {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
				float4 texcoord1 : TEXCOORD1;// second texture uv2 coordinates of the vertex
			};
	
			struct v2f {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
				float4 texcoord1 : TEXCOORD1;// second texture uv2 coordinates of the vertex
			};
			
			sampler2D _MainTex;
			sampler2D _BrushTex;
			float4 _ScorchColor;
			float _ScorchUCoord;
			float _ScorchVCoord;
			float _HalfTexelWidth;
			float _PollenAmount;
			float _Ratio;
			float _Hardness;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = v.texcoord;
				o.texcoord1 = v.texcoord1;
				return o;
			}
			
			float4 frag (v2f i) : COLOR
			{

				//TODO make faster by making vector pass in vector2 for uv
				float ratio = 0.916667;
				ratio = _Ratio;

				const float2 one = float2(1.0, 1.0);
				const float2 zero = float2(0.0, 0.0);
				const float2 halfMN = (float2(0.5, 0.5));
				float2 pollenPt = float2(_ScorchUCoord, _ScorchVCoord); //o

				float2 d = (pollenPt - i.texcoord.xy)*float2( ratio, 1.0);
				float distSqrd = dot(d, d);


				//figure out distance with wrapping
				float2 needsWrap = min(one, max(zero, (abs(d) - halfMN)*1000000.0));//0 or 1

				float2 greaterThanHalf = min(one,  max(zero,(pollenPt - halfMN)*1000000.0 ));
				float2 newTextCoord = i.texcoord.xy;
				newTextCoord += (greaterThanHalf - (1 - greaterThanHalf))*needsWrap;
				d = (pollenPt - newTextCoord)*float2( ratio, 1.0);
				distSqrd = dot(d, d);//sq
				



				float fragChanged = min(distSqrd  - (_HalfTexelWidth), 0.0);
				fragChanged *= -_Hardness;
				fragChanged = min(fragChanged, 1.0)*_PollenAmount;

				
				float2 f = max( float2(_HalfTexelWidth, _HalfTexelWidth) - abs(d), zero);
				fragChanged = f.x*f.y*1000000;
				fragChanged = min(fragChanged, 1.0)*_PollenAmount;
				float2 bUV = zero;
				bUV.x = -d.x / (2.0*_HalfTexelWidth);
				bUV.x += 0.5;
				bUV.y = -d.y / (2.0*_HalfTexelWidth);
				bUV.y += 0.5;
				
				bUV.x = clamp(bUV.x, 0.0, 1.0);
				bUV.y = clamp(bUV.y, 0.0, 1.0);



				float3 br = tex2D(_BrushTex, bUV);
				fragChanged *= br.r;
				
				
	
			    float finalColor = fragChanged + tex2D(_MainTex, i.texcoord.xy).r*(1 - fragChanged);
			//	float finalColor = max(fragChanged, tex2D(_MainTex, i.texcoord.xy).r);
				
			
				return finalColor;


				///////////////
					
				//if(abs(_ScorchUCoord - i.texcoord.x) < _HalfTexelWidth * 16 && abs(_ScorchVCoord - i.texcoord.y) < _HalfTexelWidth * 16
				// ) //mostly black may remove
				//{
				//	
				//	finalColor = float4(0.0f, 1.0f, 0.0f, 1.0f);
				//	fragChanged = 1;
				//}
				
			}
			ENDCG
		}
	}
	
	Fallback off
}