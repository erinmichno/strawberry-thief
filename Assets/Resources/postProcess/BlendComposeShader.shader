Shader "Post/BlendComposeShader"
{
	Properties
	{
		_MainTex ("Input", 2D) = "white" {}
		_MainTex2 ("bright tex", 2D) = "black" {}
		_BlendMapTexture ("blury tex", 2D) = "black" {}
	}
	
	SubShader
	{
		ZTest Always Cull Off ZWrite Off
		Fog { Mode off }
		
		Pass
		{

		    BindChannels
		    { 
				Bind "vertex", vertex 
				Bind "texcoord", texcoord0
				Bind "texcoord1", texcoord1
			}
			
			CGPROGRAM
			#pragma target 3.0
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			
			#include "UnityCG.cginc"


			struct appdata_t {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
				float4 texcoord1 : TEXCOORD1;// second texture uv2 coordinates of the vertex
			};
	
			struct v2f {
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
				float4 texcoord1 : TEXCOORD1;// second texture uv2 coordinates of the vertex
				
			};
			
			sampler2D _MainTex;
			sampler2D _MainTex2;
			sampler2D _BlendMapTexture;
			
			v2f vert (appdata_t v)
			{
				v2f o;
				//float4 outPos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.texcoord = v.texcoord;
				o.texcoord1 = v.texcoord1;
				return o;
			}
			
			float4 frag (v2f i) : COLOR
			{
				float4 sourceColor = tex2D(_MainTex, i.texcoord.xy);
				float4 sourceColor2 = tex2D(_MainTex2, i.texcoord.xy);
				
				//float2 d = (_ScreenCentreOffset - i.screenPos.xy) * float2(_Ratio, 1.0);
				//float dist = length(d);
				//dist = min(dist / _TransitionBlendRadius, 1.0);
				
				//float alpha = max(tex2D(_BlendMapTexture, i.texcoord1.xy).r, tex2D(_BrushTex, float2(0.5 + (dist / 2.0), 0.5)).r);
				
				float alpha = tex2D(_BlendMapTexture, i.texcoord1.xy).r;
				float OneMinus = max(0.0, 1.0 - alpha);
				//if (Result.b>0.5) {Result.b = 1-(1-2*(Result.b-0.5))*(1-Pixel.b);}		//Calculate Overlay of blue, first half
				//else {Result.b = (2*Result.b)*Pixel.b;}	//pixel is layer1 (blurey)   result is layer0 (feed)

				float4 finalC =  sourceColor*OneMinus + sourceColor2*(alpha); //straight up blend for now
				
				finalC.a = 1.0f;
				
				return finalC;
			}
			ENDCG
		}
	}
	
	Fallback off
}