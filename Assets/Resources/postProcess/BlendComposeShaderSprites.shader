Shader "Sprites/Blend"
{
	Properties
	{
		_BlendMap ("blend map tex", 2D) = "red" {}
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		
		_MapWidth ("Map Width Coordinate", float) = 2816
		_MapHeight ("Map Height Coordinate", float) = 3072
		[MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
	}

	SubShader
	{
		Tags
		{ 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
		}

		Cull Off
		Lighting Off
		ZWrite Off
		//ZTest Always
		Fog { Mode Off }
		Blend SrcAlpha OneMinusSrcAlpha
		//BlendOp Add
		//Blend OneMinusSrcAlpha SrcAlpha

		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile DUMMY PIXELSNAP_ON
			#include "UnityCG.cginc"
			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 texcoord : TEXCOORD0;// base texture uv coordinates of the vertex
				float4 texcoord1 : TEXCOORD1;// second texture uv2 coordinates of the vertex
				
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				float4 texcoord  : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;

			};
			
			
			float _MapWidth;
			float _MapHeight;
			

			v2f vert(appdata_t IN)
			{
				v2f OUT;
				//float4 v = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.vertex = mul(UNITY_MATRIX_MVP, IN.vertex);
				OUT.texcoord = IN.texcoord;
				
				float4 o = mul(_Object2World, IN.vertex);
				//o /= o.w;
				o.x *= 768;//not sure if this should be in there
				o.y *= 768;
				//o.xyz -= _WorldSpaceCameraPos;
		
		
				o.x /= _MapWidth;
				o.y /= _MapHeight;
				
				OUT.texcoord1 = o;
				
				
				#ifdef PIXELSNAP_ON
				OUT.vertex = UnityPixelSnap (OUT.vertex);
				#endif

				return OUT;
			}

			sampler2D _MainTex;
			sampler2D _BlendMap;

			float4 frag(v2f IN) : COLOR
			{
				float4 c = tex2D(_MainTex, IN.texcoord.xy);		

				
				return float4(c.r, c.g, c.b, tex2D(_BlendMap, IN.texcoord1.xy).r);
			}
		ENDCG
		}
	}
}
