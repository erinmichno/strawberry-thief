﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SplashScreenController : MonoBehaviour
{
    public List<UIPanel> splashPanels = new List<UIPanel>();

    private EventDelegate callback = null;

    private int currentPanel = -1;

    void Awake()
    {
        foreach (UIPanel splashPanel in splashPanels)
        {
            splashPanel.gameObject.SetActive(false);
        }
    }

    public void StartSplash(EventDelegate _callback)
    {
        callback = _callback;
        currentPanel = -1;
        ActivateNextPanel();
    }

    private void ActivateNextPanel()
    {
        if (currentPanel >= 0)
        {
            splashPanels[currentPanel].gameObject.SetActive(false);
        }

        currentPanel++;

        if (currentPanel < splashPanels.Count)
        {
            splashPanels[currentPanel].gameObject.SetActive(true);

            UIScreenFade.GetScreenFadeFromBlack(2.0f, new EventDelegate(OnFadeInFinished));
        }
        else
        {
            UIScreenFade.GetScreenFadeFromBlack(2.0f, null);
            if (callback != null)
            {
                callback.Execute();
            }
        }
    }

    private void OnFadeInFinished()
    {
        UIScreenFade.GetScreenFadeToBlack(2.0f, new EventDelegate(ActivateNextPanel));
    }
}
