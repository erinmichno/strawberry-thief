
ShaderWizard CG Shader Generator - Version 2.1
Copyright 2013 Paul West/Venus12 LLC


Using ShaderWizard

Simply click on the ShaderWizard prefab and use the inspector! No need to add the prefab to your scene!

Now includes a simple demo - just load the MultiTexture Demo scene and hit Play!


Introduction

ShaderWizard is a quick and easy way to generate shaders for your Unity project without having to write
shader code. It currently features a growing list of 33 unique shader functions:

Color (applies a variable color only, no textures)
VertexColor (applies colors from vertices only, no textures)
Texture (applies one texture only)
Texture+Color (applies one texture and multiplies it by a variable color)
Texture+VertexColor (applies one texture and multiplies it by colors from the vertices)
MultiTexture (applies your choice of 28 merge functions between 2 to 16 texture layers)

The MultiTexture function, new to ShaderWizard 2.0, allows you to combine 2 to 16 texture layers
(hardware texture units permitting) using familiar Photoshop-style `blends` such as Multiply, Overlay, Screen,
Color Dodge, Alpha Blend etc, as well as a few blends unique to ShaderWizard. Without factoring in
variations in the number of MultiTexture layers, ShaderWizard can generate over 850 unique shaders,
combining shader functions and merges with useful blend modes. When considering variations from
other settings (and the handy ability to `Generate All` settings), ShaderWizard can literally output
millions of shaders - and their materials too.

All of ShaderWizard's shaders are generally Shader Model 2.0 using (e.g. DirectX 9) vertex and fragment
programs, written in the CG shader programming language. However, since MultiTexture shader programs with a
larger number of layers and/or complex merges can require a lot of arithmetic/texture instructions and variables,
Shader Model 2.0 may not support all shaders. This is why ShaderWizard now supports a switch to output Shader
Model 3.0 shaders which gives much greater scope for multiple texture layers. So if Unity reports not
having enough registers or too many instructions, switch on Shader Model 3.0 and everything should work.

ShaderWizard provides absolutely no support for the old `fixed-function` graphics pipeline. For mobile devices
like iPhone you will be able to target iPhone 3GS and later with OpenGLES 2.0, but not 3G or prior
(Apple has dropped 3G support now). Over 80% of Windows PCs and almost all Macs can support Shader Model 2.0
pixel shaders and a large pecentage of those can support Shader Model 3.0. The shaders are optimized to be
efficient on mobile devices (and indeed, the desktop) using the cheapest data formats possible. You may wish to
change the texture coordinate (TEXCOORD0) variables from `half` to `float` format for desktop if you need the
extra range/precision, and in some cases could use `fixed` for use with non-clamping textures. If you find that
color accuracy isn't specific enough, try editing the shader code by search-replacing all instances of
`fixed` with `half`, or replace `half` with `float`.

ShaderWizard sports many useful shaders, combining popular and interesting blend modes with multiple texture
support and support for fixed colors or vertex colors. But there are so many possible shaders that could be
supported and it doesn't currently support shadows, lights or normal maps. Future versions will feature more
advanced cool stuff like:
  - More functions for making cool 2D games/effects such as fast bumpmapping, tilemaps, particles etc
  - More functions that support 3D such as normal maps, shadows, lighting etc
  - Color changes like grayscale, hue shift, colorizing, paletted textures, color cycling etc
  - Even more Multitexturing merges and support for per-layer Opacity, Vertex Colors, UV2 etc
  - Vectorgraphics
  and much more!


Motivations

Unity has done a great job of simplifying the shader-creation process. Unity's `ShaderLab` is a structured
language designed for designing the behavior of a shader, supporting the older fixed-function pipeline
and also the more modern vertex and fragment shaders via CG code. You don't have to worry too much about
making the shader work on a piece of hardware or what features are supported - unless you want to do
something more advanced, and you definitely don't need to deal with compilation or graphics API's.

But for a beginner or intermediate user, creating a new shader can be painful and time consuming with a
learning curve all of its own. It's like learning a whole new language, or two! Creating the shader is a
drag - you first must learn the structure of a shader you have to remember what each section is for,
you have to get the syntax exactly right otherwise you'll get cryptic shader errors, you have to
remember what parameters are allowed for each command and exactly how to type them, and on top of that
you have to figure out how to make the shader get the result you want. It can be a tedious process.

This can make shader construction a challenging task even for really simple shaders. I know because, like
many Unity developers, I've experienced the learning curve first hand and I still have to keep looking at
the documentation to get it right. Having to reference documentation over and over again to get things to
work is not an easy workflow - shader syntax is picky. This is where ShaderWizard steps in.

ShaderWizard provides a simple user interface for the novice/average user wishing to create
simple-to-intermediate and often-times cool shaders in Unity. There are no scripts to be written, no
structures to figure out, no syntax to learn, no parameters to remember, no spelling errors, no confusing
shader code in a whole new language, and absolutely no shader errors. A few clicks to choose what you want
the shader to do and you're done! ShaderWizard writes the shader for you - it can even mass-produce large
batches of variations very quickly. In fact it can generate 100's, even 1000's of shaders per minute on an
average computer. Try doing that by hand!

ShaderWizard's shaders are relatively simple, but it's often these simple shaders that we need,
especially if you're making a 2D game and need to do some cool things other than alphablend. If you're
looking for advanced 3D shaders with lighting and fancy effects, ShaderWizard may not be the tool for you,
at least not yet. It creates shaders that are great for 2D games and simpler 3D games in a quick and easy
manner. Some people like using a visual shader editor instead of writing shaders by hand, but you may find
these too have a learning curve and even though you're putting toget the shader `visually` it's still not
far removed from `programming` - you have to know what you're doing. ShaderWizard provides a super-easy
interface to quickly generate useful shaders, letting you make artistic decisions where they matter
and try out multiple variations of textures quickly, rather than in figuring out how to implement them.

One final reason that ShaderWizard exists is that I myself found in need of a number of shaders with no
quick path to creating them. I didn't want to sit and create them all by hand so I figured it would be faster
to let the computer automate the process. It is prohibitive to include a large number of shaders
in a project (especially not millions of them - every one takes like a second to import and compile by Unity)
so simply generating them and distributing the end results was not an option. A shader generator was the
natural solution and this is where ShaderWizard fits the bill, acting as a flexible distribution system,
an efficient shader storage system and a graphical directory.



How it works

1) Unity feeds information into the shader. ShaderWizard currently supports a fixed color, vertex colors, and
either of those in combination with a single texture. It also supports MultiTexturing using 2-16 textures where
each pair of textures can be combined using one of 28 `Merge` modes.

2) You design your shaders and choose a shader function to match one of these combinations of input:
   - Fixed Color = A single fixed color is output
   - Vertex Colors = Colors are input from the vertex color data, interpolated with gouraud shading and output
   - Texture = A single texture is input and its pixel colors are output
   - Texture with a Fixed Color = A single texture is input and its pixel colors are multiplied by a fixed
color then output
   - Texture with Vertex Colors = A single texture is input and its pixel colors are multiplied by gouraud
interpolated colors from the vertex data then output
   - MultiTexture = From 2 to 16 textures are combined one after the other against a running result, in
any one of 28 `merge` modes, most of which will be familiar to users of Photoshop and other graphics software.

3) The shaders generated by ShaderWizard will take the inputs from Unity and process them through a
`function` and possible a `merge` (for MultiTexture shaders). It then outputs the result to the `blender`.
The blender then combines the shader output with the existing pixel in the backbuffer to give you the final
output. The blender features 26 useful blend modes. This means for every function, there are 26 versions
of how that shader can output. The blend modes are:
   - Solid = Pixels are completely overwritten
   - Solid with Alpha Test - Pixels are overwritten if the alpha value output from the shader meets a
certain threshold within a given testing mode
   - AlphaBlend - Pixels are alphablended/crossfaded between shader output and the backbuffer
   - AlphaBlend with Alpha Test - As above but with alpha testing
   - AlphaBlend Add - This is like alphablending but instead of scaling the shader output it is added in
full to an inversely  scaled backbuffer pixel - it's like an Add mode that lets you alpahblend the
source texture
   - AlphaBlend Add with Alpha Test - As above but with alpha testing
   - AlphaBlend Subtract - This is like AlphaBlendAdd but instead of adding the shader output to the
scaled backbuffer pixel, it subtracts from it
   - AlphaBlend Subtract with Alpha Test - As above but with alpha testing
   - Add - Adds the source pixel to the destination pixel
   - Add Soft - Adds the source pixel `softly` to the destination pixel - result stays less white
   - Subtract - Subtracts the source pixel from the destination pixel
   - Subtract Soft - Subtracts the source pixel `softly` from the destination pixel - result stays less black
   - Subtract Reverse - Inversely subtracts the destination pixel from the shader output
   - Multiply - Multiplies the shader output with the destination pixel
   - Multiply Double - Multiplies the shader output with the destination pixel and then doubles
it (multiplies twice then adds the two results together)
   - Min - Only lets the minimum value of the shader output and the destination pixel be output to
the backbuffer
   - Max - Only lets the maximum value of the shader output and the destination pixel be output to
the backbuffer
   - Screen - Performs a `screen` operation, which is kind of an inverse multiply that brightens
the image instead of darkening
   - Dest AlphaBlend - Uses the backbuffer's alpha channel to alphablend between the shader output and
the destination pixel
   - Dest AlphaBlend Add - As above but instead of scaling the shader output it adds it in full
   - Dest AlphaBlend Subtract - As above but instead of adding the shader output to the inversely
scaled backbuffer value, it subtracts from it
   - Add to Dest Alpha - Adds a source alpha value to the existing alpha value in the backbuffer
   - Subtract from Dest Alpha - Subtracts a source alpha value from the existing alpha value in the backbuffer
   - Multiply Dest Alpha - Multiplies a source alpha value by the destination alpha value in the backbuffer
   - Dest Alpha Colorize Add - Uses the destination alpha value to scale the source color, and adds it
to the source alpha value multiplied by the destination color - ie it uses destination alpha
(grayscale version of destination rgb) to colorize the backbuffer
   - Dest Alpha Colorize Subtract - As above but with a subtract operation

4) You can also set up some other shader parameters such as:
   - Culling = Whether front/back-facing triangles are removed from rendering
   - Z-Test = Whether the Depth Buffer (Z-Buffer) is tested to see if the new pixel is nearer to the camera etc
   - Z-Write = Whether the new pixel's Z coordinate is written to the Depth Buffer (Z-Buffer)
   - Mask = Which color channels are allowed to be written in the backbuffer
   - Adjust = (For Texture functions only) decide whether the texture coords can be affected by Tiling/Offset
   - Color = (For Color functions only) decide what fixed color will be used as input to the shader
   - Default Texture = (For Texture functions only) decide what texture will be used by default
   - Alpha Threshold = (For Alpha-testing blends only) what threshold to use in the alpha test (0..1)

5) You can set up some extra features that let you customize how the shaders are generated:
   - Model = Specify whether the shader will be written for ShaderModel 2.0 hardware or ShaderModel 3.0
hardware. 3.0 mainly comes in handy when you are making big MultiTexture shaders with multiple layers.
   - Comments = The amount of detail that will be added to the shaders as comments - can range from none
through header, header and sections, header and sections and per-line comments, and verbose (extra info)
   - Folder Mode = Whether shaders will be grouped into folders by Function, Blend Mode, both or neither (flat)
   - Counter = Whether a numerical counter will be included (starting at an Offset) on shader names and folders
   - Naming = Automatic or Manual - if automatic a shader name is generated including Prefix and Suffix,
otherwise only Prefix and Suffix are used. If there is no shader name and no counter, a name will be generated
   - Include = Whether to include the names of every `Merge` being used in MultiTexture shaders, as part of the
generated name. With large MultiTexture shaders comprising many layers, this can produce very long filenames
which may be rejected by Unity and the Operating System, which can cause Unity to crash! Set this option to
`exclude` merge names from very large shaders, or use the `manual` naming system.
   - Menu Name = A name for a menu to put the shaders under in the `Shaders` dropdown (on a material) - optional
   - Sub Name = A submenu name which will go under the Menu Name menu (if there is one) - also optional
   - Output Path = ShaderWizard outputs to a `Resources` folder that it will auto-generate within whatever
folder path you specify - if you leave this empty it will generate under Resources in the Assets folder. Do
not include a forward or backwards slash!
   
6) You can tell ShaderWizard to generate a material for each shader. When it does this every shader will be
first generated, imported to Unity, compiled, and then a material will be added with the shader automatically
attached. If there is a fixed Color or Default Texture or other adjustable parameter, these too will be
automatically applied.   

7) When you are ready, the generate button will tell you how many shaders will be generated. This represents
the total number of combinations of all `Generate All` settings from each dropdown menu. Press the button
to generate the shaders. You can cancel at any time from the progress bar that will appear. If you try to generate
more than 100 shaders the system will warn you first just so you don't accidentally create thousands of
them - note you can cancel the operation at any time from the progress bar, which may leave some shaders created
and possibly some but not all materials created. ShaderWizard will not delete any generated files when you cancel.

8) When you want to generate multiple shaders you can do so by choosing `Generate All` from one of the dropdown
menu's. Every option from the menu will generate an individual shader, and therefore every combination of
options across multiple menus will produce a unique shader. As you can imagine, the number of options in each
menu will be successively *multiplied*, so the total number of shaders can increase exponentially when you
use multiple `Generate All` settings. If you want to create shaders in several batches for some reason, e.g. you
only want 2 or 3 blend modes, a tip is to create one set with the Counter Offset at 0 and then the next set with
a higher counter to start the next batch counting where you left off. Also use the counter to force previously
generated shaders to be overwritten rather than starting from 0 for a partial batch.

9) Just from every combination of Function and Mege and Blend Mode with no other variations, which represents the
current full diversity of shader outputs, ShaderWizard can generate over 850 unique shaders! Each function can
output in 26 variations. When you mix in options such as Z-buffering on/off, Culling off/back/front etc
the numbers quickly multiply. ShaderWizard can currently generate over 12 million unique shaders!

10) When you're ready to use the generated shader in your project, either add it to a material or
use the material you generated. Simply choose your game object, make sure it has a Mesh Renderer (or add one),
select the MeshRenderer and expand the Materials list, then either drag a new material to the list or
click the selector to choose it from the Material Selection window. Once chosen, tweak your shader properties
(assign a different texture, set a color, adjust alpha testing, etc) and you're good to go!

NB: Remember that complex MultiTexture shaders with many layers/complex merges may require using ShaderModel 3.0
just to enable the shader to be compilable, but none of the shaders require any other ShaderModel 3.0 features
and will all otherwise run on ShaderModel 2.0 hardware.


Enjoy.

Paul West (ImaginaryHuman on the Unity forums)
http://forum.unity3d.com/members/17044-imaginaryhuman
