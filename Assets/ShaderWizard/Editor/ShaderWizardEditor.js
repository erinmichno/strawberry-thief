@CustomEditor (ShaderWizard)

//Editor for Shader Wizard - Copyright (c) 2013 Paul West/Venus12 LLC

//Version 2.1

class ShaderWizardEditor extends Editor {
	//Editor for the ShaderWizard script

	var NumberOfFunctions:int=7;			//How many functions including generate-all
	var NumberOfTextureCounts:int=15;		//How many texture counts
	var NumberOfMerges:int=29;				//How many Merge operations for multitexturing
	var NumberOfScaleTranslateTextures:int=3;	//How many texture scale/translate options including generate-all
	var NumberOfBlends:int = 27;			//How many blend mode options including generate-all
	var NumberOfTextureAdjusts:int=3;		//How many texture adjustment modes including generate-all
	var NumberOfZWrites:int=3;				//How many Z-writes including generate-all
	var NumberOfZTests:int=9;				//How many Z-tests including generate-all
	var NumberOfMasks:int=16;				//How many color masks including generate-all
	var NumberOfCullings:int=4;				//How many culling options including generate-all
	
	var BatchMode:boolean=false;			//Need to generate multiple shaders?
	var BatchTotal:int=0;					//How many total shaders will be generated?
	var GenerateButtonText:String;			//Text for generate button
	var GenerateButtonCaption:String;		//Text for generate button's caption
	var FirstShaderObject:String;			//Resource-relative path to the first shader object generated

	//Names of each option for dropdowns
	var FunctionNames:String[] = ["Color","VertexColor","Texture","Texture+Color","Texture+VertexColor","MultiTexture","Generate All"];			//Function names
	var TextureCountNames:String[]=["2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"];	//Multitexturing number of textures
	var MergeNames:String[] = ["Alpha Blend","Alpha Blend Add","Alpha Blend Subtract","Add","Add Soft","Subtract","Subtract Soft","Multipy","Multiply Double","Divide","Min","Max","Screen","Alpha Mask","Alpha Fade","Difference","Negative","Exclusion","Overlay","Hard Light","Soft Light","Color Dodge","Linear Dodge","Color Burn","Linear Burn","Vivid Light","Linear Light","Pin Light","Generate All"];
	var ScaleTranNames:String[] = ["Ignore Tiling and Offset","Allow Tiling and Offset","Generate All"];								//Scale/Translate texture names
	var BlendNames:String[] = ["Solid","Solid + Test","Alpha Blend","Alpha Blend Test","Alpha Blend Add","Alpha Blend Add Test","Alpha Blend Subtract","Alpha Blend Subtract Test","Add","Add Soft","Subtract","Subtract Soft","Subtract Reverse","Multiply","Multiply Double","Min","Max","Screen","Dest Alpha Blend","Dest Alpha Blend Add","Dest Alpha Blend Subtract","Add To Dest Alpha","Subtract From Dest Alpha","Multiply Dest Alpha","Dest Alpha Colorize Add","Dest Alpha Colorize Subtract","Generate All"];	//Blend mode names
	var ZWriteNames:String[] = ["Off","On","Generate All"];				//ZWrite names
	var ZTestNames:String[] = ["Off","Less","Greater","LEqual","GEqual","Equal","NotEqual","Always","Generate All"];	//ZTest names
	var MaskNames:String[] = ["RGBA","RGB","A","R","G","B","RG","RB","RA","GB","GA","BA","RBA","RGA","GBA","Generate All"];				//Color mask names
	var CullingNames:String[] = ["Off","Back","Front","Generate All"];	//Culling names
	
	//GUI element dropdown lists and comments
	var ShaderFunctionPresets : GUIContent[] = [GUIContent(FunctionNames[0],"Output a fixed color"), GUIContent(FunctionNames[1],"Output color stored in the geometry's vertex data"), GUIContent(FunctionNames[2],"Output texel color from a texture"), GUIContent(FunctionNames[3],"Output texel color from a texture multiplied by a fixed color"), GUIContent(FunctionNames[4],"Output texel color from a texture multiplied by color stored in the geometry's vertex data"), GUIContent(FunctionNames[5],"Output texel color from multiple textures after Merge operation(s)"), GUIContent(FunctionNames[6],"Generate a shader for every function")];
	var TextureCountPresets : GUIContent[] = [GUIContent(TextureCountNames[0],"Merge 2 Textures"), GUIContent(TextureCountNames[1],"Merge 3 Textures"), GUIContent(TextureCountNames[2],"Merge 4 Textures"), GUIContent(TextureCountNames[3],"Merge 5 Textures"), GUIContent(TextureCountNames[4],"Merge 6 Textures"), GUIContent(TextureCountNames[5],"Merge 7 Textures"), GUIContent(TextureCountNames[6],"Merge 8 Textures"), GUIContent(TextureCountNames[7],"Merge 9 Textures"), GUIContent(TextureCountNames[8],"Merge 10 Textures"), GUIContent(TextureCountNames[9],"Merge 11 Textures"), GUIContent(TextureCountNames[10],"Merge 12 Textures"), GUIContent(TextureCountNames[11],"Merge 13 Textures"), GUIContent(TextureCountNames[12],"Merge 14 Textures"), GUIContent(TextureCountNames[13],"Merge 15 Textures"), GUIContent(TextureCountNames[14],"Merge 16 Textures")]; 
	var MergePresets : GUIContent[] = [GUIContent(MergeNames[0],""),GUIContent(MergeNames[1],""),GUIContent(MergeNames[2],""),GUIContent(MergeNames[3],""),GUIContent(MergeNames[4],""),GUIContent(MergeNames[5],""),GUIContent(MergeNames[6],""),GUIContent(MergeNames[7],""),GUIContent(MergeNames[8],""),GUIContent(MergeNames[9],""),GUIContent(MergeNames[10],""),GUIContent(MergeNames[11],""),GUIContent(MergeNames[12],""),GUIContent(MergeNames[13],""),GUIContent(MergeNames[14],""),GUIContent(MergeNames[15],""),GUIContent(MergeNames[16],""),GUIContent(MergeNames[17],""),GUIContent(MergeNames[18],""),GUIContent(MergeNames[19],""),GUIContent(MergeNames[20],""),GUIContent(MergeNames[21],""),GUIContent(MergeNames[22],""),GUIContent(MergeNames[23],""),GUIContent(MergeNames[24],""),GUIContent(MergeNames[25],""),GUIContent(MergeNames[26],""),GUIContent(MergeNames[27],""),GUIContent(MergeNames[28],"")];
	var ScaleTranslateTexturePresets : GUIContent[] = [GUIContent(ScaleTranNames[0],"Ignore the Tiling and Offset adjustments for texture coordinates on Texture Layer 0"), GUIContent(ScaleTranNames[1],"Allow the texture coordinates to be affected by Tiling and Offset adjustments on Texture Layer 0"), GUIContent(ScaleTranNames[2],"Generate a shader for every Texture Adjustment state")];
	var BlendModePresets : GUIContent[] = [GUIContent(BlendNames[0],"Replace the destination pixel with the shader output"), GUIContent(BlendNames[1],"Replace the destination pixel with the shader output if the output's alpha value is greater than the Threshold"), GUIContent(BlendNames[2],"Alphablend/Crossfade between the shader output value and the destination pixel based on the output's alpha value"), GUIContent(BlendNames[3],"Alphablend/Crossfade between the shader output value and the destination pixel based on the output's alpha value, but only if the output's alpha value is greater than the Threshold"), GUIContent(BlendNames[4],"Alphablend/Crossfade between the shader output value and the destination pixel based on the output's alpha value, but add the full value of the output instead of scaling it by the output's alpha (an alphablending 'add' shader)"), GUIContent(BlendNames[5],"Alphablend/Crossfade between the shader output value and the destination pixel based on the output's alpha value, but add the full value of the output instead of scaling it by the output's alpha (an alphablending 'add' shader), but only output if the output's alpha is greater than the Threshold"), GUIContent(BlendNames[6],"Alphablend/Crossfade between the shader output value and the destination pixel based on the output's alpha value, but subtract the full value of the output from the destination pixel value instead of scaling it by the output's alpha (an alphablending 'subtract' shader)"), GUIContent(BlendNames[7],"Alphablend/Crossfade between the shader output value and the destination pixel based on the output's alpha value, but subtract the full value of the output from the destination pixel value instead of scaling it by the output's alpha (an alphablending 'subtract' shader), but only output if the output's alpha is greater than the Threshold"), GUIContent(BlendNames[8],"Add the output value to the value of the destination pixel"), GUIContent(BlendNames[9],"Add the output value to the inverse of the value of the destination pixel"), GUIContent(BlendNames[10],"Subtract the output value from the destination pixel value"), GUIContent(BlendNames[11],"Subtract the output value from the inverse of the destination pixel value"), GUIContent(BlendNames[12],"Subtract the destination pixel value from the output value (inverse)"), GUIContent(BlendNames[13],"Multiply the output value by the destination pixel value"), GUIContent(BlendNames[14],"Multiply the output value by the destination pixel value twice and add the two results together (2x multiply)"), GUIContent(BlendNames[15],"Output the lower value of the output and the destination pixel value"), GUIContent(BlendNames[16],"Output the higher value of the output and the destination pixel value"), GUIContent(BlendNames[17],"Inversely multiply the output value with the destination pixel value"), GUIContent(BlendNames[18],"Use the destination alpha value to alphablend/crossfade between the output value and the destination color value"), GUIContent(BlendNames[19],"Use the destination alpha value to alphablend/crossfade between the output value and the destination color value, but add the full value of the output instead of scaling it"), GUIContent(BlendNames[20],"Use the destination alpha value to alphablend/crossfade between the output value and the destination color value, but subtract the full value of the output instead of scaling it"), GUIContent(BlendNames[21],"Add the output's alpha value to the destination alpha value"), GUIContent(BlendNames[22],"Subtract the output's alpha value from the destination alpha value"), GUIContent(BlendNames[23],"Multiply the destination alpha value by the output's alpha value"), GUIContent(BlendNames[24], "Multiply the destination alpha value by the source output value and add it to the source alpha value multiplied by the destination color value"), GUIContent(BlendNames[25], "Multiply the destination alpha value by the source output value and subtract it from the source alpha value multiplied by the destination color value"), GUIContent(BlendNames[26],"Generate a shader for every blend mode")];
	var ZWritePresets : GUIContent[] = [GUIContent(ZWriteNames[0],"Do not write Z coordinates to the Z/Depth buffer"), GUIContent(ZWriteNames[1],"Write Z coordinates to the Z/Depth buffer"), GUIContent(ZWriteNames[2],"Generate a shader for every Z Write state")];
	var ZTestPresets : GUIContent[] = [GUIContent(ZTestNames[0],"Do not test the Z/Depth buffer"), GUIContent(ZTestNames[1],"Only allow pixels to be output if their Z coordinate is less than the existing Z coordinate in the Z/Depth buffer"), GUIContent(ZTestNames[2],"Only allow pixels to be output if their Z coordinate is greater than the existing Z coordinate in the Z/Depth buffer"), GUIContent(ZTestNames[3],"Only allow pixels to be output if their Z coordinate is less than or equal to the existing Z coordinate in the Z/Depth buffer"), GUIContent(ZTestNames[4],"Only allow pixels to be output if their Z coordinate is greater than or equal to the existing Z coordinate in the Z/Depth buffer"), GUIContent(ZTestNames[5],"Only allow pixels to be output if their Z coordinate is equal to the existing Z coordinate in the Z/Depth buffer"), GUIContent(ZTestNames[6],"Only allow pixels to be output if their Z coordinate is not equal to/other than the existing Z coordinate in the Z/Depth buffer"), GUIContent(ZTestNames[7],"Always allow pixels to be output regardless of the Z coordinate stored in the Z/Depth buffer"), GUIContent(ZTestNames[8],"Generate a shader for every Z Test")];
	var ColorMaskPresets : GUIContent[] = [GUIContent(MaskNames[0],"Output to RGBA channels"), GUIContent(MaskNames[1],"Output to RGB channels only"), GUIContent(MaskNames[2],"Output to Alpha channel only"), GUIContent(MaskNames[3],"Output to Red channel only"), GUIContent(MaskNames[4],"Output to Green channel only"), GUIContent(MaskNames[5],"Output to Blue channel only"), GUIContent(MaskNames[6],"Output to Red and Green channels only"), GUIContent(MaskNames[7],"Output to Red and Blue channels only"), GUIContent(MaskNames[8],"Output to Red and Alpha channels only"), GUIContent(MaskNames[9],"Output to Green and Blue channels only"), GUIContent(MaskNames[10],"Output to Green and Alpha channels only"), GUIContent(MaskNames[11],"Output to Blue and Alpha channels only"), GUIContent(MaskNames[12],"Output to Red, Blue and Alpha channels only"), GUIContent(MaskNames[13],"Output to Red, Green and Alpha channels only"), GUIContent(MaskNames[14],"Output to Green, Blue and Alpha channels only"), GUIContent(MaskNames[15],"Generate a shader for every channel combination")];
	var CullingPresets : GUIContent[] = [GUIContent(CullingNames[0],"Do not cull faces"), GUIContent(CullingNames[1],"Cull back-facing triangles only"), GUIContent(CullingNames[2],"Cull front-facing triangles only"), GUIContent(CullingNames[3],"Generate a shader for every Culling state")];
	var MaterialPresets : GUIContent[] = [GUIContent("Generate","Generate a Material for each shader"), GUIContent("Don't Generate","Do not generate a Material for each shader")];
	var ShaderModelPresets : GUIContent[] = [GUIContent("Shader Model 2.0","Target Shader Model 2.0 hardware (e.g. Direct3D 9), providing a 96 instruction limit (32 texture + 64 arithmetic), 16 temporary registers and 4 texture indirections."), GUIContent("Shader Model 3.0","Target Shader Model 3.0 hardware (e.g. Direct3D 9), providing a 1024 instruction limit (512 texture + 512 arithmetic), 32 temporary registers and 4 texture indirections.")];
	var CommentPresets : GUIContent[] = [GUIContent("None","Do not add any comments"), GUIContent("Header","Add only header comments"), GUIContent("Header and Sections","Add header comments and comments for each section"), GUIContent("Every Line","Add a comment on every line"), GUIContent("Verbose","Include all comments plus additional information such as parameter options and notes")];
	var FolderModePresets : GUIContent[] = [GUIContent("Folders by Function","Output multiple shaders into a list of folders grouped by function"), GUIContent("Folders by Blend Mode","Output multiple shaders into a list of folders grouped by blend mode"), GUIContent("Folders by Function and Blend","Output multiple shaders into a list of folders grouped by function, with sub-folders grouped by blend mode"), GUIContent("Flat","Output all shaders into a single location")];
	var ShaderCountingPresets : GUIContent[] = [GUIContent("Include","Include an automatically incrementing counter in the name of each shader"), GUIContent("Exclude","Do not include an automatically incrementing counter in the name of each shader")];
	var ShaderNamingPresets : GUIContent[] = [GUIContent("Automatic","Include an automatically generated name in the name of each shader"), GUIContent("Manual","Do not include automatic shader names in the name of each shader (use prefix and/or suffix)")];
	var ShaderNamingIncludeMergesPresets : GUIContent[] = [GUIContent("Include Merge Names","Include the names of every merge mode in MultiTexture shaders when generating names"), GUIContent("Exclude Merge Names","Do not include the name of each layer's merge mode in MultiTexture shaders when generating names")];

	function OnInspectorGUI () {
		//Generate and operate the editor
		
		//One-time startup
		var Temp:int;
		var Temp2:int;
		//Create array of default textures
		if (target.DefaultTexture.length==0) {
			target.DefaultTexture = new Texture2D[NumberOfTextureCounts+1];		//Create room for 16 default textures
			target.NumberOfTexturesPreset==0;				//Default to 2 layers
		}
		//Create array of merge presets
		if (target.MergePreset.length==0) {
			target.MergePreset = new int[NumberOfTextureCounts+1];	//Create room for 16 merge presets
			for (Temp=0; Temp<NumberOfTextureCounts+1; Temp++) {
				target.MergePreset[0]=0;					//Default each to AlphaBlend
			}
		}
		//Create array of Scale-Translate presets
		if (target.ScaleTranslateTexturePreset.length==0){
			target.ScaleTranslateTexturePreset = new int[NumberOfTextureCounts+1];	//Create room for 16 presets
			for (Temp=0; Temp<NumberOfTextureCounts+1; Temp++) {
				target.ScaleTranslateTexturePreset[Temp]=0;	//Default each to 0/Off
			}
		}
		//Create array of AlphaMaskThresholds
		if (target.AlphaMaskThreshold.length==0) {
			target.AlphaMaskThreshold = new float[NumberOfTextureCounts+1];		//Create room for 16 layers
		}
		//Create array of AlphaFadeThresholds
		if (target.AlphaFadeThreshold.length==0) {
			target.AlphaFadeThreshold = new float[NumberOfTextureCounts+1];		//Create room for 16 layers
		}
		
		//Get relative top of inspector area
		EditorGUILayout.Space();		//Create a space, relative to the top of the script section
		if(Event.current.type == EventType.Repaint) { 		//When it repaints
			TopLeftX = GUILayoutUtility.GetLastRect().x; 	//Get the rect of the space, so we have relative coordinates for the top left of the GUI
			TopLeftY = GUILayoutUtility.GetLastRect().y;	//Otherwise EditorGUI would be relative to the very top of the inspector window
		}

		//Show title
		EditorGUIUtility.LookLikeControls(430,0);
		EditorGUI.LabelField(Rect(TopLeftX,TopLeftY+40,430,20),"ShaderWizard Shader Generator 2.1","");
		EditorGUI.LabelField(Rect(TopLeftX+35,TopLeftY+60,430,20),"Copyright (c) 2012 Paul West/Venus12 LLC","");
		EditorGUI.DrawPreviewTexture(Rect(TopLeftX,TopLeftY+10,375,50),target.TitleImage,null,ScaleMode.ScaleToFit);
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUILayout.Space();
		EditorGUIUtility.LookLikeControls(230,130);

		//Shader function dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Function","How will the shader process the incoming color/texel data and what will it output to the blender?"),GUILayout.MaxWidth(60));
		target.ShaderFunctionPreset=EditorGUILayout.Popup(GUIContent("",""),target.ShaderFunctionPreset,ShaderFunctionPresets,GUILayout.MinWidth(180));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();		

		//Scale-Translate texture and select default texture(s)
		if ((target.ShaderFunctionPreset==2) || (target.ShaderFunctionPreset==3) || (target.ShaderFunctionPreset==4) || (target.ShaderFunctionPreset==5) || (target.ShaderFunctionPreset==NumberOfFunctions-1)) {
			//Decide if we allow scale/translate of texture coords and specify default texture(s)

			if ((target.ShaderFunctionPreset==5) || (target.ShaderFunctionPreset==NumberOfFunctions-1)){
				//Multitexturing shader, need to show extra dropdowns

				//Show texture count dropdown
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.LabelField(GUIContent("  Layers","How many textures will be combined in the MultiTexture shader(s)"),GUILayout.MaxWidth(60));
				target.NumberOfTexturesPreset=EditorGUILayout.Popup(GUIContent("",""),target.NumberOfTexturesPreset,TextureCountPresets,GUILayout.MinWidth(180));
				GUILayout.Space(10);
				GUILayout.EndHorizontal();
			}
			
			//Default textures
			var TexCount:int=0;
			var TexMax:int=target.NumberOfTexturesPreset+1;
			if ((target.ShaderFunctionPreset !=5) && (target.ShaderFunctionPreset != NumberOfFunctions-1)){
				TexMax=0;
			}

			for (TexCount=0; TexCount<=Mathf.Min(TexMax,15); TexCount++){

				GUILayout.Space(10);	//Gap

				if (((target.ShaderFunctionPreset==5) || (target.ShaderFunctionPreset==NumberOfFunctions-1)) && (TexCount>0)) {
					//Draw multitexturing mergers
					GUILayout.BeginHorizontal();
					GUILayout.FlexibleSpace();
					EditorGUILayout.LabelField(GUIContent("  Merge","How will the shader combine textures 0 and 1 of the multitexturing shader?"),GUILayout.MaxWidth(60));
					target.MergePreset[TexCount]=EditorGUILayout.Popup(GUIContent("",""),target.MergePreset[TexCount],MergePresets,GUILayout.MinWidth(180));
					GUILayout.Space(10);
					GUILayout.EndHorizontal();		
				}
				
				//Multi/single texture
				GUILayout.BeginHorizontal();
				GUILayout.FlexibleSpace();
				EditorGUILayout.LabelField(GUIContent("Stage "+TexCount+" Default Tex","Which Texture will be used as the default for the shader's texture number "+TexCount),GUILayout.MaxWidth(130));
				target.DefaultTexture[TexCount]=EditorGUILayout.ObjectField(GUIContent("",""),target.DefaultTexture[TexCount],Texture2D,false,GUILayout.MinWidth(180));
				GUILayout.Space(30);
				GUILayout.EndHorizontal();
			}

							
			//Scale/translate (outside loop implements only 1 allowed, not per layer)
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			EditorGUILayout.LabelField(GUIContent("  Adjust","Allow texture coordinate for Texture "+0+" to be adjusted with Tiling and Offset?"),GUILayout.MaxWidth(60));
			target.ScaleTranslateTexturePreset[0]=EditorGUILayout.Popup(GUIContent("",""),target.ScaleTranslateTexturePreset[0],ScaleTranslateTexturePresets,GUILayout.MinWidth(180));
			GUILayout.Space(10);
			GUILayout.EndHorizontal();			
		}
		
		//Fixed color default
		if ((target.ShaderFunctionPreset==0) || (target.ShaderFunctionPreset==3) || (target.ShaderFunctionPreset==NumberOfFunctions-1)) {
			//Fixed color needed
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			EditorGUILayout.LabelField(GUIContent("  Color","What fixed color will be input to the shader function?"),GUILayout.MaxWidth(60));
			target.FixedColor=EditorGUILayout.ColorField(GUIContent("",""),target.FixedColor,GUILayout.MinWidth(140));
			GUILayout.Space(10);
			GUILayout.EndHorizontal();
		}

		//Blend mode dropdown
		GUILayout.Space(10);
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Blend","How will the output from the shader be combined with the pixel already on the screen?"),GUILayout.MaxWidth(60));
		target.BlendModePreset=EditorGUILayout.Popup(GUIContent("",""),target.BlendModePreset,BlendModePresets,GUILayout.MinWidth(180));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();
		GUILayout.Space(10);

		//Alpha-test threshold default
		if ((target.BlendModePreset==1) || (target.BlendModePreset==3) || (target.BlendModePreset==5) || (target.BlendModePreset==7) || (target.BlendModePreset==NumberOfBlends-1)) {
			//Alpha testing needed
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			EditorGUILayout.LabelField(GUIContent("  Threshold","What value must the output's alpha be in order for the alpha test to pass the output to the blender?"),GUILayout.MaxWidth(80));
			target.AlphaTestThreshold=EditorGUILayout.FloatField(GUIContent("",""),target.AlphaTestThreshold,GUILayout.MinWidth(157));
			GUILayout.Space(10);
			GUILayout.EndHorizontal();
		}

		//Z-Write dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Z Write","Will Z coordinate values be written to the Z/Depth Buffer?"),GUILayout.MaxWidth(60));
		target.ZWritePreset=EditorGUILayout.Popup(GUIContent("","Will Z coordinate values be written to the Z/Depth Buffer?"),target.ZWritePreset,ZWritePresets,GUILayout.MinWidth(180));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Z-test dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Z Test","How will the Z/Depth buffer be tested?"),GUILayout.MaxWidth(60));
		target.ZTestPreset=EditorGUILayout.Popup(GUIContent("",""),target.ZTestPreset,ZTestPresets,GUILayout.MinWidth(180));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Color mask dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Mask","What destination color channels will be affected by the shader/blender?"),GUILayout.MaxWidth(60));
		target.ColorMaskPreset=EditorGUILayout.Popup(GUIContent("","What destination color channels will be affected by the shader/blender?"),target.ColorMaskPreset,ColorMaskPresets,GUILayout.MinWidth(180));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Culling dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Culling","How will geometry faces be culled?"),GUILayout.MaxWidth(60));
		target.CullingPreset=EditorGUILayout.Popup(GUIContent("",""),target.CullingPreset,CullingPresets,GUILayout.MinWidth(180));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();
		
		//ShaderModel dropdown
		GUILayout.Space(15);
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Model","What shader model should the shader be targetted at? (if the shader can't compile due to too many instructions or registers, especially MultiTexture shaders with multiple layers, try Shader Model 3 instead of 2). Shader Model 2 gives a 96 instruction limit (32 texture + 64 arithmetic), 16 temporary registers and 4 texture indirections. Shader Model 3 gives a 1024 instruction limit (512 texture + 512 arithmetic), 32 temporary registers and 4 texture indirections."),GUILayout.MaxWidth(85));
		target.ShaderModelPreset=EditorGUILayout.Popup(GUIContent("",""),target.ShaderModelPreset,ShaderModelPresets,GUILayout.MinWidth(130));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();
		
		//Comments dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Comments","What level of commenting will be included in the shader sourcecode?"),GUILayout.MaxWidth(85));
		target.CommentPreset=EditorGUILayout.Popup(GUIContent("",""),target.CommentPreset,CommentPresets,GUILayout.MinWidth(130));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Folder mode dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Folder Mode","How will the generated shaders be organized in folders?"),GUILayout.MaxWidth(85));
		target.FolderModePreset=EditorGUILayout.Popup(GUIContent("",""),target.FolderModePreset,FolderModePresets,GUILayout.MinWidth(130));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Shader counting dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Counter","Will shader names include a counter?"),GUILayout.MaxWidth(85));
		target.ShaderCountingPreset=EditorGUILayout.Popup(GUIContent("",""),target.ShaderCountingPreset,ShaderCountingPresets,GUILayout.MinWidth(130));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();
		
		//Shader counting base
		if (target.ShaderCountingPreset==0) {
			//Include counter
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			EditorGUILayout.LabelField(GUIContent("  Offset","At what number to start the counter"),GUILayout.MaxWidth(85));
			target.CounterOffset=EditorGUILayout.IntField(GUIContent("",""),target.CounterOffset,GUILayout.MinWidth(157));
			GUILayout.Space(10);
			GUILayout.EndHorizontal();
		}
		
		//Shader naming dropdown
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Naming","Include a pre-generated name for each shader?"),GUILayout.MaxWidth(85));
		target.ShaderNamingPreset=EditorGUILayout.Popup(GUIContent("",""),target.ShaderNamingPreset,ShaderNamingPresets,GUILayout.MinWidth(130));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Shader naming inclusion dropdown
		if ( ((target.ShaderFunctionPreset==5) || (target.ShaderFunctionPreset==NumberOfFunctions-1)) && (target.ShaderNamingPreset==0) ) {
			//Only when multitexturing and automatically generating names
			GUILayout.BeginHorizontal();
			GUILayout.FlexibleSpace();
			EditorGUILayout.LabelField(GUIContent("  Include","Include MultiTexture layer merges in the generated name? (may produce file names which are too long when using a large number of layers)"),GUILayout.MaxWidth(85));
			target.ShaderNamingIncludeMergesPreset=EditorGUILayout.Popup(GUIContent("",""),target.ShaderNamingIncludeMergesPreset,ShaderNamingIncludeMergesPresets,GUILayout.MinWidth(130));
			GUILayout.Space(10);
			GUILayout.EndHorizontal();
		}
		
		//Shader name prefix
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("Name prefix","Text to be inserted before the name of a shader"),GUILayout.MaxWidth(75));
		target.ShaderNamePrefix = EditorGUILayout.TextField(GUIContent("",""),target.ShaderNamePrefix,GUILayout.MaxWidth(157));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Shader name suffix
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("Name suffix","Text to be inserted after the name of a shader"),GUILayout.MaxWidth(75));
		target.ShaderNameSuffix = EditorGUILayout.TextField(GUIContent("",""),target.ShaderNameSuffix,GUILayout.MaxWidth(157));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Shader menu name text field
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("Menu name","Name for the shader menu entry under which the shaders will be listed, if any, otherwise shader will be added to the main menu or as defined by a Sub name"),GUILayout.MaxWidth(75));
		target.MenuName = EditorGUILayout.TextField(GUIContent("",""),target.MenuName,GUILayout.MaxWidth(157));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Shader sub name text field
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("Sub name","Name for the shader menu submenu under which the shaders will be listed, relative to the Menu name if any, otherwise relative to the main menu"),GUILayout.MaxWidth(75));
		target.SubName = EditorGUILayout.TextField(GUIContent("",""),target.SubName,GUILayout.MaxWidth(157));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Output path text field
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("Output Path","Path to output generated shaders to, relative to your Assets folder. Depending on Folder Mode, subfolders may be created within this location"),GUILayout.MaxWidth(75));
		target.OutputPath = EditorGUILayout.TextField(GUIContent("",""),target.OutputPath,GUILayout.MaxWidth(157));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Material dropdown
		GUILayout.Space(15);
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		EditorGUILayout.LabelField(GUIContent("  Material","Generate a material for each shader?"),GUILayout.MaxWidth(60));
		target.MaterialPreset=EditorGUILayout.Popup(GUIContent("",""),target.MaterialPreset,MaterialPresets,GUILayout.MinWidth(180));
		GUILayout.Space(10);
		GUILayout.EndHorizontal();

		//Build button
		GUILayout.Space(15);
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace();
		var MultiTotal:int=1;
		if ((target.ShaderFunctionPreset==5) || (target.ShaderFunctionPreset==NumberOfFunctions-1)){
			//Add-up multitexturing merge combinations
			for (Temp=1;Temp<=target.NumberOfTexturesPreset+1;Temp++){
				if (target.MergePreset[Temp]==NumberOfMerges-1){
					MultiTotal*=(NumberOfMerges-1);	//Multiply combinations of all merges
				}
			}
		}
		if ((target.ShaderFunctionPreset==NumberOfFunctions-1) || (target.BlendModePreset==NumberOfBlends-1) || (target.ZWritePreset==NumberOfZWrites-1) || (target.ZTestPreset==NumberOfZTests-1) || (target.ColorMaskPreset==NumberOfMasks-1) || (target.CullingPreset==NumberOfCullings-1) || (target.ScaleTranslateTexturePreset[0]==NumberOfScaleTranslateTextures-1) || (MultiTotal>1) ) {
			//Generate multiple shaders
			BatchMode=true;
			BatchTotal=MultiTotal;	//1 or more multitexture merges
			if (target.ShaderFunctionPreset==NumberOfFunctions-1) {BatchTotal*=(NumberOfFunctions-1);}
			if (target.BlendModePreset==NumberOfBlends-1) {BatchTotal*=(NumberOfBlends-1);}
			if (target.ZWritePreset==NumberOfZWrites-1) {BatchTotal*=(NumberOfZWrites-1);}
			if (target.ZTestPreset==NumberOfZTests-1) {BatchTotal*=(NumberOfZTests-1);}
			if (target.ColorMaskPreset==NumberOfMasks-1) {BatchTotal*=(NumberOfMasks-1);}
			if (target.CullingPreset==NumberOfCullings-1) {BatchTotal*=(NumberOfCullings-1);}
			if (target.ScaleTranslateTexturePreset[0]==NumberOfScaleTranslateTextures-1) {BatchTotal*=(NumberOfScaleTranslateTextures-1);}
			GenerateButtonText="\nGenerate "+BatchTotal+" Shaders!\n";
			GenerateButtonCaption="Generate "+BatchTotal+" unique shaders from the above combinations of settings";
		} else {
			//Generate only one shader
			BatchMode=false;
			BatchTotal=1;
			GenerateButtonText="\nGenerate Shader!\n";
			GenerateButtonCaption="Generate this shader only";
		}
		if (GUILayout.Button(GUIContent(GenerateButtonText,GenerateButtonCaption),GUILayout.ExpandWidth(false),GUILayout.MinWidth(150))==true) {
			GenerateShaders();					//Generate shaders
			if (target.MaterialPreset==0) {
				//Generate a material for each shader that has been generated
				//It's 200-300% faster to do this after all shaders are made because the database has to be refreshed after the shader is created in order to import it to create a material
				//Could generate the material one at a time after each shader but it is much slower overall
				GenerateMaterials();			//Generate materials for each shader
			}
		}
		GUILayout.Space(50);
		GUILayout.EndHorizontal();
	
		//Update the GUI
		if (GUI.changed) {
			EditorUtility.SetDirty (target);
		}
	}	//End of OnInspectorGUI function
	
	function GenerateShaders() {
		//Generate shaders

		if (BatchTotal>100) {
			//Warn user to be sure if they are about to generate more than 100 shaders
			var Warning:String = "You are about to generate "+BatchTotal+" Shaders";
			if (target.MaterialPreset==0) {
				Warning+=" and "+BatchTotal+" Materials";	//Also materials
			}
			Warning+=". Are you sure?";
			if (EditorUtility.DisplayDialog("Shader Wizard",Warning,"Generate!","Stop!")==false) {return;}	//Cancel if user says so
		}

		EditorUtility.DisplayCancelableProgressBar("Shader Wizard","Generating Shader(s)",0);
		var Progress:float = 0;					//Progress made 0..1

		var FunctionName:String;				//Temporary name for a shader function
		var ScaleTranName:String;				//Temporary name for a scale/translate mode
		var BlendName:String;					//Temporary name for a blend mode
		var ZWriteName:String;					//Temporary name for a zwrite mode
		var ZTestName:String;					//Temporary name for a ztest mode
		var MaskName:String;					//Temporary name for a color mask mode
		var CullName:String;					//Temporary name for a culling mode
		var MergeName:String;					//Temporary name for a merge mode
		
		var ShaderName:String;					//Name for the shader
		var FilePath:String;					//Location for the shader and material files
		var SubPath:String;						//Sub-location inside a resources folder within FilePath
		var Counter:int=target.CounterOffset;	//Shader number counter, start at offset
		var CounterText:String="";				//Text version of function/blend counter
		var CounterText2:String="";				//Text version of folder counter
		var ShaderText:String="";				//Text of the shader to output
		var HasTextures:boolean=false;			//Whether the shader uses a texture
		var HasColor:boolean=false;				//Whether the shader uses a fixed color
		var HasTest:boolean=false;				//Whether the shader uses alpha testing
		var HasBlendOp:boolean=false;			//Whether the shader uses BlendOp
		var BlendOpName:String="";				//Name of the BlendOp to use
		var Comments:int=target.CommentPreset;	//What level of commenting?
		var CurrentShader:Shader;				//Current shader file
		var Mat:Material;						//Current material
		var ShaderPath:String;					//Temporary path to shader
		var Temp:int;							//Temporary

		//CG code for each function
		var CGCode:String[] = new String[NumberOfFunctions-1];	//Allocate space
		
		//Color Function
		if (Comments>2) {
			CGCode[0]="\n\n\t\t\tCGPROGRAM\t\t\t\t\t\t//Start a program in the CG language";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[0]+="\n\t\t\t#pragma target 2.0\t\t\t\t//Run this shader on at least Shader Model 2.0 hardware (e.g. Direct3D 9)";
			} else {
				//Shader model 3.0
				CGCode[0]+="\n\t\t\t#pragma target 3.0\t\t\t\t//Run this shader on at least Shader Model 3.0 hardware (e.g. Direct3D 9)";
			}
			CGCode[0]+="\n\t\t\t#pragma fragment frag\t\t\t//The fragment shader is named 'frag'";
			CGCode[0]+="\n\t\t\t#pragma vertex vert	\t\t\t//The vertex shader is named 'vert'";
			CGCode[0]+="\n\t\t\t#include \"UnityCG.cginc\"\t\t//Include Unity's predefined inputs and macros";
		} else {
			CGCode[0]="\n\n\t\t\tCGPROGRAM";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[0]+="\n\t\t\t#pragma target 2.0";
			} else {
				//Shader model 3.0
				CGCode[0]+="\n\t\t\t#pragma target 3.0";
			}
			CGCode[0]+="\n\t\t\t#pragma fragment frag";
			CGCode[0]+="\n\t\t\t#pragma vertex vert";
			CGCode[0]+="\n\t\t\t#include \"UnityCG.cginc\"";		
		}
		CGCode[0]+="\n";
		if (Comments>1) {CGCode[0]+="\n\t\t\t//Unity variables to be made accessible to Vertex and/or Fragment shader";}
		CGCode[0]+="\n\t\t\tfixed4 _Color;";
		if (Comments>2) {CGCode[0]+="\t\t\t\t\t\t\t\t//Receive input from the _Color property";}
		CGCode[0]+="\n";
		if (Comments>1) {CGCode[0]+="\n\t\t\t//Data structure communication from Unity to the vertex shader";}
		if (Comments>1) {CGCode[0]+="\n\t\t\t//Defines what inputs the vertex shader accepts";}
		CGCode[0]+="\n\t\t\tstruct AppData {";
		CGCode[0]+="\n\t\t\t\tfloat4 vertex : POSITION;";
		if (Comments>2) {CGCode[0]+="\t\t\t\t\t//Receive vertex position";}
		if (Comments>3) {
			CGCode[0]+="\n\t\t\t\t\t\t\t//half2 texcoord : TEXCOORD0;\t\t\t\t//Receive texture coordinates";
			CGCode[0]+="\n\t\t\t\t\t\t\t//half2 texcoord1 : TEXCOORD1;\t\t\t\t//Receive texture coordinates";
			CGCode[0]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Receive vertex colors";
		}
		CGCode[0]+="\n\t\t\t};";
		CGCode[0]+="\n";
		if (Comments>1) {CGCode[0]+="\n\t\t\t//Data structure for communication from vertex shader to fragment shader";}
		if (Comments>1) {CGCode[0]+="\n\t\t\t//Defines what inputs the fragment shader accepts";}
		CGCode[0]+="\n\t\t\tstruct VertexToFragment {";
		CGCode[0]+="\n\t\t\t\tfloat4 pos : POSITION;";
		if (Comments>2) {CGCode[0]+="\t\t\t\t\t\t//Send fragment position to fragment shader";}
		if (Comments>3) {
			CGCode[0]+="\n\t\t\t\t\t\t\t//half2 uv : TEXCOORD0;\t\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
			CGCode[0]+="\n\t\t\t\t\t\t\t//half2 uv2 : TEXCOORD1;\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
			CGCode[0]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Send interpolated gouraud-shaded vertex color to fragment shader";
		}
		CGCode[0]+="\n\t\t\t};";
		CGCode[0]+="\n";
		if (Comments>1) {CGCode[0]+="\n\t\t\t//Vertex shader";}
		CGCode[0]+="\n\t\t\tVertexToFragment vert(AppData v) {";
		CGCode[0]+="\n\t\t\t\tVertexToFragment o;";
		if (Comments>2) {CGCode[0]+="\t\t\t\t\t\t\t//Create a data structure to pass to fragment shader";}
		CGCode[0]+="\n\t\t\t\to.pos = mul(UNITY_MATRIX_MVP,v.vertex);";
		if (Comments>2) {CGCode[0]+="\t\t//Include influence of Modelview + Projection matrices";}
		if (Comments>3) {
			CGCode[0]+="\n\t\t\t\t\t\t\t//o.uv = v.texcoord.xy;\t\t\t\t\t\t//Send texture coords from unit 0 to fragment shader";
			CGCode[0]+="\n\t\t\t\t\t\t\t//o.uv2 = v.texcoord1.xy;\t\t\t\t\t//Send texture coords from unit 1 to fragment shader";
			CGCode[0]+="\n\t\t\t\t\t\t\t//o.color = v.color;\t\t\t\t\t\t//Send interpolated vertex color to fragment shader";
			CGCode[0]+="\n\t\t\t\t\t\t\t//o.color = _Color;\t\t\t\t\t\t\t//Send solid color to fragment shader";
		}
		CGCode[0]+="\n\t\t\t\treturn o;";
		if (Comments>2) {CGCode[0]+="\t\t\t\t\t\t\t\t\t//Transmit data to the fragment shader";}
		CGCode[0]+="\n\t\t\t}";
		CGCode[0]+="\n";   
		if (Comments>1) {CGCode[0]+="\n\t\t\t//Fragment shader";}
		CGCode[0]+="\n\t\t\tfixed4 frag(VertexToFragment i) : COLOR {";
		CGCode[0]+="\n\t\t\t\treturn fixed4(_Color);";
		if (Comments>2) {CGCode[0]+="\t\t\t\t\t\t//Output RGBA color";}
		CGCode[0]+="\n\t\t\t}";
    	CGCode[0]+="\n";
		CGCode[0]+="\n\t\t\tENDCG";
		if (Comments>2) {CGCode[0]+="\t\t\t\t\t\t\t//End of CG program";}
		CGCode[0]+="\n";
						
		//VertexColor Function
		if (Comments>2) {
			CGCode[1]="\n\n\t\t\tCGPROGRAM\t\t\t\t\t\t//Start a program in the CG language";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[1]+="\n\t\t\t#pragma target 2.0\t\t\t\t//Run this shader on at least Shader Model 2.0 hardware (e.g. Direct3D 9)";
			} else {
				//Shader model 3.0
				CGCode[1]+="\n\t\t\t#pragma target 3.0\t\t\t\t//Run this shader on at least Shader Model 3.0 hardware (e.g. Direct3D 9)";
			}
			CGCode[1]+="\n\t\t\t#pragma fragment frag\t\t\t//The fragment shader is named 'frag'";
			CGCode[1]+="\n\t\t\t#pragma vertex vert	\t\t\t//The vertex shader is named 'vert'";
			CGCode[1]+="\n\t\t\t#include \"UnityCG.cginc\"\t\t//Include Unity's predefined inputs and macros";
		} else {
			CGCode[1]="\n\n\t\t\tCGPROGRAM";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[1]+="\n\t\t\t#pragma target 2.0";
			} else {
				//Shader model 3.0
				CGCode[1]+="\n\t\t\t#pragma target 3.0";
			}
			CGCode[1]+="\n\t\t\t#pragma fragment frag";
			CGCode[1]+="\n\t\t\t#pragma vertex vert";
			CGCode[1]+="\n\t\t\t#include \"UnityCG.cginc\"";		
		}
		CGCode[1]+="\n";
		if (Comments>1) {CGCode[1]+="\n\t\t\t//Data structure communication from Unity to the vertex shader";}
		if (Comments>1) {CGCode[1]+="\n\t\t\t//Defines what inputs the vertex shader accepts";}
		CGCode[1]+="\n\t\t\tstruct AppData {";
		CGCode[1]+="\n\t\t\t\tfloat4 vertex : POSITION;";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t//Receive vertex position";}
		if (Comments>3) {
			CGCode[1]+="\n\t\t\t\t\t\t\t//half2 texcoord : TEXCOORD0;\t\t\t\t//Receive texture coordinates";
			CGCode[1]+="\n\t\t\t\t\t\t\t//half2 texcoord1 : TEXCOORD1;\t\t\t\t//Receive texture coordinates";
		}
		CGCode[1]+="\n\t\t\t\tfixed4 color : COLOR;";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t//Receive vertex colors";}
		CGCode[1]+="\n\t\t\t};";
		CGCode[1]+="\n";
		if (Comments>1) {CGCode[1]+="\n\t\t\t//Data structure for communication from vertex shader to fragment shader";}
		if (Comments>1) {CGCode[1]+="\n\t\t\t//Defines what inputs the fragment shader accepts";}
		CGCode[1]+="\n\t\t\tstruct VertexToFragment {";
		CGCode[1]+="\n\t\t\t\tfloat4 pos : POSITION;";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t//Send fragment position to fragment shader";}
		if (Comments>3) {
			CGCode[1]+="\n\t\t\t\t\t\t\t//half2 uv : TEXCOORD0;\t\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
			CGCode[1]+="\n\t\t\t\t\t\t\t//half2 uv2 : TEXCOORD1;\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
		}
		CGCode[1]+="\n\t\t\t\tfixed4 color : COLOR;";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t//Send interpolated gouraud-shaded vertex color to fragment shader";}
		CGCode[1]+="\n\t\t\t};";
		CGCode[1]+="\n";
		if (Comments>1) {CGCode[1]+="\n\t\t\t//Vertex shader";}
		CGCode[1]+="\n\t\t\tVertexToFragment vert(AppData v) {";
		CGCode[1]+="\n\t\t\t\tVertexToFragment o;";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t\t//Create a data structure to pass to fragment shader";}
		CGCode[1]+="\n\t\t\t\to.pos = mul(UNITY_MATRIX_MVP,v.vertex);";
		if (Comments>2) {CGCode[1]+="\t\t//Include influence of Modelview + Projection matrices";}
		if (Comments>3) {
			CGCode[1]+="\n\t\t\t\t\t\t\t//o.uv = v.texcoord.xy;\t\t\t\t\t\t//Send texture coords from unit 0 to fragment shader";
			CGCode[1]+="\n\t\t\t\t\t\t\t//o.uv2 = v.texcoord1.xy;\t\t\t\t\t//Send texture coords from unit 1 to fragment shader";
		}
		CGCode[1]+="\n\t\t\t\to.color = v.color;";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t\t//Send interpolated vertex color to fragment shader";}
		if (Comments>3) {CGCode[1]+="\n\t\t\t\t\t\t\t//o.color = _Color;\t\t\t\t\t\t\t//Send solid color to fragment shader";}
		CGCode[1]+="\n\t\t\t\treturn o;";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t\t\t\t//Transmit data to the fragment shader";}
		CGCode[1]+="\n\t\t\t}";
		CGCode[1]+="\n";   
		if (Comments>1) {CGCode[1]+="\n\t\t\t//Fragment shader";}
		CGCode[1]+="\n\t\t\tfixed4 frag(VertexToFragment i) : COLOR {";
		CGCode[1]+="\n\t\t\t\treturn fixed4(i.color);";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t//Output RGBA interpolated vertex color";}
		CGCode[1]+="\n\t\t\t}";
    	CGCode[1]+="\n";
		CGCode[1]+="\n\t\t\tENDCG";
		if (Comments>2) {CGCode[1]+="\t\t\t\t\t\t\t//End of CG program";}
		CGCode[1]+="\n";

		//Texture Function
		if (Comments>2) {
			CGCode[2]="\n\n\t\t\tCGPROGRAM\t\t\t\t\t\t//Start a program in the CG language";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[2]+="\n\t\t\t#pragma target 2.0\t\t\t\t//Run this shader on at least Shader Model 2.0 hardware (e.g. Direct3D 9)";
			} else {
				//Shader model 3.0
				CGCode[2]+="\n\t\t\t#pragma target 3.0\t\t\t\t//Run this shader on at least Shader Model 3.0 hardware (e.g. Direct3D 9)";
			}
			CGCode[2]+="\n\t\t\t#pragma fragment frag\t\t\t//The fragment shader is named 'frag'";
			CGCode[2]+="\n\t\t\t#pragma vertex vert	\t\t\t//The vertex shader is named 'vert'";
			CGCode[2]+="\n\t\t\t#include \"UnityCG.cginc\"\t\t//Include Unity's predefined inputs and macros";
		} else {
			CGCode[2]="\n\n\t\t\tCGPROGRAM";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[2]+="\n\t\t\t#pragma target 2.0";
			} else {
				//Shader model 3.0
				CGCode[2]+="\n\t\t\t#pragma target 3.0";
			}
			CGCode[2]+="\n\t\t\t#pragma fragment frag";
			CGCode[2]+="\n\t\t\t#pragma vertex vert";
			CGCode[2]+="\n\t\t\t#include \"UnityCG.cginc\"";		
		}
		CGCode[2]+="\n";
		if (Comments>1) {CGCode[2]+="\n\t\t\t//Unity variables to be made accessible to Vertex and/or Fragment shader";}
		CGCode[2]+="\n\t\t\tuniform sampler2D _Texture0;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t//Define _Texture0 from Texture Unit 0 to be sampled in 2D";}
		CGCode[2]+="\n\t\t\tuniform float4 _Texture0_ST;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t//Use the Float _Texture0_ST to pass the Offset and Tiling for the texture(s)";}
		CGCode[2]+="\n";
		if (Comments>1) {CGCode[2]+="\n\t\t\t//Data structure communication from Unity to the vertex shader";}
		if (Comments>1) {CGCode[2]+="\n\t\t\t//Defines what inputs the vertex shader accepts";}
		CGCode[2]+="\n\t\t\tstruct AppData {";
		CGCode[2]+="\n\t\t\t\tfloat4 vertex : POSITION;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t//Receive vertex position";}
		CGCode[2]+="\n\t\t\t\thalf2 texcoord : TEXCOORD0;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t//Receive texture coordinates";}
		if (Comments>3) {
			CGCode[2]+="\n\t\t\t\t\t\t\t//half2 texcoord1 : TEXCOORD1;\t\t\t\t//Receive texture coordinates";
			CGCode[2]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Receive vertex colors";
		}
		CGCode[2]+="\n\t\t\t};";
		CGCode[2]+="\n";
		if (Comments>1) {CGCode[2]+="\n\t\t\t//Data structure for communication from vertex shader to fragment shader";}
		if (Comments>1) {CGCode[2]+="\n\t\t\t//Defines what inputs the fragment shader accepts";}
		CGCode[2]+="\n\t\t\tstruct VertexToFragment {";
		CGCode[2]+="\n\t\t\t\tfloat4 pos : POSITION;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t\t//Send fragment position to fragment shader";}
		CGCode[2]+="\n\t\t\t\thalf2 uv : TEXCOORD0;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";}
		if (Comments>3) {
			CGCode[2]+="\n\t\t\t\t\t\t\t//half2 uv2 : TEXCOORD1;\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
			CGCode[2]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Send interpolated gouraud-shaded vertex color to fragment shader";
		}
		CGCode[2]+="\n\t\t\t};";
		CGCode[2]+="\n";
		if (Comments>1) {CGCode[2]+="\n\t\t\t//Vertex shader";}
		CGCode[2]+="\n\t\t\tVertexToFragment vert(AppData v) {";
		CGCode[2]+="\n\t\t\t\tVertexToFragment o;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t\t\t//Create a data structure to pass to fragment shader";}
		CGCode[2]+="\n\t\t\t\to.pos = mul(UNITY_MATRIX_MVP,v.vertex);";
		if (Comments>2) {CGCode[2]+="\t\t//Include influence of Modelview + Projection matrices";}
		CGCode[2]+="\n\t\t\t\to.uv = v.texcoord.xy;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t\t//Send texture coords from unit 0 to fragment shader";}
		if (Comments>3) {
			CGCode[2]+="\n\t\t\t\t\t\t\t//o.uv2 = v.texcoord1.xy;\t\t\t\t\t//Send texture coords from unit 1 to fragment shader";
			CGCode[2]+="\n\t\t\t\t\t\t\t//o.color = v.color;\t\t\t\t\t\t//Send interpolated vertex color to fragment shader";
			CGCode[2]+="\n\t\t\t\t\t\t\t//o.color = _Color;\t\t\t\t\t\t\t//Send solid color to fragment shader";
		}
		CGCode[2]+="\n\t\t\t\treturn o;";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t\t\t\t\t//Transmit data to the fragment shader";}
		CGCode[2]+="\n\t\t\t}";
		CGCode[2]+="\n";   
		if (Comments>1) {CGCode[2]+="\n\t\t\t//Fragment shader";}
		CGCode[2]+="\n\t\t\tfixed4 frag(VertexToFragment i) : COLOR {";
		CGCode[2]+="\n\t\t\t\treturn tex2D(_Texture0, i.uv);";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t//Output RGBA color";}
		CGCode[2]+="\n\t\t\t}";
    	CGCode[2]+="\n";
		CGCode[2]+="\n\t\t\tENDCG";
		if (Comments>2) {CGCode[2]+="\t\t\t\t\t\t\t//End of CG program";}
		CGCode[2]+="\n";

		//Texture+Color Function
		if (Comments>2) {
			CGCode[3]="\n\n\t\t\tCGPROGRAM\t\t\t\t\t\t//Start a program in the CG language";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[3]+="\n\t\t\t#pragma target 2.0\t\t\t\t//Run this shader on at least Shader Model 2.0 hardware (e.g. Direct3D 9)";
			} else {
				//Shader model 3.0
				CGCode[3]+="\n\t\t\t#pragma target 3.0\t\t\t\t//Run this shader on at least Shader Model 3.0 hardware (e.g. Direct3D 9)";
			}
			CGCode[3]+="\n\t\t\t#pragma fragment frag\t\t\t//The fragment shader is named 'frag'";
			CGCode[3]+="\n\t\t\t#pragma vertex vert	\t\t\t//The vertex shader is named 'vert'";
			CGCode[3]+="\n\t\t\t#include \"UnityCG.cginc\"\t\t//Include Unity's predefined inputs and macros";
		} else {
			CGCode[3]="\n\n\t\t\tCGPROGRAM";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[3]+="\n\t\t\t#pragma target 2.0";
			} else {
				//Shader model 3.0
				CGCode[3]+="\n\t\t\t#pragma target 3.0";
			}
			CGCode[3]+="\n\t\t\t#pragma fragment frag";
			CGCode[3]+="\n\t\t\t#pragma vertex vert";
			CGCode[3]+="\n\t\t\t#include \"UnityCG.cginc\"";		
		}
		CGCode[3]+="\n";
		if (Comments>1) {CGCode[3]+="\n\t\t\t//Unity variables to be made accessible to Vertex and/or Fragment shader";}
		CGCode[3]+="\n\t\t\tuniform sampler2D _Texture0;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t//Define _Texture0 from Texture Unit 0 to be sampled in 2D";}
		CGCode[3]+="\n\t\t\tuniform float4 _Texture0_ST;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t//Use the Float _Texture0_ST to pass the Offset and Tiling for the texture(s)";}
		CGCode[3]+="\n\t\t\tuniform fixed4 _Color;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t\t\t//Use the Color _Color provided by Unity";}
		CGCode[3]+="\n";
		if (Comments>1) {CGCode[3]+="\n\t\t\t//Data structure communication from Unity to the vertex shader";}
		if (Comments>1) {CGCode[3]+="\n\t\t\t//Defines what inputs the vertex shader accepts";}
		CGCode[3]+="\n\t\t\tstruct AppData {";
		CGCode[3]+="\n\t\t\t\tfloat4 vertex : POSITION;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t//Receive vertex position";}
		CGCode[3]+="\n\t\t\t\thalf2 texcoord : TEXCOORD0;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t//Receive texture coordinates";}
		if (Comments>3) {
			CGCode[3]+="\n\t\t\t\t\t\t\t//half2 texcoord1 : TEXCOORD1;\t\t\t\t//Receive texture coordinates";
			CGCode[3]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Receive vertex colors";
		}
		CGCode[3]+="\n\t\t\t};";
		CGCode[3]+="\n";
		if (Comments>1) {CGCode[3]+="\n\t\t\t//Data structure for communication from vertex shader to fragment shader";}
		if (Comments>1) {CGCode[3]+="\n\t\t\t//Defines what inputs the fragment shader accepts";}
		CGCode[3]+="\n\t\t\tstruct VertexToFragment {";
		CGCode[3]+="\n\t\t\t\tfloat4 pos : POSITION;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t\t//Send fragment position to fragment shader";}
		CGCode[3]+="\n\t\t\t\thalf2 uv : TEXCOORD0;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";}
		if (Comments>3) {
			CGCode[3]+="\n\t\t\t\t\t\t\t//half2 uv2 : TEXCOORD1;\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
			CGCode[3]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Send interpolated gouraud-shaded vertex color to fragment shader";
		}
		CGCode[3]+="\n\t\t\t};";
		CGCode[3]+="\n";
		if (Comments>1) {CGCode[3]+="\n\t\t\t//Vertex shader";}
		CGCode[3]+="\n\t\t\tVertexToFragment vert(AppData v) {";
		CGCode[3]+="\n\t\t\t\tVertexToFragment o;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t\t\t//Create a data structure to pass to fragment shader";}
		CGCode[3]+="\n\t\t\t\to.pos = mul(UNITY_MATRIX_MVP,v.vertex);";
		if (Comments>2) {CGCode[3]+="\t\t//Include influence of Modelview + Projection matrices";}
		CGCode[3]+="\n\t\t\t\to.uv = v.texcoord.xy;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t\t//Send texture coords from unit 0 to fragment shader";}
		if (Comments>3) {
			CGCode[3]+="\n\t\t\t\t\t\t\t//o.uv2 = v.texcoord1.xy;\t\t\t\t\t//Send texture coords from unit 1 to fragment shader";
			CGCode[3]+="\n\t\t\t\t\t\t\t//o.color = v.color;\t\t\t\t\t\t//Send interpolated vertex color to fragment shader";
			CGCode[3]+="\n\t\t\t\t\t\t\t//o.color = _Color;\t\t\t\t\t\t\t//Send solid color to fragment shader";
		}
		CGCode[3]+="\n\t\t\t\treturn o;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t\t\t\t\t//Transmit data to the fragment shader";}
		CGCode[3]+="\n\t\t\t}";
		CGCode[3]+="\n";   
		if (Comments>1) {CGCode[3]+="\n\t\t\t//Fragment shader";}
		CGCode[3]+="\n\t\t\tfixed4 frag(VertexToFragment i) : COLOR {";
		CGCode[3]+="\n\t\t\t\tfixed4 tex=tex2D(_Texture0, i.uv);";
		CGCode[3]+="\n\t\t\t\treturn tex*_Color;";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t//Output RGBA texture color multiplied by a fixed color";}
		CGCode[3]+="\n\t\t\t}";
    	CGCode[3]+="\n";
		CGCode[3]+="\n\t\t\tENDCG";
		if (Comments>2) {CGCode[3]+="\t\t\t\t\t\t\t//End of CG program";}
		CGCode[3]+="\n";

		//Texture+VertexColor Function
		if (Comments>2) {
			CGCode[4]="\n\n\t\t\tCGPROGRAM\t\t\t\t\t\t//Start a program in the CG language";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[4]+="\n\t\t\t#pragma target 2.0\t\t\t\t//Run this shader on at least Shader Model 2.0 hardware (e.g. Direct3D 9)";
			} else {
				//Shader model 3.0
				CGCode[4]+="\n\t\t\t#pragma target 3.0\t\t\t\t//Run this shader on at least Shader Model 3.0 hardware (e.g. Direct3D 9)";
			}
			CGCode[4]+="\n\t\t\t#pragma fragment frag\t\t\t//The fragment shader is named 'frag'";
			CGCode[4]+="\n\t\t\t#pragma vertex vert	\t\t\t//The vertex shader is named 'vert'";
			CGCode[4]+="\n\t\t\t#include \"UnityCG.cginc\"\t\t//Include Unity's predefined inputs and macros";
		} else {
			CGCode[4]="\n\n\t\t\tCGPROGRAM";
			if (target.ShaderModelPreset==0) {
				//Shader model 2.0
				CGCode[4]+="\n\t\t\t#pragma target 2.0";
			} else {
				//Shader model 3.0
				CGCode[4]+="\n\t\t\t#pragma target 3.0";
			}
			CGCode[4]+="\n\t\t\t#pragma fragment frag";
			CGCode[4]+="\n\t\t\t#pragma vertex vert";
			CGCode[4]+="\n\t\t\t#include \"UnityCG.cginc\"";		
		}
		CGCode[4]+="\n";
		if (Comments>1) {CGCode[4]+="\n\t\t\t//Unity variables to be made accessible to Vertex and/or Fragment shader";}
		CGCode[4]+="\n\t\t\tuniform sampler2D _Texture0;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t//Define _Texture0 from Texture Unit 0 to be sampled in 2D";}
		CGCode[4]+="\n\t\t\tuniform float4 _Texture0_ST;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t//Use the Float _Texture0_ST to pass the Offset and Tiling for the texture(s)";}
		CGCode[4]+="\n";
		if (Comments>1) {CGCode[4]+="\n\t\t\t//Data structure communication from Unity to the vertex shader";}
		if (Comments>1) {CGCode[4]+="\n\t\t\t//Defines what inputs the vertex shader accepts";}
		CGCode[4]+="\n\t\t\tstruct AppData {";
		CGCode[4]+="\n\t\t\t\tfloat4 vertex : POSITION;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t//Receive vertex position";}
		CGCode[4]+="\n\t\t\t\thalf2 texcoord : TEXCOORD0;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t//Receive texture coordinates";}
		if (Comments>3) {
			CGCode[4]+="\n\t\t\t\t\t\t\t//half2 texcoord1 : TEXCOORD1;\t\t\t\t//Receive texture coordinates";
			CGCode[4]+="\n\t\t\t\tfixed4 color : COLOR;\t\t\t\t\t\t//Receive vertex colors";
		}
		CGCode[4]+="\n\t\t\t};";
		CGCode[4]+="\n";
		if (Comments>1) {CGCode[4]+="\n\t\t\t//Data structure for communication from vertex shader to fragment shader";}
		if (Comments>1) {CGCode[4]+="\n\t\t\t//Defines what inputs the fragment shader accepts";}
		CGCode[4]+="\n\t\t\tstruct VertexToFragment {";
		CGCode[4]+="\n\t\t\t\tfloat4 pos : POSITION;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t\t//Send fragment position to fragment shader";}
		CGCode[4]+="\n\t\t\t\thalf2 uv : TEXCOORD0;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";}
		if (Comments>3) {
			CGCode[4]+="\n\t\t\t\t\t\t\t//half2 uv2 : TEXCOORD1;\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
			CGCode[4]+="\n\t\t\t\tfixed4 color : COLOR;\t\t\t\t\t\t//Send interpolated gouraud-shaded vertex color to fragment shader";
		}
		CGCode[4]+="\n\t\t\t};";
		CGCode[4]+="\n";
		if (Comments>1) {CGCode[4]+="\n\t\t\t//Vertex shader";}
		CGCode[4]+="\n\t\t\tVertexToFragment vert(AppData v) {";
		CGCode[4]+="\n\t\t\t\tVertexToFragment o;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t\t\t//Create a data structure to pass to fragment shader";}
		CGCode[4]+="\n\t\t\t\to.pos = mul(UNITY_MATRIX_MVP,v.vertex);";
		if (Comments>2) {CGCode[4]+="\t\t//Include influence of Modelview + Projection matrices";}
		CGCode[4]+="\n\t\t\t\to.uv = v.texcoord.xy;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t\t//Send texture coords from unit 0 to fragment shader";}
		if (Comments>3) {
			CGCode[4]+="\n\t\t\t\t\t\t\t//o.uv2 = v.texcoord1.xy;\t\t\t\t\t//Send texture coords from unit 1 to fragment shader";
			CGCode[4]+="\n\t\t\t\to.color = v.color;\t\t\t\t\t\t\t//Send interpolated vertex color to fragment shader";
			CGCode[4]+="\n\t\t\t\t\t\t\t//o.color = _Color;\t\t\t\t\t\t\t//Send solid color to fragment shader";
		}
		CGCode[4]+="\n\t\t\t\treturn o;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t\t\t\t\t//Transmit data to the fragment shader";}
		CGCode[4]+="\n\t\t\t}";
		CGCode[4]+="\n";   
		if (Comments>1) {CGCode[4]+="\n\t\t\t//Fragment shader";}
		CGCode[4]+="\n\t\t\tfixed4 frag(VertexToFragment i) : COLOR {";
		CGCode[4]+="\n\t\t\t\tfixed4 tex=tex2D(_Texture0, i.uv);";
		CGCode[4]+="\n\t\t\t\treturn tex*i.color;";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t//Output RGBA texture color multiplied by vertex color";}
		CGCode[4]+="\n\t\t\t}";
    	CGCode[4]+="\n";
		CGCode[4]+="\n\t\t\tENDCG";
		if (Comments>2) {CGCode[4]+="\t\t\t\t\t\t\t//End of CG program";}
		CGCode[4]+="\n";

		//CGCode[5] for MultiTexture shader is implemented with loops below
																
		//Blend mode GL tag combinations
		var BlendModeTags:String[] = new String[NumberOfBlends-1];	//Allocate space
		BlendModeTags[0]="Off";							//Solid
		BlendModeTags[1]="Off";							//Solid Test
		BlendModeTags[2]="SrcAlpha OneMinusSrcAlpha";	//AlphaBlend
		BlendModeTags[3]="SrcAlpha OneMinusSrcAlpha";	//AlphaBlend Test
		BlendModeTags[4]="SrcAlpha One";				//AlphaBlend Add
		BlendModeTags[5]="SrcAlpha One";				//AlphaBlend Add Test
		BlendModeTags[6]="SrcAlpha One";				//Alphablend Subtract
		BlendModeTags[7]="SrcAlpha One";				//Alphablend Subtract Test
		BlendModeTags[8]="One One";						//Add
		BlendModeTags[9]="OneMinusDstColor One";		//Add Soft
		BlendModeTags[10]="One One";					//Subtract
		BlendModeTags[11]="OneMinusDstColor One";		//Subtract Soft
		BlendModeTags[12]="One One";					//Subtract Reverse
		BlendModeTags[13]="DstColor Zero";				//Multiply
		BlendModeTags[14]="DstColor SrcColor";			//Multiply Double
		BlendModeTags[15]="One One";					//Min
		BlendModeTags[16]="One One";					//Max
		BlendModeTags[17]="One OneMinusSrcColor";		//Screen
		BlendModeTags[18]="DstAlpha OneMinusDstAlpha";	//Dest AlphaBlend
		BlendModeTags[19]="DstAlpha One";				//Dest AlphaBlend Add
		BlendModeTags[20]="DstAlpha One";				//Dest AlphaBlend Subtract
		BlendModeTags[21]="One One";					//Add To Dest Alpha
		BlendModeTags[22]="One One";					//Subtract From Dest Alpha
		BlendModeTags[23]="Zero SrcAlpha";				//Multiply Dest Alpha
		BlendModeTags[24]="DstAlpha SrcAlpha";			//Dest Alpha Colorize Add
		BlendModeTags[25]="DstAlpha SrcAlpha";			//Dest Alpha Colorize Subtract

		var MergeCount:int[] = new int[NumberOfTextureCounts+1];	//16 texture layer merges for multitexturing
		var MergeStart:int[] = new int[NumberOfTextureCounts+1];	//16 texture layer merges for multitexturing
		var MergeEnd:int[] = new int[NumberOfTextureCounts+1];		//16 texture layer merges for multitexturing

		//Prepare to iterate through shader functions
		var FunctionCount:int=0;				//Counter for iterating through functions
		var FunctionStart:int=0;				//Which function to start generating at
		var FunctionEnd:int=0;					//Which function to end generating at
		if (target.ShaderFunctionPreset==NumberOfFunctions-1) {
			//Generate all functions
			FunctionStart=0;					//Start at beginning
			FunctionEnd=NumberOfFunctions-2;	//Do them all
		} else {
			//Generate a specific function
			FunctionStart=target.ShaderFunctionPreset;	//Do just this one
			FunctionEnd=FunctionStart;			//Stop here
		}

		for (FunctionCount=FunctionStart; FunctionCount<=FunctionEnd; FunctionCount+=1) {
			//Generate shader(s) for each Function
			

			//Prepare to iterate through texture scale/translate modes
			var ScaleTranCount:int=0;				//Counter for iterating through texture scale/translate modes
			var ScaleTranStart:int=0;				//Which mode to start generating at
			var ScaleTranEnd:int=0;					//Which mode to end generating at
			if (target.ScaleTranslateTexturePreset[0]==NumberOfScaleTranslateTextures-1) {
				//Generate all Scale/Translate modes
				ScaleTranStart=0;					//Start at beginning
				ScaleTranEnd=NumberOfScaleTranslateTextures-2;	//Do them all
			} else {
				//Generate a specific Scale/Translate mode
				ScaleTranStart=target.ScaleTranslateTexturePreset[0];	//Do just this one
				ScaleTranEnd=ScaleTranStart;		//Stop here
			}
			for (ScaleTranCount=ScaleTranStart; ScaleTranCount<=ScaleTranEnd; ScaleTranCount+=1) {
				//Generate shader(s) for each Scale/Translate mode
				
		
				//Prepare to iterate through Blend modes
				var BlendModeCount:int=0;				//Counter for iterating through blend modes
				var BlendModeStart:int=0;				//Which mode to start generating at
				var BlendModeEnd:int=0;					//Which mode to end generating at
				if (target.BlendModePreset==NumberOfBlends-1) {
					//Generate all Blend modes
					BlendModeStart=0;					//Start at beginning
					BlendModeEnd=NumberOfBlends-2;		//Do them all
				} else {
					//Generate a specific Blend mode
					BlendModeStart=target.BlendModePreset;	//Do just this one
					BlendModeEnd=BlendModeStart;		//Stop here
				}
				for (BlendModeCount=BlendModeStart; BlendModeCount<=BlendModeEnd; BlendModeCount+=1) {
					//Generate shader(s) for each Blend mode
					
					
					//Prepare to iterate through ZWrite modes
					var ZWriteCount:int=0;					//Counter for iterating through ZWrite modes
					var ZWriteStart:int=0;					//Which mode to start generating at
					var ZWriteEnd:int=0;					//Which mode to end generating at
					if (target.ZWritePreset==NumberOfZWrites-1) {
						//Generate all ZWrite modes
						ZWriteStart=0;						//Start at beginning
						ZWriteEnd=NumberOfZWrites-2;			//Do them all
					} else {
						//Generate a specific ZWrite mode
						ZWriteStart=target.ZWritePreset;	//Do just this one
						ZWriteEnd=ZWriteStart;				//Stop here
					}
					for (ZWriteCount=ZWriteStart; ZWriteCount<=ZWriteEnd; ZWriteCount+=1) {
						//Generate shader(s) for each ZWrite mode
						
						
						//Prepare to iterate through ZTest modes
						var ZTestCount:int=0;					//Counter for iterating through ZTest modes
						var ZTestStart:int=0;					//Which mode to start generating at
						var ZTestEnd:int=0;						//Which mode to end generating at
						if (target.ZTestPreset==NumberOfZTests-1) {
							//Generate all ZTest modes
							ZTestStart=0;						//Start at beginning
							ZTestEnd=NumberOfZTests-2;			//Do them all
						} else {
							//Generate a specific ZTest mode
							ZTestStart=target.ZTestPreset;		//Do just this one
							ZTestEnd=ZTestStart;				//Stop here
						}
						for (ZTestCount=ZTestStart; ZTestCount<=ZTestEnd; ZTestCount+=1) {
							//Generate shader(s) for each ZTest mode
							
						
							//Prepare to iterate through Mask modes
							var MaskCount:int=0;					//Counter for iterating through Mask modes
							var MaskStart:int=0;					//Which mode to start generating at
							var MaskEnd:int=0;						//Which mode to end generating at
							if (target.ColorMaskPreset==NumberOfMasks-1) {
								//Generate all Mask modes
								MaskStart=0;						//Start at beginning
								MaskEnd=NumberOfMasks-2;			//Do them all
							} else {
								//Generate a specific Mask mode
								MaskStart=target.ColorMaskPreset;	//Do just this one
								MaskEnd=MaskStart;					//Stop here
							}
							for (MaskCount=MaskStart; MaskCount<=MaskEnd; MaskCount+=1) {
								//Generate shader(s) for each Mask mode
							
						
								//Prepare to iterate through Culling modes
								var CullingCount:int=0;					//Counter for iterating through Culling modes
								var CullingStart:int=0;					//Which mode to start generating at
								var CullingkEnd:int=0;					//Which mode to end generating at
								if (target.CullingPreset==NumberOfCullings-1) {
									//Generate all Culling modes
									CullingStart=0;						//Start at beginning
									CullingEnd=NumberOfCullings-2;		//Do them all
								} else {
									//Generate a specific Culling mode
									CullingStart=target.CullingPreset;	//Do just this one
									CullingEnd=CullingStart;			//Stop here
								}
								for (CullingCount=CullingStart; CullingCount<=CullingEnd; CullingCount+=1) {
									//Generate shader(s) for each Culling mode
									

									//Prepare to iterate through MultiTexture layer 1 merge modes
									MergeCount[1]=0;				//Counter for iterating through Merge modes
									MergeStart[1]=0;				//Which merge to start generating at
									MergeEnd[1]=0;					//Which merge to end generating at
									if ((target.ShaderFunctionPreset==5) && (target.MergePreset[1]==NumberOfMerges-1)) {
										//Generate all Merge 1 modes
										MergeStart[1]=0;						//Start at beginning
										MergeEnd[1]=NumberOfMerges-2;			//Do them all
									} else {
										//Generate a specific Merge mode
										MergeStart[1]=target.MergePreset[1];	//Do just this one
										MergeEnd[1]=MergeStart[1];				//Stop here
									}
									for (MergeCount[1]=MergeStart[1]; MergeCount[1]<=MergeEnd[1]; MergeCount[1]+=1) {
										//Generate shader(s) for each Merge mode
									

										//Prepare to iterate through MultiTexture layer 2 merge modes
										MergeCount[2]=0;				//Counter for iterating through Merge modes
										MergeStart[2]=0;				//Which merge to start generating at
										MergeEnd[2]=0;					//Which merge to end generating at
										if ((target.ShaderFunctionPreset==5) && (target.MergePreset[2]==NumberOfMerges-1)) {
											//Generate all Merge 2 modes
											MergeStart[2]=0;					//Start at beginning
											MergeEnd[2]=NumberOfMerges-2;			//Do them all
										} else {
											//Generate a specific Merge mode
											MergeStart[2]=target.MergePreset[2];	//Do just this one
											MergeEnd[2]=MergeStart[2];				//Stop here
										}
										for (MergeCount[2]=MergeStart[2]; MergeCount[2]<=MergeEnd[2]; MergeCount[2]+=1) {
											//Generate shader(s) for each Merge mode


											//Prepare to iterate through MultiTexture layer 3 merge modes
											MergeCount[3]=0;				//Counter for iterating through Merge modes
											MergeStart[3]=0;				//Which merge to start generating at
											MergeEnd[3]=0;					//Which merge to end generating at
											if ((target.ShaderFunctionPreset==5) && (target.MergePreset[3]==NumberOfMerges-1)) {
												//Generate all Merge 3 modes
												MergeStart[3]=0;						//Start at beginning
												MergeEnd[3]=NumberOfMerges-2;			//Do them all
											} else {
												//Generate a specific Merge mode
												MergeStart[3]=target.MergePreset[3];	//Do just this one
												MergeEnd[3]=MergeStart[3];				//Stop here
											}
											for (MergeCount[3]=MergeStart[3]; MergeCount[3]<=MergeEnd[3]; MergeCount[3]+=1) {
												//Generate shader(s) for each Merge mode


												//Prepare to iterate through MultiTexture layer 4 merge modes
												MergeCount[4]=0;				//Counter for iterating through Merge modes
												MergeStart[4]=0;				//Which merge to start generating at
												MergeEnd[4]=0;					//Which merge to end generating at
												if ((target.ShaderFunctionPreset==5) && (target.MergePreset[4]==NumberOfMerges-1)) {
													//Generate all Merge 4 modes
													MergeStart[4]=0;						//Start at beginning
													MergeEnd[4]=NumberOfMerges-2;			//Do them all
												} else {
													//Generate a specific Merge mode
													MergeStart[4]=target.MergePreset[4];	//Do just this one
													MergeEnd[4]=MergeStart[4];				//Stop here
												}
												for (MergeCount[4]=MergeStart[4]; MergeCount[4]<=MergeEnd[4]; MergeCount[4]+=1) {
													//Generate shader(s) for each Merge mode


													//Prepare to iterate through MultiTexture layer 5 merge modes
													MergeCount[5]=0;				//Counter for iterating through Merge modes
													MergeStart[5]=0;				//Which merge to start generating at
													MergeEnd[5]=0;					//Which merge to end generating at
													if ((target.ShaderFunctionPreset==5) && (target.MergePreset[5]==NumberOfMerges-1)) {
														//Generate all Merge 5 modes
														MergeStart[5]=0;						//Start at beginning
														MergeEnd[5]=NumberOfMerges-2;			//Do them all
													} else {
														//Generate a specific Merge mode
														MergeStart[5]=target.MergePreset[5];	//Do just this one
														MergeEnd[5]=MergeStart[5];				//Stop here
													}
													for (MergeCount[5]=MergeStart[5]; MergeCount[5]<=MergeEnd[5]; MergeCount[5]+=1) {
														//Generate shader(s) for each Merge mode


														//Prepare to iterate through MultiTexture layer 6 merge modes
														MergeCount[6]=0;				//Counter for iterating through Merge modes
														MergeStart[6]=0;				//Which merge to start generating at
														MergeEnd[6]=0;					//Which merge to end generating at
														if ((target.ShaderFunctionPreset==5) && (target.MergePreset[6]==NumberOfMerges-1)) {
															//Generate all Merge 6 modes
															MergeStart[6]=0;						//Start at beginning
															MergeEnd[6]=NumberOfMerges-2;			//Do them all
														} else {
															//Generate a specific Merge mode
															MergeStart[6]=target.MergePreset[6];	//Do just this one
															MergeEnd[6]=MergeStart[6];				//Stop here
														}
														for (MergeCount[6]=MergeStart[6]; MergeCount[6]<=MergeEnd[6]; MergeCount[6]+=1) {
															//Generate shader(s) for each Merge mode


															//Prepare to iterate through MultiTexture layer 7 merge modes
															MergeCount[7]=0;				//Counter for iterating through Merge modes
															MergeStart[7]=0;				//Which merge to start generating at
															MergeEnd[7]=0;					//Which merge to end generating at
															if ((target.ShaderFunctionPreset==5) && (target.MergePreset[7]==NumberOfMerges-1)) {
																//Generate all Merge 7 modes
																MergeStart[7]=0;						//Start at beginning
																MergeEnd[7]=NumberOfMerges-2;			//Do them all
															} else {
																//Generate a specific Merge mode
																MergeStart[7]=target.MergePreset[7];	//Do just this one
																MergeEnd[7]=MergeStart[7];				//Stop here
															}
															for (MergeCount[7]=MergeStart[7]; MergeCount[7]<=MergeEnd[7]; MergeCount[7]+=1) {
																//Generate shader(s) for each Merge mode


																//Prepare to iterate through MultiTexture layer 8 merge modes
																MergeCount[8]=0;				//Counter for iterating through Merge modes
																MergeStart[8]=0;				//Which merge to start generating at
																MergeEnd[8]=0;					//Which merge to end generating at
																if ((target.ShaderFunctionPreset==5) && (target.MergePreset[8]==NumberOfMerges-1)) {
																	//Generate all Merge 8 modes
																	MergeStart[8]=0;						//Start at beginning
																	MergeEnd[8]=NumberOfMerges-2;			//Do them all
																} else {
																	//Generate a specific Merge mode
																	MergeStart[8]=target.MergePreset[8];	//Do just this one
																	MergeEnd[8]=MergeStart[8];				//Stop here
																}
																for (MergeCount[8]=MergeStart[8]; MergeCount[8]<=MergeEnd[8]; MergeCount[8]+=1) {
																	//Generate shader(s) for each Merge mode


																	//Prepare to iterate through MultiTexture layer 9 merge modes
																	MergeCount[9]=0;				//Counter for iterating through Merge modes
																	MergeStart[9]=0;				//Which merge to start generating at
																	MergeEnd[9]=0;					//Which merge to end generating at
																	if ((target.ShaderFunctionPreset==5) && (target.MergePreset[9]==NumberOfMerges-1)) {
																		//Generate all Merge 9 modes
																		MergeStart[9]=0;						//Start at beginning
																		MergeEnd[9]=NumberOfMerges-2;			//Do them all
																	} else {
																		//Generate a specific Merge mode
																		MergeStart[9]=target.MergePreset[9];	//Do just this one
																		MergeEnd[9]=MergeStart[9];				//Stop here
																	}
																	for (MergeCount[9]=MergeStart[9]; MergeCount[9]<=MergeEnd[9]; MergeCount[9]+=1) {
																		//Generate shader(s) for each Merge mode


																		//Prepare to iterate through MultiTexture layer 10 merge modes
																		MergeCount[10]=0;				//Counter for iterating through Merge modes
																		MergeStart[10]=0;				//Which merge to start generating at
																		MergeEnd[10]=0;					//Which merge to end generating at
																		if ((target.ShaderFunctionPreset==5) && (target.MergePreset[10]==NumberOfMerges-1)) {
																			//Generate all Merge 10 modes
																			MergeStart[10]=0;						//Start at beginning
																			MergeEnd[10]=NumberOfMerges-2;			//Do them all
																		} else {
																			//Generate a specific Merge mode
																			MergeStart[10]=target.MergePreset[10];	//Do just this one
																			MergeEnd[10]=MergeStart[10];				//Stop here
																		}
																		for (MergeCount[10]=MergeStart[10]; MergeCount[10]<=MergeEnd[10]; MergeCount[10]+=1) {
																			//Generate shader(s) for each Merge mode


																			//Prepare to iterate through MultiTexture layer 11 merge modes
																			MergeCount[11]=0;				//Counter for iterating through Merge modes
																			MergeStart[11]=0;				//Which merge to start generating at
																			MergeEnd[11]=0;					//Which merge to end generating at
																			if ((target.ShaderFunctionPreset==5) && (target.MergePreset[11]==NumberOfMerges-1)) {
																				//Generate all Merge 11modes
																				MergeStart[11]=0;						//Start at beginning
																				MergeEnd[11]=NumberOfMerges-2;			//Do them all
																			} else {
																				//Generate a specific Merge mode
																				MergeStart[11]=target.MergePreset[11];	//Do just this one
																				MergeEnd[11]=MergeStart[11];				//Stop here
																			}
																			for (MergeCount[11]=MergeStart[11]; MergeCount[11]<=MergeEnd[11]; MergeCount[11]+=1) {
																				//Generate shader(s) for each Merge mode


																				//Prepare to iterate through MultiTexture layer 12 merge modes
																				MergeCount[12]=0;				//Counter for iterating through Merge modes
																				MergeStart[12]=0;				//Which merge to start generating at
																				MergeEnd[12]=0;					//Which merge to end generating at
																				if ((target.ShaderFunctionPreset==5) && (target.MergePreset[12]==NumberOfMerges-1)) {
																					//Generate all Merge 12 modes
																					MergeStart[12]=0;						//Start at beginning
																					MergeEnd[12]=NumberOfMerges-2;			//Do them all
																				} else {
																					//Generate a specific Merge mode
																					MergeStart[12]=target.MergePreset[12];	//Do just this one
																					MergeEnd[12]=MergeStart[12];				//Stop here
																				}
																				for (MergeCount[12]=MergeStart[12]; MergeCount[12]<=MergeEnd[12]; MergeCount[12]+=1) {
																					//Generate shader(s) for each Merge mode


																					//Prepare to iterate through MultiTexture layer 13 merge modes
																					MergeCount[13]=0;				//Counter for iterating through Merge modes
																					MergeStart[13]=0;				//Which merge to start generating at
																					MergeEnd[13]=0;					//Which merge to end generating at
																					if ((target.ShaderFunctionPreset==5) && (target.MergePreset[13]==NumberOfMerges-1)) {
																						//Generate all Merge 13 modes
																						MergeStart[13]=0;						//Start at beginning
																						MergeEnd[13]=NumberOfMerges-2;			//Do them all
																					} else {
																						//Generate a specific Merge mode
																						MergeStart[13]=target.MergePreset[13];	//Do just this one
																						MergeEnd[13]=MergeStart[13];				//Stop here
																					}
																					for (MergeCount[13]=MergeStart[13]; MergeCount[13]<=MergeEnd[13]; MergeCount[13]+=1) {
																						//Generate shader(s) for each Merge mode


																						//Prepare to iterate through MultiTexture layer 14 merge modes
																						MergeCount[14]=0;				//Counter for iterating through Merge modes
																						MergeStart[14]=0;				//Which merge to start generating at
																						MergeEnd[14]=0;					//Which merge to end generating at
																						if ((target.ShaderFunctionPreset==5) && (target.MergePreset[14]==NumberOfMerges-1)) {
																							//Generate all Merge 14 modes
																							MergeStart[14]=0;						//Start at beginning
																							MergeEnd[14]=NumberOfMerges-2;			//Do them all
																						} else {
																							//Generate a specific Merge mode
																							MergeStart[14]=target.MergePreset[14];	//Do just this one
																							MergeEnd[14]=MergeStart[14];				//Stop here
																						}
																						for (MergeCount[14]=MergeStart[14]; MergeCount[14]<=MergeEnd[14]; MergeCount[14]+=1) {
																							//Generate shader(s) for each Merge mode


																							//Prepare to iterate through MultiTexture layer 15 merge modes
																							MergeCount[15]=0;				//Counter for iterating through Merge modes
																							MergeStart[15]=0;				//Which merge to start generating at
																							MergeEnd[15]=0;					//Which merge to end generating at
																							if (((target.ShaderFunctionPreset==5) && target.MergePreset[15]==NumberOfMerges-1)) {
																								//Generate all Merge 15 modes
																								MergeStart[15]=0;						//Start at beginning
																								MergeEnd[15]=NumberOfMerges-2;			//Do them all
																							} else {
																								//Generate a specific Merge mode
																								MergeStart[15]=target.MergePreset[15];	//Do just this one
																								MergeEnd[15]=MergeStart[15];				//Stop here
																							}
																							for (MergeCount[15]=MergeStart[15]; MergeCount[15]<=MergeEnd[15]; MergeCount[15]+=1) {
																								//Generate shader(s) for each Merge mode


																								//Get names for this shader's settings
																								FunctionName=FunctionNames[FunctionCount];		//Name of the function
																								ScaleTranName=ScaleTranNames[ScaleTranCount];	//Name of the scale/translation mode
																								BlendName=BlendNames[BlendModeCount];			//Name of the blend mode
																								ZWriteName=ZWriteNames[ZWriteCount];			//Name of the zwrite mode
																								ZTestName=ZTestNames[ZTestCount];				//Name of the ztest mode
																								MaskName=MaskNames[MaskCount];					//Name of the color mask mode
																								CullingName=CullingNames[CullingCount];			//Name of the culling mode
																								MergeName="";									//Name of the merge modes for multitexturing
																								if ((FunctionCount==5) && (target.ShaderNamingIncludeMergesPreset==0)) {
																									//Multitexturing, but only if automatically adding merge names
																									for (Temp=1;Temp<target.NumberOfTexturesPreset+2;Temp++) {
																										MergeName+=MergeNames[MergeCount[Temp]]+"_";	//Append Merge mode
																									}
																								}
																								

																								//Create a name for the shader
																								ShaderName="";									//Init
																								if (target.ShaderNamePrefix != "") {
																									ShaderName=target.ShaderNamePrefix+"-";		//Start with the prefix
																								}
																								if ((target.ShaderNamingPreset==0) || ((target.ShaderNamePrefix.Length==0) && (target.ShaderNameSuffix.Length==0) && (target.ShaderCountingPreset != 0)) ) {
																									//Include an automatic name (either requested or required as a result of no prefix/suffix)
																									ShaderName+=FunctionName+"-";				//Add function
																									if (target.ShaderCountingPreset==0) {
																										//Include counter
																										CounterText=Counter.ToString();			//Convert counter to string
																										if (CounterText.Length==1) {CounterText="00"+CounterText;}	//Pad counter
																										if (CounterText.Length==2) {CounterText="0"+CounterText;}	//Pad counter
																										ShaderName+=CounterText+"-";			//Add counter
																									}
																									ShaderName+=MergeName;						//Add merge modes
																									if (ShaderName[ShaderName.Length-1]=="_") {
																										ShaderName=ShaderName.Substring(0,ShaderName.Length-1)+"-";	//Convert end '_' to '-'
																									}
																									ShaderName+=BlendName+"-";					//Add blend mode
																									if (ZWriteCount==1) {
																										//Add ZWrite when it's on, ignore when off
																										ShaderName+="ZWrite-";					//Add zwrite
																									}
																									if (ZTestCount>0) {
																										//Add ZTest when it's other than off
																										ShaderName+="ZTest"+ZTestName+"-";		//Add ztest
																									}
																									if (MaskCount>0) {
																										//Add color mask when it's other than RGBA
																										ShaderName+=MaskName+"-";				//Add mask name
																									}
																									if ((CullingCount==1) || (CullingCount==2)) {
																										//Add culling if Front or Back, not Off
																										ShaderName+="Cull"+CullingName+"-";		//Add cull name
																									}
																									if (ScaleTranCount==1) {
																										//Add Texture scale/translate when active
																										ShaderName+="Tile&Offset-";				//Add tile/offset
																									}
																								} else {
																									//Manual name, add counter?
																									if (target.ShaderCountingPreset==0) {
																										//Include counter
																										CounterText=Counter.ToString();			//Convert counter to string
																										if (CounterText.Length==1) {CounterText="00"+CounterText;}	//Pad counter
																										if (CounterText.Length==2) {CounterText="0"+CounterText;}	//Pad counter
																										ShaderName+=CounterText+"-";			//Add counter
																									}
																								}
																								if (target.ShaderNameSuffix != "") {
																									ShaderName+=target.ShaderNameSuffix;		//End with the suffix
																								}
																								if (ShaderName[ShaderName.Length-1]=="-") {
																									ShaderName=ShaderName.Substring(0,ShaderName.Length-1);	//Trim extra '-' from end
																								}
																								ShaderName=ShaderName.Replace(" ","");			//Remove spaces
															
																								//Determine if this shader uses a texture
																								HasTextures=false;
																								if ((FunctionCount==2) || (FunctionCount==3) || (FunctionCount==4) || (FunctionCount==5)) {
																									//This shader has a texture
																									HasTextures=true;
																								}
																								
																								//Determine if this shader uses a fixed color
																								HasColor=false;
																								if ((FunctionCount==0) || (FunctionCount==3)) {
																									//This shader has a fixed color
																									HasColor=true;
																								}
																								
																								//Determine if this shader uses alpha testing
																								HasTest=false;
																								if ((BlendModeCount==1) || (BlendModeCount==3) || (BlendModeCount==5) || (BlendModeCount==7)) {
																									//This shader uses alpha testing
																									HasTest=true;
																								}
																								
																								//Determine if this shader uses BlendOp
																								HasBlendOp=false;
																								if ((BlendModeCount==6) || (BlendModeCount==7) || (BlendModeCount==10) || (BlendModeCount==11) || (BlendModeCount==12) || (BlendModeCount==15) || (BlendModeCount==16) || (BlendModeCount==20) || (BlendModeCount==22) || (BlendModeCount==25)) {
																									//This shader uses BlendOp
																									HasBlendOp=true;
																								}
																								
																								//Create shader header
																								ShaderText="Shader \"";
																								if (target.MenuName != "") {
																									ShaderText+=target.MenuName+"/";			//Append menu name
																								}
																								if (target.SubName != "") {
																									ShaderText+=target.SubName+"/";				//Append sub menu name
																								}
																								switch(target.FolderModePreset){
																									case 0:
																										//Grouped by function
																										ShaderText+=FunctionName.Replace(" ","")+"/";	//Append function name
																										break;
																									case 1:
																										//Grouped by blend mode
																										ShaderText+=BlendName.Replace(" ","")+"/";		//Append blend mode name
																										break;
																									case 2:
																										//Grouped by function then blend mode
																										ShaderText+=FunctionName.Replace(" ","")+"/"+BlendName.Replace(" ","")+"/";	//Append function and blend as hierarchy
																										break;
																									case 3:
																										//Flat - don't add a folder
																										break;
																								}
																								ShaderText+=ShaderName+"\" {\n";				//Append shader name
																								
																								//Add header
																								if (Comments>0) {
																									//Commenting includes at least a header
																									ShaderText+="\n\t//\""+FunctionName+"\" shader with \""+BlendName+"\" blending\n";		//Append header
																									if (target.ShaderFunctionPreset==5) {
																										//MultiTexture header
																										ShaderText+="\t//MultiTexturing with "+(target.NumberOfTexturesPreset+2)+" texture layers:\n";
																										for (Temp=1;Temp<target.NumberOfTexturesPreset+2;Temp++){
																											ShaderText+="\t\t//Layer "+(Temp)+", Merged by "+MergeNames[MergeCount[Temp]]+"\n";	//Append merge
																										}
																									}
																									ShaderText+="\t//Z-Writing is "+ZWriteName+" and Z-Testing is "+ZTestName+"\n";
																									ShaderText+="\t//Color masking is "+MaskName+" and Culling is "+CullingName+"\n";
																									if (HasTextures==true) {
																										if (target.ScaleTranslateTexturePreset[0]==0) {
																											//Ignore Tiling/Offset
																											ShaderText+="\t//Texture coords cannot be adjusted with Tiling and Offset in material";
																										} else {
																											//Include Tiling/Offset
																											ShaderText+="\t//Texture coords can be adjusted with Tiling and Offset in material";
																										}
																									}
																								}
																								
																								//Add properties
																								if ((HasTextures==true) || (HasColor==true) || (HasTest==true)) {
																									//There are properties to add
																									ShaderText+="\n\n";
																									if (Comments>1) {
																										//Add section comment
																										ShaderText+="\t//Set up the shader to receive external inputs from Unity\n";	//Append property section commment
																									}
																									ShaderText+="\tProperties {";
																									if (HasTextures==true) {
																										if (target.ShaderFunctionPreset==5) {
																											for (Temp=0;Temp<target.NumberOfTexturesPreset+2;Temp++) {
																												if (target.DefaultTexture[Temp]) {
																													//Default texture is defined, refer by name
																													ShaderText+="\n\t\t_Texture"+Temp+" (\"Texture "+Temp+"\", 2D) = \""+target.DefaultTexture[Temp].name+"\" {}";	//Append texture property
																												} else {
																													//Default texture is undefined, leave blank
																													ShaderText+="\n\t\t_Texture"+Temp+" (\"Texture "+Temp+"\", 2D) = \"\" {}";	//Append texture property
																												}
																												if (Comments>1) {
																													ShaderText+="\t\t//Receive input from a Texture on Texture Unit "+Temp;	//Append per-line comment
																												}
																												//Select which merge to use and implement the property for _AlphaMask and _AlphaFade
																												switch(MergeCount[Temp]){
																													case 13:
																														//AlphaMask merge
																														ShaderText+="\n\t\t_AlphaMask"+Temp+" (\"AlphaMask"+Temp+"\", Range(0,1)) = "+target.AlphaMaskThreshold[Temp];	//Append AlphaMask property
																														if (Comments>1) {
																															ShaderText+="\t//Receive input from a Float representing the AlphaMask"+Temp+" merge threshold";	//Append per-line comment
																														}
																											    	 	break;
																											        case 14:
																											        	//AlphaFade merge
																														ShaderText+="\n\t\t_AlphaFade"+Temp+" (\"AlphaFade"+Temp+"\", Range(0,2)) = "+target.AlphaFadeThreshold[Temp];	//Append AlphaFade property
																														if (Comments>1) {
																															ShaderText+="\t//Receive input from a Float representing the AlphaFade"+Temp+" merge threshold";	//Append per-line comment
																														}
																														break;
																												}
																											}
																										} else {
																											if (target.DefaultTexture[0]) {
																												//Default texture is defined, refer by name
																												ShaderText+="\n\t\t_Texture0 (\"Texture 0\", 2D) = \""+target.DefaultTexture[0].name+"\" {}";	//Append texture property
																											} else {
																												//Default texture is undefined, leave blank
																												ShaderText+="\n\t\t_Texture0 (\"Texture 0\", 2D) = \"\" {}";	//Append texture property
																											}
																											if (Comments>1) {
																												ShaderText+="\t\t//Receive input from a Texture on Texture Unit 0";	//Append per-line comment
																											}
																										}
																									}
																									if (HasColor==true) {
																										ShaderText+="\n\t\t_Color (\"Solid Color\", Color) = ("+target.FixedColor.r+","+target.FixedColor.g+","+target.FixedColor.b+","+target.FixedColor.a+")";	//Append color property
																										if (Comments>1) {
																											ShaderText+="\t\t//Receive input from a fixed Color";	//Append per-line comment
																										}
																									}
																									if (HasTest==true) {
																										ShaderText+="\n\t\t_AlphaTestThreshold (\"AlphaTest Treshold\", Range(0,1)) = "+target.AlphaTestThreshold;	//Append alpha-test property
																										if (Comments>1) {
																											ShaderText+="\t\t//Receive input from a Float representing the AlphaTest threshold";	//Append per-line comment
																										}
																									}
																									ShaderText+="\n\t}";
																								}
															
																								//Add SubShader
																								ShaderText+="\n";
																								if (Comments>1) {
																									//Include SubShader section comment
																									ShaderText+="\n\t//Define a shader";		//Append subshader comment
																								}
																								ShaderText+="\n\tSubShader {";					//Append subshader
																								
																								//Add Tags
																								ShaderText+="\n";
																								if (Comments>1) {
																									//Include Tags section comment
																									ShaderText+="\n\t\t//Define what queue/order to render this shader in";	//Append queue comment
																								}
																								if (ZWriteCount==0) {
																									//ZWrite is off, so make queue transparent unless it's a solid/min/max shader with no partial blending
																									if ((BlendModeCount==0) || (BlendModeCount==15) || (BlendModeCount==16)) {
																										//Solid/Min/Max, so queue is geometry
																										ShaderText+="\n\t\tTags {\"Queue\" = \"Geometry\" \"RenderType\" = \"Opaque\"}";	//Apend tag
																										if (Comments>2) {ShaderText+="\t\t//Background | Geometry | AlphaTest | Transparent | Overlay - Render this shader as part of the geometry queue because it does not use blending";}	//Append per-line comment
																									} else {
																										if (BlendModeCount==1) {
																											//Solid blend + alphatest (not alphablend), so queue is alphatest
																											ShaderText+="\n\t\tTags {\"Queue\" = \"AlphaTest\" \"RenderType\" = \"Opaque\"}";	//Append tag
																											if (Comments>2) {ShaderText+="\t\t//Background | Geometry | AlphaTest | Transparent | Overlay - Render this shader as part of the AlphaTest queue because it uses alpha testing and not blending";}	//Append per-line comment
																										} else {
																											//Queue is transparent provided there is no ZWrite
																											ShaderText+="\n\t\tTags {\"Queue\" = \"Transparent\" \"RenderType\" = \"Transparent\"}";	//Append tag
																											if (Comments>2) {ShaderText+="\t\t//Background | Geometry | AlphaTest | Transparent | Overlay - Render this shader as part of the Transparent queue because it uses blending and no Z-Write";}	//Append per-line comment
																										}
																									}
																								} else {
																									//ZWrite is on, so can't do a transparent queue
																									if ((BlendModeCount==0) || (BlendModeCount==15) || (BlendModeCount==16)) {
																										//Solid/Min/Max, so queue is geometry
																										ShaderText+="\n\t\tTags {\"Queue\" = \"Geometry\" \"RenderType\" = \"Opaque\"}";	//Apend tag
																										if (Comments>2) {ShaderText+="\t\t//Background | Geometry | AlphaTest | Transparent | Overlay - Render this shader as part of the geometry queue because it does not use blending";}	//Append per-line comment
																									} else {
																										if (HasTest==true) {
																											//Has alphatesting and regardless of whether it's transparent, we do alphatest queue because ZWrite is on
																											ShaderText+="\n\t\tTags {\"Queue\" = \"AlphaTest\"";	//Append tag
																											if ((BlendModeCount==0) || (BlendModeCount==1) || (BlendModeCount==15) || (BlendModeCount==16)) {
																												//Solid/min/max, rendertype is opaque
																												ShaderText+=" \"RenderType\" = \"Opaque\"";			//Append render type
																											} else {
																												//Transparent rendertype
																												ShaderText+=" \"RenderType\" = \"Transparent\"";	//Append render type
																											}
																											ShaderText+="}";	//Finish tag
																											if (Comments>2) {ShaderText+="\t\t//Background | Geometry | AlphaTest | Transparent | Overlay - Render this shader as part of the AlphaTest queue because it uses alpha testing and Z-Write is On";}	//Append per-line comment
																										} else {
																											//Queue would be transparent but has to be geometry because Z-Write is on
																											ShaderText+="\n\t\tTags {\"Queue\" = \"Geometry\"}";	//Append tag
																											if (Comments>2) {ShaderText+="\t\t//Background | Geometry | AlphaTest | Transparent | Overlay - Render this shader as part of the Geometry queue because although it uses blending, Z-Write is On";}	//Append per-line comment
																										}
																									}
															
																								}
																								
																								//Add Pass
																								ShaderText+="\n";
																								if (Comments>1) {
																									//Include Pass section comment
																									ShaderText+="\n\t\t//Define a pass";		//Append pass comment
																								}
																								ShaderText+="\n\t\tPass {";						//Append pass
															
																								//Blend mode code header
																								ShaderText+="\n";
																								if (Comments>1) {
																									ShaderText+="\n\t\t\t//Set up blending and other operations";
																								}
															
																								//Culling
																								ShaderText+="\n\t\t\tCull "+CullingName;		//Add culling
																								if (Comments>2) {
																									ShaderText+="\t\t\t// Back | Front | Off";	//Add per-line comment
																									if (CullingCount==0) {
																										ShaderText+=" - Do not cull any triangle faces";
																									} else {
																										if (CullingCount==1) {
																											ShaderText+=" - Remove/cull the back-facing triangles only";
																										} else {
																											ShaderText+=" - Remove/cull the front-facing triangles only";
																										}
																									}
																								}
																								
																								//ZTest
																								ShaderText+="\n\t\t\tZTest "+ZTestName;			//Add ztest
																								if (Comments>2) {
																									ShaderText+="\t\t\t//Less | Greater | LEqual | GEqual | Equal | NotEqual | Always";	//Add per-line comment
																									if ((ZTestCount==0) || (ZTestCount==7)) {
																										ShaderText+=" - Z-Buffer/Depth testing is off";
																									} else {
																										ShaderText+=" - Pixels will only be allowed to continue through the rendering pipeline if the Z coordinate of their position is "+ZTestName+" the existing Z coordinate in the Z/Depth buffer";
																									}
																								}
																								
																								//ZWrite
																								ShaderText+="\n\t\t\tZWrite "+ZWriteName;		//Add zwrite
																								if (Comments>2) {
																									ShaderText+="\t\t\t//On | Off";				//Add per-line comment
																									if (ZTestCount==0) {
																										ShaderText+=" - Z coordinates from pixel positions will not be written to the Z/Depth buffer";
																									} else {
																										ShaderText+=" - Z coordinates from pixel positions will be written to the Z/Depth buffer";
																									}
																								}
																								
																								//Alpha test
																								ShaderText+="\n\t\t\t";
																								if (HasTest==true) {
																									//Has alpha test
																									ShaderText+="AlphaTest Greater [_AlphaTestThreshold]";	//Add alpha test
																									if (Comments>2) {
																										ShaderText+="\t//Off | Less | Greater | LEqual | GEqual | Equal | NotEqual | Always   (also: 0.0 (float value) | [_AlphaTestThreshold]) - Pixels will only be allowed to continue through the rendering pipeline if their alpha values (0..1) are greater than the value of _AlphaTestThreshold";	//Add per-line comment
																									}
																								} else {
																									//Does not alpha test
																									ShaderText+="AlphaTest Off";
																									if (Comments>2) {
																										ShaderText+=" //0.0\t//Less | Greater | LEqual | GEqual | Equal | NotEqual | Always   (also 0.0 (float value) | [_AlphaTestThreshold]) - All pixels will continue through the graphics pipeline because alpha testing is Off";	//Add per-line comment
																									}
																								}
																								
																								//Lighting
																								ShaderText+="\n\t\t\tLighting Off";				//Add lighting
																								if (Comments>2) {
																									ShaderText+="\t\t\t//On | Off - Lighting will not be calculated or applied";			//Add per-line comment
																								}
																																	
																								//Color mask
																								ShaderText+="\n\t\t\tColorMask "+MaskName;		//Add color mask
																								if (Comments>2) {
																									ShaderText+="\t\t//RGBA | RGB | A | 0 | any combination of R, G, B, A - Color channels allowed to be modified in the backbuffer are: "+MaskName;	//Add per-line comment
																								}
																								
																								//BlendOp
																								BlendOpName="";
																								if (HasBlendOp==true) {
																									//Need to use blend op
																									if (BlendModeCount==12) {
																										//Reverse Sub
																										ShaderText+="\n\t\t\tBlendOp RevSub";				//Add RevSub blend op
																										BlendOpName="Reverse Subtract";
																									} else {
																										if (BlendModeCount==15) {
																											//Min
																											ShaderText+="\n\t\t\tBlendOp Min";			//Add Min blend op
																											BlendOpName="Minimum";
																										} else {
																											if (BlendModeCount==16) {
																												//Max
																												ShaderText+="\n\t\t\tBlendOp Max";		//Add Max blend op
																												BlendOpName="Maximum";
																											} else {
																												//Sub
																												ShaderText+="\n\t\t\tBlendOp Sub";		//Add Sub blend op
																												BlendOpName="Subtract";
																											}
																										}
																									}
																									if (Comments>2) {
																										ShaderText+="\t\t//Min | Max | Sub | RevSub - BlendOp is set to combine source and destination parts of the blend mode using a "+BlendOpName+" operation";	//Add per-line comment
																									}
																								} else {
																									//Does not use blend op
																									if (Comments>2) {
																										ShaderText+="\n\t\t\t//BlendOp\t//Add\t\t// Min | Max | Sub | RevSub - BlendOp is not being used and will default to an Add operation when combining the source and destination parts of the blend mode";	//Add dummy blendop and per-line comment
																									}
																								}
																								
																								//Blend mode
																								ShaderText+="\n\t\t\tBlend "+BlendModeTags[BlendModeCount];	//Add blend mode
																								if (Comments>2) {
																									ShaderText+="\t\t\t//SrcFactor DstFactor (also:, SrcFactorA DstFactorA) = One | Zero | SrcColor | SrcAlpha | DstColor | DstAlpha | OneMinusSrcColor | OneMinusSrcAlpha | OneMinusDstColor | OneMinusDstAlpha - Blending between shader output and the backbuffer will use blend mode '"+BlendName+"'";	//Add per-line comment
																								}
																								if (Comments==4) {
																									//Add verbose comments
																									ShaderText+="\n\t\t\t\t\t\t\t\t//Blend SrcAlpha OneMinusSrcAlpha     = Alpha blending";
																									ShaderText+="\n\t\t\t\t\t\t\t\t//Blend One One                       = Additive";
																									ShaderText+="\n\t\t\t\t\t\t\t\t//Blend OneMinusDstColor One          = Soft Additive";
																									ShaderText+="\n\t\t\t\t\t\t\t\t//Blend DstColor Zero                 = Multiplicative";
																									ShaderText+="\n\t\t\t\t\t\t\t\t//Blend DstColor SrcColor             = 2x Multiplicative";
																								}

if (target.ShaderFunctionPreset==5) {
	//Generate multitexture shade code based on number of layers and selected merges
	
	//MultiTexture Function
	if (Comments>2) {
		CGCode[5]="\n\n\t\t\tCGPROGRAM\t\t\t\t\t\t//Start a program in the CG language";
		if (target.ShaderModelPreset==0) {
			//Shader model 2.0
			CGCode[5]+="\n\t\t\t#pragma target 2.0\t\t\t\t//Run this shader on at least Shader Model 2.0 hardware (e.g. Direct3D 9)";
		} else {
			//Shader model 3.0
			CGCode[5]+="\n\t\t\t#pragma target 3.0\t\t\t\t//Run this shader on at least Shader Model 3.0 hardware (e.g. Direct3D 9)";
		}
		CGCode[5]+="\n\t\t\t#pragma fragment frag\t\t\t//The fragment shader is named 'frag'";
		CGCode[5]+="\n\t\t\t#pragma vertex vert	\t\t\t//The vertex shader is named 'vert'";
		CGCode[5]+="\n\t\t\t#include \"UnityCG.cginc\"\t\t//Include Unity's predefined inputs and macros";
	} else {
		CGCode[5]="\n\n\t\t\tCGPROGRAM";
		if (target.ShaderModelPreset==0) {
			//Shader model 2.0
			CGCode[5]+="\n\t\t\t#pragma target 2.0";
		} else {
			//Shader model 3.0
			CGCode[5]+="\n\t\t\t#pragma target 3.0";
		}
		CGCode[5]+="\n\t\t\t#pragma fragment frag";
		CGCode[5]+="\n\t\t\t#pragma vertex vert";
		CGCode[5]+="\n\t\t\t#include \"UnityCG.cginc\"";		
	}
	CGCode[5]+="\n";
	if (Comments>1) {CGCode[5]+="\n\t\t\t//Unity variables to be made accessible to Vertex and/or Fragment shader";}
	CGCode[5]+="\n\t\t\tuniform sampler2D _Texture0;";
	if (Comments>2) {CGCode[5]+="\t\t\t\t\t//Define _Texture0 from Texture Unit 0 to be sampled in 2D";}
	CGCode[5]+="\n\t\t\tuniform float4 _Texture0_ST;";
	if (Comments>2) {CGCode[5]+="\t\t\t\t\t//Use the Float _Texture0_ST to pass the Offset and Tiling for the texture(s)";}
	for (Temp=1;Temp<target.NumberOfTexturesPreset+2;Temp++){
		CGCode[5]+="\n\t\t\tuniform sampler2D _Texture"+Temp+";";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t//Define _Texture"+Temp+" from Texture Unit "+Temp+" to be sampled in 2D";}
		CGCode[5]+="\n\t\t\tuniform float4 _Texture"+Temp+"_ST;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t//Use the Float _Texture"+Temp+"_ST to pass the Offset and Tiling for the texture(s)";}
	}
	if (target.ShaderFunctionPreset==5){
		//Multitexture
		var Index3:int;
		for (Index3=1; Index3<target.NumberOfTexturesPreset+2; Index3++) {
			//Select which merge to use and implement the variable for _AlphaMask and _AlphaFade
			switch(MergeCount[Index3]){
				case 13:
					//AlphaMask merge
					CGCode[5]+="\n\t\t\tuniform float _AlphaMask"+Index3+";";	//Append AlphaMask property
					if (Comments>1) {
						CGCode[5]+="\t\t\t\t\t\t//Define _AlphaMask"+Index3+" to pass the AlphaMask"+Index3+" float for adjusting AlphaMask merge mode between textures";	//Append per-line comment
					}
		    	 	break;
		        case 14:
		        	//AlphaFade merge
					CGCode[5]+="\n\t\t\tuniform float _AlphaFade"+Index3+";";	//Append AlphaFade property
					if (Comments>1) {
						CGCode[5]+="\t\t\t\t\t\t//Define _AlphaFade"+Index3+" to pass the AlphaFade"+Index3+" float for adjusting AlphaFade merge mode between textures";	//Append per-line comment
					}
					break;
			}
		}
	}

//TODO: Implement similar loop to at end of properties, to define floats for _AlphaMask0-15, and _AlphaBlend0-15
		CGCode[5]+="\n";
		if (Comments>1) {CGCode[5]+="\n\t\t\t//Data structure communication from Unity to the vertex shader";}
		if (Comments>1) {CGCode[5]+="\n\t\t\t//Defines what inputs the vertex shader accepts";}
		CGCode[5]+="\n\t\t\tstruct AppData {";
		CGCode[5]+="\n\t\t\t\tfloat4 vertex : POSITION;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t//Receive vertex position";}
		CGCode[5]+="\n\t\t\t\thalf2 texcoord : TEXCOORD0;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t//Receive texture coordinates";}
		if (Comments>3) {
			CGCode[5]+="\n\t\t\t\t\t\t\t//half2 texcoord1 : TEXCOORD1;\t\t\t\t//Receive texture coordinates";
			CGCode[5]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Receive vertex colors";
		}
		CGCode[5]+="\n\t\t\t};";
		CGCode[5]+="\n";
		if (Comments>1) {CGCode[5]+="\n\t\t\t//Data structure for communication from vertex shader to fragment shader";}
		if (Comments>1) {CGCode[5]+="\n\t\t\t//Defines what inputs the fragment shader accepts";}
		CGCode[5]+="\n\t\t\tstruct VertexToFragment {";
		CGCode[5]+="\n\t\t\t\tfloat4 pos : POSITION;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Send fragment position to fragment shader";}
		CGCode[5]+="\n\t\t\t\thalf2 uv : TEXCOORD0;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";}
		if (Comments>3) {
			CGCode[5]+="\n\t\t\t\t\t\t\t//half2 uv2 : TEXCOORD1;\t\t\t\t\t//Send interpolated texture coordinate to fragment shader";
			CGCode[5]+="\n\t\t\t\t\t\t\t//fixed4 color : COLOR;\t\t\t\t\t\t//Send interpolated gouraud-shaded vertex color to fragment shader";
		}
		CGCode[5]+="\n\t\t\t};";
		CGCode[5]+="\n";
		if (Comments>1) {CGCode[5]+="\n\t\t\t//Vertex shader";}
		CGCode[5]+="\n\t\t\tVertexToFragment vert(AppData v) {";
		CGCode[5]+="\n\t\t\t\tVertexToFragment o;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t//Create a data structure to pass to fragment shader";}
		CGCode[5]+="\n\t\t\t\to.pos = mul(UNITY_MATRIX_MVP,v.vertex);";
		if (Comments>2) {CGCode[5]+="\t\t//Include influence of Modelview + Projection matrices";}
		CGCode[5]+="\n\t\t\t\to.uv = v.texcoord.xy;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Send texture coords from unit 0 to fragment shader";}
		if (Comments>3) {
			CGCode[5]+="\n\t\t\t\t\t\t\t//o.uv2 = v.texcoord1.xy;\t\t\t\t\t//Send texture coords from unit 1 to fragment shader";
			CGCode[5]+="\n\t\t\t\t\t\t\t//o.color = v.color;\t\t\t\t\t\t//Send interpolated vertex color to fragment shader";
			CGCode[5]+="\n\t\t\t\t\t\t\t//o.color = _Color;\t\t\t\t\t\t\t//Send solid color to fragment shader";
		}
		CGCode[5]+="\n\t\t\t\treturn o;";
		if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t\t\t//Transmit data to the fragment shader";}
		CGCode[5]+="\n\t\t\t}";
		CGCode[5]+="\n";   
		if (Comments>1) {CGCode[5]+="\n\t\t\t//Fragment shader";}
		CGCode[5]+="\n\t\t\tfixed4 frag(VertexToFragment i) : COLOR {";
		
	//Customize the shader based on number of layers and their merges
	if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Multitexturing Shader - Start with Layer 0";}
	CGCode[5]+="\n\t\t\t\tfixed4 Result = tex2D(_Texture0, i.uv);";
	if (Comments>2) {CGCode[5]+="\t\t//Start with Texture Layer 0 as-is";}
	CGCode[5]+="\n\t\t\t\tfixed4 Pixel;";
	if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t\t//Temporary pixel from texture layer";}
	
	var Index:int;
	var VAlpha:boolean=false;
	var VIAlpha:boolean=false;
	for (Index=1; Index<target.NumberOfTexturesPreset+2; Index++) {
		//Select which merge to use and implement the code
		switch(MergeCount[Index]){
			case 0:
				//Alpha Blend
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Alpha Blend Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				if (VAlpha==true) {
					CGCode[5]+="\n\t\t\t\tAlpha=Pixel.a;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t\t//Get the alpha value of the pixel";}
				} else {
					CGCode[5]+="\n\t\t\t\tfixed Alpha=Pixel.a;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Get the alpha value of the pixel";}
					VAlpha=true;
				}
				if (VIAlpha==true) {
					CGCode[5]+="\n\t\t\t\tIAlpha=1.0-Alpha;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t//Calculate the inverse of the alpha value";}
				} else {
					CGCode[5]+="\n\t\t\t\tfixed IAlpha=1.0-Alpha;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Calculate the inverse of the alpha value";}
					VIAlpha=true;
				}
				CGCode[5]+="\n\t\t\t\tResult=fixed4(Result.r*IAlpha, Result.g*IAlpha, Result.b*IAlpha, Result.a) + fixed4(Pixel.r*Alpha, Pixel.g*Alpha, Pixel.b*Alpha,0);";
				if (Comments>2) {CGCode[5]+="\t//Alphablend this texture with the previous result";}
				break;
			case 1:
				//Alpha Blend Add
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Alpha Blend Add Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				if (VAlpha==true) {
					CGCode[5]+="\n\t\t\t\tAlpha=Pixel.a;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t\t//Get the alpha value of the pixel";}
				} else {
					CGCode[5]+="\n\t\t\t\tfixed Alpha=Pixel.a;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Get the alpha value of the pixel";}
					VAlpha=true;
				}
				CGCode[5]+="\n\t\t\t\tResult += fixed4(Pixel.r*Alpha, Pixel.g*Alpha, Pixel.b*Alpha,0);";
				if (Comments>2) {CGCode[5]+="\t//Alphablend Add this texture to the previous result";}
				break;
			case 2:
				//Alpha Blend Subtract
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Alpha Blend Subtract Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				if (VAlpha==true) {
					CGCode[5]+="\n\t\t\t\tAlpha=Pixel.a;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t\t//Get the alpha value of the pixel";}
				} else {
					CGCode[5]+="\n\t\t\t\tfixed Alpha=Pixel.a;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Get the alpha value of the pixel";}
					VAlpha=true;
				}
				CGCode[5]+="\n\t\t\t\tResult -= fixed4(Pixel.r*Alpha, Pixel.g*Alpha, Pixel.b*Alpha,0);";
				if (Comments>2) {CGCode[5]+="\t//Alphablend Subtract this texture from the previous result";}
				break;
			case 3:
				//Add
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Add Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult += fixed4(Pixel.r, Pixel.g, Pixel.b,0);";
				if (Comments>2) {CGCode[5]+="\t//Add this texture to the previous result";}
				break;
			case 4:
				//Add Soft
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Add Soft Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult += fixed4((1.0-Result.r)*Pixel.r, (1.0-Result.g)*Pixel.g, (1.0-Result.b)*Pixel.b,0);";
				if (Comments>2) {CGCode[5]+="\t//Add this texture Softly to the previous result";}
				break;
			case 5:
				//Subtract
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Subtract Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult -= fixed4(Pixel.r, Pixel.g, Pixel.b,0);";
				if (Comments>2) {CGCode[5]+="\t//Subtract this texture from the previous result";}
				break;
			case 6:
				//Subtract Soft
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Subtract Soft Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult -= fixed4((1.0-Result.r)*Pixel.r, (1.0-Result.g)*Pixel.g, (1.0-Result.b)*Pixel.b,0);";
				if (Comments>2) {CGCode[5]+="\t//Subtract this texture Softly from the previous result";}
				break;
			case 7:
				//Multipy
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Multiply Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult *= fixed4(Pixel.r, Pixel.g, Pixel.b,1);";
				if (Comments>2) {CGCode[5]+="\t//Multiply this texture by the previous result";}
				break;
			case 8:
				//Multiply Double
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Multiply Double Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = (Result * fixed4(Pixel.r, Pixel.g, Pixel.b,1)) * fixed4(2,2,2,1);";
				if (Comments>2) {CGCode[5]+="\t//Multiply this texture twice by the previous result";}
				break;
			case 9:
				//Divide
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Divide Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult /= fixed4(Pixel.r, Pixel.g, Pixel.b,1);";
				if (Comments>2) {CGCode[5]+="\t//Divide this texture into the previous result";}
				break;
			case 10:
				//Min
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Minimum Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = min(Result, fixed4(Pixel.r, Pixel.g, Pixel.b,1));";
				if (Comments>2) {CGCode[5]+="\t//Find the Minimum of this texture and the previous result";}
				break;
			case 11:
				//Max
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Maximum Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = max(Result, fixed4(Pixel.r, Pixel.g, Pixel.b,0));";
				if (Comments>2) {CGCode[5]+="\t//Find the Minimum of this texture and the previous result";}
				break;
			case 12:
				//Screen
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Screen Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = fixed4(1.0-((1.0-Result.r)*(1.0-Pixel.r)), 1.0-((1.0-Result.g)*(1.0-Pixel.g)), 1.0-((1.0-Result.b)*(1.0-Pixel.b)), Result.a);";
				if (Comments>2) {CGCode[5]+="\t//Screen this texture with the previous result";}
				break;
			case 13:
				//Alpha Mask
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Alpha Mask Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tif (Pixel.a<=_AlphaMask"+Index+"){";
				if (Comments>2) {CGCode[5]+="\t\t\t\t\t//Only allow pixels through if texture alpha is <= _AlphaMask"+Index+" threshold";}
				CGCode[5]+="\n\t\t\t\t\tResult=fixed4(Pixel.r, Pixel.g, Pixel.b, Result.a);";
				if (Comments>2) {CGCode[5]+="\t//Copy RGB pixel to result";}
				CGCode[5]+="\n\t\t\t\t}";
				break;
			case 14:
				//Alpha Fade
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Alpha Fade Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				if (VAlpha==true) {
					CGCode[5]+="\n\t\t\t\tAlpha=min(1,max(0,(Pixel.a-1.0)+_AlphaFade"+Index+"));";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t\t//Get the alpha value of the pixel and fade it based on the _AlphaFade"+Index+" threshold";}
				} else {
					CGCode[5]+="\n\t\t\t\tfixed Alpha=min(1,max(0,(Pixel.a-1.0)+_AlphaFade"+Index+"));";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Get the alpha value of the pixel and fade it based on the _AlphaFade"+Index+" threshold";}
					VAlpha=true;
				}
				if (VIAlpha==true) {
					CGCode[5]+="\n\t\t\t\tIAlpha=1.0-Alpha;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t//Calculate the inverse of the alpha value";}
				} else {
					CGCode[5]+="\n\t\t\t\tfixed IAlpha=1.0-Alpha;";
					if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t//Calculate the inverse of the alpha value";}
					VIAlpha=true;
				}
				CGCode[5]+="\n\t\t\t\tResult=fixed4(Result.r*IAlpha, Result.g*IAlpha, Result.b*IAlpha, Result.a) + fixed4(Pixel.r*Alpha, Pixel.g*Alpha, Pixel.b*Alpha,0);";
				if (Comments>2) {CGCode[5]+="\t//AlphaBlend RGB pixel with result";}
				break;
			case 15:
				//Difference
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Difference Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = fixed4(abs(Result.r-Pixel.r), abs(Result.g-Pixel.g), abs(Result.b-Pixel.b), Result.a);";
				if (Comments>2) {CGCode[5]+="\t//Find the Difference of this texture from the previous result";}
				break;
			case 16:
				//Negative.
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Negative Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = fixed4(1.0-abs(1.0-Result.r-Pixel.r), 1.0-abs(1.0-Result.g-Pixel.g), 1.0-abs(1.0-Result.b-Pixel.b), Result.a);";
				if (Comments>2) {CGCode[5]+="\t//Find the Negation of this texture from the previous result";}
				break;
			case 17:
				//Exclusion
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Exclude Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = fixed4(0.5-2*(Result.r-0.5)*(Pixel.r-0.5), 0.5-2*(Result.g-0.5)*(Pixel.g-0.5), 0.5-2*(Result.b-0.5)*(Pixel.b-0.5), Result.a);";
				if (Comments>2) {CGCode[5]+="\t//Find the Exclusion of this texture from the previous result";}
				break;
			case 18:
				//Overlay
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Overlay Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tif (Result.r>0.5) {Result.r = 1-(1-2*(Result.r-0.5))*(1-Pixel.r);}";
				if (Comments>2) {CGCode[5]+="\t\t//Calculate Overlay of red, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.r = (2*Result.r)*Pixel.r;}";
				if (Comments>2) {CGCode[5]+="\t\t//Calculate Overlay of red, second half";}
				CGCode[5]+="\n\t\t\t\tif (Result.g>0.5) {Result.g = 1-(1-2*(Result.g-0.5))*(1-Pixel.g);}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Overlay of green, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.g = (2*Result.g)*Pixel.g;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Overlay of green, second half";}
				CGCode[5]+="\n\t\t\t\tif (Result.b>0.5) {Result.b = 1-(1-2*(Result.b-0.5))*(1-Pixel.b);}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Overlay of blue, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.b = (2*Result.b)*Pixel.b;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Overlay of blue, second half";}
				break;
			case 19:
				//Hard Light
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Hard Light Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tif (Pixel.r>0.5) {Result.r = 1-(1-Result.r)*(2*(1.0-Pixel.r));}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Hard Light of red, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.r = 2*Result.r*Pixel.r;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Hard Light of red, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.g>0.5) {Result.g = 1-(1-Result.g)*(2*(1.0-Pixel.g));}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Hard Light of green, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.g = 2*Result.g*Pixel.g;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Hard Light of green, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.b>0.5) {Result.b = 1-(1-Result.b)*(2*(1.0-Pixel.b));}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Hard Light of blue, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.b = 2*Result.b*Pixel.b;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Hard Light of blue, second half";}
				break;
			case 20:
				//Soft Light
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Soft Light Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tif (Pixel.r>0.5) {Result.r = 2.0*sqrt(Result.r)*Pixel.r-sqrt(Result.r)+2.0*Result.r-2.0*Result.r*Pixel.r;}";
				//CGCode[5]+="\n\t\t\t\tif (Pixel.r>0.5) {Result.r = Result.r*(1-(1-Result.r)*(1-2*(Pixel.r)));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Soft Light of red, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.r = 2.0*Result.r*Pixel.r+Result.r*Pixel.r-2.0*Result.r*Result.r*Pixel.r;}";
				//CGCode[5]+="\n\t\t\t\telse {Result.r = 1-(1-Result.r)*(1-(Result.r*(2*Pixel.r)));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Soft Light of red, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.g>0.5) {Result.g = 2.0*sqrt(Result.g)*Pixel.g-sqrt(Result.g)+2.0*Result.g-2.0*Result.g*Pixel.g;}";
				//CGCode[5]+="\n\t\t\t\tif (Pixel.g>0.5) {Result.g = Result.g*(1-(1-Result.g)*(1-2*(Pixel.g)));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Soft Light of green, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.g = 2.0*Result.g*Pixel.g+Result.g*Pixel.g-2.0*Result.g*Result.g*Pixel.g;}";
				//CGCode[5]+="\n\t\t\t\telse {Result.g = 1-(1-Result.g)*(1-(Result.g*(2*Pixel.g)));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Soft Light of green, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.b>0.5) {Result.b = 2.0*sqrt(Result.b)*Pixel.b-sqrt(Result.b)+2.0*Result.b-2.0*Result.b*Pixel.b;}";
				//CGCode[5]+="\n\t\t\t\tif (Pixel.b>0.5) {Result.b = Result.b*(1-(1-Result.b)*(1-2*(Pixel.b)));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Soft Light of blue, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.b = 2.0*Result.b*Pixel.b+Result.b*Pixel.b-2.0*Result.b*Result.b*Pixel.b;}";
				//CGCode[5]+="\n\t\t\t\telse {Result.b = 1-(1-Result.b)*(1-(Result.b*(2*Pixel.b)));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Soft Light of blue, second half";}
				break;
			case 21:
				//Color Dodge
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Color Dodge Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult /= fixed4(1.0-Pixel.r, 1.0-Pixel.g, 1.0-Pixel.b, 1.0);";
				if (Comments>2) {CGCode[5]+="\t//Color Dodge this texture with the previous result";}
				break;
			case 22:
				//Linear Dodge (same as Add)
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Linear Dodge Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult += fixed4(Pixel.r, Pixel.g, Pixel.b, 0);";
				if (Comments>2) {CGCode[5]+="\t//Linear Dodge this texture with the previous result";}
				break;
			case 23:
				//Color Burn
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Color Burn Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = fixed4(1.0-(1.0-Result.r)/Pixel.r, 1.0-(1.0-Result.g)/Pixel.g, 1.0-(1.0-Result.b)/Pixel.b, Result.a);";
				if (Comments>2) {CGCode[5]+="\t//Color Burn this texture with the previous result";}
				break;
			case 24:
				//Linear Burn
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Linear Burn Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tResult = fixed4(Result.r+Pixel.r-1.0, Result.g+Pixel.g-1.0, Result.b+Pixel.b-1.0, Result.a);";
				if (Comments>2) {CGCode[5]+="\t//Linear Burn this texture with the previous result";}
				break;
			case 25:
				//Vivid Light
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Vivid Light Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tif (Pixel.r>0.5) {Result.r = 1-(1-Result.r)/(2*(Pixel.r-0.5));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Vivid Light of red, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.r = Result.r/(1-2*Pixel.r);}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Vivid Light of red, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.g>0.5) {Result.g = 1-(1-Result.g)/(2*(Pixel.g-0.5));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Vivid Light of green, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.g = Result.g/(1-2*Pixel.g);}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Vivid Light of green, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.b>0.5) {Result.b = 1-(1-Result.b)/(2*(Pixel.b-0.5));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Vivid Light of blue, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.b = Result.b/(1-2*Pixel.b);}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Vivid Light of blue, second half";}
				break;
			case 26:
				//Linear Light
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Linear Light Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tif (Pixel.r>0.5) {Result.r = Result.r+2*(Pixel.r-0.5);}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Linear Light of red, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.r = Result.r+2*Pixel.r-1;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Linear Light of red, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.g>0.5) {Result.g = Result.g+2*(Pixel.g-0.5);}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Linear Light of green, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.g = Result.g+2*Pixel.g-1;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Linear Light of green, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.b>0.5) {Result.b = Result.b+2*(Pixel.b-0.5);}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Linear Light of blue, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.b = Result.b+2*Pixel.b-1;}";
    			if (Comments>2) {CGCode[5]+="\t\t//Calculate Linear Light of blue, second half";}
				break;
			case 27:
				//Pin Light
				if (Comments>3) {CGCode[5]+="\n\t\t\t\t//Pin Light Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tPixel=tex2D(_Texture"+Index+", i.uv);";
				if (Comments>2) {CGCode[5]+="\t\t\t\t//Read pixel from Layer "+Index;}
				CGCode[5]+="\n\t\t\t\tif (Pixel.r>0.5) {Result.r = max(Result.r, 2*(Pixel.r-0.5));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Pin Light of red, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.r = min(Result.r, 2*Pixel.r);}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Pin Light of red, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.g>0.5) {Result.g = max(Result.g, 2*(Pixel.g-0.5));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Pin Light of green, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.g = min(Result.g, 2*Pixel.g);}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Pin Light of green, second half";}
				CGCode[5]+="\n\t\t\t\tif (Pixel.b>0.5) {Result.b = max(Result.b, 2*(Pixel.b-0.5));}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Pin Light of blue, first half";}
				CGCode[5]+="\n\t\t\t\telse {Result.b = min(Result.b, 2*Pixel.b);}";
    			if (Comments>2) {CGCode[5]+="\t//Calculate Pin Light of blue, second half";}
				break;
		}
	}
	
    CGCode[5]+="\n\t\t\t\treturn Result;";
    if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t\t//Return the final result";}
}

																								//Finish multitexture shaders
 																								if (target.ShaderFunctionPreset==5) {
																									//MultiTexture shader, finish it
																									CGCode[5]+="\n\t\t\t}";
																							    	CGCode[5]+="\n";
																									CGCode[5]+="\n\t\t\tENDCG";
																									if (Comments>2) {CGCode[5]+="\t\t\t\t\t\t\t//End of CG program";}
																									CGCode[5]+="\n";
																								}
																								
																								//Implement texture tiling and offset
																								if (HasTextures==true) {
																									if (ScaleTranCount==1) {
																										//Include Tiling/Offset in texture coords
																										CGCode[FunctionCount]=(CGCode[FunctionCount].Replace("o.uv = v.texcoord.xy;","o.uv = TRANSFORM_TEX(v.texcoord,_Texture0);").Replace("\t\t\t\t\t\t//Send texture coords from unit 0 to fragment shader","//Send texture coords from unit 0 to fragment shader"));	//Change the texture coord handling to receive influence from Tiling/Offset from Unity
																									}
																								}
																								
																								//Output CG code
																								ShaderText+=CGCode[FunctionCount];					//Add CG code program
																								
																								//Finish the pass, subshader and shader
																								ShaderText+="\n\t\t}";
																								ShaderText+="\n\t}";
																								ShaderText+="\n}";
																								
																								//Add copyright notice
																								ShaderText+="\n\n//This shader was generated by ShaderWizard and is Copyright (c) 2012 Paul West/Venus12 LLC\n";
																								
																								//Shader is done being generated, output to a file
																								
																								//Calculate folder path
																								FilePath=Application.dataPath;						//Main assets folder
																								if (target.OutputPath != ""){
																									FilePath+="/"+target.OutputPath;				//Add custom relative output path
																								}
																								FilePath+="/";
																								if (target.FolderModePreset==0) {
																									//Folders by function
																									SubPath=FunctionName;				//Sub folders by function
																									if (target.ShaderCountingPreset==0) {
																										//Adding counters, so add a counter of the function - it's index
																										CounterText2=FunctionCount.ToString();
																										if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																										SubPath=CounterText2+"-"+SubPath;	//Append counter to start of name
																									}
																								} else {
																									if (target.FolderModePreset==1) {
																										//Folders by blend
																										SubPath=BlendName;				//Sub folders by blend mode
																										if (target.ShaderCountingPreset==0) {
																											//Adding counters, so add a counter of the blend mode - it's index
																											CounterText2=BlendModeCount.ToString();
																											if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																											SubPath=CounterText2+"-"+SubPath;	//Append counter to start of name
																										}
																									} else {
																										if (target.FolderModePreset==2) {
																											//Folders by function then by blend
																											SubPath=FunctionName;			//Sub folders by function
																											if (target.ShaderCountingPreset==0) {
																												//Adding counters, so add a counter of the function - it's index
																												CounterText2=FunctionCount.ToString();
																												if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																												SubPath=CounterText2+"-"+SubPath;	//Append counter to start of name
																											}
																											if (target.ShaderCountingPreset==0) {
																												//Adding counters, so add a counter of the blend mode - it's index
																												CounterText2=BlendModeCount.ToString();
																												if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																												SubPath+="/"+CounterText2+"-"+BlendName+"/";	//Append counter to start of name
																											} else {
																												//Not adding counters
																												SubPath+="/"+BlendName+"/";			//Sub folders by blend
																											}
																										} else {
																											//Flat
																											SubPath="";						//No sub folders
																										}
																									}
																								}
																								
																								//Create folders
																								System.IO.Directory.CreateDirectory(FilePath+"Resources");	//Create the Resources folder here if none
																								System.IO.Directory.CreateDirectory(FilePath+"Resources/"+SubPath);	//Create the Resources folder here if none
																								if (SubPath != "") {SubPath+="/";}	//End with /
																								
																								//Write the shader
																								System.IO.File.WriteAllText(FilePath+"Resources/"+SubPath+ShaderName+".shader", ShaderText);	//Save it!
																								if (Counter==0) {
																									//It's the first object, store it for hiliting later
																									FirstShaderObject=SubPath+ShaderName;		//Generate path for Resources.Load
																								}
																								//if (target.MaterialPreset==0) {
																								//	//Creating materials so must refresh the database
																								//	AssetDatabase.Refresh();					//Update changes to the Assets folder - causes Unity to `import`/compile the shader and material - need to do this in order for Resources.Load to work
																								//	//AssetDatabase.ImportAsset("Assets/"+FilePath.Replace(Application.dataPath+"/","")+"Resources/"+SubPath+ShaderName, ImportAssetOptions.ForceUpdate);	//Finish asset import
																								//}
																								
																								//Show Progress
																								Progress=0.5*((1.0/BatchTotal) * ((Counter-target.CounterOffset)+1));	//First half only
																								if (EditorUtility.DisplayCancelableProgressBar("Shader Wizard - Generating Shaders...","Shader "+((Counter-target.CounterOffset)+1)+" of "+BatchTotal+" for "+ShaderName,Progress)) {	//Display progress bar
																									//User cancelled the operation, quit!
																									EditorUtility.ClearProgressBar();			//Remove the progress bar
																									AssetDatabase.Refresh();					//Do a final refresh
																									EditorUtility.DisplayDialog("Shader Wizard","Operation was cancelled - "+((Counter-target.CounterOffset)+1)+" shader(s) may have been generated","Thank You!");	//Display cancellation message
																									CurrentShader = Resources.Load(FirstShaderObject,Shader) as Shader;	//Load the first shader
																									if (CurrentShader) {EditorGUIUtility.PingObject(CurrentShader);}	//Hilite the first object yellow
																									return;										//And exit!
																								}
																								Counter+=1;										//Next shader


																							}	//End of generating by MultiTexture Merge Layer 15
																						
																						}	//End of generating by MultiTexture Merge Layer 14
																					
																					}	//End of generating by MultiTexture Merge Layer 13
																				
																				}	//End of generating by MultiTexture Merge Layer 12
																			
																			}	//End of generating by MultiTexture Merge Layer 11
																		
																		}	//End of generating by MultiTexture Merge Layer 10
																	
																	}	//End of generating by MultiTexture Merge Layer 9
																
																}	//End of generating by MultiTexture Merge Layer 8
															
															}	//End of generating by MultiTexture Merge Layer 7
														
														}	//End of generating by MultiTexture Merge Layer 6
													
													}	//End of generating by MultiTexture Merge Layer 5
												
												}	//End of generating by MultiTexture Merge Layer 4
											
											}	//End of generating by MultiTexture Merge Layer 3
									
										}	//End of generating by MultiTexture Merge Layer 2
									
									}	//End of generating by MultiTexture Merge Layer 1
									
								}	//End of generating by Cullings
							
							}	//End of generating by Masks	

						}	//End of generating by ZTests						
				
					}	//End of generating by ZWrites	

				}	//End of generating by Blend mode
				
			} //End of generating by Scale/Translate modes
			
		}	//End of generating by shader functions
		
		if (target.MaterialPreset != 0) {
			EditorUtility.ClearProgressBar();			//Remove the progress bar if we're not going to make materials also
		} else {
			//Generating materials, say we're compiling
			EditorUtility.DisplayCancelableProgressBar("Shader Wizard - Compiling and Importing...","Compiling and Importing "+BatchTotal+" Shaders...",0.5);	//Display progress bar		
		}
		AssetDatabase.Refresh();					//Refresh the project folder to show/import the generated assets
		if (target.MaterialPreset != 0) {
			//Materials are not being generated so show a completion notice
			var Complete:String=BatchTotal+" Shader";	//Shader message
			if (BatchTotal>1) {Complete+="s";}			//Plural
			if (BatchTotal==1) {
				Complete+=" has been generated!";			//Singular
			} else {
				Complete+=" have been generated!";			//Multiple
			}
			EditorUtility.DisplayDialog("Shader Wizard",Complete,"Thank You!");		//Display completion message
			CurrentShader = Resources.Load(FirstShaderObject,Shader) as Shader;	//Load the first shader
			if (CurrentShader) {EditorGUIUtility.PingObject(CurrentShader);}	//Hilite the first object yellow
		}
						
	}	//End of GenerateShaders() function
	


	function GenerateMaterials() {
		//Generate materials for the shaders that were generated

//TODO: UPDATE THIS FUNCTION WITH SUPPORT FOR MULTITEXTURING
		var Progress:float = 0.5;				//Progress made 0..1, start half way because shaders are done

		var FunctionName:String;				//Temporary name for a shader function
		var ScaleTranName:String;				//Temporary name for a scale/translate mode
		var BlendName:String;					//Temporary name for a blend mode
		var ZWriteName:String;					//Temporary name for a zwrite mode
		var ZTestName:String;					//Temporary name for a ztest mode
		var MaskName:String;					//Temporary name for a color mask mode
		var CullName:String;					//Temporary name for a culling mode
		
		var ShaderName:String;					//Name for the shader
		var FilePath:String;					//Location for the shader and material files
		var SubPath:String;						//Sub-location inside a resources folder within FilePath
		var Counter:int=target.CounterOffset;	//Shader number counter, start at offset
		var CounterText:String="";				//Text version of counter
		var CounterText2:String="";				//Text version of folder counter
		var HasTextures:boolean=false;			//Whether the shader uses a texture
		var CurrentShader:Shader;				//Current shader file
		var Mat:Material;						//Current material
	
		var MergeCount:int[] = new int[NumberOfTextureCounts+1];	//16 texture layer merges for multitexturing
		var MergeStart:int[] = new int[NumberOfTextureCounts+1];	//16 texture layer merges for multitexturing
		var MergeEnd:int[] = new int[NumberOfTextureCounts+1];		//16 texture layer merges for multitexturing

		//Prepare to iterate through shader functions
		var FunctionCount:int=0;				//Counter for iterating through functions
		var FunctionStart:int=0;				//Which function to start generating at
		var FunctionEnd:int=0;					//Which function to end generating at
		if (target.ShaderFunctionPreset==NumberOfFunctions-1) {
			//Generate all functions
			FunctionStart=0;					//Start at beginning
			FunctionEnd=NumberOfFunctions-2;	//Do them all
		} else {
			//Generate a specific function
			FunctionStart=target.ShaderFunctionPreset;	//Do just this one
			FunctionEnd=FunctionStart;			//Stop here
		}

		for (FunctionCount=FunctionStart; FunctionCount<=FunctionEnd; FunctionCount+=1) {
			//Generate shader(s) for each Function
			

			//Prepare to iterate through texture scale/translate modes
			var ScaleTranCount:int=0;				//Counter for iterating through texture scale/translate modes
			var ScaleTranStart:int=0;				//Which mode to start generating at
			var ScaleTranEnd:int=0;					//Which mode to end generating at
			if (target.ScaleTranslateTexturePreset[0]==NumberOfScaleTranslateTextures-1) {
				//Generate all Scale/Translate modes
				ScaleTranStart=0;					//Start at beginning
				ScaleTranEnd=NumberOfScaleTranslateTextures-2;	//Do them all
			} else {
				//Generate a specific Scale/Translate mode
				ScaleTranStart=target.ScaleTranslateTexturePreset[0];	//Do just this one
				ScaleTranEnd=ScaleTranStart;		//Stop here
			}
			for (ScaleTranCount=ScaleTranStart; ScaleTranCount<=ScaleTranEnd; ScaleTranCount+=1) {
				//Generate shader(s) for each Scale/Translate mode
				
		
				//Prepare to iterate through Blend modes
				var BlendModeCount:int=0;				//Counter for iterating through blend modes
				var BlendModeStart:int=0;				//Which mode to start generating at
				var BlendModeEnd:int=0;					//Which mode to end generating at
				if (target.BlendModePreset==NumberOfBlends-1) {
					//Generate all Blend modes
					BlendModeStart=0;					//Start at beginning
					BlendModeEnd=NumberOfBlends-2;		//Do them all
				} else {
					//Generate a specific Blend mode
					BlendModeStart=target.BlendModePreset;	//Do just this one
					BlendModeEnd=BlendModeStart;		//Stop here
				}
				for (BlendModeCount=BlendModeStart; BlendModeCount<=BlendModeEnd; BlendModeCount+=1) {
					//Generate shader(s) for each Blend mode
					
					
					//Prepare to iterate through ZWrite modes
					var ZWriteCount:int=0;					//Counter for iterating through ZWrite modes
					var ZWriteStart:int=0;					//Which mode to start generating at
					var ZWriteEnd:int=0;					//Which mode to end generating at
					if (target.ZWritePreset==NumberOfZWrites-1) {
						//Generate all ZWrite modes
						ZWriteStart=0;						//Start at beginning
						ZWriteEnd=NumberOfZWrites-2;			//Do them all
					} else {
						//Generate a specific ZWrite mode
						ZWriteStart=target.ZWritePreset;	//Do just this one
						ZWriteEnd=ZWriteStart;				//Stop here
					}
					for (ZWriteCount=ZWriteStart; ZWriteCount<=ZWriteEnd; ZWriteCount+=1) {
						//Generate shader(s) for each ZWrite mode
						
						
						//Prepare to iterate through ZTest modes
						var ZTestCount:int=0;					//Counter for iterating through ZTest modes
						var ZTestStart:int=0;					//Which mode to start generating at
						var ZTestEnd:int=0;						//Which mode to end generating at
						if (target.ZTestPreset==NumberOfZTests-1) {
							//Generate all ZTest modes
							ZTestStart=0;						//Start at beginning
							ZTestEnd=NumberOfZTests-2;			//Do them all
						} else {
							//Generate a specific ZTest mode
							ZTestStart=target.ZTestPreset;		//Do just this one
							ZTestEnd=ZTestStart;				//Stop here
						}
						for (ZTestCount=ZTestStart; ZTestCount<=ZTestEnd; ZTestCount+=1) {
							//Generate shader(s) for each ZTest mode
							
						
							//Prepare to iterate through Mask modes
							var MaskCount:int=0;					//Counter for iterating through Mask modes
							var MaskStart:int=0;					//Which mode to start generating at
							var MaskEnd:int=0;						//Which mode to end generating at
							if (target.ColorMaskPreset==NumberOfMasks-1) {
								//Generate all Mask modes
								MaskStart=0;						//Start at beginning
								MaskEnd=NumberOfMasks-2;			//Do them all
							} else {
								//Generate a specific Mask mode
								MaskStart=target.ColorMaskPreset;	//Do just this one
								MaskEnd=MaskStart;					//Stop here
							}
							for (MaskCount=MaskStart; MaskCount<=MaskEnd; MaskCount+=1) {
								//Generate shader(s) for each Mask mode
							
						
								//Prepare to iterate through Culling modes
								var CullingCount:int=0;					//Counter for iterating through Culling modes
								var CullingStart:int=0;					//Which mode to start generating at
								var CullingkEnd:int=0;					//Which mode to end generating at
								if (target.CullingPreset==NumberOfCullings-1) {
									//Generate all Culling modes
									CullingStart=0;						//Start at beginning
									CullingEnd=NumberOfCullings-2;		//Do them all
								} else {
									//Generate a specific Culling mode
									CullingStart=target.CullingPreset;	//Do just this one
									CullingEnd=CullingStart;			//Stop here
								}
								for (CullingCount=CullingStart; CullingCount<=CullingEnd; CullingCount+=1) {
									//Generate shader(s) for each Culling mode
									
							
									//Prepare to iterate through MultiTexture layer 1 merge modes
									MergeCount[1]=0;				//Counter for iterating through Merge modes
									MergeStart[1]=0;				//Which merge to start generating at
									MergeEnd[1]=0;					//Which merge to end generating at
									if (target.MergePreset[1]==NumberOfMerges-1) {
										//Generate all Merge 1 modes
										MergeStart[1]=0;						//Start at beginning
										MergeEnd[1]=NumberOfMerges-2;			//Do them all
									} else {
										//Generate a specific Merge mode
										MergeStart[1]=target.MergePreset[1];	//Do just this one
										MergeEnd[1]=MergeStart[1];				//Stop here
									}
									for (MergeCount[1]=MergeStart[1]; MergeCount[1]<=MergeEnd[1]; MergeCount[1]+=1) {
										//Generate shader(s) for each Merge mode
									

										//Prepare to iterate through MultiTexture layer 2 merge modes
										MergeCount[2]=0;				//Counter for iterating through Merge modes
										MergeStart[2]=0;				//Which merge to start generating at
										MergeEnd[2]=0;					//Which merge to end generating at
										if (target.MergePreset[2]==NumberOfMerges-1) {
											//Generate all Merge 2 modes
											MergeStart[2]=0;						//Start at beginning
											MergeEnd[2]=NumberOfMerges-2;			//Do them all
										} else {
											//Generate a specific Merge mode
											MergeStart[2]=target.MergePreset[2];	//Do just this one
											MergeEnd[2]=MergeStart[2];				//Stop here
										}
										for (MergeCount[2]=MergeStart[2]; MergeCount[2]<=MergeEnd[2]; MergeCount[2]+=1) {
											//Generate shader(s) for each Merge mode


											//Prepare to iterate through MultiTexture layer 3 merge modes
											MergeCount[3]=0;				//Counter for iterating through Merge modes
											MergeStart[3]=0;				//Which merge to start generating at
											MergeEnd[3]=0;					//Which merge to end generating at
											if (target.MergePreset[3]==NumberOfMerges-1) {
												//Generate all Merge 3 modes
												MergeStart[3]=0;						//Start at beginning
												MergeEnd[3]=NumberOfMerges-2;			//Do them all
											} else {
												//Generate a specific Merge mode
												MergeStart[3]=target.MergePreset[3];	//Do just this one
												MergeEnd[3]=MergeStart[3];				//Stop here
											}
											for (MergeCount[3]=MergeStart[3]; MergeCount[3]<=MergeEnd[3]; MergeCount[3]+=1) {
												//Generate shader(s) for each Merge mode


												//Prepare to iterate through MultiTexture layer 4 merge modes
												MergeCount[4]=0;				//Counter for iterating through Merge modes
												MergeStart[4]=0;				//Which merge to start generating at
												MergeEnd[4]=0;					//Which merge to end generating at
												if (target.MergePreset[4]==NumberOfMerges-1) {
													//Generate all Merge 4 modes
													MergeStart[4]=0;						//Start at beginning
													MergeEnd[4]=NumberOfMerges-2;			//Do them all
												} else {
													//Generate a specific Merge mode
													MergeStart[4]=target.MergePreset[4];	//Do just this one
													MergeEnd[4]=MergeStart[4];				//Stop here
												}
												for (MergeCount[4]=MergeStart[4]; MergeCount[4]<=MergeEnd[4]; MergeCount[4]+=1) {
													//Generate shader(s) for each Merge mode


													//Prepare to iterate through MultiTexture layer 5 merge modes
													MergeCount[5]=0;				//Counter for iterating through Merge modes
													MergeStart[5]=0;				//Which merge to start generating at
													MergeEnd[5]=0;					//Which merge to end generating at
													if (target.MergePreset[5]==NumberOfMerges-1) {
														//Generate all Merge 5 modes
														MergeStart[5]=0;						//Start at beginning
														MergeEnd[5]=NumberOfMerges-2;			//Do them all
													} else {
														//Generate a specific Merge mode
														MergeStart[5]=target.MergePreset[5];	//Do just this one
														MergeEnd[5]=MergeStart[5];				//Stop here
													}
													for (MergeCount[5]=MergeStart[5]; MergeCount[5]<=MergeEnd[5]; MergeCount[5]+=1) {
														//Generate shader(s) for each Merge mode


														//Prepare to iterate through MultiTexture layer 6 merge modes
														MergeCount[6]=0;				//Counter for iterating through Merge modes
														MergeStart[6]=0;				//Which merge to start generating at
														MergeEnd[6]=0;					//Which merge to end generating at
														if (target.MergePreset[6]==NumberOfMerges-1) {
															//Generate all Merge 6 modes
															MergeStart[6]=0;						//Start at beginning
															MergeEnd[6]=NumberOfMerges-2;			//Do them all
														} else {
															//Generate a specific Merge mode
															MergeStart[6]=target.MergePreset[6];	//Do just this one
															MergeEnd[6]=MergeStart[6];				//Stop here
														}
														for (MergeCount[6]=MergeStart[6]; MergeCount[6]<=MergeEnd[6]; MergeCount[6]+=1) {
															//Generate shader(s) for each Merge mode


															//Prepare to iterate through MultiTexture layer 7 merge modes
															MergeCount[7]=0;				//Counter for iterating through Merge modes
															MergeStart[7]=0;				//Which merge to start generating at
															MergeEnd[7]=0;					//Which merge to end generating at
															if (target.MergePreset[7]==NumberOfMerges-1) {
																//Generate all Merge 7 modes
																MergeStart[7]=0;						//Start at beginning
																MergeEnd[7]=NumberOfMerges-2;			//Do them all
															} else {
																//Generate a specific Merge mode
																MergeStart[7]=target.MergePreset[7];	//Do just this one
																MergeEnd[7]=MergeStart[7];				//Stop here
															}
															for (MergeCount[7]=MergeStart[7]; MergeCount[7]<=MergeEnd[7]; MergeCount[7]+=1) {
																//Generate shader(s) for each Merge mode


																//Prepare to iterate through MultiTexture layer 8 merge modes
																MergeCount[8]=0;				//Counter for iterating through Merge modes
																MergeStart[8]=0;				//Which merge to start generating at
																MergeEnd[8]=0;					//Which merge to end generating at
																if (target.MergePreset[8]==NumberOfMerges-1) {
																	//Generate all Merge 8 modes
																	MergeStart[8]=0;						//Start at beginning
																	MergeEnd[8]=NumberOfMerges-2;			//Do them all
																} else {
																	//Generate a specific Merge mode
																	MergeStart[8]=target.MergePreset[8];	//Do just this one
																	MergeEnd[8]=MergeStart[8];				//Stop here
																}
																for (MergeCount[8]=MergeStart[8]; MergeCount[8]<=MergeEnd[8]; MergeCount[8]+=1) {
																	//Generate shader(s) for each Merge mode


																	//Prepare to iterate through MultiTexture layer 9 merge modes
																	MergeCount[9]=0;				//Counter for iterating through Merge modes
																	MergeStart[9]=0;				//Which merge to start generating at
																	MergeEnd[9]=0;					//Which merge to end generating at
																	if (target.MergePreset[9]==NumberOfMerges-1) {
																		//Generate all Merge 9 modes
																		MergeStart[9]=0;						//Start at beginning
																		MergeEnd[9]=NumberOfMerges-2;			//Do them all
																	} else {
																		//Generate a specific Merge mode
																		MergeStart[9]=target.MergePreset[9];	//Do just this one
																		MergeEnd[9]=MergeStart[9];				//Stop here
																	}
																	for (MergeCount[9]=MergeStart[9]; MergeCount[9]<=MergeEnd[9]; MergeCount[9]+=1) {
																		//Generate shader(s) for each Merge mode


																		//Prepare to iterate through MultiTexture layer 10 merge modes
																		MergeCount[10]=0;				//Counter for iterating through Merge modes
																		MergeStart[10]=0;				//Which merge to start generating at
																		MergeEnd[10]=0;					//Which merge to end generating at
																		if (target.MergePreset[10]==NumberOfMerges-1) {
																			//Generate all Merge 10 modes
																			MergeStart[10]=0;						//Start at beginning
																			MergeEnd[10]=NumberOfMerges-2;			//Do them all
																		} else {
																			//Generate a specific Merge mode
																			MergeStart[10]=target.MergePreset[10];	//Do just this one
																			MergeEnd[10]=MergeStart[10];				//Stop here
																		}
																		for (MergeCount[10]=MergeStart[10]; MergeCount[10]<=MergeEnd[10]; MergeCount[10]+=1) {
																			//Generate shader(s) for each Merge mode


																			//Prepare to iterate through MultiTexture layer 11 merge modes
																			MergeCount[11]=0;				//Counter for iterating through Merge modes
																			MergeStart[11]=0;				//Which merge to start generating at
																			MergeEnd[11]=0;					//Which merge to end generating at
																			if (target.MergePreset[11]==NumberOfMerges-1) {
																				//Generate all Merge 11 modes
																				MergeStart[11]=0;						//Start at beginning
																				MergeEnd[11]=NumberOfMerges-2;			//Do them all
																			} else {
																				//Generate a specific Merge mode
																				MergeStart[11]=target.MergePreset[11];	//Do just this one
																				MergeEnd[11]=MergeStart[11];				//Stop here
																			}
																			for (MergeCount[11]=MergeStart[11]; MergeCount[11]<=MergeEnd[11]; MergeCount[11]+=1) {
																				//Generate shader(s) for each Merge mode


																				//Prepare to iterate through MultiTexture layer 12 merge modes
																				MergeCount[12]=0;				//Counter for iterating through Merge modes
																				MergeStart[12]=0;				//Which merge to start generating at
																				MergeEnd[12]=0;					//Which merge to end generating at
																				if (target.MergePreset[12]==NumberOfMerges-1) {
																					//Generate all Merge 12 modes
																					MergeStart[12]=0;						//Start at beginning
																					MergeEnd[12]=NumberOfMerges-2;			//Do them all
																				} else {
																					//Generate a specific Merge mode
																					MergeStart[12]=target.MergePreset[12];	//Do just this one
																					MergeEnd[12]=MergeStart[12];				//Stop here
																				}
																				for (MergeCount[12]=MergeStart[12]; MergeCount[12]<=MergeEnd[12]; MergeCount[12]+=1) {
																					//Generate shader(s) for each Merge mode


																					//Prepare to iterate through MultiTexture layer 13 merge modes
																					MergeCount[13]=0;				//Counter for iterating through Merge modes
																					MergeStart[13]=0;				//Which merge to start generating at
																					MergeEnd[13]=0;					//Which merge to end generating at
																					if (target.MergePreset[13]==NumberOfMerges-1) {
																						//Generate all Merge 13 modes
																						MergeStart[13]=0;						//Start at beginning
																						MergeEnd[13]=NumberOfMerges-2;			//Do them all
																					} else {
																						//Generate a specific Merge mode
																						MergeStart[13]=target.MergePreset[13];	//Do just this one
																						MergeEnd[13]=MergeStart[13];				//Stop here
																					}
																					for (MergeCount[13]=MergeStart[13]; MergeCount[13]<=MergeEnd[13]; MergeCount[13]+=1) {
																						//Generate shader(s) for each Merge mode


																						//Prepare to iterate through MultiTexture layer 14 merge modes
																						MergeCount[14]=0;				//Counter for iterating through Merge modes
																						MergeStart[14]=0;				//Which merge to start generating at
																						MergeEnd[14]=0;					//Which merge to end generating at
																						if (target.MergePreset[14]==NumberOfMerges-1) {
																							//Generate all Merge 14 modes
																							MergeStart[14]=0;						//Start at beginning
																							MergeEnd[14]=NumberOfMerges-2;			//Do them all
																						} else {
																							//Generate a specific Merge mode
																							MergeStart[14]=target.MergePreset[14];	//Do just this one
																							MergeEnd[14]=MergeStart[14];				//Stop here
																						}
																						for (MergeCount[14]=MergeStart[14]; MergeCount[14]<=MergeEnd[14]; MergeCount[14]+=1) {
																							//Generate shader(s) for each Merge mode


																							//Prepare to iterate through MultiTexture layer 15 merge modes
																							MergeCount[15]=0;				//Counter for iterating through Merge modes
																							MergeStart[15]=0;				//Which merge to start generating at
																							MergeEnd[15]=0;					//Which merge to end generating at
																							if (target.MergePreset[15]==NumberOfMerges-1) {
																								//Generate all Merge 15 modes
																								MergeStart[15]=0;						//Start at beginning
																								MergeEnd[15]=NumberOfMerges-2;			//Do them all
																							} else {
																								//Generate a specific Merge mode
																								MergeStart[15]=target.MergePreset[15];	//Do just this one
																								MergeEnd[15]=MergeStart[15];				//Stop here
																							}
																							for (MergeCount[15]=MergeStart[15]; MergeCount[15]<=MergeEnd[15]; MergeCount[15]+=1) {
																								//Generate shader(s) for each Merge mode


																								//Get names for this shader's settings
																								FunctionName=FunctionNames[FunctionCount];		//Name of the function
																								ScaleTranName=ScaleTranNames[ScaleTranCount];	//Name of the scale/translation mode
																								BlendName=BlendNames[BlendModeCount];			//Name of the blend mode
																								ZWriteName=ZWriteNames[ZWriteCount];			//Name of the zwrite mode
																								ZTestName=ZTestNames[ZTestCount];				//Name of the ztest mode
																								MaskName=MaskNames[MaskCount];					//Name of the color mask mode
																								CullingName=CullingNames[CullingCount];			//Name of the culling mode
																								MergeName="";									//Name of the merge modes for multitexturing
																								if ((FunctionCount==5) && (target.ShaderNamingIncludeMergesPreset==0)) {
																									//Multitexturing, but only if automatically adding merge names
																									for (Temp=1;Temp<target.NumberOfTexturesPreset+2;Temp++) {
																										MergeName+=MergeNames[MergeCount[Temp]]+"_";	//Append Merge mode
																									}
																								}
																								

																								//Create a name for the shader
																								ShaderName="";									//Init
																								if (target.ShaderNamePrefix != "") {
																									ShaderName=target.ShaderNamePrefix+"-";		//Start with the prefix
																								}
																								if ((target.ShaderNamingPreset==0) || ((target.ShaderNamePrefix.Length==0) && (target.ShaderNameSuffix.Length==0) && (target.ShaderCountingPreset != 0)) ) {
																									//Include an automatic name (either requested or required as a result of no prefix/suffix)
																									ShaderName+=FunctionName+"-";				//Add function
																									if (target.ShaderCountingPreset==0) {
																										//Include counter
																										CounterText=Counter.ToString();			//Convert counter to string
																										if (CounterText.Length==1) {CounterText="00"+CounterText;}	//Pad counter
																										if (CounterText.Length==2) {CounterText="0"+CounterText;}	//Pad counter
																										ShaderName+=CounterText+"-";			//Add counter
																									}
																									ShaderName+=MergeName;						//Add merge modes
																									if (ShaderName[ShaderName.Length-1]=="_") {
																										ShaderName=ShaderName.Substring(0,ShaderName.Length-1)+"-";	//Convert end '_' to '-'
																									}
																									ShaderName+=BlendName+"-";					//Add blend mode
																									if (ZWriteCount==1) {
																										//Add ZWrite when it's on, ignore when off
																										ShaderName+="ZWrite-";					//Add zwrite
																									}
																									if (ZTestCount>0) {
																										//Add ZTest when it's other than off
																										ShaderName+="ZTest"+ZTestName+"-";		//Add ztest
																									}
																									if (MaskCount>0) {
																										//Add color mask when it's other than RGBA
																										ShaderName+=MaskName+"-";				//Add mask name
																									}
																									if ((CullingCount==1) || (CullingCount==2)) {
																										//Add culling if Front or Back, not Off
																										ShaderName+="Cull"+CullingName+"-";		//Add cull name
																									}
																									if (ScaleTranCount==1) {
																										//Add Texture scale/translate when active
																										ShaderName+="Tile&Offset-";				//Add tile/offset
																									}
																								} else {
																									//Manual name, add counter?
																									if (target.ShaderCountingPreset==0) {
																										//Include counter
																										CounterText=Counter.ToString();			//Convert counter to string
																										if (CounterText.Length==1) {CounterText="00"+CounterText;}	//Pad counter
																										if (CounterText.Length==2) {CounterText="0"+CounterText;}	//Pad counter
																										ShaderName+=CounterText+"-";			//Add counter
																									}
																								}
																								if (target.ShaderNameSuffix != "") {
																									ShaderName+=target.ShaderNameSuffix;		//End with the suffix
																								}
																								if (ShaderName[ShaderName.Length-1]=="-") {
																									ShaderName=ShaderName.Substring(0,ShaderName.Length-1);	//Trim extra '-' from end
																								}
																								ShaderName=ShaderName.Replace(" ","");			//Remove spaces
															
																								//Determine if this shader uses a texture
																								HasTextures=false;
																								if ((FunctionCount==2) || (FunctionCount==3) || (FunctionCount==4) || (FunctionCount==5)) {
																									//This shader has one or more textures
																									HasTextures=true;
																								}
																																	
																								//Shader is already generated and output to a file, now need to create material
																								
																								//Calculate folder path
																								FilePath=Application.dataPath;						//Main assets folder
																								if (target.OutputPath != ""){
																									FilePath+="/"+target.OutputPath;				//Add custom relative output path
																								}
																								FilePath+="/";
																								if (target.FolderModePreset==0) {
																									//Folders by function
																									SubPath=FunctionName+"/";				//Sub folders by function
																									if (target.ShaderCountingPreset==0) {
																										//Adding counters, so add a counter of the function - it's index
																										CounterText2=FunctionCount.ToString();
																										if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																										SubPath=CounterText2+"-"+SubPath;	//Append counter to start of name
																									}
																								} else {
																									if (target.FolderModePreset==1) {
																										//Folders by blend
																										SubPath=BlendName+"/";				//Sub folders by blend mode
																										if (target.ShaderCountingPreset==0) {
																											//Adding counters, so add a counter of the blend mode - it's index
																											CounterText2=BlendModeCount.ToString();
																											if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																											SubPath=CounterText2+"-"+SubPath;	//Append counter to start of name
																										}
																									} else {
																										if (target.FolderModePreset==2) {
																											//Folders by function then by blend
																											SubPath=FunctionName;			//Sub folders by function
																											if (target.ShaderCountingPreset==0) {
																												//Adding counters, so add a counter of the function - it's index
																												CounterText2=FunctionCount.ToString();
																												if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																												SubPath=CounterText2+"-"+SubPath;	//Append counter to start of name
																											}
																											if (target.ShaderCountingPreset==0) {
																												//Adding counters, so add a counter of the blend mode - it's index
																												CounterText2=BlendModeCount.ToString();
																												if (CounterText2.Length==1) {CounterText2="0"+CounterText2;}	//Append 0 to start
																												SubPath+="/"+CounterText2+"-"+BlendName+"/";	//Append counter to start of name
																											} else {
																												//Not adding counters
																												SubPath+="/"+BlendName+"/";			//Sub folders by blend
																											}
																										} else {
																											//Flat
																											SubPath="";						//No sub folders
																										}
																									}
																								}
															
																								//Import Shader
																								CurrentShader = Resources.Load(SubPath+ShaderName,Shader) as Shader;	//Load the shader
															
																								//Generate material with shader
																								if (target.MaterialPreset==0) {
																									//Generate material
																									Mat = new Material(CurrentShader);									//Create material from this shader
																									if (HasTextures==true) {
																										if (FunctionCount==5) {
																											//Multitexture
																											var Tex:int;
																											for (Tex=0;Tex<target.NumberOfTexturesPreset+2;Tex++) {
																												Mat.SetTexture("_Texture"+Tex,target.DefaultTexture[Tex]);			//Set the default texture
																											}
																										} else {
																											//Single texture
																											Mat.SetTexture("_Texture0",target.DefaultTexture[0]);			//Set the default texture
																										}
																									}
																									AssetDatabase.CreateAsset(Mat, "Assets/"+FilePath.Replace(Application.dataPath+"/","")+"Resources/"+SubPath+ShaderName+".mat");			//Save the material
																								}
																								
																								//Show Progress
																								Progress=0.5+(0.5*((1.0/BatchTotal) * ((Counter-target.CounterOffset)+1)));
																								if (EditorUtility.DisplayCancelableProgressBar("Shader Wizard - Generating Materials...","Material "+((Counter-target.CounterOffset)+1)+" of "+BatchTotal+" for "+ShaderName,Progress)) {	//Display progress bar
																									//User cancelled the operation, quit!
																									EditorUtility.ClearProgressBar();			//Remove the progress bar
																									AssetDatabase.Refresh();					//Do a final refresh
																									EditorUtility.DisplayDialog("Shader Wizard","Operation was cancelled - "+BatchTotal+" shader(s) were generated and "+((Counter-target.CounterOffset)+1)+" material(s) may have been generated","Thank You!");	//Display cancellation message
																									CurrentShader = Resources.Load(FirstShaderObject,Shader) as Shader;	//Load the first shader
																									if (CurrentShader) {EditorGUIUtility.PingObject(CurrentShader);}	//Hilite the first object yellow
																									return;										//And exit!
																								}
																								Counter+=1;										//Next shader
															

																							}	//End of generating by MultiTexture Merge Layer 15
																						
																						}	//End of generating by MultiTexture Merge Layer 14
																					
																					}	//End of generating by MultiTexture Merge Layer 13
																				
																				}	//End of generating by MultiTexture Merge Layer 12
																			
																			}	//End of generating by MultiTexture Merge Layer 11
																		
																		}	//End of generating by MultiTexture Merge Layer 10
																	
																	}	//End of generating by MultiTexture Merge Layer 9
																
																}	//End of generating by MultiTexture Merge Layer 8
															
															}	//End of generating by MultiTexture Merge Layer 7
														
														}	//End of generating by MultiTexture Merge Layer 6
													
													}	//End of generating by MultiTexture Merge Layer 5
												
												}	//End of generating by MultiTexture Merge Layer 4
											
											}	//End of generating by MultiTexture Merge Layer 3
									
										}	//End of generating by MultiTexture Merge Layer 2
									
									}	//End of generating by MultiTexture Merge Layer 1									
								
								}	//End of generating by Cullings
							
							}	//End of generating by Masks	

						}	//End of generating by ZTests						
				
					}	//End of generating by ZWrites	

				}	//End of generating by Blend mode
				
			}	//End of generating by Scale/Translate modes
			
		}	//End of generating by shader functions
		
		EditorUtility.ClearProgressBar();			//Remove the progress bar
		AssetDatabase.Refresh();					//Refresh the project folder to show/import the generated assets
		var Complete:String=BatchTotal+" Shader";	//Shader message
		if (BatchTotal>1) {Complete+="s";}			//Plural
		if (target.MaterialPreset==0) {
			Complete+=" and "+BatchTotal+" Material";						//Material message
			if (BatchTotal>1) {Complete+="s";}		//Plural
		}
		Complete+=" have been generated!";			//Multiple because it's at least 1 shader and 1 material
		EditorUtility.DisplayDialog("Shader Wizard",Complete,"Thank You!");		//Display completion message
		CurrentShader = Resources.Load(FirstShaderObject,Shader) as Shader;	//Load the first shader
		if (CurrentShader) {EditorGUIUtility.PingObject(CurrentShader);}	//Hilite the first object yellow
				
	}	//End of GenerateMaterials() function
	
	

}	//End of ShaderWizardEditor class
