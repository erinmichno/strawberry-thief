 #pragma strict

//ShaderWizard CG Shader Generator
//Copyright 2013 Paul West/Venus12 LLC

//Version 2.1

//Unity has done a great job of simplifying the shader-creation process. Unity's `ShaderLab` is a structured
//language designed for designing the behavior of a shader, supporting the older fixed-function pipeline
//and also the more modern vertex and fragment shaders via CG code. You don't have to worry too much about
//making the shader work on a piece of hardware or what features are supported - unless you want to do
//something more advanced, and you definitely don't need to deal with compilation or graphics API's. But for
//a beginner or intermediate user, creating a new shader can be painful and time consuming. Creating the shader
//itself is a drag - you have to remember what each section is for and how it must be structured, you have to
//get the syntax exactly right otherwise you'll get cryptic shader errors, you have to remember what parameters
//are allowed for each command and exactly how to type them, and on top of that you have to figure out how to
//make the shader get the result you want. This can make shader construction a tedius and challenging task
//even for really simply shaders. I know because I've experienced the learning curve first hand - having to
//reference the ShaderLab documentation over and over again to get things to work. This is where ShaderWizard steps in.

//ShaderWizard provides a simple user interface for the novice/average user wishing to create simple-to-intermediate
//shaders in Unity. There are no scripts to be written, no syntax to learn, no parameters to remember, no
//structure to get right, no spelling errors, no confusing shader code and absolutely no shader errors.
//A few clicks to choose what you want the shader to do and you're done. ShaderWizard writes the shader for you.
//It can even mass-produce large batches of variations very quickly.

//ShaderWizard's shaders are relatively simple, but it's often these simple shaders that we need. If you're looking
//for advanced 3D shaders with lighting and fancy effects, ShaderWizard may not be the tool for you. It creates
//shaders that are great for 2D games and simpler 3D games. It is `easy` after all. But at least you don't have
//to learn a visual shader editor which can be almost as daunting as writing the shader code yourself. ShaderWizard
//provides a super-simple `basic` interface to quickly generate common useful shaders, and a more `advanced`
//interface to provide greater design flexibility.

//ShaderWizard will be your reliable companion for years to come.

//Editor variables
var TitleImage : Texture2D;					//Title.png image for the editor
var BlendModePreset : int=0;				//Which preset blend mode is chosen
var ShaderFunctionPreset : int=0;			//Which preset shader function is chosen
var ZTestPreset : int=0;					//Which preset z-test function is chosen
var ZWritePreset : int=0;					//Whether to write Z coords to the depth buffer
var OutputPath : String="";					//Path relative to Assets folder to output to
var FolderModePreset : int=0;				//What folder output mode to use
var MenuName : String="";					//Main menu name to list shaders in (shader selection dropdown)
var SubName : String = "";					//Submenu name to list shaders in (shader selection dropdown)
var ShaderNamePrefix : String = "";			//Text to insert before the name of a shader
var ShaderNameSuffix : String = "";			//Text to insert after the name of a shader
var ShaderNamingPreset : int = 0;			//Whether to name shaders
var ShaderNamingIncludeMergesPreset : int = 0;	//Whether to include merge names in shader name generation for MultiTexture layers
var MaterialPreset : int = 0;				//Whether to generate material
var CommentPreset : int = 0;				//What level of commenting to include
var ShaderCountingPreset : int=0;			//Whether to add a counter to shader names
var ColorMaskPreset : int = 0;				//What color masking to do
var CullingPreset : int = 0;				//What culling to do
var AlphaTestThreshold : float = 0.0;		//What value the alpha must be for alpha test to pass
var FixedColor : Color = Color(1,1,1,1);	//What fixed color to use
var ScaleTranslateTexturePreset : int[];	//Whether to allow scale/translate of texture coords on a each texture layer (only currently supports layer 0)
var DefaultTexture : Texture2D[];			//Default texture to use
var CounterOffset : int=0;					//What offset to start the counter at
var MergePreset : int[];					//What Merge operation is used between multitexture layers
var NumberOfTexturesPreset : int=0;			//How many textures in multitexturing? 0=2, 1=3, 2=4 etc
var AlphaMaskThreshold : float[];			//Thresholds for each multitexture layer if using AlphaMask merge
var AlphaFadeThreshold : float[];			//Thresholds for each multitexture layer if using AlphaFade merge
var ShaderModelPreset:int=0;				//Shader model 0=2.0, 1=3.0