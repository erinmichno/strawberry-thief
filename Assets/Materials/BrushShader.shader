Shader "Custom/Brush/Alpha Blended"
{
	Properties
	{
		_TintColor ("Tint Color", Color) = (1.0, 1.0, 1.0, 1.0)
		_MainTex ("Main Texture", 2D) = "white" {}
	}

	Category
	{
		Tags
		{
			"Queue"="Transparent"
			"IgnoreProjector"="True"
			"RenderType"="Transparent"
		}
		
		Blend SrcAlpha OneMinusSrcAlpha
		Cull Off
		Lighting Off
		ZWrite Off
		Fog { Mode off }
		
		BindChannels
		{
			Bind "Vertex", vertex
			Bind "TexCoord", texcoord
			Bind "Color", color
		}
		
		SubShader
		{
			Pass
			{
				CGPROGRAM
				
				#pragma target 3.0
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				
				#include "UnityCG.cginc"
				
				struct appdata_t
				{
					float4 vertex : POSITION;
					float2 texCoord : TEXCOORD0;
					float4 color : COLOR;
				};
		
				struct v2f
				{
					float4 vertex : POSITION;
					float2 texCoord : TEXCOORD0;// base texture uv coordinates of the vertex
					float4 color : COLOR;
				};
				
				float4 _TintColor;
				sampler2D _MainTex;
				
				v2f vert (appdata_t v)
				{
					v2f o;
					o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
					o.texCoord = v.texCoord;
					o.color = v.color;
					return o;
				}
				
				float4 frag (v2f i) : COLOR
				{
					float4 color = tex2D(_MainTex, i.texCoord);
					color.rgb *= _TintColor;
					color.a *= i.color.a;
					return color;
				}
				
				ENDCG
			}
		}
	}
}
