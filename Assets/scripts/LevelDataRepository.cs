﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LevelDataRepository : MonoBehaviour
{
    public GameObject spriteRoot = null;

    public List<string> instrument01ClipNames = new List<string>(3);
    public List<string> instrument02ClipNames = new List<string>(3);
    public List<string> instrument03ClipNames = new List<string>(3);
    public List<string> instrument04ClipNames = new List<string>(3);
    

    public string backroundTexPrefix = "";
    public string backroundTexSuffix = "";
    public int backGroundTexCount = 0;
    public int backgroundTexNumber = 16;

    public string blendTexPrefix = "";
    public string blendTexSuffix = "";
    public int blendTexCount = 0;

    public List<List<AudioClip>> instrumentClips = new List<List<AudioClip>>();
    public List<Texture2D> backgroundTextures = new List<Texture2D>();
    public List<Texture2D> blendTextures = new List<Texture2D>();

    public bool asyncLoad = false;

    private List<IEnumerator> dataCoroutines = new List<IEnumerator>();

	void Awake ()
    {
        for (int i = 0; i < 4; ++i)
        {
            instrumentClips.Add(null);
        }

        dataCoroutines.Add(LoadInstrument(instrument01ClipNames, 0));
        dataCoroutines.Add(LoadInstrument(instrument02ClipNames, 1));
        dataCoroutines.Add(LoadInstrument(instrument03ClipNames, 2));
        dataCoroutines.Add(LoadInstrument(instrument04ClipNames, 3));
        
        dataCoroutines.Add(LoadBackgroundTextures());
        dataCoroutines.Add(LoadBlendTextures());

        StartCoroutine(dataCoroutines[0]);
	}

    private IEnumerator LoadInstrument(List<string> clipNames, int instrumentNumber)
    {
        List<AudioClip> clips = new List<AudioClip>();

        foreach (string clipName in clipNames)
        {
            if (clipName == "")
            {
                clips.Add(null);
                continue;
            }

            clips.Add(Resources.Load<AudioClip>(clipName));

            if (asyncLoad)
            {
                yield return new WaitForSeconds(0.1f);
            }
        }

        instrumentClips[instrumentNumber] = clips;

        dataCoroutines.RemoveAt(0);

        if (dataCoroutines.Count != 0)
        {
            StartCoroutine(dataCoroutines[0]);
        }
    }

    private IEnumerator LoadBackgroundTextures()
    {
        if (backGroundTexCount == backgroundTexNumber)
        {
            for (int i = 1; i <= backGroundTexCount; ++i)
            {
                string bgNumber = (i < 10) ? "0" + i.ToString() : i.ToString();

                backgroundTextures.Add(Resources.Load<Texture2D>(backroundTexPrefix + bgNumber + backroundTexSuffix));
                if (asyncLoad)
                {
                    yield return new WaitForSeconds(0.1f);
                }
            }
        }
        else //special case for optimization only
        {
            string bgNumber = "01";
            Texture2D baseTex = Resources.Load<Texture2D>(backroundTexPrefix + bgNumber + backroundTexSuffix);
            for (int i = 1; i <= backGroundTexCount; ++i)
            {
                backgroundTextures.Add(baseTex);
                if (asyncLoad)
                {
                    yield return new WaitForSeconds(0.1f);
                }
            }

        }

        dataCoroutines.RemoveAt(0);

        if (dataCoroutines.Count != 0)
        {
            StartCoroutine(dataCoroutines[0]);
        }
    }

    private IEnumerator LoadBlendTextures()
    {
        for (int i = 1; i <= blendTexCount; ++i)
        {
            string bgNumber = (i < 10) ? "0" + i.ToString() : i.ToString();

            blendTextures.Add(Resources.Load<Texture2D>(blendTexPrefix + bgNumber + blendTexSuffix));
            if (asyncLoad)
            {
                yield return new WaitForSeconds(0.1f);
            }
        }

        dataCoroutines.RemoveAt(0);

        if (dataCoroutines.Count != 0)
        {
            StartCoroutine(dataCoroutines[0]);
        }
    }

    void Update()
    {
        if (asyncLoad && dataCoroutines.Count == 0)
        {
            // ALL DATA LOADED
            LevelManager.NotifyAsyncDataLoaded();
            this.enabled = false;
        }
    }
}
