﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class VertPoint
{
    public Vector2 pos = Vector2.zero;
    public Vector2 uv = Vector2.zero;
    public Vector2 uv2 = Vector2.zero; //this will be used for the painting uvs

    public void GLPoint()
    {
        //  GL.TexCoord2(uv.x, uv.y);
        GL.MultiTexCoord2(0, uv.x, uv.y);
        GL.MultiTexCoord2(1, uv2.x, uv2.y);
        GL.Vertex3(pos.x, pos.y, 0.1f);
    }

}