﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UISprite))]
public class PauseButtonAtlasToggle : MonoBehaviour
{

    public UIAtlas pauseAtlas = null;
    public UIAtlas unPauseAtlas = null;

    private UISprite uiSprite = null;

    // Use this for initialization
    void Start()
    {
        uiSprite = GetComponent<UISprite>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.timeScale < 0.95f)
        {
            uiSprite.atlas = pauseAtlas;
        }
        else
        {
            uiSprite.atlas = unPauseAtlas;
        }
    }
}
