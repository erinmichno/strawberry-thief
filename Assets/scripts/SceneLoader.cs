﻿using UnityEngine;
using System.Collections;

public class SceneLoaderNoMono
{
    public string NextScene = "Welcome";

    public SceneLoaderNoMono(string s = "Complete")
    {
        NextScene = s;
    }
    public void LoadNextScene()
    {
        Application.LoadLevel(NextScene);
    }
    static public void LoadMenuScene()
    {
        Application.LoadLevel("Menu");
    }

    public void OnSceneExit()
    {

        UIScreenFade.GetScreenFadeToBlack(1f, new EventDelegate(LoadNextScene), false, 0f, true);
    }

    public void OnSceneExitToMenuNonStatic()
    {
        OnSceneExitToMenu();
    }
    static public void OnSceneExitToMenu()
    {
        //do fade too

        UIScreenFade.GetScreenFadeToBlack(1f, new EventDelegate(LoadMenuScene), false, 0f, true);
    }
}

public class SceneLoader : MonoBehaviour 
{
    public string NextScene = "Welcome";
    public bool killAudio = false;

    //public void ChangePathToAurora()
    //{
    //    Main.SwitchCurrentPath(true);
    //}
    //public void ChangePathToPhoto()
    //{
    //    Main.SwitchCurrentPath(false);
    //}
    public void OnSceneExitToMenuNonStatic()
    {
        OnSceneExitToMenu();
    }
    public void LoadNextScene()
    {
        Application.LoadLevel(NextScene);
    }
   static public void LoadMenuScene()
    {
        Application.LoadLevel("Menu");
    }
   
    public void OnSceneExit()
    {
     
        UIScreenFade.GetScreenFadeToBlack(1f, new EventDelegate(LoadNextScene), false, 0f, true);

        if (killAudio)
        {
            if (!VolumeController.muteState == true)
            {
                StartCoroutine(FadeOutAudio(0.9f));
            }
        }
    }

   static public void OnSceneExitToMenu()
    {
        //do fade too
        
       UIScreenFade.GetScreenFadeToBlack(1f, new EventDelegate(LoadMenuScene), false, 0f, true);
    }

   private IEnumerator FadeOutAudio(float seconds)
   {
       float timer = 0.0f;

       while (timer < 1.0f)
       {
           AudioListener.volume = 1.0f - timer * timer * timer;

           timer += Time.deltaTime / seconds;

           yield return null;
       }

       AudioListener.volume = 0.0f;
   }
}
