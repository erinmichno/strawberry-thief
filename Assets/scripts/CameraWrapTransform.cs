﻿using UnityEngine;
using System.Collections;

public class CameraWrapTransform : MonoBehaviour
{
    private Camera2DS relativeWrapCamera = null;

    public Vector3 originalPos;

	void Start()
    {
        relativeWrapCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DS>();

        if (relativeWrapCamera == null)
        {
            throw new UnityException("Camera not found in CameraWrapTransform");
        }

        originalPos = transform.localPosition;
	}

    void LateUpdate()
    {
        float xPos = 0.0f;
        float yPos = 0.0f;

        float x0 = Mathf.Abs((originalPos.x - Camera2DS.MapDimensions.x) - relativeWrapCamera.transform.localPosition.x);
        float x1 = Mathf.Abs(originalPos.x - relativeWrapCamera.transform.localPosition.x);
        float x2 = Mathf.Abs((originalPos.x + Camera2DS.MapDimensions.x) - relativeWrapCamera.transform.localPosition.x);

        if (x0 <= x1 && x0 <= x2)
        {
            xPos = originalPos.x - Camera2DS.MapDimensions.x;
        }
        else if (x1 <= x2)
        {
            xPos = originalPos.x;
        }
        else
        {
            xPos = originalPos.x + Camera2DS.MapDimensions.x;
        }

        float y0 = Mathf.Abs((originalPos.y - Camera2DS.MapDimensions.y) - relativeWrapCamera.transform.localPosition.y);
        float y1 = Mathf.Abs(originalPos.y - relativeWrapCamera.transform.localPosition.y);
        float y2 = Mathf.Abs((originalPos.y + Camera2DS.MapDimensions.y) - relativeWrapCamera.transform.localPosition.y);

        if (y0 <= y1 && y0 <= y2)
        {
            yPos = originalPos.y - Camera2DS.MapDimensions.y;
        }
        else if (y1 <= y2)
        {
            yPos = originalPos.y;
        }
        else
        {
            yPos = originalPos.y + Camera2DS.MapDimensions.y;
        }

        transform.localPosition = new Vector3(xPos, yPos, 0.0f);
    }
}
