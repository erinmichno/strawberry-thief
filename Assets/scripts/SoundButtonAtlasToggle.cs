﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UISprite))]
public class SoundButtonAtlasToggle : MonoBehaviour
{
    public UIAtlas muteAtlas = null;
    public UIAtlas unMuteAtlas = null;

    private UISprite uiSprite = null;
    private UISpriteAnimation uiSpriteAnimation = null;

    public bool canPause = false;

    // Use this for initialization
    void Start()
    {
        uiSprite = GetComponent<UISprite>();
        uiSpriteAnimation = GetComponent<UISpriteAnimation>();
    }

    void OnClick()
    {
        Update();
    }

    // Update is called once per frame
    void Update()
    {
        if (uiSpriteAnimation != null && uiSpriteAnimation.isPlaying)
        {
            return;
        }

        if (VolumeController.muteState == false || (canPause && VolumeController.muteFromPause))
        {
            uiSprite.atlas = unMuteAtlas;
        }
        else
        {
            uiSprite.atlas = muteAtlas;
        }
    }
}
