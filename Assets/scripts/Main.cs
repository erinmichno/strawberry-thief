/**

    This class demonstrates the code discussed in these two articles:

    http://devmag.org.za/2011/04/05/bzier-curves-a-tutorial/
    http://devmag.org.za/2011/06/23/bzier-path-algorithms/

    Use this code as you wish, at your own risk. If it blows up 
    your computer, makes a plane crash, or otherwise cause damage,
    injury, or death, it is not my fault.

    @author Herman Tulleken, dev.mag.org.za

*/


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Main : MonoBehaviour 
{
    private const int NGUI_LAYER = 8;
    

    static public bool ShowWelcome = true;
    static private bool showSplash = true;
    static GameObject menuPannel;
    static GameObject welcomePannel;

    //static bool CurrentPathAurora = true;

	private static Main _instance;

	AudioConductor conductor;
    	
    //public static Main instance
    //{
    //    get
    //    {
    //        if(_instance == null)
    //        {
    //            _instance = GameObject.FindObjectOfType<Main>();
				
    //            //Tell unity not to destroy this object when loading a new scene!
    //            DontDestroyOnLoad(_instance.gameObject);
    //        }
			
    //        return _instance;
    //    }
    //}
	
	
	
    
    
    
    
	
	void Awake() 
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
          //  int setup = IndexedDataRepository.GetIndexByDate(System.DateTime.Now);
          //  setup += 1;

          //  FacebookCombo.init();
          //  TwitterCombo.init(SocialSaving.TWITTER_CONSUMER_KEY, SocialSaving.TWITTER_CONSUMER_SECRET);

			
            
	
			

			 conductor = gameObject.AddComponent<AudioConductor>();

			DontDestroyOnLoad(this.gameObject);

          //  GPSManager.Initialise();
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
		
		

        
	}

    // Use this for initialization
	void Start ()
	{
        if (showSplash)
        {
            showSplash = false;

            //SplashScreenController splashController = GameObject.FindGameObjectWithTag("SplashScreenController").GetComponent<SplashScreenController>();

            //splashController.StartSplash(new EventDelegate(ShowMenus));
        }
        else
        {
            ShowMenus();
        }
	}

    void ShowMenus()
    {
        menuPannel = GameObject.FindGameObjectWithTag("MenuPannel");
        welcomePannel = GameObject.FindGameObjectWithTag("WelcomePannel");
        if (menuPannel != null && welcomePannel != null)
        {
            if (menuPannel != null)
            {
                menuPannel.SetActive(!ShowWelcome);
            }

            if (welcomePannel != null)
            {
                welcomePannel.SetActive(ShowWelcome);
            }
        }
    }

	void OnLevelWasLoaded(int level)
	{
        print("levelLoaded" + level);
        if (Application.loadedLevelName == "3LevelMain")
        {
            //UIScreenFade.GetScreenFadeFromBlack(3.0f, null, true, 0.0f);
           
        }
        else
        {
            UIScreenFade.GetScreenFadeFromBlack(1.0f, null, true, 0f);
        }
        

        // refresh the GPS service...
      //  GPSManager.Initialise();

		if (Application.loadedLevelName == "Menu")
		{
			ShowMenus();
		}

        //when level loads see if camera and need to move to main camera
        if(Camera.main != null)
        {
            

            AudioListener listeners = GameObject.FindObjectOfType(typeof(AudioListener)) as AudioListener;
            if (listeners == null)
            {
                Camera cam = Camera.main;
                if (cam == null) cam = GameObject.FindObjectOfType(typeof(Camera)) as Camera;
                if (cam != null) listeners = cam.gameObject.AddComponent<AudioListener>();
            }
          //  transform.position = listeners.transform.position;
            
        }
	}
	
    public void WtoM()
    {
        OnWelcomeToMain();
    }
    public static void OnWelcomeToMain()
    {
        ShowWelcome = false;

        TweenAlpha welcomeTween = welcomePannel.AddComponent<TweenAlpha>();
        welcomeTween.duration = 0.5f;
        welcomeTween.from = 1.0f;
        welcomeTween.to = 0.0f;
        welcomeTween.animationCurve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);
        welcomeTween.AddOnFinished(OnWelcomePanelFadeOut);

        welcomeTween.PlayForward();
    }

    public static void OnWelcomePanelFadeOut()
    {
        welcomePannel.SetActive(false);

        menuPannel.SetActive(true);

        TweenAlpha menuTween = menuPannel.AddComponent<TweenAlpha>();
        menuTween.duration = 0.5f;
        menuTween.from = 0.0f;
        menuTween.to = 1.0f;
        menuTween.animationCurve = AnimationCurve.EaseInOut(0.0f, 0.0f, 1.0f, 1.0f);

        menuTween.PlayForward();
    }

    public static void ToggleWelcomePannel()
    {
        ShowWelcome = !ShowWelcome;
        menuPannel.SetActive(!ShowWelcome);
        welcomePannel.SetActive(ShowWelcome);
    }

	// Update is called once per frame
	void Update () 
	{
		ProcessInput();
		Render();
	}

    void OnApplicationQuit()
    {
       // GPSManager.Deinitialise();
    }

    // handles application being backgrounded / going to sleep
    void OnApplicationFocus(bool focusStatus)
    {
        if (focusStatus)
        {
           // GPSManager.Initialise();
        }
    }
	
	private void ProcessInput()
	{
        //if (mode == Mode.BezierReduced)
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        //points.Clear();
        //    }
        //    if (Input.GetMouseButton(0))
        //    {
        //        Vector2 screenPosition = Input.mousePosition;
        //        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(new Vector3(screenPosition.x, screenPosition.y, 4));

        //        points.Add(worldPosition);
        //    }
        //}
        //else
        //{
        //    if (Input.GetMouseButtonDown(0))
        //    {
        //        Vector2 screenPosition = Input.mousePosition;
        //        Vector3 worldPosition = Camera.main.ScreenToWorldPoint(new Vector3(screenPosition.x, screenPosition.y, 4));

        //        points.Add(worldPosition);
        //    }
        //}
		
        //if(Input.GetKeyDown(KeyCode.F1))
        //{
        //    mode = Mode.Line;
        //}
		
        //if(Input.GetKeyDown(KeyCode.F2))
        //{
        //    mode = Mode.Bezier;
        //}
		
        //if(Input.GetKeyDown(KeyCode.F3))
        //{
        //    mode = Mode.BezierInterpolated;
        //}
		
        //if(Input.GetKeyDown(KeyCode.F4))
        //{
        //    mode = Mode.BezierReduced;
        //}

        //if (Input.GetKeyDown(KeyCode.X))
        //{
        //    points.Clear();
        //}
	}
	

    ///Note: this file merely illustrate the algorithms.
    ///Generally, they should NOT be called each frame!
	private void Render()
	{
        //switch(mode)
        //{
        //    case Mode.Line:
        //        RenderLineSegments();
        //    break;
        //    case Mode.Bezier:
        //        RenderBezier();
        //    break;
        //    case Mode.BezierInterpolated:
        //        BezierInterpolate();
        //    break;
        //    case Mode.BezierReduced:
        //    BezierReduce();
        //    break;


        //}
	}
	
	
    public void OnDrawGizmos()
    {
        //if (gizmos == null)
        //{
        //    return;
        //}        

        //for (int i = 0; i < gizmos.Count; i++)
        //{
        //    Gizmos.DrawWireSphere(gizmos[i], 1f);            
        //}
    }

    public void OnGUI()
    {
        /*GUILayout.BeginArea(new Rect(10, 10, 300, 300));
        GUILayout.Label("F1 Line Segments (Click to add points)");
        GUILayout.Label("F2 Bezier curve (Click to add points)");
        GUILayout.Label("F3 Bezier interpolation (Click to add points)");
        GUILayout.Label("F4 Bezier sampling / reduction (Drag to add points)");
        GUILayout.Label("X  Clear");
        GUILayout.Label("");
        GUILayout.Label("Switch on Gizmos in Unity to view control points");

        GUILayout.EndArea();*/
    }
}
