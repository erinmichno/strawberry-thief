﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour {

    public float PollenLinearAmount = 100;
    public int MaxPollenAmount = 100;
    public float rateOfDecrease = 10;
    public Animator anim = null;
    public Cursor cursor = null;
    float counter = 0;
    float idleCounter = 0.0f;
    bool Idle1 = false;
	public bool usingDistance = true;

    private float lastRecordedSpeed = 0.0f;
    private float lastRecordedTurningVal = 0.0f;
	

    public float GetPollenLinear()
    {
        return PollenLinearAmount / (float)MaxPollenAmount;
    }
	public float GetPollen()
    {
    	float x = PollenLinearAmount / (float)MaxPollenAmount;
    	float y = x;
    	
    	if(x < 0.25f)
    	{
    		y = x;
    	}
    	else
    	{
    		y =  1.0f;
    	}
    	return y;
    }
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		//cursor = GetComponentInParent<Cursor>();
		cursor = NGUITools.FindInParents<Cursor>(transform);
        print("d");
	}
	
	void SetAnim()
	{
		float speed = cursor.NormalizedSpeed; //speed of bird between 0 and 1, rounded down to next 0.1
		float turningVal = cursor.turningFactor; // turning -1 hard left 0 not turning 1 hard right

        //if (Mathf.Abs(speed - lastRecordedSpeed) < 0.08f)
        //{
        //    speed += lastRecordedSpeed;
        //}

        //if (Mathf.Abs(turningVal - lastRecordedTurningVal) < 0.08f)
        //{
        //    turningVal = lastRecordedTurningVal;
        //}

        speed = lastRecordedSpeed + (speed - lastRecordedSpeed) * 0.5f;
        turningVal = lastRecordedTurningVal + (turningVal - lastRecordedTurningVal) * 0.5f;

        lastRecordedSpeed = speed;
        lastRecordedTurningVal = turningVal;

        // don't turn if speed is too low...
        if (speed < 0.1f)
        {
            turningVal = 0.0f;
        }

        bool dive = cursor.IsDiving;
        bool loop = cursor.IsLooping;

		counter += Time.deltaTime;
		if(counter > 9)
		{
			counter = 0;
		}
		
		idleCounter += Time.deltaTime;
		if(idleCounter >= 4.5f)
		{
			idleCounter -= 4.5f;
			Idle1 = Random.Range (0,2) > 0 ;
			
		}
        bool idle2 = BrushPath.timerNotDrawing > 7.0f ? true : false;
		bool Glide = counter > 6.0f  ? true : false;
//		print ("TURNING VAL: " + turningVal);
		anim.SetFloat("Speed", speed);
		anim.SetFloat("turningVal", turningVal);
		anim.SetBool ("useIdle1", idle2);
		anim.SetBool ("useFastGlide", Glide);
        anim.SetBool("useDive", dive);
        anim.SetBool("useLoop", loop);
	}
	
	// Update is called once per frame
	void Update () {

		float subtractAmount = rateOfDecrease * Time.deltaTime; 
		if(usingDistance && cursor != null)
		{
			subtractAmount =   cursor.GetAmountMoved();
		}
		PollenLinearAmount -= subtractAmount;
		if (PollenLinearAmount < 0)
		{
			PollenLinearAmount = 0;
		}
	//	print("Pollen: " + PollenLinearAmount);
		SetAnim ();
		
	}
	
	public void ReceivePollenPercentage(float amount)
    {
        Debug.Log("BIRD RECEIVED " + (amount * 100.0f).ToString() + "%");
        PollenLinearAmount = Mathf.Min(PollenLinearAmount + MaxPollenAmount * amount, MaxPollenAmount);
        Debug.Log("NEW POLLEN AMOUNT: " + PollenLinearAmount.ToString());
    }

    public void RemovePollenPercentage(float amount)
    {
        PollenLinearAmount = Mathf.Max(PollenLinearAmount - MaxPollenAmount * amount, 0.0f);
    }

    public void StartSpinOut(Vector3 origin)
    {
        if (cursor != null)
        {
            cursor.StartSpinOut(origin);
        }
    }

    //void OnTriggerEnter2D(Collider2D other)
    //{
    //    print("bird trigger enter");
    //    PollenLinearAmount = MaxPollenAmount;
    //}

    //void OnCollisionEnter2D(Collision2D collision) 
    //{
    //    print("bird coll Enter");
    //}

    
}
