﻿using UnityEngine;
using System.Collections;

public class SetSpriteRendererBlend : MonoBehaviour
{
    BlendPollen pollenMaster;
    public Texture currentTexture;
    Shader defaultShader = null;
    Shader transShader = null;
 
	// Use this for initialization
	void Start ()
	{
		pollenMaster = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BlendPollen>();

        defaultShader = renderer.material.shader; //use one on sprite already
        transShader = pollenMaster.spritesTransShader; //use a different shader
        // call update once to ensure material is correctly set
        renderer.material.shader = new Shader();
        Update();
	}
	
	// Update is called once per frame
	void Update()
	{
	    currentTexture = pollenMaster.GetCurrentBlendMap();
	    Material mat = renderer.material;
        if (pollenMaster.UseTrans())
        {
            mat.shader = transShader;
        }
        else
        {
            mat.shader = defaultShader;
        }
	
		mat.SetTexture("_BlendMap", currentTexture);
        mat.SetFloat("_MapWidth", Camera2DS.MapDimensions.x);  //mapWidth/768
        mat.SetFloat("_MapHeight", Camera2DS.MapDimensions.y);
	}
		
}
