﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(Animator))]
public class UIPlayBird : MonoBehaviour {

	Animator animator = null;
	
	void Awake()
	{
		animator	= GetComponent<Animator>();
	}
	// Use this for initialization
	void Start () {
		animator.Play("Menu_still");
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
