﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(UISprite))]
public class ChevronAtlasToggle : MonoBehaviour
{
    public UIAtlas ChevronAtlasDefault = null;
    public UIAtlas ChevronAtlasAlternate = null;

    private UISprite uiSprite = null;
    private UISpriteAnimation uiSpriteAnimation = null;

    public bool useDefault = false;

    // Use this for initialization
    void Start()
    {
        uiSprite = GetComponent<UISprite>();
        uiSpriteAnimation = GetComponent<UISpriteAnimation>();
    }

    void OnClick()
    {
        useDefault = !useDefault;
        Toggle();
    }

    // Update is called once per frame
    void Toggle()
    {
        if (uiSpriteAnimation != null && uiSpriteAnimation.isPlaying)
        {
            return;
        }

        if (!useDefault)
        {
            uiSprite.atlas = ChevronAtlasAlternate;
        }
        else
        {
            uiSprite.atlas = ChevronAtlasDefault;
        }
    }
}
