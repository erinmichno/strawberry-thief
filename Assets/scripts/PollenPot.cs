﻿using UnityEngine;
using System.Collections;

public class PollenPot : MonoBehaviour {

    private const float TIMER_DURATION = 5.0f;
    private float timer = TIMER_DURATION;

    private int numberOfUses = 3;

    private float currentPollenReductionAmount = 0.0f;

    private ParticleSystem[] burstParticles = null;

    private bool scaling = false;

    private float originalScale = 1.0f;
    private float targetScale = 1.0f;

    public bool bringBackToLife = true;
    public float backToLifeTime = 10.0f;

    private bool deactivateOnScaleOut = false;

    public Vector3 RespawnLocation = Vector3.zero;
    private Vector3 RespawnLocationAlt = Vector3.zero;
    bool useRespawnLocation = true;

    public enum MovementType
    {
        STATIC = 0,
        MOVING = 1,
        RESPAWNING = 2
    }

    public MovementType movementType = MovementType.STATIC;
    public bool doesFade = false;

    Camera2DS camera2DS = null;
    
	// Use this for initialization
	void Awake()
    {
        burstParticles = GetComponentsInChildren<ParticleSystem>();

        if (movementType != MovementType.MOVING)
        {
            GetComponent<MovingOval>().enabled = false;
        }
        else
        {
            GetComponent<MovingOval>().enabled = true;
        }

        camera2DS = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DS>();

        originalScale = transform.localScale.x;

        transform.localScale = Vector3.zero;

        targetScale = originalScale;
        scaling = true;
        RespawnLocationAlt = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (scaling)
        {
            if (Mathf.Abs(transform.localScale.x - targetScale) < 0.01f)
            {
                transform.localScale = new Vector3(targetScale, targetScale, targetScale);

                int particleCount = 0;

                foreach (ParticleSystem burst in burstParticles)
                {
                    particleCount += burst.particleCount;
                }

                if (particleCount == 0)
                {
                    scaling = false;

                    if (targetScale < 0.1f)
                    {
                        if (deactivateOnScaleOut)
                        {
                            gameObject.SetActive(false);
                        }
                        else if (numberOfUses == 0)
                        {
                            if (bringBackToLife)
                            {
                                collider2D.enabled = false;
                                renderer.enabled = false;

                                StartCoroutine(BringBackToLife(backToLifeTime));
                            }
                            else
                            {
                                gameObject.SetActive(false);
                            }
                        }
                        else if (movementType == MovementType.RESPAWNING && targetScale < 0.1f)
                        {
                            Respawn();
                            GetComponent<SpriteRenderer>().color = Color.white;
                            targetScale = originalScale;
                            scaling = true;
                        }
                    }
                }
                else
                {
                    // apply a non zero scale or particles will not display...
                    transform.localScale += new Vector3(0.05f, 0.05f, 0.05f);
                }
            }
            else
            {
                float currentScale = transform.localScale.x + (targetScale - transform.localScale.x) * 0.1f;
                transform.localScale = new Vector3(currentScale, currentScale, currentScale);
            }
        }

        timer += Time.deltaTime;
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        Bird bird = other.GetComponent<Bird>();

        if (bird != null)
        {
            if (timer >= TIMER_DURATION)
            {
                InGameAudio.PlayPollenPotClip();

                timer = 0.0f;
                print("TRIGGER ENTER");

                bird.ReceivePollenPercentage(1.0f - currentPollenReductionAmount);
                foreach (ParticleSystem burst in burstParticles)
                {
                    burst.Play();
                }

                if (doesFade)
                {
                    scaling = true;
                    if (--numberOfUses == 0)
                    {
                        targetScale = 0.0f;
                    }
                    else
                    {
                        currentPollenReductionAmount += 0.25f;

                        Color spriteColor = GetComponent<SpriteRenderer>().color;
                        spriteColor.r *= 0.75f;
                        spriteColor.g *= 0.75f;
                        spriteColor.b *= 0.75f;
                        GetComponent<SpriteRenderer>().color = spriteColor;

                        targetScale =  transform.localScale.x * 0.75f;
                    }
                }

                if (movementType == MovementType.RESPAWNING)
                {
                    scaling = true;
                    targetScale = 0.0f;
                }
            }
        }
    }

    private IEnumerator BringBackToLife(float seconds)
    {
        yield return new WaitForSeconds(seconds);

        collider2D.enabled = true;
        renderer.enabled = true;

        GetComponent<SpriteRenderer>().color = Color.white;

        transform.localScale = Vector3.zero;

        targetScale = originalScale;
        scaling = true;

        timer = TIMER_DURATION - 1.0f;

        numberOfUses = 3;
        currentPollenReductionAmount = 0.0f;
    }

    public void ScaleOutAndDeactivate()
    {
        timer = 0.0f;
        scaling = true;
        deactivateOnScaleOut = true;
        targetScale = 0.0f;

        // kill any coroutines to avoid deactivated fading pots from popping back in
        StopAllCoroutines();
    }

    void Respawn()
    {
        //int distance = 900;

        //Vector2 direction = Random.insideUnitCircle;
        //direction.Normalize();
        //direction *= distance;

       // Vector3 final = new Vector3(transform.localPosition.x + direction.x, transform.localPosition.y + direction.y, transform.localPosition.z);
        Vector3 final = useRespawnLocation ? RespawnLocation : RespawnLocationAlt;
        useRespawnLocation = !useRespawnLocation;

    //    final = WrapToLevel(final, (int)Camera2DS.MapDimensions.x, (int)Camera2DS.MapDimensions.y);
        transform.localPosition = final;

        CameraWrapTransform wrapTransform = GetComponent<CameraWrapTransform>();

        if (wrapTransform != null)
        {
            wrapTransform.originalPos = final;
        }
    }
    Vector3 WrapToLevel(Vector3 vec, int w, int h)
    {
//        print(vec);
        if (vec.x < 0)
        {
            vec.x = w + vec.x;
        }
        if (vec.y < 0)
        {
            vec.y = h + vec.y;
        }
        vec.x = (int)vec.x % w;
        vec.y = (int)vec.y % h;
        vec.x -= w * 0.5f;
        vec.y -= h * 0.5f;
        
        return vec;
    }
}
