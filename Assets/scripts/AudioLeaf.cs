﻿using UnityEngine;
using System.Collections;

public class AudioLeaf : MonoBehaviour {

	public AudioSource source;
	public AudioClip clip = null;

    public bool IsPlaying
    {
        get
        {
            if (source == null)
            {
                return false;
            }

            return source.isPlaying;
        }
    }

    public int TimeSamples
    {
        get
        {
            if (source == null)
            {
                return 0;
            }

            return source.timeSamples;
        }
    }
	
	void Awake()
	{
		source = gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
        source.Stop();
		source.minDistance = 50;
		source.loop = false;
	}
	
	public void LoadClip(string name)
	{
        clip = Resources.Load<AudioClip>(name);

        source.clip = clip;
	}
	
	public void UnloadClip()
	{
		if(clip != null)
		{
			Resources.UnloadAsset(clip);
		}
	}

    public void SetClip(AudioClip newClip)
    {
        clip = newClip;

        source.clip = clip;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
