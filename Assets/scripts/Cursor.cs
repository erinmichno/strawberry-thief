﻿using UnityEngine;
using System.Collections;

public class Cursor : MonoBehaviour
{
    private float maxSpeed = 1.0f;
    private float speedMultiplier = 1.0f;
    private float CurrentMaxSpeed
    {
        get
        {
            return maxSpeed * speedMultiplier;
        }
    }
    private float filterPower = 0.02f;

    private float circlingDistance = 0.4f;
    private float circlingAngularSpeed = 75.0f;

    Vector3 targetPos; //current pos is transform.pos
    Quaternion targetRotation; //current rot is transform.rot
    Vector3 lastFingerPosition; //mouse location

    Vector3 targetLookAt;
    Vector3 currentLookAt;
    int number = 0;
    //public int delayValue = 1;
    //public int delayScale = 1024;
    public Interpolate.EaseType easeType = Interpolate.EaseType.Linear;
    /// <summary>
    /// higher number to need to get more paint more often
    /// </summary>
    public float PaintRefeshRatio = 12.0f;

    Camera2DS camera2DS = null;
    
    BrushPath brushPath = null;

    Vector3 circleAxis = Vector3.zero;
    public Vector3 vel = Vector3.zero;

    float amountMoved = 0.0f;
    public float GetAmountMoved() { return amountMoved * PaintRefeshRatio; }

    private float currentSpeed = 0.0f;
    public float NormalizedSpeed
    {
        get
        {
            return Mathf.Clamp(currentSpeed / CurrentMaxSpeed, 0.0f, 1.0f);
        }
    }

    public float maxRotationSpeed = 300.0f;
    private float currentRotationSpeed = 0.0f;
    public float turningFactor;

    public bool IsDiving = false;
    public bool IsLooping = false;
    public bool IsSpinningOut = false;

    public AnimationPath introPath = null;

    public AnimationPath outroPath = null;

    private GameObject birdObject = null;

    private float speedBoostDuration = 1.0f;
    private float speedBoostTimer = 0.0f;

    public float birdIntroScaleFactor = 2.0f;
    public float birdIntroScaleDuration = 4.0f;
    private float birdScaleTimer = 0.0f;
    private float birdScale = 1.0f;
    private float targetBirdScale = 1.0f;

    public float birdOutroScaleFactor = 2.0f;
    public float birdOutroScaleDuration = 5.0f;

    public float spinOutDistance = 480.0f;
    public float spinOutSpread = 60.0f;

    private float travellingTimer = 0.0f;

    public enum CursorState
    {
        FOLLOWING_INTRO,
        FOLLOWING_OUTRO,
        FOLLOWING_FINGER,
        TRAVELLING_TO_CENTRE,
        CIRCLING
    }

    CursorState currentState = CursorState.FOLLOWING_INTRO;

    public CursorState CurrentState
    {
        get
        {
            return currentState;
        }
    }

    // Use this for initialization
    void Start()
    {
        camera2DS = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DS>();
        brushPath = GameObject.FindGameObjectWithTag("BrushPath").GetComponent<BrushPath>();
        birdObject = GameObject.FindGameObjectWithTag("Bird2");

        birdScale = birdObject.transform.localScale.x * birdIntroScaleFactor;
        targetBirdScale = birdObject.transform.localScale.x;

        LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnCursorCentred += OnCursorCentred;
        LevelManager.OnNewLevelStarted += OnNewLevelStarted;
        LevelManager.OnIntroFinished += OnIntroFinished;

        OnNewLevelStarted();
    }

    private void OnLevelComplete()
    {
        IsSpinningOut = false;
        currentState = CursorState.TRAVELLING_TO_CENTRE;

        speedMultiplier = 1.0f;
        speedBoostTimer = speedBoostDuration;
    }

    private void OnCursorCentred()
    {
        targetPos = transform.position;
        targetRotation = transform.rotation;

        circleAxis = (transform.position - (Vector3)camera2DS.GetLevelEndWorld()).normalized * circlingDistance;

        if (LevelManager.CurrentLevel == 2)
        {
            currentState = CursorState.FOLLOWING_OUTRO;

            birdScale = birdObject.transform.localScale.x;
            targetBirdScale = birdObject.transform.localScale.x * birdOutroScaleFactor;
        }
        else
        {
            currentState = CursorState.CIRCLING;
        }
    }

    private void OnNewLevelStarted()
    {
        LevelManager.LevelData levelData = LevelManager.GetCurrentLevelData();

        maxSpeed = levelData.cursorMaxSpeed;
        filterPower = levelData.cursorFilterPower;

        targetPos = transform.position;
        targetRotation = transform.rotation;

        birdScaleTimer = 0.0f;

        travellingTimer = 0.0f;

        if (LevelManager.CurrentLevel == 0)
        {
            currentState = CursorState.FOLLOWING_INTRO;
        }
        else
        {
            currentState = CursorState.FOLLOWING_FINGER;
        }

        speedMultiplier = 1.0f;
        speedBoostTimer = speedBoostDuration;
    }

    private void OnIntroFinished()
    {
        currentState = CursorState.FOLLOWING_FINGER;
    }

    void ApplyNoFilter()
    {
        print(targetPos);
        // transform.LookAt(targetPos, new Vector3(0, 0, 1));
        transform.position = targetPos;
        transform.rotation = targetRotation;


    }
    void ApplyAlphaFilter()
    {
        if (currentState != CursorState.FOLLOWING_FINGER && currentState != CursorState.FOLLOWING_INTRO/* && currentState != CursorState.FOLLOWING_OUTRO*/)
        {
            vel = (targetPos - transform.position) * filterPower;
            if (vel.magnitude > CurrentMaxSpeed * Time.deltaTime)
            {
                vel = vel.normalized * (CurrentMaxSpeed * Time.deltaTime);
            }

            if (currentState != CursorState.CIRCLING)
            {
                currentSpeed = vel.magnitude / Time.deltaTime;
            }
            else
            {
                currentSpeed = 1.0f;
                turningFactor = 1.0f;
            }

            transform.position += vel;
        }
        else
        {
            currentSpeed = ((transform.position - targetPos).magnitude * 2.0f) / Time.deltaTime;

            transform.position = targetPos;
        }

        Quaternion newRotation = Quaternion.RotateTowards(transform.rotation, targetRotation, maxRotationSpeed * Time.deltaTime);

        Vector3 newFacing = newRotation * Vector3.up;
        Vector3 oldFacing = transform.rotation * Vector3.up;
        
        transform.rotation = newRotation;
        
        currentRotationSpeed = (Vector3.Angle(oldFacing, newFacing) / Time.deltaTime) * Mathf.Sign(Vector3.Cross(newFacing, oldFacing).z);
    }

    static float Linear(float start, float distance, float elapsedTime, float duration)
    {
        // clamp elapsedTime to be <= duration
        if (elapsedTime > duration) { elapsedTime = duration; }
        return distance * (elapsedTime / duration) + start;
    }
    void ApplyEaseFilter()
    {

    }
    void CaptureTargets()
    {
        amountMoved = 0.0f;
        switch (currentState)
        {
            case CursorState.FOLLOWING_INTRO:
                {
                    introPath.UpdateTargetPos(transform.position, ref targetPos, ref targetRotation, ref turningFactor, CurrentMaxSpeed / 2.0f, filterPower);
                }
                break;
            case CursorState.FOLLOWING_OUTRO:
                {
                    //introPath.UpdateTargetPos(transform.position, ref targetPos, ref targetRotation, ref turningFactor, maxSpeed / 2.0f, filterPower);

                    Quaternion angularRotation = Quaternion.Euler(Vector3.forward * circlingAngularSpeed * Time.deltaTime);

                    circleAxis = angularRotation * circleAxis;
                    targetPos = circleAxis + (Vector3)camera2DS.GetLevelEndWorld();

                    targetRotation = Quaternion.LookRotation(Vector3.forward, targetPos - transform.position);

                    Vector3 targetVec = targetRotation * Vector3.up;
                    float turningAngle = Vector3.Angle(transform.up, targetVec); ;

                    turningFactor = turningAngle < 180 ? -Mathf.Min(turningAngle / 90.0f, 1.0f) : Mathf.Min((turningAngle - 180) / 90.0f, 1.0f);
                }
                break;
            case CursorState.FOLLOWING_FINGER:
                {
                    brushPath.UpdateTargetPos(transform.position, ref targetPos, ref targetRotation, ref turningFactor, CurrentMaxSpeed / 2.0f, filterPower);
                    amountMoved = Mathf.Abs(transform.position.magnitude - targetPos.magnitude);

                    //  remove( amountMoved * max speed / 4);
                }
                break;
            case CursorState.TRAVELLING_TO_CENTRE:
                {
                    targetPos = camera2DS.GetLevelEndWorld();
                    targetRotation = Quaternion.LookRotation(Vector3.forward, targetPos - transform.position);

                    Vector3 targetVec = targetRotation * Vector3.up;
                    float turningAngle = Vector3.Angle(transform.up, targetVec); ;

                    turningFactor = turningAngle < 180 ? -Mathf.Min(turningAngle / 90.0f, 1.0f) : Mathf.Min((turningAngle - 180) / 90.0f, 1.0f);
                }
                break;
            case CursorState.CIRCLING:
                {
                    Quaternion angularRotation = Quaternion.Euler(Vector3.forward * circlingAngularSpeed * Time.deltaTime);

                    circleAxis = angularRotation * circleAxis;
                    targetPos = circleAxis + (Vector3)camera2DS.GetLevelEndWorld();

                    targetRotation = Quaternion.LookRotation(Vector3.forward, targetPos - transform.position);

                    Vector3 targetVec = targetRotation * Vector3.up;
                    float turningAngle = Vector3.Angle(transform.up, targetVec); ;

                    turningFactor = turningAngle < 180 ? -Mathf.Min(turningAngle / 90.0f, 1.0f) : Mathf.Min((turningAngle - 180) / 90.0f, 1.0f);
                }
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        CaptureTargets();
        //ApplyNoFilter();
        ApplyAlphaFilter();
    }

    void LateUpdate()
    {

        if (currentState == CursorState.TRAVELLING_TO_CENTRE)
        {
            travellingTimer += Time.deltaTime;

            if ((targetPos - transform.position).magnitude <= circlingDistance || travellingTimer > 10.0f)
            {
                travellingTimer = 0;
                LevelManager.NotifyCursorCentred();
            }


        }
        else if (currentState == CursorState.FOLLOWING_INTRO)
        {
            if (introPath.pathFinished)
            {
                LevelManager.NotifyIntroFinished();
            }
            else
            {
                birdScaleTimer += Time.deltaTime;
                float currentBirdScale = Interpolate.Ease(Interpolate.EaseType.Linear)(birdScale, targetBirdScale - birdScale, birdScaleTimer, birdIntroScaleDuration);

                birdObject.transform.localScale = new Vector3(currentBirdScale, currentBirdScale, currentBirdScale);

                if (birdScaleTimer < birdIntroScaleDuration)
                {
                    IsDiving = true;
                }
                else
                {
                    IsDiving = false;
                }
            }
        }
        else if (currentState == CursorState.FOLLOWING_OUTRO)
        {
            //if (introPath.pathFinished)
            //{
            //    LevelManager.NotifyIntroFinished();
            //}
            //else
            //{
                birdScaleTimer += Time.deltaTime;
                float currentBirdScale = Interpolate.Ease(Interpolate.EaseType.Linear)(birdScale, targetBirdScale - birdScale, birdScaleTimer, birdOutroScaleDuration);
                
                birdObject.transform.localScale = new Vector3(currentBirdScale, currentBirdScale, currentBirdScale);

                if (birdScaleTimer < birdIntroScaleDuration)
                {
                    IsLooping = true;
                }
                else
                {
                    IsLooping = false;
                }
            //}
        }
        else if (currentState == CursorState.FOLLOWING_FINGER)
        {
            if (IsSpinningOut)
            {
                if (brushPath.IsPathFinished)
                {
                    brushPath.canDraw = true;
                    IsSpinningOut = false;
                }
            }
        }

        if (speedBoostTimer < speedBoostDuration)
        {
            speedBoostTimer += Time.deltaTime;
            if (speedBoostTimer >= speedBoostDuration)
            {
                speedMultiplier = 1.0f;
            }
        }

        // print(spriteCamera.transform.localPosition.ToString() + " " + spriteCamera.WorldToScreenPoint(transform.position).ToString() + "is Pos");
    }

    public void StartSpinOut(Vector3 origin)
    {
        if (currentState == CursorState.FOLLOWING_FINGER && !IsSpinningOut)
        {
            IsSpinningOut = true;
            brushPath.AddSpinOutPath(transform.position, (transform.position - origin).normalized, spinOutDistance, spinOutSpread);
        }
    }

   

    public void OnCameraWrap(Vector3 wrapDiff)
    {
        transform.position += (Vector3)wrapDiff;
        targetPos += (Vector3)wrapDiff;

        ParticleSystem[] particleSystems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem particleSystem in particleSystems)
        {
            ParticleSystem.Particle[] particles = new ParticleSystem.Particle[particleSystem.maxParticles];
            int particleCount = particleSystem.GetParticles(particles);

            for (int i = 0; i < particleCount; ++i)
            {
                particles[i].position += (Vector3)wrapDiff;
            }

            particleSystem.SetParticles(particles, particleCount);
        }
    }


    /// <summary>
    /// should increase the speed of the bird on the line for a duration of durationInSeconds before returning to normal speed
    /// </summary>
    /// <param name="percentageBoost"></param>
    /// <param name="durationInSeconds"></param>
 
    public void SetSpeedBoost(float percentageBoost, float durationInSeconds)
    {
        speedBoostDuration = durationInSeconds;
        speedMultiplier = percentageBoost;
        speedBoostTimer = 0.0f;
    }
}
