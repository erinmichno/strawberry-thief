﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CRPath
{
    public class SplinePointInfo
    {
        public Vector3 position;
        public Vector3 direction;
        public bool reachedEnd;
        public float turningFactor;
        public float distance;

        public SplinePointInfo()
        {
            position = Vector3.zero;
            direction = Vector3.zero;
            reachedEnd = false;
            turningFactor = 0.0f;
            distance = 0.0f;
        }

        public SplinePointInfo(Vector3 _position, Vector3 _direction, bool _reachedEnd = false, float _turningFactor = 0.0f, float _distance = 0.0f)
        {
            position = _position;
            direction = _direction;
            reachedEnd = _reachedEnd;
            turningFactor = _turningFactor;
            distance = _distance;
        }

        public SplinePointInfo Clone()
        {
            return new SplinePointInfo(position, direction, reachedEnd, turningFactor);
        }
    }

    public class CRPoint
    {
        public Vector3 point;
        public float length;
        public float distance;

        public CRPoint(Vector3 _point, float _length, float _distance)
        {
            point = _point;
            length = _length;
            distance = _distance;
        }
    }

    public class Iterator
    {
        public int justPassedPoint = 1;
        public float t = 0.0f;
        public float distance = 0.0f;

        public void Reset()
        {
            justPassedPoint = 1;
            t = 0.0f;
        }
    }

    private float totalLength = 1;

    // TODO: publicize tau value?
    private float tau = 0.5f;

    private Iterator internalIterator = new Iterator();

    private List<CRPoint> controlPoints = new List<CRPoint>();

    private const float END_POINT_DISTANCE = 0.1f;
    // TODO: publicize?
    private const int SEGMENT_SLICES = 10;

    bool isCapped = false;

    public bool Empty
    {
        get
        {
            if (controlPoints.Count < 5)
            {
                return true;
            }

            return false;
        }
    }

    public Vector3 FirstPoint
    {
        get
        {
            if (controlPoints.Count < 5)
            {
                return Vector3.zero;
            }

            return controlPoints[1].point;
        }
    }

    public float TotalLength
    {
        get
        {
            return totalLength;
        }
    }

    /// <summary> Adds a control point </summary>
    /// <param name="point"> Point to add </param>
    /// <returns> true if it generates a new segment, false otherwise </returns>
    public bool AddControlPoint(Vector3 point)
    {
        if (isCapped)
        {
            Debug.LogWarning("Cannot add further points to a capped CRPath");
            return false;
        }

        if (controlPoints.Count == 0)
        {
            // add the control point
            controlPoints.Add(new CRPoint(point, 0.0f, 0.0f));

            return false;
        }
        else if (controlPoints.Count == 1)
        {
            // add the control point
            controlPoints.Add(new CRPoint(point, 0.0f, 0.0f));
            
            // insert the first control point
            controlPoints.Insert(0, new CRPoint(controlPoints[0].point + (controlPoints[0].point - controlPoints[1].point).normalized * END_POINT_DISTANCE, 0.0f, 0.0f));
            
            // add the end control point
            controlPoints.Add(new CRPoint(controlPoints[controlPoints.Count - 1].point + (controlPoints[controlPoints.Count - 1].point - controlPoints[controlPoints.Count - 2].point).normalized * END_POINT_DISTANCE, 0.0f, 0.0f));
        }
        else
        {
            // replace the endpoint
            controlPoints[controlPoints.Count - 1] = new CRPoint(point, 0.0f, 0.0f);

            // add new endpoint
            controlPoints.Add(new CRPoint(controlPoints[controlPoints.Count - 1].point + (controlPoints[controlPoints.Count - 1].point - controlPoints[controlPoints.Count - 2].point).normalized * END_POINT_DISTANCE, 0.0f, 0.0f));
        }

        if (controlPoints.Count <= 4)
        {
            return false;
        }

        // TODO: should only really have to set / add the length for newly generated segments...
        // recalculate path length
        //CalculateLength();
        UpdateLength();

        return true;
    }

    /// <summary> Adds a final control point and generates the final spline segment </summary>
    /// <param name="point"> Point to add </param>
    /// <returns> true if it generates a new segment, false otherwise (on warning, should always generate) </returns>
    public bool AddEndCapControlPoint(Vector3 point)
    {
        isCapped = true;

        if (controlPoints.Count == 0)
        {
            Debug.LogWarning("Cannot cap a CRPath with zero points");
            return false;
        }
        else if (controlPoints.Count <= 3)
        {
            // add the control point and a duplicate of the previous control point to allow for a minimum of five points for a capped spline
            if(controlPoints.Count == 1)
            {
                controlPoints.Add(controlPoints[0]);
            }
            controlPoints.Add(new CRPoint(point, 0.0f, 0.0f));

            // insert the first control point
            controlPoints.Insert(0, new CRPoint(controlPoints[0].point + (controlPoints[0].point - controlPoints[1].point).normalized * END_POINT_DISTANCE, 0.0f, 0.0f));

            // add the end control point
            controlPoints.Add(new CRPoint(controlPoints[controlPoints.Count - 1].point + (controlPoints[controlPoints.Count - 1].point - controlPoints[controlPoints.Count - 2].point).normalized * END_POINT_DISTANCE, 0.0f, 0.0f));
        }
        else
        {
            // replace the endpoint
            controlPoints[controlPoints.Count - 1] = new CRPoint(point, 0.0f, 0.0f);

            // add new endpoint
            controlPoints.Add(new CRPoint(controlPoints[controlPoints.Count - 1].point + (controlPoints[controlPoints.Count - 1].point - controlPoints[controlPoints.Count - 2].point).normalized * END_POINT_DISTANCE, 0.0f, 0.0f));
        }

        // recalculate path length
        //CalculateLength();
        UpdateLength();

        return true;
    }

    private void CalculateLength()
    {
	    int pointCount = controlPoints.Count;
	    if(pointCount < 4)
	    {
		    throw new UnityException("CalculateLength called without enough points for CRPath");
	    }

		//do path distance correctly
		controlPoints[0].length = 0;

        totalLength = 0.0f;

        int lastPathPoint = isCapped ? pointCount - 2 : pointCount - 3;

        for (int i = 1; i < lastPathPoint; ++i)
		{
			CRPoint cpt0 = controlPoints[i - 1];
			CRPoint	cpt1 = controlPoints[i];
			CRPoint	cpt2 = controlPoints[i + 1];
			CRPoint	cpt3 = controlPoints[i + 2];

			SplinePointInfo[] segments = new SplinePointInfo[SEGMENT_SLICES + 1];
			float distances = 0.0f;
			for(int j = 0; j < SEGMENT_SLICES + 1; ++j)
			{
				float tval = (float)(j) / SEGMENT_SLICES;

				segments[j] = GetPointOnPath(cpt0.point, cpt1.point, cpt2.point, cpt3.point, tval, tau);
				if(j > 1)
				{
					distances += (segments[j].position - segments[j - 1].position).magnitude;
				}
			}
            controlPoints[i].distance = totalLength;
            controlPoints[i].length = distances;

            totalLength += distances;
		}
    }

    private void UpdateLength()
    {
        int pointCount = controlPoints.Count;
        if (pointCount < 4)
        {
            throw new UnityException("CalculateLength called without enough points for CRPath");
        }

        //do path distance correctly
        controlPoints[0].length = 0;

        int lastPathPoint = isCapped ? pointCount - 2 : pointCount - 3;
        int startPathPoint = isCapped ? lastPathPoint - 2 : lastPathPoint - 1;

        for (int i = startPathPoint; i < lastPathPoint; ++i)
        {
            CRPoint cpt0 = controlPoints[i - 1];
            CRPoint cpt1 = controlPoints[i];
            CRPoint cpt2 = controlPoints[i + 1];
            CRPoint cpt3 = controlPoints[i + 2];

            SplinePointInfo[] segments = new SplinePointInfo[SEGMENT_SLICES + 1];
            float distances = 0.0f;
            for (int j = 0; j < SEGMENT_SLICES + 1; ++j)
            {
                float tval = (float)(j) / SEGMENT_SLICES;

                segments[j] = GetPointOnPath(cpt0.point, cpt1.point, cpt2.point, cpt3.point, tval, tau);
                if (j > 1)
                {
                    distances += (segments[j].position - segments[j - 1].position).magnitude;
                }
            }
            controlPoints[i].distance = totalLength;
            controlPoints[i].length = distances;

            totalLength += controlPoints[i].length;
        }
    }

    public void Clear()
    {
        controlPoints.Clear();
        isCapped = false;
        totalLength = 0.0f;

        ResetInterpolation();
    }

    public void ResetInterpolation()
    {
        internalIterator.Reset();
    }

    public SplinePointInfo Update(float speed, float deltaTime, Iterator pathIterator = null)
    {
        int pointCount = controlPoints.Count;

        if (pathIterator == null)
        {
            pathIterator = internalIterator;
        }

        int minControlPoints = isCapped ? 4 : 5;

        if (controlPoints.Count <= minControlPoints)
        {
            return null;
        }

        if (pathIterator.justPassedPoint < 1)
        {
            pathIterator.justPassedPoint = 1;
            pathIterator.t = 0.0f;
        }

        bool reachedEnd = false;

        int maxControlPoint = isCapped ? pointCount - 3 : pointCount - 4;

        if (pathIterator.justPassedPoint > maxControlPoint)
        {
            pathIterator.justPassedPoint = maxControlPoint;
            pathIterator.t = 1.0f;
            pathIterator.distance = totalLength;
            reachedEnd = true;
        }

        if (!reachedEnd)
        {
            pathIterator.t += speed / controlPoints[pathIterator.justPassedPoint].length * deltaTime;

            while (pathIterator.t >= 1.0f)
            {
                pathIterator.justPassedPoint++;
                pathIterator.t -= 1.0f;

                if (pathIterator.justPassedPoint > maxControlPoint)
                {
                    pathIterator.justPassedPoint = maxControlPoint;
                    pathIterator.t = 1.0f;
                    pathIterator.distance = totalLength;
                    reachedEnd = true;
                    break;
                }
            }

            if (!reachedEnd)
            {
                pathIterator.distance = controlPoints[pathIterator.justPassedPoint].distance + controlPoints[pathIterator.justPassedPoint].length * pathIterator.t;
            }
        }

        CRPoint cpt0 = controlPoints[pathIterator.justPassedPoint - 1];
        CRPoint cpt1 = controlPoints[pathIterator.justPassedPoint];
        CRPoint cpt2 = controlPoints[pathIterator.justPassedPoint + 1];
        CRPoint cpt3 = controlPoints[pathIterator.justPassedPoint + 2];

        SplinePointInfo interpPoint = GetPointOnPath(cpt0.point, cpt1.point, cpt2.point, cpt3.point, pathIterator.t, tau);
        interpPoint.distance = pathIterator.distance;

        Vector3 previousDirection = GetPointOnPath(cpt0.point, cpt1.point, cpt2.point, cpt3.point, 0.0f, tau).direction;
        Vector3 nextDirection = GetPointOnPath(cpt0.point, cpt1.point, cpt2.point, cpt3.point, 1.0f, tau).direction;

        interpPoint.turningFactor = Mathf.Min(1.0f - Vector3.Dot(previousDirection, nextDirection), 1.0f) * Mathf.Sign(Vector3.Cross(previousDirection, nextDirection).z);

        interpPoint.reachedEnd = reachedEnd;

        return interpPoint;
    }

    public static SplinePointInfo GetPointOnPath(Vector3 c0, Vector3 c1, Vector3 c2, Vector3 c3, float tVal, float tau)
    {
        float t2 = tVal * tVal;
        float t3 = tVal * t2;
        Vector3 position = Vector3.zero;
        Vector3 direction = Vector3.zero;
        Vector3 gradient = Vector3.zero;

        //1 + t + t^2 +t^3
        position =  c1 +
                    (-tau * c0 + tau * c2) * tVal +
                    (2 * tau * c0 + (tau - 3) * c1 + (3 - 2 * tau) * c2 + -tau * c3) * t2 +
                    (-tau * c0 + (2 - tau) * c1 + (tau - 2) * c2 + tau * c3) * t3;

        direction = (-tau * c0 + tau * c2) +
                    (2 * tau * c0 + (tau - 3) * c1 + (3 - 2 * tau) * c2 + -tau * c3) * 2.0f * tVal +
                    (-tau * c0 + (2 - tau) * c1 + (tau - 2) * c2 + tau * c3) * 3.0f * t2;
        
        direction.Normalize();

        return new SplinePointInfo(position, direction);
    }

    public List<SplinePointInfo> GetLastSegmentPoints(float segmentSpacing)
    {
        if (controlPoints.Count < 5)
        {
            return null;
        }

        List<SplinePointInfo> splinePoints = new List<SplinePointInfo>();

        int cPoint = controlPoints.Count - 4;

        for (int i = cPoint; i < cPoint + ((isCapped) ? 2 : 1) ; ++i)
        {
            CRPoint cpt0 = controlPoints[i - 1];
            CRPoint cpt1 = controlPoints[i];
            CRPoint cpt2 = controlPoints[i + 1];
            CRPoint cpt3 = controlPoints[i + 2];

            int segmentCount = (int)Mathf.Floor(cpt1.length / segmentSpacing);

            for (int j = 0; j < segmentCount; ++j)
            {
                float tval = (float)(j + 1) / segmentCount;

                SplinePointInfo pointInfo = GetPointOnPath(cpt0.point, cpt1.point, cpt2.point, cpt3.point, tval, tau);
                pointInfo.distance = cpt1.distance + cpt1.length * tval;

                splinePoints.Add(pointInfo);
            }
        }

        // TODO: also have to add the first point if first segment in spline

        return splinePoints;
    }

    public void DebugDraw(float boxSize = 0.005f, float tangentLength = 0.05f)
    {
        ResetInterpolation();

        CRPath.SplinePointInfo pointInfo = new CRPath.SplinePointInfo();
        CRPath.SplinePointInfo lastPointInfo = null;

        while (pointInfo != null && !pointInfo.reachedEnd)
        {
            pointInfo = Update(0.005f, 1.0f);

            if (lastPointInfo != null)
            {
                Debug.DrawLine(lastPointInfo.position, pointInfo.position, Color.white);
            }

            lastPointInfo = pointInfo;
        }

        for (int i = 0; i < controlPoints.Count; ++i)
        {
            if (boxSize > 0.0f)
            {
                Color boxColor = Color.yellow;

                Vector3 BL = controlPoints[i].point - new Vector3(-boxSize, -boxSize);
                Vector3 TL = controlPoints[i].point - new Vector3(-boxSize, boxSize);
                Vector3 BR = controlPoints[i].point - new Vector3(boxSize, -boxSize);
                Vector3 TR = controlPoints[i].point - new Vector3(boxSize, boxSize);

                Debug.DrawLine(BL, TL, boxColor);
                Debug.DrawLine(TL, TR, boxColor);
                Debug.DrawLine(TR, BR, boxColor);
                Debug.DrawLine(BR, BL, boxColor);
            }


            if (tangentLength > 0.0f && i != 0 && i < controlPoints.Count - 1)
            {
                float tVal = (i != controlPoints.Count - 2) ? 0.0f : 1.0f;

                pointInfo = null;

                if (i != controlPoints.Count - 2)
                {
                    pointInfo = GetPointOnPath(controlPoints[i - 1].point, controlPoints[i].point, controlPoints[i + 1].point, controlPoints[i + 2].point, 0.0f, tau);
                }
                else
                {
                    pointInfo = GetPointOnPath(controlPoints[i - 2].point, controlPoints[i - 1].point, controlPoints[i].point, controlPoints[i + 1].point, 1.0f, tau);
                }

                Color tangentColor = Color.green;

                Vector3 tangentVector = pointInfo.direction.normalized * tangentLength;
                Vector3 p1 = pointInfo.position - tangentVector;
                Vector3 p2 = pointInfo.position + tangentVector;

                Debug.DrawLine(p1, p2, tangentColor);
            }
        }
    }
}
