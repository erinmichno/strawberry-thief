﻿using UnityEngine;
using System.Collections;

public class ParticleScaler : MonoBehaviour
{
    public float particleScale = 1.0f;

	void Awake()
    {
        if (!Mathf.Approximately(particleScale, 1.0f))
        {
            ParticleSystem[] pSystems = GetComponentsInChildren<ParticleSystem>();

            foreach (ParticleSystem pSystem in pSystems)
            {
                pSystem.startSize *= particleScale;
            }
        }
	}
}
