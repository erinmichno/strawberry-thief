using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{

    private GreenHealthBar greenBar = null;
    private RedHealthBar redBar = null;

    private int currentPollen;
    private int maxPollen = 100;
    Bird bird;

    void Start()
    {

        bird = GameObject.FindGameObjectWithTag("Bird").GetComponent<Bird>();
        maxPollen = bird.MaxPollenAmount;

        greenBar = GetComponentInChildren<GreenHealthBar>();
        redBar = GetComponentInChildren<RedHealthBar>();

        currentPollen = maxPollen;

        UpdateBars(1.0f);
    }

    void Update()
    {
		if (currentPollen != bird.PollenLinearAmount)
        {
			currentPollen = (int)bird.PollenLinearAmount;
            //print("pollen and max:" + currentPollen.ToString() + " : " + maxPollen);
           // print (bird.GetPollen());
            UpdateBars(Mathf.Max(bird.GetPollen(), 0.0f));
        }

        
    }

    private void UpdateBars(float healthRatio)
    {
        Vector3 scale = greenBar.transform.localScale;
        scale.x = 0.2f * healthRatio;
        greenBar.transform.localScale = scale;

        scale = redBar.transform.localScale;
        scale.x = 0.2f * (1.0f - healthRatio);
        redBar.transform.localScale = scale;

        Vector3 position = greenBar.transform.localPosition;
        position.x = 1.0f - healthRatio;
        greenBar.transform.localPosition = position;

        position = redBar.transform.localPosition;
        position.x = -healthRatio;
        redBar.transform.localPosition = position;
    }
}
