﻿using UnityEngine;
using System.Collections;

public class FollowCursor : MonoBehaviour {

    GameObject cursorGO;
    Camera currentCamera;
    Camera spriteCamera;
    public Vector2 offset = new Vector2(0, 0);
 //   ParticleSystem particleSystemref;
	// Use this for initialization
	void Start () {
        cursorGO = GameObject.FindGameObjectWithTag("Cursor");
        spriteCamera = GameObject.FindGameObjectWithTag("SpriteCamera").GetComponent<Camera>();
        currentCamera = GameObject.FindGameObjectWithTag("Camera3D").camera;
      //  particleSystemref = GetComponent<ParticleSystem>();
	}

    
	
	// Update is called once per frame
	void Update ()
    {
	    if(cursorGO != null)
        {
            //cursor.pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            //The z position is in world units from the camera.
            Vector3 pos = spriteCamera.WorldToScreenPoint(cursorGO.transform.position);
            Vector3 cursorpos = pos;
           // print ("cursor pos:"+  pos.z.ToString());
        //    print("cursor: " + cursorGO.transform.position.ToString() + "pos sp" + pos);
            pos.z = transform.position.z;
            pos = currentCamera.ScreenToWorldPoint(pos);
        
            Quaternion rotation = cursorGO.transform.rotation;

            //TURN BACK ON FOR ROATE
           // transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, 0.5f); // (targetRotation - transform.rotation) * 0.05f;
          
            pos.z = transform.position.z;
            pos.x += offset.x;
            pos.y += offset.y;
            transform.position = pos;


            Plane depthPlane = new Plane(-currentCamera.transform.forward, new Vector3(0, 0, transform.position.z));
            if (true)
            {
                Ray ray = currentCamera.ScreenPointToRay(cursorpos);
                float rayDistance;
                if (depthPlane.Raycast(ray, out rayDistance))
                {
                    pos = ray.GetPoint(rayDistance);
                    pos.x += offset.x;
                    pos.y += offset.y;
                    transform.position = pos;
                }

            }

        }

       
	}
}
