﻿using UnityEngine;
using System.Collections;

public class ButtonAnimationController : UISpriteAnimation
{
    public UIAtlas forwardAnimAtlasHiRes = null;
    public UIAtlas forwardAnimAtlasLoRes = null;
    private UIAtlas forwardAnimAtlas = null;
    public string forwardAnimPrefix = "";

    public UIAtlas reverseAnimAtlasHiRes = null;
    public UIAtlas reverseAnimAtlasLoRes = null;
    private UIAtlas reverseAnimAtlas = null;
    public string reverseAnimPrefix = "";

    public int fps = 30;

    private bool buttonPressed = false;
    private bool playReverseOnEnd = false;
    private bool animPlaying = false;

    protected override void Start()
    {
        mSprite = GetComponent<UISprite>();

        if (ResolutionManager.IsHighResDevice)
        {
            mSprite.atlas = forwardAnimAtlasHiRes;
            //reverseAnimAtlas = reverseAnimAtlasHiRes;
        }
        else
        {
            mSprite.atlas = forwardAnimAtlasLoRes;
            //reverseAnimAtlas = reverseAnimAtlasLoRes;
        }

        mFPS = fps;
        mLoop = false;

        //SwitchAtlas(true);

        base.Start();

        mActive = false;
        mSprite.enabled = false;
	}

    protected override void Update()
    {
        base.Update();

        if (animPlaying && !isPlaying)
        {
            animPlaying = false;
            mSprite.enabled = false;
        }
    }

    //private void SwitchAtlas(bool forward)
    //{
    //    if (forward)
    //    {
    //        mSprite.atlas = forwardAnimAtlas;
    //        mPrefix = forwardAnimPrefix;
    //    }
    //    else
    //    {
    //        mSprite.atlas = reverseAnimAtlas;
    //        mPrefix = reverseAnimPrefix;
    //    }

    //    RebuildSpriteList();
    //}

    private void PlayAnimation(bool forward)
    {
        mSprite.enabled = true;
        Reset();
        animPlaying = true;

        //SwitchAtlas(forward);

        //if (isPlaying)
        //{
        //    // flip the current index and delta
        //    mIndex = (mSpriteNames.Count - 1) - mIndex;

        //    float rate = 1f / mFPS;
        //    mDelta = rate - mDelta;

        //    // update the sprite
        //    mSprite.spriteName = mSpriteNames[mIndex];
        //    mSprite.MakePixelPerfect();
        //}
        //else
        //{
        //    Reset();
        //}
    }

    public void OnPress()
    {
        
        //if (!buttonPressed)
        //{
        //    buttonPressed = true;

        //    if (!playReverseOnEnd)
        //    {
        //        PlayAnimation(true);
        //    }

        //    playReverseOnEnd = false;
        //}
    }

    public void OnClick()
    {
        if (!animPlaying)
        {
            PlayAnimation(true);
        }
        //if (buttonPressed)
        //{
        //    buttonPressed = false;

        //    if (isPlaying)
        //    {
        //        playReverseOnEnd = true;
        //    }
        //    else
        //    {
        //        PlayAnimation(false);
        //    }
        //}
    }

    public void OnDragOut()
    {
        //if (buttonPressed)
        //{
        //    buttonPressed = false;

        //    PlayAnimation(false);
        //}
    }
}
