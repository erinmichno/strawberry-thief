﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class MovingOval : MonoBehaviour 
{
	public float Radius = 200;
	public float ScaleX = 1.0f;
	private float theta = 0.0f;
	public float AngularVel = 45;
	public Vector3 centrePoint = Vector3.zero;
	private Vector3 vec = new Vector3(0, 1, 0);
	CameraWrapTransform wrapping = null;
	
	// Use this for initialization
	void Start () {
		wrapping = GetComponent<CameraWrapTransform>();
	}
	
	
	
	// Update is called once per frame
	void Update () 
	{
		theta += AngularVel*Time.deltaTime;
		if(theta > 360) { theta -= 360;}
		Vector3 v = Quaternion.AngleAxis(theta, Vector3.forward)*vec;
		v.x *= Radius*ScaleX;
		v.y *= Radius;
		v.z = transform.localPosition.z;
		
		transform.localPosition = v + centrePoint;
		if(wrapping != null)
		{
			wrapping.originalPos = transform.localPosition;
		}
	}
}
