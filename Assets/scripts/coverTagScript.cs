﻿using UnityEngine;
using System.Collections;



public class coverTagScript : MonoBehaviour {

    Animator animator = null;    
   
	// Use this for initialization
	void Awake () {
        animator = GetComponent<Animator>();
          

        
        
	
	}

    void OnLevelWasLoaded(int level)
    {
        if(level ==1)
        {
            print("in LEVEL 1");
            animator.SetTrigger("LevelChange");
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        
        
	    
	}

    public void ReachedEndOfAnimation()
    {
        AnimatorStateInfo info = animator.GetCurrentAnimatorStateInfo(0);
        
        print("Reached to the end: " + info.ToString());
        SceneLoader sceneLoader = GetComponent<SceneLoader>();
        if(sceneLoader)
        {
           // sceneLoader.NextScene = "1Cover";
            sceneLoader.OnSceneExit();
            
        }
        else
        {
            animator.SetTrigger("LevelChange");
        }
    }
}
