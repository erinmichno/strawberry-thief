﻿using UnityEngine;
using System.Collections;

public class BrushAdapter : MonoBehaviour {


    float targetBrushSize;
    float defaultBrushSize;
    float currentBrushSize;
    float timer = 0.0f;
    float timeOfBrushLarge = 5.0f;
    public float MaxPercentage = 5.0f;
    BlendPollen blendPollen = null;

    public static float ButterflyBrushSizePercentage = 2.5f;
    public static float BeetleBrushSizePercentage = 1.5f;
    public static float BeeBrushSizePercentage = 1.5f;

    public static float ButterflyBoostTime = 2.0f;
    public static float BeetleBoostTime = 4.0f;
    public static float BeeBoostTime = 5.0f;

	// Use this for initialization
	void Start () {

       blendPollen =  GetComponent<BlendPollen>();
        LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnCursorCentred += OnCursorCentred;
        LevelManager.OnNewLevelStarted += OnNewLevelStarted;

        OnNewLevelStarted();
        currentBrushSize = targetBrushSize = defaultBrushSize;
        blendPollen.SetBrushSize((int)currentBrushSize);
        
      
	}

    public void SetTargetBrushPercentage(float percentage)
    {
        targetBrushSize = currentBrushSize * percentage;
        if(targetBrushSize > MaxPercentage*defaultBrushSize)
        {
            targetBrushSize = MaxPercentage*defaultBrushSize;
        }
    }

    public void SetTargetBrushAndTime(float percentage, float timeLarge)
    {
        timeOfBrushLarge = timeLarge;
        SetTargetBrushPercentage(percentage);
        timer = 0.0f;
        print("taret" + targetBrushSize);
    }

    public void SetTargetsBasedOnType(BugScript.BugType t)
    {
        float percentage = 1.0f;
        float timeLarge = 1.0f;
        switch (t)
        {
            case BugScript.BugType.BEE:
                percentage = BeeBrushSizePercentage;
                timeLarge = BeeBoostTime;

                break;
            case BugScript.BugType.BEETLE:
                percentage = BeetleBrushSizePercentage;
                timeLarge = BeetleBoostTime;
                break;
            case BugScript.BugType.BUTTERFLY:
                percentage = ButterflyBrushSizePercentage;
                timeLarge = ButterflyBoostTime;
                break;
        }
        SetTargetBrushAndTime(percentage, timeLarge);
    }


     private void OnLevelComplete()
    {
        
    }

    private void OnCursorCentred()
    {
       
    }

    private void OnNewLevelStarted()
    {
        //Graphics.Blit(baseTexture, pingRT);
        //Graphics.Blit(baseTexture, pongRT);
       LevelManager.LevelData ld =  LevelManager.GetCurrentLevelData();
      defaultBrushSize =  ld.brushSize;
      targetBrushSize = defaultBrushSize;
      timer = 0.0f;
      
    }
	
	// Update is called once per frame
	void Update ()
    {
        timer += Time.deltaTime;
        if(timer > timeOfBrushLarge)
        {
            targetBrushSize = defaultBrushSize;
        }

        float targetLine = targetBrushSize - currentBrushSize;
        currentBrushSize += targetLine * 4.5f * Time.deltaTime;//add max step
       // print("currentBrush" + currentBrushSize);
        blendPollen.SetBrushSize((int)currentBrushSize);
	}
}
