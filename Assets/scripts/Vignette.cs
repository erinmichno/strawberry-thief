﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Vignette : MonoBehaviour {

	Material vignetteMat;
	public Shader vignetteShader;
	public Color col = new Color(0.051f,0.031f,0.024f,1.0f);
	public float distanceIn = 0.25f;
    public float distanceOut = 0.5f;
    public float speed = 2;

    private float targetDistance = 0.5f;
    private float distance = 0.5f;
    Camera uiCamera = null;
    int cullingMask = 0;
    int nothingMask = 0;
    bool renderVin = false;
    List<GameObject> panels = new List<GameObject>();
    public SpriteRenderer hourglass = null;
    public void OnFadeInFinished()
    {
         
      //  uiCamera = GameObject.FindGameObjectWithTag("UICamera").GetComponent<Camera>();
      //  distance = 0.5f;
        targetDistance = distanceIn;
        if (hourglass != null)
        {

            hourglass.color = Color.white;
        }
        
        uiCamera.cullingMask = nothingMask;
        ColorUI(emptyColor);
       // cullingMask = uiCamera.cullingMask;
        
        
    }
	// Use this for initialization


    void Awake()
    {
       
      
    }
	void Start () 
	{
        renderVin = true;
        uiCamera = GameObject.FindGameObjectWithTag("UICamera").GetComponent<Camera>();
        UIScreenFade fade = UIScreenFade.GetScreenFadeFromBlack(2.0f, new EventDelegate(OnFadeInFinished));
		vignetteMat = new Material(vignetteShader);
        LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnIntroFinished += OnIntroFinished;
        LevelManager.OnNewLevelStarted += OnNewLevelStarted;
        distance = targetDistance = 0.8f;
        targetDistance = distanceIn;




       cullingMask = uiCamera.cullingMask;
      // nothingMask = (1 << LayerMask.NameToLayer("Nothing"));
       nothingMask = cullingMask;
      // uiCamera.cullingMask = nothingMask;
       panels.AddRange(GameObject.FindGameObjectsWithTag("GameGUI"));
       panels.Add(GameObject.FindGameObjectWithTag("FillGUI"));
       ColorUI(emptyColor);
        if (hourglass != null)
        {

           // hourglass.color = Color.white;
        }
     //  panel.SetActive(false);
       
	}

    	private void OnLevelComplete()
	{
        targetDistance = distanceIn;
        uiCamera.cullingMask = nothingMask;
        ColorUI(emptyColor);
        if (hourglass != null)
        {

            hourglass.color = Color.white;
        }
	}

        static Color emptyColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);
    void ColorUI(Color c)
    {
    foreach (GameObject g in panels)
        {
            UIWidget widget = g.GetComponent<UIWidget>();
            if (widget != null)
            {
                widget.color = c;
            }
        }
}
        private void OnIntroFinished()
	{
        targetDistance = distanceOut;

        uiCamera.cullingMask = cullingMask;
        ColorUI(Color.white);
            if(hourglass != null)
            {

                hourglass.color = emptyColor;
            }
	}

    private void OnNewLevelStarted()//This is the reset
    {
        targetDistance = distanceOut;
        uiCamera.cullingMask = cullingMask;
        ColorUI(Color.white);
        if (hourglass != null)
        {

            hourglass.color = emptyColor;
        }

    }
	
	// Update is called once per frame
	void Update () {

        float targetLine = targetDistance - distance;
        distance += targetLine * speed * Time.deltaTime;//add max step
        
	
	}
	
	void OnPostRender()
	{
        if (renderVin)
        {
            MakeVignette();
        }
	}
		
		
	void MakeVignette()
	{
		// camera.targetTexture = finalRT;
		// RenderTexture.active = finalRT;
		camera.targetTexture = null;
		RenderTexture.active = null; 
		
		vignetteMat.SetColor("_Color", col); //might need lastframe bright texture
		vignetteMat.SetFloat("_Distance", distance);
	
		// Draw a quad over the whole screen with the above shader
		BlendPollen.GLRenderThatMat(vignetteMat);
		camera.targetTexture = null;
		RenderTexture.active = null; // JC: added to avoid errors
		
	}
}
