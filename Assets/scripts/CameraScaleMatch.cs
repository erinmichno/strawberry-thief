﻿ using UnityEngine;
using System.Collections;

public class CameraScaleMatch : MonoBehaviour
{
    public Camera scalingCamera = null;

    void Awake()
    {
        if (scalingCamera == null)
        {
            Debug.LogWarning("No camera set for scale matching");
        }
    }
	
	void Update ()
    {
        if (scalingCamera != null)
        {
            transform.localScale = new Vector3(scalingCamera.orthographicSize, scalingCamera.orthographicSize, 1.0f);
        }
	}
}
