﻿using UnityEngine;
using System.Collections;

public class UIScreenFade : MonoBehaviour 
{
    //UI elements
    UITexture uiTexture;
    private TweenAlpha tweenAlpha;

    private bool destroyOnFinish = false;

    EventDelegate endDelegate;//The delegate that is to be called once the fade has been completed.

//    private GameObject previousSelectedObject = null; //Used to return focus to the pevious menu upon completion of the pop-up.

    // Use this for initialization
    void Awake()
    {
        tweenAlpha = GetComponent<TweenAlpha>();
        tweenAlpha.enabled = false;
        uiTexture = GetComponent<UITexture>();
    }

    public void Initialise(float from, float to, float duration, EventDelegate endFadeDelegate, bool _destroyOnFinish = true, float startDelay = 0.0f)
    {
        destroyOnFinish = _destroyOnFinish;

        tweenAlpha.from = from;
        tweenAlpha.to = to;
        tweenAlpha.duration = duration;
        tweenAlpha.delay = startDelay;
        uiTexture.alpha = from;

        if (endFadeDelegate != null)
        {
            endDelegate = endFadeDelegate;
            tweenAlpha.onFinished.Add(endDelegate);
        }
        if (destroyOnFinish)
        {
            tweenAlpha.onFinished.Add(new EventDelegate(OnFadeOut));
        }
        BeginFade();
    }

    public void BeginFade()
    {
        tweenAlpha.enabled = true;
    }

    void FadeIn()
    {
        tweenAlpha.onFinished.Add(new EventDelegate(OnFadeIn));
        tweenAlpha.PlayForward();
    }

    void FadeOut()
    {
        tweenAlpha.onFinished.Add(new EventDelegate(OnFadeIn));
        tweenAlpha.PlayForward();
    }

    public void OnFadeIn()
    {
       // endDelegate();
    }

    public void OnFadeOut()
    {
        Destroy();
    }

    void Destroy()
    {
        NGUITools.Destroy(gameObject);
    }

    public static UIScreenFade GetScreenFade(float from, float to, float duration, EventDelegate endDelegate, bool destroyOnFinish = true, float startDelay = 0.0f)
    {
        UIScreenFade screenFade = (Instantiate(Resources.Load("prefabs/FadeTexture")) as GameObject).GetComponent<UIScreenFade>();
        screenFade.GetComponent<UITexture>().SetDimensions(Screen.width, Screen.height);
        screenFade.Initialise(from, to, duration, endDelegate, destroyOnFinish, startDelay);
        return screenFade;
    }

    public static UIScreenFade GetScreenFadeToBlack(float duration, EventDelegate endDelegate, bool destroyOnFinish = true, float startDelay = 0.0f, bool addCollider = false)
    {
        UIScreenFade screenFade = (Instantiate(Resources.Load("prefabs/FadeTexture")) as GameObject).GetComponent<UIScreenFade>();
        UIStretch strech =  screenFade.gameObject.AddComponent<UIStretch>();
        strech.style = UIStretch.Style.Both;

        if(addCollider)
        {
            screenFade.gameObject.AddComponent<BoxCollider>();
            screenFade.GetComponent<UITexture>().ResizeCollider();
        }

        screenFade.Initialise(0f, 1f, duration, endDelegate, destroyOnFinish, startDelay);
        return screenFade;
    }

    public static UIScreenFade GetScreenFadeFromBlack(float duration, EventDelegate endDelegate, bool destroyOnFinish = true, float startDelay = 0.0f, bool addCollider = false)
    {
        UIScreenFade screenFade = (Instantiate(Resources.Load("prefabs/FadeTexture")) as GameObject).GetComponent<UIScreenFade>();
        UIStretch strech = screenFade.gameObject.AddComponent<UIStretch>();
        strech.style = UIStretch.Style.Both;

        if(addCollider)
        {
            screenFade.gameObject.AddComponent<BoxCollider>();
            screenFade.GetComponent<UITexture>().ResizeCollider();
        }

        screenFade.Initialise(1f, 0f, duration, endDelegate, destroyOnFinish, startDelay);
        return screenFade;
    }

}
