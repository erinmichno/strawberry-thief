﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TutorialScreenController : MonoBehaviour
{
    public List<UIPanel> tutorialPanels = new List<UIPanel>();
    
    private int currentPanel = 0;

    public SceneLoader previousSceneLoader = null;
    public SceneLoader nextSceneLoader = null;

    public UIButton nextButton = null;
    public UIButton tickButton = null;
    
    public float fadeDuration = 1.0f;

    void Awake()
    {
        if (previousSceneLoader == null || nextSceneLoader == null)
        {
            throw new UnityException("SceneLoaders not set up for TutorialScreenController");
        }

        if (tutorialPanels.Count == 0)
        {
            throw new UnityException("Must have at least one tutorial panel added to TutorialScreenController");
        }

        tutorialPanels[0].gameObject.SetActive(true);

        for (int i = 1; i < tutorialPanels.Count; ++i)
        {
            tutorialPanels[i].gameObject.SetActive(false);
        }
    }

    public void OnPreviousButtonClicked()
    {
        if (currentPanel == 0)
        {
            previousSceneLoader.OnSceneExit();
            return;
        }

        UIScreenFade.GetScreenFadeToBlack(fadeDuration, new EventDelegate(ActivatePreviousPanel), addCollider: true);
    }

    public void OnNextButtonClicked()
    {
        if (currentPanel == tutorialPanels.Count - 1)
        {
            nextSceneLoader.OnSceneExit();
            return;
        }

        UIScreenFade.GetScreenFadeToBlack(fadeDuration, new EventDelegate(ActivateNextPanel), addCollider: true);
    }
    
    private void ActivateNextPanel()
    {
        tutorialPanels[currentPanel].gameObject.SetActive(false);

        currentPanel++;

        if (currentPanel < tutorialPanels.Count - 1)
        {
            nextButton.gameObject.SetActive(true);
            tickButton.gameObject.SetActive(false);
        }
        else
        {
            nextButton.gameObject.SetActive(false);
            tickButton.gameObject.SetActive(true);
        }

        tutorialPanels[currentPanel].gameObject.SetActive(true);

        UIScreenFade.GetScreenFadeFromBlack(fadeDuration, null);
    }

    private void ActivatePreviousPanel()
    {
        tutorialPanels[currentPanel].gameObject.SetActive(false);

        currentPanel--;

        nextButton.gameObject.SetActive(true);
        tickButton.gameObject.SetActive(false);
        
        tutorialPanels[currentPanel].gameObject.SetActive(true);

        UIScreenFade.GetScreenFadeFromBlack(fadeDuration, null);
    }
}
