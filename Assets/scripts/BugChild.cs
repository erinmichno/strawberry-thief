﻿using UnityEngine;
using System.Collections;

public class BugChild : MonoBehaviour
{
    BugScript bugParent = null;

    void Start ()
    {
        bugParent = NGUITools.FindInParents<BugScript>(this.gameObject);
	}

    void OnTriggerEnter2D(Collider2D other)
    {
        bugParent.OnTriggerEnter2D(other);
    }
}
