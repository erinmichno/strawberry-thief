﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AnimationPath : MonoBehaviour
{
    public List<Transform> pathTransforms = new List<Transform>();

    public bool debugMode = false;

    private CRPath crPath = new CRPath();
    private CRPath.Iterator crIterator = new CRPath.Iterator();

    public bool pathFinished = false;

    private const float PATH_END_THRESHOLD = 0.1f;
    
	void Awake()
    {
        for(int i = 0; i < pathTransforms.Count - 1; ++i)
        {
            crPath.AddControlPoint(pathTransforms[i].position);
        }

        crPath.AddEndCapControlPoint(pathTransforms[pathTransforms.Count - 1].position);

        if (debugMode)
        {
            Debug.LogWarning("DEBUG DRAW OF ANIMATION PATH WILL TOTALLY MESS UP BIRD MOVEMENT...");
        }
	}

    void Update()
    {
        if (debugMode)
        {
            crPath = new CRPath();
            Awake();

            crPath.DebugDraw();
        }
    }

    private Vector2 PointToWorld(Vector2 point)
    {
        return point + (Vector2)transform.position;
    }

    public bool UpdateTargetPos(Vector3 position, ref Vector3 target, ref Quaternion targetRotation, ref float turningFactor, float maxSpeed, float filterPower)
    {
        if (crPath.Empty)
        {
            target = position;
            return false;
        }

        float pathLength = crPath.TotalLength - crIterator.distance;

        if (pathLength < PATH_END_THRESHOLD)
        {
            pathFinished = true;
        }

        float speed = pathLength * filterPower;

        if (speed > maxSpeed * Time.deltaTime)
        {
            speed = maxSpeed * Time.deltaTime;
        }

        CRPath.SplinePointInfo pointInfo = crPath.Update(speed, 1.0f, crIterator);

        if (pointInfo == null)
        {
            target = position;
            return false;
        }

        target = PointToWorld(pointInfo.position);
        targetRotation = Quaternion.LookRotation(Vector3.forward, pointInfo.direction);
        turningFactor = pointInfo.turningFactor;

        return true;
    }
}
