﻿using UnityEngine;
using System.Collections;

public class VolumeController : MonoBehaviour
{
    public Interpolate.EaseType easeType = Interpolate.EaseType.EaseInOutCubic;
    private Interpolate.Function easeFunction = null;
    public float fadeTime = 0.5f;
    private float muteTimer = 0.0f;

    private static float currentVolume = 1.0f;
    private static float targetVolume = 1.0f;

    public static bool muteState = false;

    public static bool muteFromPause = false;

    void Awake()
    {
        easeFunction = Interpolate.Ease(easeType);

        muteFromPause = false;

        muteTimer = fadeTime;
    }

    void Update()
    {
        if (muteTimer < fadeTime)
        {
            muteTimer += RealTime.deltaTime;
            currentVolume = easeFunction(muteState ? 1.0f : 0.0f, muteState ? -1.0f : 1.0f, muteTimer, fadeTime);
        }
        else
        {
            currentVolume = targetVolume;
        }

        AudioListener.volume = currentVolume;
    }

    public void ToggleMute()
    {
        if (Time.timeScale < 0.95f)
        {
            muteFromPause = !muteFromPause;

            return;
        }
        else
        {
            muteState = !muteState;
        }

        if (muteTimer < fadeTime)
        {
            muteTimer = fadeTime - muteTimer;
        }
        else
        {
            muteTimer = 0.0f;
        }

        targetVolume = muteState? 0.0f : 1.0f;
    }

    public void ToggleMuteFromPause()
    {
        if (Time.timeScale < 0.95f)
        {
            if (muteState == false)
            {
                muteFromPause = true;
                muteState = true;

                if (muteTimer < fadeTime)
                {
                    muteTimer = fadeTime - muteTimer;
                }
                else
                {
                    muteTimer = 0.0f;
                }

                targetVolume = 0.0f;

                return;
            }
        }
        else
        {
            if (muteState == true && muteFromPause == true)
            {
                muteFromPause = false;
                muteState = false;

                if (muteTimer < fadeTime)
                {
                    muteTimer = fadeTime - muteTimer;
                }
                else
                {
                    muteTimer = 0.0f;
                }

                targetVolume = 1.0f;

                return;
            }
        }
    }

    // This was good idea, but it's messing up the screen volume fades...
    //// just in case the next scene doesn't contain a volume controller, snap to target volume
    //void OnDestroy()
    //{
    //    currentVolume = targetVolume;
    //    AudioListener.volume = currentVolume;
    //}
}
