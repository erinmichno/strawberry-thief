﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BlendPollen : MonoBehaviour
{

    public Texture baseTexture;
    private RenderTexture pingRT;
    private RenderTexture pongRT;
    private RenderTexture currentPingPongRT;

    //Creates all the things needed for the blend map
    public Shader blendShader;
    private Material blendMaterial;
    public Shader clearShader;
    private Material clearMaterial;
    bool ping = true;
    private float uCoord;
    private float vCoord;
    public Color blendColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);
    public Texture brushTexture;
    public UITexture guiIcon;
    private float OriginalGUIIconHeight = 256;
    public Shader spritesTransShader;
   // Material spritesTransMaterial = null;
    bool useTrans = false;
    Material currentCompose;

    public bool UseTrans() { return useTrans; }
    
    public Texture GetCurrentBlendMap()
    {
   		 return currentPingPongRT;
    }


    //the final material / shader / textures  to take the textures and use the blend map
    Material composedMaterial;
    public Shader ComposedShader;
  //  public Texture outlineTexture;
   // public Texture colorTexture;
    
    Material transitionMaterial;
    public Shader transitionShader;

    Material BackwardTransitionMaterial;
    public Shader BackwardTransitionShader;

    Material BlitMaterial;
    public Shader blitShader;

    public static int BLEND_MAP_RES = 1024;

  //  bool firstCreation = true;
    Camera2DS cam2DS = null;
    
	Camera spriteCamera ;//= GameObject.FindGameObjectWithTag("SpriteCamera").GetComponent<Camera>();

    private int currentBrushSize = 32;

    public int brushSize = 32;
    public int brushHardness = 64;

    public void SetBrushSize(int _brushSize)
    {
        if(_brushSize < 2)
        {
            _brushSize = 2;
        }
        brushSize = _brushSize;
    }

    public List<List<float>> blendMapBools = new List<List<float>>();
    int blendMapWidth;
    float numBlendsFilledIn = 0.0f;
    float percentFilledIn = 0.0f;
   // float pollenLevel = 1.0f;
    public float pollenThreshold = 0.5f;
    public float percentageRequiredToComplete = 0.7f;

    public float transitionBlendMaxRadius = 5.0f;
    public Interpolate.EaseType blendInInterpolation = Interpolate.EaseType.Linear;
    
    Bird bird = null;

    private enum PaintingState
    {
        PAINTING,
        NONE,
        BLENDING_IN,
        BLENDING_OUT
    }

    private PaintingState currentState = PaintingState.PAINTING;

    private float blendTimer = 0.0f;

    void WriteToBoolMap(Vector2 uv)
    {
        float u = uv.x;
        float v = uv.y;
        
        //get uv from 0 -1 into correct size
        if(u >= 1)
        {
            u = 0;
        }
        if(v >= 1)
        {
            v = 0;
        }
        u *= blendMapWidth;
        v *= blendMapWidth;

        float hasBeenFilledIn = blendMapBools[(int)u][(int)v];


        if (hasBeenFilledIn < pollenThreshold)//if haven't been but now are, we are first time filling in
        {
            hasBeenFilledIn += bird != null ? bird.GetPollen() : 1.0f;
            if (hasBeenFilledIn >= pollenThreshold)
            {
                //now we've hit it add it for a filled in one
                numBlendsFilledIn++;
            }
            blendMapBools[(int)u][(int)v] = hasBeenFilledIn;
        }


        percentFilledIn = numBlendsFilledIn / (blendMapWidth * blendMapWidth);
        
        
       // print("Current Percentage: " + percentFilledIn*100);

        CheckForLevelComplete();
    }
    
    public float GetPercentageFilledIn()
    {
    return percentFilledIn;
    }
    void CheckForLevelComplete()
    {
        SetUpPercentageGUI();
        if(percentFilledIn >= percentageRequiredToComplete)
        {
            //LEVEL COMPLETE!
            //print("level COMPLETELETLETLRELT");
            LevelManager.NotifyLevelComplete();
        }
    }

    void SetUpPercentageGUI()
    {
        if(guiIcon == null)
        {
            return;
        }
        float p = percentFilledIn / percentageRequiredToComplete;
     //   guiIcon.height = 128;
       Rect uv = guiIcon.uvRect;

       uv.height = 1.0f * p;
       guiIcon.uvRect = uv;
       guiIcon.height = (int)(OriginalGUIIconHeight * p);
      // print("uv" + guiIcon.uvRect.height + " : " + guiIcon.height + " : " + p);
        
    }

    void MakeLevelCheckMap()
    {
        blendMapBools.Clear();
        blendMapWidth = BlendPollen.BLEND_MAP_RES / currentBrushSize;
        numBlendsFilledIn = 0.0f;
        percentFilledIn = 0.0f;
        for (int j = 0; j < blendMapWidth + 0.5f; ++j)
        {
            blendMapBools.Add(new List<float>());
            for (int i = 0; i < blendMapWidth + 0.5f; ++i)
            {
                blendMapBools[j].Add(0.0f);
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        cam2DS = gameObject.GetComponent<Camera2DS>();
		bird = GameObject.FindGameObjectWithTag("Bird2").GetComponent<Bird>();
		print (bird.name);
		
        blendMaterial = new Material(blendShader);
      //  spritesTransMaterial = new Material(spritesTransShader);

		spriteCamera = GameObject.FindGameObjectWithTag("SpriteCamera").GetComponent<Camera>();
		print (spriteCamera.name);

        pingRT = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGB32);//may need to change to support older phones etc
		pongRT = new RenderTexture(1024, 1024, 0, RenderTextureFormat.ARGB32);
        
        pingRT.wrapMode = TextureWrapMode.Repeat;
        pongRT.wrapMode = TextureWrapMode.Repeat;

        
        

        pingRT.MarkRestoreExpected();
        pongRT.MarkRestoreExpected();
        currentPingPongRT = pingRT;
        currentPingPongRT.wrapMode = TextureWrapMode.Repeat;
        currentPingPongRT.MarkRestoreExpected();

        composedMaterial = new Material(ComposedShader);

        clearMaterial = new Material(clearShader);
        
        transitionMaterial = new Material(transitionShader);

        BackwardTransitionMaterial = new Material(BackwardTransitionShader);
        BlitMaterial = new Material(blitShader);

        currentCompose = composedMaterial;

    //    transitionMaterial = new Material(transitionShader);
    //    transitionMaterial = new Material(transitionShader);

        LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnCursorCentred += OnCursorCentred;
        LevelManager.OnNewLevelStarted += OnNewLevelStarted;
        LevelManager.OnIntroFinished += OnIntroFinished;

        if(guiIcon != null)
        {
            OriginalGUIIconHeight = guiIcon.height;
        }

        OnNewLevelStarted();


    }
    
    void Awake()
    {
    
		spriteCamera = GameObject.FindGameObjectWithTag("SpriteCamera").GetComponent<Camera>();
		bird = GameObject.FindGameObjectWithTag("Bird2").GetComponent<Bird>();
    }

    private void OnLevelComplete()
    {
        currentState = PaintingState.NONE;
    }

    private void OnCursorCentred()
    {
        blendTimer = 0.0f;
        currentState = PaintingState.BLENDING_IN;
        useTrans = true;
        currentCompose = transitionMaterial ;
    }

    private void OnNewLevelStarted()
    {
        //Graphics.Blit(baseTexture, pingRT);
        //Graphics.Blit(baseTexture, pongRT);
        
        if(bird != null)
        {
			bird.PollenLinearAmount = 100;
        }
        
		pingRT.MarkRestoreExpected();
		pongRT.MarkRestoreExpected();
		currentPingPongRT = pingRT;
		currentPingPongRT.MarkRestoreExpected();
        
        RenderTexture.active = pingRT;
	//	camera.targetTexture = pingRT;
        GLRenderThatMat(clearMaterial);

        RenderTexture.active = pongRT;
//		camera.targetTexture = pongRT;
        
        GLRenderThatMat(clearMaterial);

//		camera.targetTexture = null;
        RenderTexture.active = null;
        //currentBrushSize = LevelManager.GetCurrentLevelData().brushSize;
        MakeLevelCheckMap();

        numBlendsFilledIn = 0.0f;
        percentFilledIn = 0.0f;
    //    pollenLevel = 1.0f;

        percentageRequiredToComplete = LevelManager.GetCurrentLevelData().percentageRequiredToComplete;

        if (LevelManager.CurrentLevel == 0)
        {
            //set blend map to all red
            RenderTexture.active = pingRT;
            GLRenderThatMat(BlitMaterial);
            RenderTexture.active = pongRT;
            GLRenderThatMat(BlitMaterial);
            RenderTexture.active = null;

            currentState = PaintingState.BLENDING_OUT;
        }
        else
        {
            currentState = PaintingState.PAINTING;
        }
        useTrans = false;
        currentCompose = composedMaterial;
        Shader.SetGlobalFloat("_TransitionBlendRadius", 0.0f);
        Shader.SetGlobalFloat("_Ratio", cam2DS.GetMapRatio());
        Shader.SetGlobalTexture("_BrushTex", brushTexture);
    }

    private void OnIntroFinished()
    {
        currentState = PaintingState.PAINTING;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void CreateTextures(out RenderTexture rt, float width, float height, float scale, bool blitBlack) //or could pass in w/h
    {
       // DestroyImmediate(rt);

        int w = (int)(scale * width);
        int h = (int)(scale * height);

        rt = new RenderTexture(w, h, 0, RenderTextureFormat.ARGBHalf);

      //  firstCreation = false;
    }


	void CreateBlendLines(Vector3 hitPos, float halfwidth, float hardness, Material mat, bool writeToKey = true)
	{
		//uCoord = hitPos.x;
		//vCoord = hitPos.y;
		//        print("pos x, y: sw/h" + hitPos.x.ToString() + ", " + hitPos.y.ToString() + ", " + Screen.width.ToString() + ", " + Screen.height.ToString() + ", ");
		//uCoord = hitPos.x / (float)(Screen.width + 0.5);
		//vCoord = hitPos.y / (float)(Screen.height + 0.5);
		//Vector3 birdPos = 
		
		Vector2 uv = cam2DS.WriteUVFinger(hitPos);
		
		float pollenAmount = 1.0f;
		if(bird != null)
		{
			pollenAmount = bird.GetPollen();
		}
		if(writeToKey)
		{
			WriteToBoolMap(uv);
		}
		
		//now have uv
		
		Texture sourceTexture = null;
		//Render to the RT w/ material and camera
		if (ping)
		{
			currentPingPongRT = pingRT;
			sourceTexture = pongRT; 
		}
		else
		{
			currentPingPongRT = pongRT;
			sourceTexture = pingRT;            
		}
		currentPingPongRT.MarkRestoreExpected();
		camera.targetTexture = currentPingPongRT;
		RenderTexture.active = currentPingPongRT;
		
		//set the shader params
        mat.SetTexture("_BrushTex", brushTexture);
		mat.SetTexture("_MainTex", sourceTexture);
		//mat.SetColor("_ScorchColor", blendColor);
		mat.SetFloat("_ScorchUCoord", uv.x);
		mat.SetFloat("_ScorchVCoord", uv.y);
		mat.SetFloat ("_PollenAmount", pollenAmount);
		mat.SetFloat("_Ratio", cam2DS.GetMapRatio());
		mat.SetFloat("_HalfTexelWidth", halfwidth);
        mat.SetFloat("_Hardness", hardness);
		//blendMaterial.Set
		
		GLRenderThatMat(mat);
		
		
		RenderTexture.active = null;
		
		
		//swap for the next time
		ping = !ping;
		
		//now the map should be ready
	}
	

    void CreateBlendLines()
    {
        //if finger is not down don't change the RT return early

        //figure out the UV coords

//		print (Input.mousePosition);


        //TODO: target brush size


		CreateBlendLines( spriteCamera.WorldToScreenPoint(bird.transform.position), (brushSize) / 2048.0f, brushHardness, blendMaterial, true);
   

      
    }

    void BlendIn()
    {
        blendTimer += Time.deltaTime;

        LevelManager.LevelData levelData = LevelManager.GetCurrentLevelData();

        float blendLevel = Interpolate.Ease(blendInInterpolation)(0.0f, 1.0f, blendTimer, levelData.transitionPeriod);

        Shader.SetGlobalFloat("_TransitionBlendRadius", blendLevel * transitionBlendMaxRadius);

        if (blendTimer >= levelData.transitionPeriod)
        {
            currentState = PaintingState.NONE;
            LevelManager.NotifyBlendComplete();
        }
    }

    void BlendOut()
    {
        blendTimer += Time.deltaTime;

        LevelManager.LevelData levelData = LevelManager.GetCurrentLevelData();

        float blendLevel = Interpolate.Ease(Interpolate.EaseType.EaseInOutCubic)(0.0f, 1.0f, blendTimer, levelData.transitionPeriod*0.4f);

        Vector3 spot = Vector3.zero;
        spot.x = 0.5f * Screen.width;
        spot.y = 0.5f * Screen.height;
        
        CreateBlendLines(spot, 1 - blendLevel, 64.0f, BackwardTransitionMaterial, false);

        if (blendTimer >= levelData.transitionPeriod)
        {
            currentState = PaintingState.NONE;
           // LevelManager.NotifyBlendComplete();
        }
    }
    
    void UseBlendMapsMultiQuad()
    {
        //get the camera2DS component in the start fcn

        List<RenderQuad> quads = cam2DS.MakeAllPannels();
        //List<RenderQuad> quads = cam2DS.Make16Panels();

        for(int i = 0; i < quads.Count; ++i)
        {

            currentPingPongRT.wrapMode = TextureWrapMode.Repeat;
            currentCompose.SetTexture("_BlendMapTexture", currentPingPongRT); //might need lastframe bright texture
            currentCompose.SetTexture("_MainTex", quads[i].GetBaseTexture());
            currentCompose.SetTexture("_MainTex2", quads[i].GetTopTexture());
            GLRenderMatOnQuad(currentCompose, quads[i]);
            //camera.targetTexture = null;
            //RenderTexture.active = null; // JC: added to avoid errors
        }
        camera.targetTexture = null;
        RenderTexture.active = null; // JC: added to avoid errors
    }

    //void UseBlendMapBlendTexture()
    //{
    //   // camera.targetTexture = finalRT;
    //   // RenderTexture.active = finalRT;
    //    composedMaterial.SetTexture("_BlendMapTexture", currentPingPongRT); //might need lastframe bright texture
    //    composedMaterial.SetTexture("_MainTex", outlineTexture);
    //    composedMaterial.SetTexture("_MainTex2", colorTexture);

    //    // Draw a quad over the whole screen with the above shader
    //    GLRenderThatMat(composedMaterial);
    //    camera.targetTexture = null;
    //    RenderTexture.active = null; // JC: added to avoid errors

    //}


    void OnPostRender()
    {
//        print("in the post render");
        switch (currentState)
        {
            case PaintingState.PAINTING:
                {
                    CreateBlendLines();
                }
                break;
            case PaintingState.NONE:
                {
                    // do nothing...
                }
                break;
            case PaintingState.BLENDING_IN:
                {
                    BlendIn();
                }
                break;
            case PaintingState.BLENDING_OUT:
                {
                    BlendOut();
                }
                break;
        }
     //   UseBlendMapBlendTexture();
        UseBlendMapsMultiQuad(); //different material depending on if transition or reg
	}
	
	
	void GLRenderMatOnQuad(Material mat, RenderQuad quad)
    {
        GL.PushMatrix();
        GL.LoadOrtho();
        for (var i = 0; i < mat.passCount; ++i)
        {
            mat.SetPass(i);
            quad.GLQuad();
        }
        GL.PopMatrix();
    }
   public static void GLRenderThatMat(Material mat)
    {
        GL.PushMatrix();
        GL.LoadOrtho();
        for (var i = 0; i < mat.passCount; ++i)
        {
            mat.SetPass(i);
            GL.Begin(GL.QUADS);
            GL.TexCoord2(0.0f, 0.0f);
            GL.Vertex3(0f, 0f, 0.1f);
            GL.TexCoord2(1.0f, 0.0f);
            GL.Vertex3(1f, 0f, 0.1f);
            GL.TexCoord2(1.0f, 1.0f);
            GL.Vertex3(1f, 1f, 0.1f);
            GL.TexCoord2(0.0f, 1.0f);
            GL.Vertex3(0f, 1f, 0.1f);

            GL.End();
        }
        GL.PopMatrix();
    }
}


    