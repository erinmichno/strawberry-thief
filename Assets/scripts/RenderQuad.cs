﻿using UnityEngine;
using System.Collections;



public class RenderQuad
{
    public VertPoint BR = new VertPoint();
    public VertPoint BL = new VertPoint();
    public VertPoint TR = new VertPoint();
    public VertPoint TL = new VertPoint();

    public int textureIndex = 0;
    public int panelIndexLocal = 0;

    public Texture GetBaseTexture()
    {
        return Camera2DS.blendTextures[0][textureIndex];
    }
    public Texture GetTopTexture()
    {
        return Camera2DS.blendTextures[1][textureIndex];
    }
    public void GLQuad()
    {
        GL.Begin(GL.QUADS);
        BL.GLPoint();
        TL.GLPoint();
        TR.GLPoint();
        BR.GLPoint();
        

        GL.End();
    }



}
