﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;





//Each AudioChild should have one source and can play one clip at a time
public class AudioChild : MonoBehaviour 
{
	public List<AudioLeaf> nodes = new List<AudioLeaf>(); //should have a Bridge Chorus and Verse
	// Use this for initialization
	public bool hasBeenScheduled = false;
	public float startThreshold = 0.0625f;
	public int index = 0;

    private AudioSource lastScheduledSource = null;
   // private bool HasChorus2 = false;
	
	void Awake()
	{
		//create three nodes
		nodes.Clear();
		nodes.Add(CreateNode("Bridge"));
		nodes.Add(CreateNode("Chorus"));
		nodes.Add(CreateNode("Verse"));
        nodes.Add(CreateNode("Chorus2"));
	}

	void Start () 
	{
	}


	public void StartLayerPlay(int bcv, double time)
	{
        if (bcv >= nodes.Count)
        {
            return;
        }

		hasBeenScheduled = true;
		SetNextLoopIfPlaying(bcv, time);
	}
	
    public void SetNextLoopIfPlaying(int bcv, double time)
    {
        if (bcv >= nodes.Count)
        {
            return;
        }

        SetNextLoopIfPlayingInternal(bcv, time);
    }


	private void SetNextLoopIfPlayingInternal(int bcv, double time)
	{
		if(hasBeenScheduled)
		{
			if(nodes[bcv].source.isPlaying)
			{
				print ("clip " + bcv.ToString() + " is already playing isssssueeess");
				//nodes[bcv].source.loop = true;
				//return;
			}
			if(nodes[index].source.isPlaying)
			{
//				nodes[index].source.SetScheduledEndTime(time);
				nodes[index].source.loop = false; //just in case
			}
            //nodes[bcv].source.Stop();
            nodes[bcv].source.PlayScheduled(time);
            lastScheduledSource = nodes[bcv].source;

            nodes[bcv].source.volume = 1.0f;
			
			index = bcv;
			
		}
	}

    public void StopLastScheduled()
    {
        if (lastScheduledSource != null)
        {
            lastScheduledSource.Stop();
        }

        hasBeenScheduled = false;
    }

    public void StopAllAtScheduledTime(double dspTime)
    {
        foreach (AudioLeaf n in nodes)
        {
            n.source.SetScheduledEndTime(dspTime);
        }
    }

    public void StopAll()
    {
        foreach (AudioLeaf n in nodes)
        {
            n.source.Stop();
        }
    }

    public void DecreaseVolume(float amount)
    {
        foreach (AudioLeaf n in nodes)
        {
            float newVolume = n.source.volume - amount;
            n.source.volume = Mathf.Max(newVolume, 0.0f);
        }
    }

    public void SetVolume(float volume)
    {
        foreach (AudioLeaf n in nodes)
        {
            n.source.volume = Mathf.Clamp(volume, 0.0f, 1.0f);
        }
    }	
	
	AudioLeaf CreateNode(string n)
	{
		GameObject child;
		child = new GameObject(n);
		child.transform.parent = gameObject.transform;
		return child.AddComponent<AudioLeaf>();
	}
	
	public void LoadClips(string prefix)
	{	
		nodes[0].LoadClip(prefix + "_Bridge");
		nodes[1].LoadClip(prefix + "_Chorus");
		nodes[2].LoadClip(prefix + "_Verse");
        nodes[3].LoadClip(prefix + "_Chorus2");

        hasBeenScheduled = false;
	}

	public void UnloadAllClips()
	{
		foreach(AudioLeaf n in nodes)
		{
			n.UnloadClip();
		}
	}

    public void SetClips(List<AudioClip> clips)
    {
        for (int i = 0; i < clips.Count; ++i)
        {
            nodes[i].SetClip(clips[i]);
            nodes[i].source.volume = 0.0f;
        }
    }	
	
	// Update is called once per frame
	void Update () {
	
	}
}
