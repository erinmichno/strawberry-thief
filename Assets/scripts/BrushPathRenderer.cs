﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class BrushPathRenderer : MonoBehaviour
{
    private BrushPath brushPath = null;

    void Awake()
    {
        brushPath = GameObject.FindGameObjectWithTag("BrushPath").GetComponent<BrushPath>();
    }

    void OnPreRender()
    {
        brushPath.RenderOp();
    }
}
