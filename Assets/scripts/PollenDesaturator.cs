﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PollenDesaturator : MonoBehaviour
{
    private Bird bird = null;

    public float minColorSaturation = 0.25f;
    public float minParticleEmission = 0.25f;

    private float lastPollen = 1.0f;

    private struct ParticleEmissionPair
    {
        public ParticleSystem particleSystem;
        public float defaultEmission;

        public ParticleEmissionPair(ParticleSystem _particleSystem, float _defaultEmission)
        {
            particleSystem = _particleSystem;
            defaultEmission = _defaultEmission;
        }
    }

    private List<ParticleEmissionPair> particleEmissionList = new List<ParticleEmissionPair>();
    private List<SpriteRenderer> spriteRendererList = new List<SpriteRenderer>();

	void Start()
    {
        GameObject birdObject = GameObject.FindGameObjectWithTag("Bird2");

        if (birdObject == null)
        {
            throw new UnityException("Couldn't find bird object in pollen desaturator"); 
        }

        bird = birdObject.GetComponent<Bird>();

        // grab all attached and child particle systems and store alongside their default emission rates
        ParticleSystem attachedParticleSystem = GetComponent<ParticleSystem>();

        if (attachedParticleSystem != null)
        {
            particleEmissionList.Add(new ParticleEmissionPair(attachedParticleSystem, attachedParticleSystem.emissionRate));
        }

        ParticleSystem[] childParticleSystems = GetComponentsInChildren<ParticleSystem>();

        foreach (ParticleSystem childParticleSystem in childParticleSystems)
        {
            particleEmissionList.Add(new ParticleEmissionPair(childParticleSystem, childParticleSystem.emissionRate));
        }

        // grab all attached and child sprite renderers and store
        SpriteRenderer attachedSpriteRenderer = GetComponent<SpriteRenderer>();

        if (attachedSpriteRenderer != null)
        {
            spriteRendererList.Add(attachedSpriteRenderer);
        }

        SpriteRenderer[] childSpriteRenderers = GetComponentsInChildren<SpriteRenderer>();

        foreach (SpriteRenderer childSpriteRenderer in childSpriteRenderers)
        {
            spriteRendererList.Add(childSpriteRenderer);
        }
	}
	
	void Update()
    {
        float currentPollen = bird.GetPollenLinear();

        if (Mathf.Abs(currentPollen - lastPollen) > float.Epsilon)
        {
            float currentSaturation = Mathf.Max(minColorSaturation, currentPollen);
            float currentEmissionRate = Mathf.Max(minParticleEmission, currentPollen);

            foreach (ParticleEmissionPair particleEmission in particleEmissionList)
            {
                particleEmission.particleSystem.emissionRate = particleEmission.defaultEmission * currentEmissionRate;
                particleEmission.particleSystem.renderer.material.SetFloat("_Saturation", currentSaturation);
            }

            foreach (SpriteRenderer spriteRenderer in spriteRendererList)
            {
                spriteRenderer.material.SetFloat("_Saturation", currentSaturation);
            }

            lastPollen = currentPollen;
        }
	}
}
