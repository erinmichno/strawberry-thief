﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class FlowerManager : MonoBehaviour
{
    //need the level1 flower level2 etc etc
    public List<GameObject> flowerRoots = new List<GameObject>();

    public List<PollenPot> currentPollenPots = new List<PollenPot>();
    public List<BugScript> currentBugs = new List<BugScript>();

    private GameObject pollenGUIMarker = null;
    private Camera2DS camera2DS = null;
    private PollenPot usedForMarker = null;

    // Use this for initialization
    void Start()
    {
        if (flowerRoots.Count != 3)
        {
            throw new UnityException("Wrong number of root objects in FlowerManager, should be 3");
        }
        pollenGUIMarker = GameObject.FindGameObjectWithTag("PollenPotCompass");
        camera2DS = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera2DS>();

        LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnCursorCentred += OnCursorCentred;
        LevelManager.OnNewLevelStarted += OnNewLevelStarted;
        LevelManager.OnIntroFinished += OnIntroFinished;

        LevelManager.OnNewLevelObjectsLoaded += OnNewLevelObjectsLoaded;

        OnNewLevelStarted();
    }

    private void OnLevelComplete()
    {
        foreach (PollenPot pollenPot in currentPollenPots)
        {
            pollenPot.ScaleOutAndDeactivate();
        }

        foreach (BugScript bug in currentBugs)
        {
            bug.ScaleOutAndDeactivate();
        }
    }

    private void OnCursorCentred()
    {

    }

    List<Vector2> possiblePoints = new List<Vector2>();
    private void OnNewLevelStarted()//This is the reset
    {
        GameObject currentRootObject = null;

        for (int i = 0; i < 3; ++i)
        {
            if (i == LevelManager.CurrentLevel)
            {
                flowerRoots[i].SetActive(true);
                currentRootObject = flowerRoots[i];
            }
            else
            {
                flowerRoots[i].SetActive(false);
            }
        }

        currentPollenPots.Clear();
        currentPollenPots.AddRange(currentRootObject.GetComponentsInChildren<PollenPot>());

        if (LevelManager.CurrentLevel == 0)
        {
            foreach (PollenPot pollenPot in currentPollenPots)
            {
                pollenPot.gameObject.SetActive(false);
            }
        }

        currentBugs.Clear();
        currentBugs.AddRange(currentRootObject.GetComponentsInChildren<BugScript>());

        if (LevelManager.CurrentLevel == 0)
        {
            foreach (BugScript bug in currentBugs)
            {
                bug.gameObject.SetActive(false);
            }
        }

        if (LevelManager.CurrentLevel != 0)
        {
            List<GameObject> levelObjects = new List<GameObject>();

            for (int i = 0; i < flowerRoots[LevelManager.CurrentLevel - 1].transform.childCount; ++i)
            {
                levelObjects.Add(flowerRoots[LevelManager.CurrentLevel - 1].transform.GetChild(i).gameObject);
            }

            StartCoroutine(DestroyLevelObjects(levelObjects));
        }
    }

    private void OnIntroFinished()
    {
        foreach (PollenPot pollenPot in currentPollenPots)
        {
            pollenPot.gameObject.SetActive(true);
        }

        foreach (BugScript bug in currentBugs)
        {
            bug.gameObject.SetActive(true);
        }
    }

    private void OnNewLevelObjectsLoaded(List<Transform> newObjectTransforms, int levelNumber)
    {
        foreach (Transform newObjectTransform in newObjectTransforms)
        {
            newObjectTransform.parent = flowerRoots[levelNumber].transform;
        }
    }

    private IEnumerator DestroyLevelObjects(List<GameObject> levelObjects)
    {
        foreach (GameObject levelObject in levelObjects)
        {
            GameObject.Destroy(levelObject);
            yield return null;
        }
    }

    void LateUpdate()
    {
        SetPollenMarker();
    }

    private void SetPollenMarker()
    {
        //make vector between closest pollenpot and camera
        PollenPot closest = null;
        if (usedForMarker != null && (usedForMarker.gameObject.activeSelf == false || (usedForMarker.collider2D != null && usedForMarker.collider2D.enabled == false)))
        {
            usedForMarker = null;
        }

        bool initClosest = false;
        for (int i = 0; i < currentPollenPots.Count; ++i)
        {
            if (currentPollenPots[i].gameObject.activeSelf == false || currentPollenPots[i].collider2D.enabled == false)
            {
                continue;
            }
            if (!initClosest)
            {
                closest = currentPollenPots[i];
                initClosest = true;
            }
            else
            {
                Vector2 camToPot = currentPollenPots[i].transform.localPosition - camera2DS.transform.localPosition;
                Vector2 camToClosest = closest.transform.localPosition - camera2DS.transform.localPosition;
                if (camToPot.sqrMagnitude < camToClosest.sqrMagnitude)
                {
                    closest = currentPollenPots[i];
                }
            }
        }
        if (usedForMarker == null)
        {
            usedForMarker = closest;
        }
        else if (closest != null && usedForMarker != null)
        {
            Vector2 camToUsedForMarker = usedForMarker.transform.localPosition - camera2DS.transform.localPosition;
            Vector2 camToClosest = closest.transform.localPosition - camera2DS.transform.localPosition;
            if (Mathf.Abs(camToUsedForMarker.sqrMagnitude - camToClosest.sqrMagnitude) > 64)
            {
                usedForMarker = closest;
            }
        }


        //IF used for marker is on screen
        if (usedForMarker == null )
        {
            pollenGUIMarker.SetActive(false);
            return;//leave here
        }
        pollenGUIMarker.SetActive(true);


        //GET MY A B C D
        Vector2 centerCam = camera2DS.transform.localPosition;
        Vector2 halves = Vector2.zero;
        halves.x = Camera2DS.BASE_WIDTH / (2 * camera2DS.zoomLevel);
        halves.y = Camera2DS.BASE_HEIGHT / (2 * camera2DS.zoomLevel);

        //now i need to get all 4 tVals  and find the closest
        float t = BAD.x;  // and the line

        Vector2 C = Vector2.zero;
        Vector2 D = Vector2.zero;
        //test Left
        Vector2 TopLeft = centerCam;
        TopLeft.y += halves.y;
        TopLeft.x -= halves.x;
        Vector2 TopRight = centerCam + halves;
        Vector2 BottomLeft = centerCam - halves;
        Vector2 BottomRight = centerCam;
        BottomRight.y -= halves.y;
        BottomRight.x += halves.x;


        ////test Up
     //   GetCDT(centerCam, TopLeft, TopRight, ref C, ref D, ref t, false);
        //GetCDT(centerCam, BottomLeft, TopLeft, ref C, ref D, ref t, true);
        //GetCDT(centerCam, BottomLeft, BottomRight, ref C, ref D, ref t, false);
        //GetCDT(centerCam, BottomRight, TopRight, ref C, ref D, ref t, true);
     //   print("CenterCam: " + centerCam.ToString() + usedForMarker.ToString() + ": " + usedForMarker.transform.localPosition.ToString() + " TL: " + TopLeft.ToString() + " TR: " + TopRight.ToString());
        //if (t < -10)
        //{
        //    print("tTttt" + t);
        //    pollenGUIMarker.gameObject.SetActive(false);
        //    return;
        //}
        pollenGUIMarker.gameObject.SetActive(true);
        //Test Right
        //Test down
        //   print("used For Marker")
        //Place
        //Vector2 localPosition = (C + (D - C) * t);
        //print("CenterCam: " + centerCam.ToString() + usedForMarker.ToString() + ": " + usedForMarker.transform.localPosition.ToString() + " TL: " + TopLeft.ToString() + " TR: " + TopRight.ToString() + "inter: " + localPosition);
        //pollenGUIMarker.transform.localPosition = (localPosition - centerCam) * camera2DS.zoomLevel;
        //pollenGUIMarker.transform.rotation = Quaternion.LookRotation(Vector3.forward, pollenGUIMarker.transform.localPosition.normalized);

        possiblePoints.Clear();
        possiblePoints.Add( LineSegmentIntersection(centerCam, usedForMarker.transform.localPosition, TopLeft, TopRight));
        possiblePoints.Add(LineSegmentIntersection(centerCam, usedForMarker.transform.localPosition, BottomLeft, TopLeft));
        possiblePoints.Add(LineSegmentIntersection(centerCam, usedForMarker.transform.localPosition, BottomLeft, BottomRight));
        possiblePoints.Add(LineSegmentIntersection(centerCam, usedForMarker.transform.localPosition, BottomRight, TopRight));
        Vector2 res = FindClosest(centerCam, possiblePoints);
        //pick one closest to 
        if(RoughApproximately(res, BAD))
        {
           // pollenGUIMarker.gameObject.SetActive(false);
            return;
        }
        if(RoughApproximately(res, BAD2))
        {
            pollenGUIMarker.gameObject.SetActive(false);
            return;
        }

        Vector2 pos = (res - centerCam) * camera2DS.zoomLevel;
        if (Mathf.Abs(pos.x) > 3000 || Mathf.Abs(pos.y) > 3000 )
        {
            return;
        }
        

        pollenGUIMarker.transform.localPosition = pos;
        pollenGUIMarker.transform.rotation = Quaternion.LookRotation(Vector3.forward, pollenGUIMarker.transform.localPosition.normalized);

    }

    bool IsOnSegment(Vector2 result, Vector2 A, Vector2 B)
    {

        return (IsWithinRange(result.x, A.x, B.x) && IsWithinRange(result.y, A.y, B.y));
    }

    bool IsWithinRange(float r, float A, float B)
    {
        if (Mathf.Min(A, B) - 32 <= r && r <= Mathf.Max(A, B) + 32)
        {
            return true;
        }
        return false;
    }

     Vector2 BAD = new Vector2(-99999, -99999);
     Vector2 BAD2 = new Vector2(-88888, -88888);
    Vector2 FindClosest(Vector2 test, List<Vector2> set)
    {
        Vector2 retVal = BAD;
        for(int i = 0; i < set.Count; ++i)
        {
            

            if ((test - set[i]).sqrMagnitude < (test - retVal).sqrMagnitude)
            {
                retVal = set[i];
            }
            
            

        }
        return retVal;
    }

    Vector2 LineSegmentIntersection(Vector2 ptA1, Vector2 ptA2, Vector2 ptB1, Vector2 ptB2)
    {
        Vector2 returnVal = Vector2.zero;
        returnVal = LineLineIntersection(ptA1, ptA2, ptB1, ptB2);
        
        if (!IsOnSegment(returnVal, ptA1, ptA2) )
        {
            //on line but not seg
         //   print(returnVal);
            returnVal = BAD2;
        }

        return returnVal;
    }

    Vector2 LineLineIntersection(Vector2 ptA1, Vector2 ptA2, Vector2 ptB1, Vector2 ptB2)
    {
        Vector2 returnVal = Vector2.zero;
        float A1 = ptA2.y - ptA1.y;
        float B1 = ptA1.x - ptA2.x;
        float C1 = A1 * ptA1.x + B1 * ptA1.y;


        float A2 = ptB2.y - ptB1.y;
        float B2 = ptB1.x - ptB2.x;
        float C2 = A2 * ptB1.x + B2 * ptB1.y;

        float det = A1 * B2 - A2 * B1;
        if (det == 0)
        {
            //Lines are parallel
            returnVal = BAD;
        }
        else
        {
            returnVal.x = (B2 * C1 - B1 * C2) / det;
            returnVal.y = (A1 * C2 - A2 * C1) / det;
        }
        return returnVal;
    }


    void GetCDT(Vector2 centerCam, Vector2 inC, Vector2 inD, ref Vector2 C, ref Vector2 D, ref float t, bool useX)
    {

        float tValUP = GetTVal(centerCam, usedForMarker.transform.localPosition, inC, inD, useX);
        if (tValUP <= 1 && tValUP >= 0)
        {
            C = inC;
            D = inD;
            t = tValUP;
            //  print(t);
            return;
        }




    }


    public static bool RoughApproximately(Vector2 a, Vector2 b)
    {
        return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y);
    }
    float GetTVal(Vector2 A, Vector2 B, Vector2 C, Vector2 D, bool useX)
    {
        Vector2 dirA = (B - A).normalized;
        Vector2 dirB = (D - C).normalized;
        if (RoughApproximately(dirA, dirB) || RoughApproximately(-dirA, dirB))
        {
            print("paral");
            return BAD.x; //parallel lines
        }
        float tX = GetTVal(A.x, B.x, C.x, D.x);
        float tY = GetTVal(A.y, B.y, C.y, D.y);

        //// if (!Mathf.Approximately(tX, tY))
        // {
        //     // print("INTERSECTION ISSUESSSSSSS");
        //  //   return -9999999.0f;

        // }
       // print("tX: " + tX.ToString() + " ty: " + tY.ToString());
        if (useX)
        {
            return tX;
        }

        return tY;

    }

    float GetTVal(float A, float B, float C, float D)
    {

        float bottom = (B - A + C - D);
        if (bottom == 0) { return BAD.x; }
        return (C - A) / (B - A + C - D);
    }
}
