﻿using UnityEngine;
using System.Collections;

public class BugScript : MonoBehaviour
{
    public float Radius = 200;
    public float ScaleX = 1.0f;
    private float theta = 0.0f;
    public float AngularVel = 45;
    public Vector3 centrePoint = Vector3.zero;
    private Vector3 vec = new Vector3(0, 1, 0);
    CameraWrapTransform wrapping = null;
    bool TurnOffTranslationRotationAnimation = false;

    BugChild childBug;

    private const float TIMER_DURATION = 5.0f;
    private float timer = TIMER_DURATION;

    private bool scaling = false;

    private float originalScale = 1.0f;
    private float targetScale = 1.0f;

    private bool deactivateOnScaleOut = false;

    public float pollenRemovalAmount = 0.5f;

    private ParticleSystem[] burstParticles = null;

    private SpriteRenderer spriteRenderer = null;
    private BrushAdapter brushAdapter = null;


    


    public enum BugType
    {
        BUTTERFLY = 0,
        BEETLE = 1,
        BEE = 2
    }
    private BugType bugType = BugType.BUTTERFLY;

    // Use this for initialization
    void Start()
    {
        wrapping = GetComponent<CameraWrapTransform>();
        childBug = GetComponentInChildren<BugChild>();

        brushAdapter = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BrushAdapter>();

        spriteRenderer = childBug.GetComponent<SpriteRenderer>();

        originalScale = childBug.transform.localScale.x;

        childBug.transform.localScale = Vector3.zero;
        
        targetScale = originalScale;
        scaling = true;

        burstParticles = GetComponentsInChildren<ParticleSystem>();

       if( gameObject.name.Contains("BubbleBee"))
       {
           bugType = BugType.BEE;
       }
       else if (gameObject.name.Contains("Beetle"))
       {
           bugType = BugType.BEETLE;
       }
       else
       {
           bugType = BugType.BUTTERFLY;
       }
    }



    // Update is called once per frame
    void Update()
    {
        if (timer < TIMER_DURATION && spriteRenderer != null)
        {
            spriteRenderer.color = new Color(0.8f, 0.8f, 0.8f, 1.0f);

            if (!scaling && Mathf.Approximately(childBug.transform.localScale.x, originalScale) && !deactivateOnScaleOut)
            {
                scaling = true;
                targetScale = originalScale * 0.7f;
            }
        }
        else
        {
            spriteRenderer.color = Color.white;

            if (!scaling && childBug.transform.localScale.x < originalScale * 0.95f && !deactivateOnScaleOut)
            {
                scaling = true;
                targetScale = originalScale;
            }
        }

        //moveInOvalScript()
        if(TurnOffTranslationRotationAnimation && childBug != null)
        {
            Animator animator = childBug.GetComponent<Animator>();
         //   print(animator.GetLayerName(1)); //movement
            animator.SetLayerWeight(1, 0);
        }

        if (wrapping != null)
        {
            wrapping.originalPos = transform.localPosition;
        }

        if (scaling)
        {
            if (Mathf.Abs(childBug.transform.localScale.x - targetScale) < 0.01f)
            {
                childBug.transform.localScale = new Vector3(targetScale, targetScale, targetScale);
                scaling = false;
            }
            else
            {
                float currentScale = childBug.transform.localScale.x + (targetScale - childBug.transform.localScale.x) * 0.1f;
                childBug.transform.localScale = new Vector3(currentScale, currentScale, currentScale);
            }
        }

        timer += Time.deltaTime;
    }

    void MoveInOvalScriptInstead()
    {
        theta += AngularVel * Time.deltaTime;
        if (theta > 360) { theta -= 360; }
        Vector3 v = Quaternion.AngleAxis(theta, Vector3.forward) * vec;
        v.x *= Radius * ScaleX;
        v.y *= Radius;
        v.z = transform.localPosition.z;

        transform.localPosition = v + centrePoint;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (deactivateOnScaleOut)
        {
            return;
        }

        Bird bird = other.GetComponent<Bird>();
       

        if (bird != null)
        {
            if (timer >= TIMER_DURATION)
            {
                InGameAudio.PlayBugClip();

                timer = 0.0f;

                //bird.RemovePollenPercentage(pollenRemovalAmount);
                //bird.StartSpinOut(childBug.transform.position);
                switch(bugType)
                {
                    case BugType.BEE:
                        Cursor cursor = NGUITools.FindInParents<Cursor>(other.gameObject);
                        if(cursor != null)
                        {
                            cursor.SetSpeedBoost(1.5f, BrushAdapter.BeeBoostTime);
                        }

                        break;
                    case BugType.BEETLE:
                        
                        break;
                    case BugType.BUTTERFLY:
                        break;
                }
                brushAdapter.SetTargetsBasedOnType(bugType);


                foreach (ParticleSystem burst in burstParticles)
                {
                    burst.Play();
                }
            }
        }
    }

    public void ScaleOutAndDeactivate()
    {
        timer = 0.0f;
        scaling = true;
        deactivateOnScaleOut = true;
        targetScale = 0.0f;
    }
}
