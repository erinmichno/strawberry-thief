﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//[RequireComponent(typeof(AudioSource))]
//public class ExampleClass : MonoBehaviour {
//    public AudioClip sourceClip;
//    private AudioSource audio1;
//    private AudioSource audio2;
//    public AudioClip cutClip1;
//    public AudioClip cutClip2;
//    private float overlap = 0.2F;
//    private int len1 = 0;
//    private int len2 = 0;
//    public void Setup() {
//        GameObject child;
//        child = new GameObject("Player1");
//        child.transform.parent = gameObject.transform;
//        audio1 = child.AddComponent("AudioSource") as AudioSource;
//        child = new GameObject("Player2");
//        child.transform.parent = gameObject.transform;
//        audio2 = child.AddComponent("AudioSource") as AudioSource;
//        int overlapSamples;
//        //			if (sourceClip != null) {
//        //				len1 = sourceClip.samples / 2;
//        //				len2 = sourceClip.samples - len1;
//        //				overlapSamples = (int)overlap * sourceClip.frequency;
//        //				cutClip1 = AudioClip.Create("cut1", len1 + overlapSamples, sourceClip.channels, sourceClip.frequency, false, false);
//        //				cutClip2 = AudioClip.Create("cut2", len2 + overlapSamples, sourceClip.channels, sourceClip.frequency, false, false);
//        //				float[] smp1 = new float[(len1 + overlapSamples) * sourceClip.channels];
//        //				float[] smp2 = new float[(len2 + overlapSamples) * sourceClip.channels];
//        //				sourceClip.GetData(smp1, 0);
//        //				sourceClip.GetData(smp2, len1 - overlapSamples);
//        //				cutClip1.SetData(smp1, 0);
//        //				cutClip2.SetData(smp2, 0);
//        //			} else {
//        overlapSamples = (int)overlap * cutClip1.frequency;
//        len1 = cutClip1.samples - overlapSamples;
//        len2 = cutClip2.samples - overlapSamples;
//        //}
//    }
//    void OnGUI() {
//        if (GUI.Button(new Rect(10, 50, 230, 40), "Trigger source"))
//            audio1.PlayOneShot(sourceClip);
		
//        if (GUI.Button(new Rect(10, 100, 230, 40), "Trigger cut 1"))
//            audio1.PlayOneShot(cutClip1);
		
//        if (GUI.Button(new Rect(10, 150, 230, 40), "Trigger cut 2"))
//            audio1.PlayOneShot(cutClip2);
		
//        if (GUI.Button(new Rect(10, 200, 230, 40), "Play stitched")) {
//            audio1.clip = cutClip1; //len1 is clip.samples
//            audio2.clip = cutClip2;
//            double t0 = AudioSettings.dspTime + 3.0F;
//            double clipTime1 = len1;
//            clipTime1 /= cutClip1.frequency;
//            audio1.PlayScheduled(t0);
//            audio1.SetScheduledEndTime(t0 + clipTime1);
//            Debug.Log("t0 = " + t0 + ", clipTime1 = " + clipTime1 +  ", clipTime1 = " + audio1.clip.length.ToString() +", cutClip1.frequency = " + cutClip1.frequency);
//            Debug.Log("cutClip2.frequency = " + cutClip2.frequency + ", samplerate = " + AudioSettings.outputSampleRate);
//            audio2.PlayScheduled(t0 + clipTime1);
//            audio2.time = overlap;
//        }
//    }
//}




public class AudioConductor : MonoBehaviour //maybe have conductor for each level?
{


	BlendPollen pollenMaster = null;


	List<AudioChild> currentAudioLayers = new List<AudioChild>();

    List<AudioSource> transitionLayers = new List<AudioSource>();

    AudioSource nextSceneBaseTrack = null;
	//maybe have a next

	public  const int BRIDGE = 0;
	public  const int CHORUS = 1;
	public  const int VERSE = 2;
    public const int CHORUS2 = 3;
    
	
	int bcvIndex = -1;
    bool Level3Passed50 = false;

 //Phase 1   VCVCBC-VCVCBC
 

//Phase 2: VCVCB-VCVCB
	
	
	public static List< List<int>>  CurrentBCVListAllLevels = new List< List<int> > {new List<int>{VERSE, CHORUS, VERSE, CHORUS, BRIDGE, CHORUS}, //LEVEL 1
																			new List<int>{VERSE, CHORUS, VERSE, CHORUS, BRIDGE}, //LEVEL 2
																			new List<int>{VERSE, CHORUS, BRIDGE, VERSE, CHORUS, BRIDGE}}; //LEVEL 3
																			// CurrentBCVList[LVL_1][bcvIndex]  also if (bcvIndex >= CurrentBCVList[LVL_1].count) {bcvIndex = 0;} etc etc

  
	
	public static List<int> currentBCVList;
	
	int NextBCVIndex
	{
		get
        {
            return (bcvIndex+ 1) >= currentBCVList.Count ? 0 : bcvIndex + 1;;
        }
	}
	
	const float BPM = 90.0f;
	const float BarsPerClip = 8;
	const float QuaterNoteInSeconds = 0.6677f;//2.0f/3.0f;ideally uncompressed
	double secondsPerBar = 2.6667;//8.0f/3.0f;
	double secondsPerLoop = 21.3333; //64/3

    double transitionLength = 10.6666;
    bool transitionPlaying = false;
    double transitionScheduledTime = 0.0;
    int currentTransition = 0;

    private double lastScheduledTime = 0.0;

    private bool conductorStarted = false;
	
	
//	double SamplesPerLoop = 0.5f; // that is just clip.samples

	//double TotalSamples = total number of samples played since start = (dsp - dspStart)*frequency
	
	 double frequencyOfBaseLoop = 1;
	 int samplesOfBaseLoop = 1;
	 
	
	
	//double DSPStartTime = 0.0;
	double DSPLevelStartTime = 0.0;
	
	//double TotalDSPTime = 0; // dspTime - dspStart
	double DSPTimeSinceLevelStart = 0; //dspTime - dspLevelStart
	
	
	int TotalSamplesPlayed = 0;
	int SamplesPerBar = 0; // if 1 bar is .25 sec long 0.25*frequency //should this be an int?
	//int SamplesPerLoop = 0; // clip.samples
	
	int CurrentBar = 0;
	int NumSamplesIntoCurrentBar = 0;
	double ScheduleTimeForNextBar = 0.0;
	
	int CurrentLoop = -1;
	int NumSamplesIntoCurrentLoop = 0;
	double ScheduleTimeForNextLoop = 0.0;
	
	
	float percentageTillComplete = 0.0f;

    AudioSource menuSource = null;
    AudioSource tutorialSource = null;
	
	//when current loop changes schedule the next one
	
	// Update is called once per frame
	void Update ()
	{
        if (conductorStarted && !transitionPlaying)
        {
            SetLevelPercentages();
            UpdateConductor();
            CheckToAddNewLayer();
        }
	}

	List<float> starts = new List<float>{0.0f, 0.0625f, 0.25f, 0.5625f};
	void CheckToAddNewLayer()
	{
        if (LevelManager.CurrentLevel == 2 && starts[2] <= percentageTillComplete)
        {
            Level3Passed50 = true;

            int nextBCV = NextBCVIndex;
            
            // manually update the bcvIndex
            int loopNum = (int)(TotalSamplesPlayed) / (int)(samplesOfBaseLoop);
            bcvIndex = loopNum % 2;

            currentBCVList.Clear();

            if(nextBCV == CHORUS && bcvIndex == 1)
            {
                // we know we're going to index 0 next, so place CHORUS2 at start of list
                currentBCVList.Add(CHORUS2);
                currentBCVList.Add(BRIDGE);
            }
            else
            {
                currentBCVList.Add(BRIDGE);
                currentBCVList.Add(CHORUS2);
            }
        }

		//go through children 
		//if percentage > starts[i] and child hasn't yet been scheduled
		//turn the bool on in the child
		//scedule for the next loop with the next Chorus/Verse/Bridge
		for(int i = 0; i < starts.Count; ++i)
		{
			//if we haven't scheduled it but we are good to go!
			if(starts[i] <= percentageTillComplete && !currentAudioLayers[i].hasBeenScheduled)
			{
                Debug.Log("SCHEDULING LAYER " + i.ToString());
                
				//schedule the clip
                currentAudioLayers[i].StartLayerPlay(currentBCVList[NextBCVIndex], ScheduleTimeForNextLoop);
			}
			
		}
	}
	
	//when current loop changes schedule the next one
	//Should get called when we are say 0.25 way through current verse etc
	void ScheduleNextLoops()
	{
        Debug.Log("CURRENT DSP TIME: " + AudioSettings.dspTime.ToString());
        Debug.Log("SCHEDULING " + currentBCVList[NextBCVIndex].ToString() + " FOR " + ScheduleTimeForNextLoop.ToString());
		//update our index
		// 
		for(int i = 0; i < currentAudioLayers.Count; ++i)
		{
            //schedule the clip
            currentAudioLayers[i].SetNextLoopIfPlaying(currentBCVList[NextBCVIndex], ScheduleTimeForNextLoop);
		}
		//call all the kids with the next one

        lastScheduledTime = ScheduleTimeForNextLoop;
	}
	
	//from notebook 
	void UpdateConductor()
	{
		//go through all common things
		DSPTimeSinceLevelStart = AudioSettings.dspTime - DSPLevelStartTime;

		TotalSamplesPlayed = Mathf.CeilToInt((float)(DSPTimeSinceLevelStart*frequencyOfBaseLoop));
       // SamplesPerBar = Mathf.CeilToInt((float)(SecondsPerBar * frequencyOfBaseLoop));
       //SamplesPerLoop = Mathf.CeilToInt((float)(SecondsPerLoop * frequencyOfBaseLoop));
		
		CurrentBar = (int)(TotalSamplesPlayed) / (int)(SamplesPerBar);
        NumSamplesIntoCurrentBar = (int)(TotalSamplesPlayed) % (int)(SamplesPerBar);
		ScheduleTimeForNextBar = AudioSettings.dspTime + (SamplesPerBar - NumSamplesIntoCurrentBar) / frequencyOfBaseLoop;

        int loopNum = (int)(TotalSamplesPlayed) / (int)(samplesOfBaseLoop);

        NumSamplesIntoCurrentLoop = (int)(TotalSamplesPlayed) % (int)(samplesOfBaseLoop);
        //ScheduleTimeForNextLoop = AudioSettings.dspTime + (samplesOfBaseLoop - NumSamplesIntoCurrentLoop) / frequencyOfBaseLoop;
        ScheduleTimeForNextLoop = lastScheduledTime + secondsPerLoop;

        int previousBCVIndex = bcvIndex;
        bcvIndex = loopNum % currentBCVList.Count; //also check against just ++

        ////Could Check if level3 and using second half of stuff, if bcvIndex is verse add one
        //if (Level3Passed50 && previousBCVIndex != bcvIndex &&  currentBCVList[bcvIndex] == VERSE)
        //{
        //    bcvIndex++;//skip verse if on second half
        //}

        if (conductorStarted && !transitionPlaying)
        {
            if (loopNum > CurrentLoop)
            {
                //we've moved on so time to schedule things for teh next round
                ScheduleNextLoops();
            }

            CurrentLoop = loopNum;
        }
	}
	
	void Start()
	{
        GameObject menuMusicGO = new GameObject();
        menuMusicGO.name = "MenuMusicSource";
        DontDestroyOnLoad(menuMusicGO);

        menuSource = menuMusicGO.AddComponent<AudioSource>();
        menuSource.clip = Resources.Load<AudioClip>("Audio/New/Menu");
        menuSource.loop = true;
        menuSource.Play();


        GameObject tutorialMusicGO = new GameObject();
        tutorialMusicGO.name = "TutorialMusicSource";
        DontDestroyOnLoad(tutorialMusicGO);
        tutorialSource = menuMusicGO.AddComponent<AudioSource>();
        tutorialSource.clip = Resources.Load<AudioClip>("Audio/New/Tutorial");
        tutorialSource.loop = true;
        tutorialSource.playOnAwake = false;


		LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnNewLevelObjectsLoaded += OnNewLevelObjectsLoaded;
		LevelManager.OnCursorCentred += OnCursorCentred;
		LevelManager.OnNewLevelStarted += OnNewLevelStarted;
	}
	
	private void OnLevelComplete()
	{
        if (LevelManager.CurrentLevel == 0)
        {
            StartTransition(0);
        }
        else if (LevelManager.CurrentLevel == 1)
        {
            StartTransition(1);
        }
        else
        {
            transitionPlaying = false;
        }
	}

    private void OnNewLevelObjectsLoaded(List<Transform> transforms, int levelNumber)
    {
        nextSceneBaseTrack.clip = LevelManager.GetNextSceneBaseTrack(CurrentBCVListAllLevels[levelNumber][0]);

        // schedule the track if transition playing
        if (transitionPlaying)
        {
            Debug.Log("Scheduling next base track for: " + (transitionScheduledTime + transitionLength).ToString());

            nextSceneBaseTrack.PlayScheduled(transitionScheduledTime + transitionLength);
        }
    }
	
	private void OnCursorCentred()
	{
		
	}
	
	private void OnNewLevelStarted()//This is the reset
	{
        conductorStarted = false;
        Level3Passed50 = false;
        bcvIndex = -1;

        percentageTillComplete = 0.0f;

        TotalSamplesPlayed = 0;

        CurrentBar = 0;
        NumSamplesIntoCurrentBar = 0;

        CurrentLoop = -1;
        NumSamplesIntoCurrentLoop = 0;

        ScheduleTimeForNextBar = 0.0;
        ScheduleTimeForNextLoop = 0.0;

		currentBCVList = CurrentBCVListAllLevels[LevelManager.CurrentLevel];

        // unload any old ones, load from LevelData
        //LoadClipsAndSources(LevelManager.GetCurrentLevelData().instrumentNames);
        SetClipsAndSources(LevelManager.currentInstrumentClips);

        DSPLevelStartTime = AudioSettings.dspTime;
        DSPTimeSinceLevelStart = 0; //dspTime - dspLevelStart
                
        StartCoroutine(StartConductor());
	}

    private IEnumerator StartConductor()
    {
        yield return new WaitForSeconds(1.0f);

        lastScheduledTime = AudioSettings.dspTime;

        if(transitionPlaying)
        {
            lastScheduledTime = transitionScheduledTime + transitionLength + secondsPerLoop;
            ++CurrentLoop;
            bcvIndex = 0;
        }

        currentAudioLayers[0].StartLayerPlay(currentBCVList[NextBCVIndex], lastScheduledTime);

        //while (!currentAudioLayers[0].nodes[currentBCVList[0]].IsPlaying)
        while (AudioSettings.dspTime < lastScheduledTime)
        {
            yield return null;
        }

        DSPTimeSinceLevelStart = currentAudioLayers[0].nodes[currentBCVList[0]].TimeSamples / frequencyOfBaseLoop;
        DSPLevelStartTime = AudioSettings.dspTime - DSPTimeSinceLevelStart - (transitionPlaying ? secondsPerLoop : 0.0);

        conductorStarted = true;
        transitionPlaying = false;
    }

    private void StartTransition(int layer)
    {
        if(AudioSettings.dspTime < (lastScheduledTime - transitionLength - 0.25))
        {
            lastScheduledTime -= transitionLength;

            for (int i = 0; i < currentAudioLayers.Count; ++i)
            {
                currentAudioLayers[i].StopAllAtScheduledTime(lastScheduledTime);
            }
        }

        for (int i = 0; i < currentAudioLayers.Count; ++i)
        {
            currentAudioLayers[i].StopLastScheduled();
        }

        //Debug.Log("Scheduling transition for: " + lastScheduledTime.ToString());
        transitionLayers[layer].PlayScheduled(lastScheduledTime);

        currentTransition = layer;
        transitionScheduledTime = lastScheduledTime;
        transitionPlaying = true;

        StartCoroutine(SheduledMuteThenReturn(lastScheduledTime, 2.0f, 0.5f));
    }

    private IEnumerator SheduledMuteThenReturn(double dspEndTime, float muteDelay, float returnDelay)
    {
        float timeTillEnd = (float)(dspEndTime - AudioSettings.dspTime);

        if (timeTillEnd < 0.0f)
        {
            yield break;
        }

        float initialDelay = Mathf.Max(timeTillEnd - muteDelay, 0.0f);

        if (timeTillEnd < muteDelay)
        {
            muteDelay = timeTillEnd;
        }

        if (muteDelay < 0.0f)
        {
            Debug.LogWarning("Mute delay < 0, you're gonna get an infinite loop buddy...");
            yield break;
        }

        yield return new WaitForSeconds(initialDelay);

        float muteTimer = 0.0f;

        while(muteTimer < 1.0f)
        {
            foreach(AudioChild childLayer in currentAudioLayers)
            {
                childLayer.SetVolume(1.0f - muteTimer * muteTimer * muteTimer);
            }

            muteTimer += Time.deltaTime * (1.0f / muteDelay);

            yield return null;
        }

        foreach (AudioChild childLayer in currentAudioLayers)
        {
            childLayer.SetVolume(0.0f);
            childLayer.StopAll();
        }

        yield return new WaitForSeconds(returnDelay);

        foreach (AudioChild childLayer in currentAudioLayers)
        {
            childLayer.SetVolume(1.0f);
        }
    }
	
    //void LoadClipsAndSources(List<string> instrumentNames)
    //{
    //    if (instrumentNames.Count < 4)
    //    {
    //        throw new UnityException("Not enough instruments passed to create Audio sources");
    //    }

    //    throw new UnityException("Should never get here  Audio sources");
    //    CreateAndAdd(0, instrumentNames[0]);
    //    CreateAndAdd(1, instrumentNames[1]);
    //    CreateAndAdd(2, instrumentNames[2]);
    //    CreateAndAdd(3, instrumentNames[3]);

    //    //get the frequency of base track
    //    frequencyOfBaseLoop = currentAudioLayers[0].nodes[currentBCVList[0]].clip.frequency;
    //    SamplesPerBar = (int)(frequencyOfBaseLoop*SecondsPerBar);  //seconds = samples / frequency
    //    samplesOfBaseLoop = currentAudioLayers[0].nodes[currentBCVList[0]].clip.samples;
    //    //samplesOfBaseLoop
    //    //play the base track?
    //}
	
	void CreateAndAdd(int layerNumber, string pathname, bool hasAlt)
	{
        if (layerNumber >= currentAudioLayers.Count)
        {
            GameObject go;
            go = new GameObject("AudioKid");
            go.transform.parent = gameObject.transform;
            AudioChild child = go.AddComponent<AudioChild>();

            currentAudioLayers.Add(child);
            child.LoadClips(pathname);
        }
        else
        {
            currentAudioLayers[layerNumber].UnloadAllClips();
            currentAudioLayers[layerNumber].LoadClips(pathname);
        }
		
	}

    void SetClipsAndSources(List<List<AudioClip>> instrumentClips)
    {
        if (instrumentClips.Count < 4)
        {
            throw new UnityException("Not enough instruments passed to create Audio sources");
        }

        CreateAndAdd(0, instrumentClips[0]);
        CreateAndAdd(1, instrumentClips[1]);
        CreateAndAdd(2, instrumentClips[2]);
        CreateAndAdd(3, instrumentClips[3]);
       // CreateAndAdd(4, instrumentClips[4]);

        //for (int i = 0; i < 4; ++i)
        //{
        //    for (int j = 0; j < 3; ++j)
        //    {
        //        if (currentAudioLayers[i].nodes[j].clip == null)
        //        {
        //            Debug.Log("NO CLIP");
        //        }
        //        else
        //        {
        //            Debug.Log(currentAudioLayers[i].nodes[j].clip.name);
        //        }
        //    }
        //}

        //get the frequency of base track
        frequencyOfBaseLoop = currentAudioLayers[0].nodes[0].clip.frequency;
        //secondsPerLoop = currentAudioLayers[0].nodes[0].clip.samples / frequencyOfBaseLoop;
        secondsPerBar = secondsPerLoop / 4.0;
        SamplesPerBar = (int)(frequencyOfBaseLoop * secondsPerBar);  //seconds = samples / frequency
        samplesOfBaseLoop = currentAudioLayers[0].nodes[0].clip.samples;
        //samplesOfBaseLoop
        //play the base track?
    }

    void SetTransitions()
    {
        if (transitionLayers.Count == 0)
        {
            GameObject go;
            go = new GameObject("IntroTransition");
            go.transform.parent = gameObject.transform;

            transitionLayers.Add(go.AddComponent<AudioSource>());
            transitionLayers[0].clip = Resources.Load<AudioClip>("Audio/New/Transition 1");
            transitionLayers[0].playOnAwake = false;
            transitionLayers[0].loop = false;

            go = new GameObject("OutroTransition");
            go.transform.parent = gameObject.transform;

            transitionLayers.Add(go.AddComponent<AudioSource>());
            transitionLayers[1].clip = Resources.Load<AudioClip>("Audio/New/Transition 2");
            transitionLayers[1].playOnAwake = false;
            transitionLayers[1].loop = false;

            go = new GameObject("NextSceneBase");
            go.transform.parent = gameObject.transform;

            nextSceneBaseTrack = go.AddComponent<AudioSource>();
            nextSceneBaseTrack.clip = null;
            nextSceneBaseTrack.playOnAwake = false;
            nextSceneBaseTrack.loop = false;

            transitionLength = transitionLayers[0].clip.samples / (double)transitionLayers[0].clip.frequency;
        }
    }

    void CreateAndAdd(int layerNumber, List<AudioClip> clips)
    {
        if (layerNumber >= currentAudioLayers.Count)
        {
            GameObject go;
            go = new GameObject("AudioKid");
            go.transform.parent = gameObject.transform;
            AudioChild child = go.AddComponent<AudioChild>();

            currentAudioLayers.Add(child);
            child.SetClips(clips);
        }
        else
        {
            currentAudioLayers[layerNumber].SetClips(clips);
        }

    }
	
	
	void PlayClipAtNextBar(AudioClip clip, AudioSource source)
	{
	
	}
	
	void PlayClipAtNextLoop(AudioClip clip, AudioSource source)
	{
//		audio1.clip = cutClip1; //len1 is clip.samples
//		audio2.clip = cutClip2;
//		double t0 = AudioSettings.dspTime + 3.0F;
//		double clipTime1 = len1;
//		clipTime1 /= cutClip1.frequency;
//		audio1.PlayScheduled(t0);
//		audio1.SetScheduledEndTime(t0 + clipTime1);
//		Debug.Log("t0 = " + t0 + ", clipTime1 = " + clipTime1 +  ", clipTime1 = " + audio1.clip.length.ToString() +", cutClip1.frequency = " + cutClip1.frequency);
//		Debug.Log("cutClip2.frequency = " + cutClip2.frequency + ", samplerate = " + AudioSettings.outputSampleRate);
//		audio2.PlayScheduled(t0 + clipTime1);
//		audio2.time = overlap;
	}
	

	

	void OnLevelWasLoaded(int level)
	{
		print ("dfaladl level sfasdfa");
		if(level == 4)
		{
            if (menuSource != null && menuSource.isPlaying)
            {
                menuSource.volume = 0.0f;
                menuSource.Stop();
            }
            if (tutorialSource != null && tutorialSource.isPlaying)
            {
                tutorialSource.volume = 0.0f;
                tutorialSource.Stop();
            }


            foreach (AudioChild audioChild in currentAudioLayers)
            {
                audioChild.SetVolume(0.0f);
            }

            foreach (AudioSource transitionSource in transitionLayers)
            {
                transitionSource.volume = 0.0f;
            }

			pollenMaster = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<BlendPollen>();

            SetTransitions();

            OnNewLevelStarted();

            // fade in game music
            StartCoroutine(FadeInGameAudio(1.0f));
		}
		else
		{
            conductorStarted = false;
            transitionPlaying = false;

            foreach (AudioChild audioChild in currentAudioLayers)
            {
                audioChild.SetVolume(0.0f);
                audioChild.StopAll();
            }

            foreach (AudioSource transitionSource in transitionLayers)
            {
                transitionSource.volume = 0.0f;
                transitionSource.Stop();
            }
            if (level == 2)//fade in tutorial
            {
                if(menuSource != null && menuSource.isPlaying)
                {
                    StartCoroutine(FadeOutMenuAudio(0.5f));
                }
                if(tutorialSource != null && !tutorialSource.isPlaying)
                {
                    tutorialSource.Play();
                    tutorialSource.volume = 0.0f;
                    StartCoroutine(FadeInTutorialAudio(0.5f, menuSource.isPlaying? 0.25f : 0.0f));
                }
            }
            else
            {
                if (menuSource != null && !menuSource.isPlaying)
                {
                    menuSource.Play();
                }
            }

            // fade in music after game
            if (level == 6)
            {
                menuSource.volume = 0.0f;

                StartCoroutine(FadeInMenuAudio(1.0f));
            }

			pollenMaster = null;
		}
	}

    private IEnumerator FadeInGameAudio(float seconds)
    {
        if (!VolumeController.muteState)
        {
            AudioListener.volume = 1.0f;
        }

        float timer = 0.0f;

        while (timer < 1.0f)
        {
            float volume = timer * timer * timer;

            foreach (AudioChild audioChild in currentAudioLayers)
            {
                audioChild.SetVolume(volume);
            }

            foreach (AudioSource transitionSource in transitionLayers)
            {
                transitionSource.volume = volume;
            }

            timer += Time.deltaTime / seconds;
            yield return null;
        }

        foreach (AudioChild audioChild in currentAudioLayers)
        {
            audioChild.SetVolume(1.0f);
        }

        foreach (AudioSource transitionSource in transitionLayers)
        {
            transitionSource.volume = 1.0f;
        }
    }

    private IEnumerator FadeInMenuAudio(float seconds)
    {
        if (!VolumeController.muteState)
        {
            AudioListener.volume = 1.0f;
        }

        float timer = 0.0f;

        while (timer < 1.0f)
        {
            menuSource.volume = timer * timer * timer;

            timer += Time.deltaTime / seconds;
            yield return null;
        }

        menuSource.volume = 1.0f;
    }

    private IEnumerator FadeOutMenuAudio(float seconds)
    {
        if (!VolumeController.muteState)
        {
            AudioListener.volume = 1.0f;
        }

        float timer = 0.0f;

        while (timer < 1.0f)
        {
            menuSource.volume =  1 - timer * timer * timer;

            timer += Time.deltaTime / seconds;
            yield return null;
        }

        menuSource.volume = 0.0f;
        
    }

    private IEnumerator FadeInTutorialAudio(float seconds, float delay)
    {
        if (!VolumeController.muteState)
        {
            AudioListener.volume = 1.0f;
        }
        yield return new  WaitForSeconds(delay);

        float timer = 0.0f;

        while (timer < 1.0f)
        {
            tutorialSource.volume = timer * timer * timer;

            timer += Time.deltaTime / seconds;
            yield return null;
        }

        tutorialSource.volume = 1.0f;
    }

	
	void SetLevelPercentages()
	{
		
		if(pollenMaster != null)
		{
//			print ("FDSFD");
			float percentageFilledIn = 0.0f;
			float percentageRequired = 0.0f;
			percentageFilledIn = pollenMaster.GetPercentageFilledIn();
			percentageRequired = pollenMaster.percentageRequiredToComplete;
			percentageTillComplete = percentageFilledIn/percentageRequired;
			
		}
		
	}

	
	
	
	

}
