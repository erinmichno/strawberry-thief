﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(SceneLoader))]
public class LevelManager : MonoBehaviour
{
    public static event Action OnLevelComplete = null;
    public static event Action OnCursorCentred = null;
    public static event Action OnNewLevelStarted = null;
    public static event Action<List<Transform>, int> OnNewLevelObjectsLoaded = null;
    public static event Action OnIntroFinished = null;

    public class LevelData
    {
        public float cursorMaxSpeed;
        public float cursorFilterPower;
        public int panelsWide;
        public int panelsHigh;
        public int baseTexture;
        public int blendTexture;
        public float defaultZoom;
        public float nextZoom;
        public float transitionPeriod = 20.0f;
        public Vector2 levelEnd;
        public float percentageRequiredToComplete = 0.25f;
        public int brushSize;

        public LevelData(float _cursorMaxSpeed, float _cursorFilterPower, int _panelsWide, int _panelsHigh, int _baseTexture, int _blendTexture, float _defaultZoom, float _nextZoom, Vector2 _levelEnd, float percent, int _brushSize)
        {
            brushSize = _brushSize;
            cursorMaxSpeed = _cursorMaxSpeed;
            cursorFilterPower = _cursorFilterPower;
            panelsWide = _panelsWide;
            panelsHigh = _panelsHigh;
            baseTexture = _baseTexture;
            blendTexture = _blendTexture;
            defaultZoom = _defaultZoom;
            nextZoom = _nextZoom;
            levelEnd = _levelEnd;
			percentageRequiredToComplete = percent;
        }
    }

    private static List<LevelData> levelDataList = new List<LevelData>();
    private static int currentLevel = 0;
    
    public static int CurrentLevel
    {
        get
        {
            return currentLevel;
        }
    }

    private static bool zoomComplete = false;
    private static bool blendComplete = false;
    private static bool dataLoaded = false;

    private static SceneLoader nextSceneLoader = null;

    public Vector2 Level1End = new Vector2(1636.0f, 1824.0f);
    public Vector2 Level2End = new Vector2(3048.0f, 3072.0f);
    public Vector2 Level3End = new Vector2(2816.0f, 3072.0f);
    
	public float Level1Zoom = 2.0f;
	public float Level2Zoom = 1.0f;
	public float Level3Zoom = 0.75f;
	public float Level4Zoom = 0.5f;

    public float Level1MaxSpeed = 1.0f;
    public float Level2MaxSpeed = 2.0f;
    public float Level3MaxSpeed = 4.0f;

    public float Level1FilterPower = 0.2f;
    public float Level2FilterPower = 0.2f;
    public float Level3FilterPower = 0.2f;
    
    
	public float Level1Percent = 0.25f;
	public float Level2Percent = 0.4f;
	public float Level3Percent = 0.3f;

    public int Level1BrushSize = 96;
    public int Level2BrushSize = 96;
    public int Level3BrushSize = 96;

    public static List<List<AudioClip>> currentInstrumentClips = null;
    private List<List<AudioClip>> nextInstrumentClips = null;

    public static List<Texture2D> backgroundTextures = null;
    public static List<Texture2D> blendTextures = null;
    private List<Texture2D> nextTextures = null;

    private const string level2DataSceneName = "Level2Data";
    private const string level3DataSceneName = "Level3Data";

    private static List<UnityEngine.Object> cleanUpObjects = new List<UnityEngine.Object>();

	void Awake()
    {
        currentLevel = 0;

        zoomComplete = false;
        blendComplete = false;
        dataLoaded = false;

        currentInstrumentClips = null;

        backgroundTextures = null;
        blendTextures = null;

        nextSceneLoader = GetComponent<SceneLoader>();

        levelDataList = new List<LevelData>();

        if (levelDataList.Count == 0)
        {
            levelDataList.Add(new LevelData(Level1MaxSpeed, Level1FilterPower, 1, 1, 1, 2, Level1Zoom, Level2Zoom, Level1End, Level1Percent, Level1BrushSize));
            levelDataList.Add(new LevelData(Level2MaxSpeed, Level2FilterPower, 2, 1, 2, 3, Level2Zoom, Level3Zoom, Level2End, Level2Percent, Level2BrushSize));
            levelDataList.Add(new LevelData(Level3MaxSpeed, Level3FilterPower, 2, 2, 3, 4, Level3Zoom, Level4Zoom, Level3End, Level3Percent, Level3BrushSize));
        }

        LevelDataRepository dataRepository = GameObject.FindGameObjectWithTag("LevelDataRepository").GetComponent<LevelDataRepository>();
        currentInstrumentClips = dataRepository.instrumentClips;

        backgroundTextures = dataRepository.backgroundTextures;
        blendTextures = dataRepository.blendTextures;

        GameObject.Destroy(dataRepository.gameObject);

        OnNewLevelStarted += OnNewLevelStartedInternal;
	}

    private void OnNewLevelStartedInternal()
    {
        StartCoroutine(UnloadCleanupObjects());
    }

    private IEnumerator UnloadCleanupObjects()
    {
        // yield once to ensure all objects are finished with
        yield return null;

        foreach (UnityEngine.Object cleanUpObject in cleanUpObjects)
        {
            Resources.UnloadAsset(cleanUpObject);
            yield return null;
        }

        cleanUpObjects.Clear();
    }

    void OnDestroy()
    {
        OnLevelComplete = null;
        OnCursorCentred = null;
        OnNewLevelStarted = null;
        OnNewLevelObjectsLoaded = null;
        OnIntroFinished = null;
    }

    public static LevelData GetCurrentLevelData()
    {
        if (levelDataList == null || currentLevel < 0 || currentLevel >= levelDataList.Count)
        {
            return null;
        }

        return levelDataList[currentLevel];
    }

    public static AudioClip GetNextSceneBaseTrack(int bcv)
    {
        return currentInstrumentClips[0][bcv];
    }

    // currently called from cursor
    public static void NotifyIntroFinished()
    {
        if (OnIntroFinished != null)
        {
            OnIntroFinished();
        }
    }

    // currently called from BlendPollen
    public static void NotifyLevelComplete()
    {
        if (OnLevelComplete != null)
        {
            OnLevelComplete();
        }

        if (currentLevel == 0)
        {
            Application.LoadLevelAdditiveAsync(level2DataSceneName);
        }
        else if (currentLevel == 1)
        {
            Application.LoadLevelAdditiveAsync(level3DataSceneName);
        }
        else
        {
            // no data to load, mark as true to pass data gate
            dataLoaded = true;
        }
    }

    // currently called from Cursor
    public static void NotifyCursorCentred()
    {
        if (OnCursorCentred != null)
        {
            OnCursorCentred();
        }
    }

    // currently called from Camera2DS
    public static void NotifyZoomComplete()
    {
        zoomComplete = true;

        if (blendComplete && dataLoaded)
        {
            NotifyNewLevelStarted();
        }
    }

    // currently called from BlendPollen
    public static void NotifyBlendComplete()
    {
        blendComplete = true;

        if (zoomComplete && dataLoaded)
        {
            //print ("got here");
            NotifyNewLevelStarted();
        }
    }

    // currently called from LevelDataRepository
    public static void NotifyAsyncDataLoaded()
    {
        dataLoaded = true;

        cleanUpObjects.AddRange(backgroundTextures.ToArray());
        foreach (List<AudioClip> instrumentClips in currentInstrumentClips)
        {
            cleanUpObjects.AddRange(instrumentClips.ToArray());
        }

        LevelDataRepository dataRepository = GameObject.FindGameObjectWithTag("LevelDataRepository").GetComponent<LevelDataRepository>();

        currentInstrumentClips = dataRepository.instrumentClips;

        backgroundTextures = blendTextures;
        blendTextures = dataRepository.blendTextures;

        List<Transform> newObjectTransforms = new List<Transform>();

        for(int i = 0; i < dataRepository.spriteRoot.transform.childCount; ++i)
        {
            newObjectTransforms.Add(dataRepository.spriteRoot.transform.GetChild(i));
        }

        OnNewLevelObjectsLoaded(newObjectTransforms, currentLevel + 1);

        GameObject.Destroy(dataRepository.spriteRoot);
        GameObject.Destroy(dataRepository.gameObject);

        if (zoomComplete && blendComplete)
        {
            NotifyNewLevelStarted();
        }
    }

    private static void NotifyNewLevelStarted()
    {
        zoomComplete = false;
        blendComplete = false;
        dataLoaded = false;

        if (currentLevel + 1 >= levelDataList.Count)
        {
            // exit game I guess?
            nextSceneLoader.OnSceneExit();
        }
        else
        {
            ++currentLevel;

            if (OnNewLevelStarted != null)
            {
                OnNewLevelStarted();
            }
        }
    }
}
