﻿using UnityEngine;
using System.Collections;

// NGUI seems to automatically put all UI elements into the "NGUI" layer, this script overrides this behaviour
public class NGUILayerOverride : MonoBehaviour
{
    public int layerNumber = -1;

	void Update()
    {
        if (layerNumber != -1)
        {
            SetLayerRecursively(gameObject, layerNumber);
        }

        enabled = false;
	}

    void SetLayerRecursively(GameObject obj, int newLayer)
    {
        //Debug.Log("Setting layer for " + obj.name);

        obj.layer = newLayer;
        
        foreach(Transform child in obj.transform)
        {
            SetLayerRecursively(child.gameObject, newLayer);
        }
    }
}
