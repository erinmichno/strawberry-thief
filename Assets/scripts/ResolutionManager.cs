﻿using UnityEngine;
using System.Collections;

public static class ResolutionManager
{
    private static int LOW_RES_THRESHOLD = 480;

    public static bool IsLowResDevice
    {
        get
        {
            return Screen.width <= LOW_RES_THRESHOLD;
        }
    }

    public static bool IsHighResDevice
    {
        get
        {
            return Screen.width > LOW_RES_THRESHOLD;
        }
    }
}
