using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider))]
public class BrushPath : MonoBehaviour
{
    private int MaxThreads = 100;
    private Camera renderCamera = null;
    public Material brushMaterial = null;
    public float drawingThreshold = 16.0f;

    private Vector2 quadSize = Vector2.zero;

    private List<LivePoint> drawingPoints = new List<LivePoint>();
    private List<DyingPoint> dyingPoints = new List<DyingPoint>();

    private CRPath crPath = new CRPath();
    private CRPath.Iterator crIterator = new CRPath.Iterator();
    private bool pathMovementStarted = false;
    
    public float splineControlPointThreshold = 160.0f;

    private Vector2 lastDrawPoint = Vector2.zero;

    public float dyingLifeTime = 1.0f;
    public float fadeInSpeed = 8.0f;

    public bool canDraw = true;
    private bool isDrawing = false;

    private const float PIXEL_SCALE = 1536.0f;

    private float scaleFactor = 1.0f;

    public static float timerNotDrawing = 0.0f;

    private float travellingTimer = 0.0f;
    
    public float textureScale = 1.0f;
    private Vector2 renderSize = Vector2.zero;
    private Vector2 TL, TR, BL, BR;

    public bool IsPathFinished
    {
        get
        {
            if (Mathf.Abs(crIterator.distance - crPath.TotalLength) < 0.05f)
            {
                return true;
            }
            return false;
        }
    }

    private struct LivePoint
    {
        public Vector2 position;
        public Quaternion rotation;
        public float alpha;
        public float splineDistance;
        public bool fullyFadedIn;

        public LivePoint(Vector2 _position, Quaternion _rotation, float _alpha, float _splineDistance)
        {
            position = _position;
            rotation = _rotation;
            alpha = _alpha;
            splineDistance = _splineDistance;
            fullyFadedIn = false;
        }
    }

    private struct DyingPoint
    {
        public Vector2 position;
        public Quaternion rotation;
        public float lifeTime;
        public float remainingLife;

        public float Alpha
        {
            get
            {
                return remainingLife / lifeTime;
            }
        }

        public DyingPoint(Vector2 _position, Quaternion _rotation, float _lifeTime, float _remainingLife)
        {
            position = _position;
            rotation = _rotation;
            lifeTime = _lifeTime;
            remainingLife = _remainingLife;
        }
    }

    void Start()
    {
        renderCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        Texture brushTexture = brushMaterial.mainTexture;

        quadSize = new Vector2(brushTexture.width / PIXEL_SCALE, brushTexture.height / PIXEL_SCALE) * textureScale;

        if (drawingThreshold < 1.0f)
        {
            throw new UnityException("Drawing threshold for brush must be at least one pixel to avoid looping issues");
        }

        LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnNewLevelStarted += OnNewLevelStarted;
        LevelManager.OnIntroFinished += OnIntroFinished;

        OnNewLevelStarted();
    }

    void OnLevelComplete()
    {
        crIterator.Reset();
        crPath.Clear();
        pathMovementStarted = false;

        KillAllPoints();

        isDrawing = false;
        canDraw = false;
    }

    void OnNewLevelStarted()
    {
        canDraw = LevelManager.CurrentLevel != 0;

        scaleFactor = 2.0f / LevelManager.GetCurrentLevelData().defaultZoom;
        renderSize = quadSize * scaleFactor;
        timerNotDrawing = 0.0f;

        travellingTimer = 0.0f;
    }

    private void OnIntroFinished()
    {
        canDraw = true;
    }

    void OnPress(bool isDown)
    {
        
        if (canDraw)
        {
            if (isDown)
            {
                if (Time.timeScale > 0.0f)
                {
                    KillAllPoints();

                    lastDrawPoint = renderCamera.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y)) - transform.position;

                    isDrawing = true;

                    crIterator.Reset();
                    pathMovementStarted = false;

                    crPath.Clear();
                    crPath.AddControlPoint(lastDrawPoint);
                }
            }
            else
            {
                if (isDrawing)
                {
                    Vector2 drawingPos = renderCamera.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y)) - transform.position;

                    AddPathEndPoint(drawingPos, true);

                    isDrawing = false;
                }
            }
        }
    }

    void LateUpdate()
    {
        if(isDrawing)
        {
            timerNotDrawing = 0.0f;

            Vector2 drawingPos = renderCamera.ScreenToWorldPoint(new Vector2(Input.mousePosition.x, Input.mousePosition.y)) - transform.position;

            
            if(drawingPoints.Count > MaxThreads)
            {
                OnPress(true);
            }

            if ((drawingPos - lastDrawPoint).magnitude >= (splineControlPointThreshold * scaleFactor) / PIXEL_SCALE)
            {
                AddPathPoint(drawingPos, true);
            }
        }
        else
        {
            timerNotDrawing += Time.deltaTime;
        }

        //crPath.DebugDraw();

        // update the live points alpha values
        
        // fade in new points
        for (int i = 0; i < drawingPoints.Count; ++i)
        {
            if (!drawingPoints[i].fullyFadedIn)
            {
                LivePoint point = drawingPoints[i];
                point.alpha = point.alpha + Time.deltaTime * fadeInSpeed;

                if (point.alpha > 1.0f)
                {
                    point.fullyFadedIn = true;
                    point.alpha = 1.0f;
                }

                drawingPoints[i] = point;
            }
        }

        

        // update the dying points
        for (int i = 0; i < dyingPoints.Count; ++i)
        {
            DyingPoint point = dyingPoints[i];
            point.remainingLife -= Time.deltaTime;

            if (dyingPoints[i].remainingLife <= 0)
            {
                dyingPoints.RemoveAt(i);
                --i;
            }
            else
            {
                dyingPoints[i] = point;
            }
        }
    }

    public void AddSpinOutPath(Vector3 origin, Vector3 direction, float distance, float spread)
    {
        if (isDrawing)
        {
            AddPathEndPoint(lastDrawPoint, true);

            isDrawing = false;
        }

        KillAllPoints();

        crIterator.Reset();
        crPath.Clear();
        pathMovementStarted = false;

        canDraw = false;

        //while (origin.x >= Camera2DS.MapDimensions.x)
        //{
        //    origin.x -= Camera2DS.MapDimensions.x;
        //}
        //while (origin.x < 0.0f)
        //{
        //    origin.x += Camera2DS.MapDimensions.x;
        //}

        //while (origin.y >= Camera2DS.MapDimensions.y)
        //{
        //    origin.y -= Camera2DS.MapDimensions.y;
        //}
        //while (origin.y < 0.0f)
        //{
        //    origin.y += Camera2DS.MapDimensions.y;
        //}

        distance *= scaleFactor;
        spread *= scaleFactor;

        int segmentCount = Mathf.FloorToInt(distance / (drawingThreshold * scaleFactor));
        
        if(segmentCount < 2)
        {
            segmentCount = 2;
        }

        Vector2 normal = new Vector2(-direction.y, direction.x);

        AddPathPoint((Vector2)origin - (Vector2)transform.position, false);

        for (int i = 1; i < segmentCount; ++i)
        {
            Vector2 point = (Vector2)origin + (Vector2)direction * ((distance * i) / segmentCount) / PIXEL_SCALE;
            point += normal * Random.Range(-spread, spread) / PIXEL_SCALE;

            if (i == segmentCount - 1)
            {
                AddPathEndPoint(point - (Vector2)transform.position, false);
            }
            else
            {
                AddPathPoint(point - (Vector2)transform.position, false);
            }
        }
    }


    private void AddPathPoint(Vector2 point, bool draw)
    {
        if (crPath.AddControlPoint(point))
        {
            if (draw)
            {
                
                List<CRPath.SplinePointInfo> splinePoints = crPath.GetLastSegmentPoints((drawingThreshold * scaleFactor) / PIXEL_SCALE);

                for (int i = 0; i < splinePoints.Count; ++i)
                {
                    Vector2 nextDrawPoint = splinePoints[i].position;
                    float rotationAngle = Mathf.Acos(Vector2.Dot(Vector2.up, splinePoints[i].direction)) * Mathf.Sign(Vector3.Cross(Vector2.up, splinePoints[i].direction).z);

                    drawingPoints.Add(new LivePoint(nextDrawPoint, Quaternion.Euler(0.0f, 0.0f, rotationAngle * Mathf.Rad2Deg), -((float)i / splinePoints.Count) / fadeInSpeed, splinePoints[i].distance));
                }
            }
        }

        lastDrawPoint = point;
    }

    private void AddPathEndPoint(Vector2 point, bool draw)
    {
        if (crPath.AddEndCapControlPoint(point))
        {
            if (draw)
            {
                List<CRPath.SplinePointInfo> splinePoints = crPath.GetLastSegmentPoints((drawingThreshold * scaleFactor) / PIXEL_SCALE);

                for (int i = 0; i < splinePoints.Count; ++i)
                {
                    Vector2 nextDrawPoint = splinePoints[i].position;
                    float rotationAngle = Mathf.Acos(Vector2.Dot(Vector2.up, splinePoints[i].direction)) * Mathf.Sign(Vector3.Cross(Vector2.up, splinePoints[i].direction).z);

                    drawingPoints.Add(new LivePoint(nextDrawPoint, Quaternion.Euler(0.0f, 0.0f, rotationAngle * Mathf.Rad2Deg), -((float)i / splinePoints.Count) / fadeInSpeed, splinePoints[i].distance));
                }
            }
        }

        lastDrawPoint = point;
    }

    private Vector2 PointToWorld(Vector2 point)
    {
        return point + (Vector2)transform.position;
    }

    private Vector2 GetPathWorldPoint(int index)
    {
        if (index < 0 || index >= drawingPoints.Count)
        {
            throw new UnityException("Bad Index in brush path");
        }

        return drawingPoints[index].position + (Vector2)transform.position;
    }

    private Vector2 GetDyingPathWorldPoint(int index)
    {
        if (index < 0 || index >= dyingPoints.Count)
        {
            throw new UnityException("Bad Index in brush path");
        }

        return dyingPoints[index].position + (Vector2)transform.position;
    }

    public bool UpdateTargetPos(Vector3 position, ref Vector3 target, ref Quaternion targetRotation, ref float turningFactor, float maxSpeed, float filterPower)
    {
        if (crPath.Empty)
        {
            target = position;
            return false;
        }

        float pathLength = 0.0f;
        float distToPath = (PointToWorld(crPath.FirstPoint) - (Vector2)position).magnitude;

        if (!pathMovementStarted)
        {
            pathLength = distToPath + crPath.TotalLength;
        }
        else
        {
            pathLength = crPath.TotalLength - crIterator.distance;
        }
        
        float speed = pathLength * filterPower;

        if (speed > maxSpeed * Time.deltaTime)
        {
            speed = maxSpeed * Time.deltaTime;
        }

        if (!pathMovementStarted && distToPath > speed)
        {
            target = position + ((Vector3)PointToWorld(crPath.FirstPoint) - position).normalized * speed;
            targetRotation = Quaternion.LookRotation(Vector3.forward, target - position);

            //float amountMoved = Mathf.Abs(transform.position.magnitude - target.magnitude);

            travellingTimer += Time.deltaTime;

            if (travellingTimer > 5.0f)
            {
                KillAllPoints();

                crIterator.Reset();
                crPath.Clear();
                pathMovementStarted = false;

                canDraw = true;

                travellingTimer = 0.0f;

                return true;
            }

            return false;
        }

        travellingTimer = 0.0f;

        pathMovementStarted = true;

        CRPath.SplinePointInfo pointInfo = crPath.Update(speed, 1.0f, crIterator);

        if (pointInfo == null)
        {
            target = position;
            return false;
        }

        target = PointToWorld(pointInfo.position);
        targetRotation = Quaternion.LookRotation(Vector3.forward, pointInfo.direction);
        turningFactor = pointInfo.turningFactor;

        if ((position - target).magnitude / Time.deltaTime > maxSpeed * 10.0f)
        {
            // path overshoot, reset drawing and cancel move to suppress movement bug
            OnPress(true);
            target = position;
            return false;

            Debug.LogWarning("CRAZY MOVEMENT SUPPRESSED...");
        }

        // kill any points we've travelled past
        while(drawingPoints.Count > 0 && drawingPoints[0].splineDistance <= pointInfo.distance)
        {
            KillPoint(0);
        }

        return true;
    }

    private void KillAllPoints()
    {
        while (drawingPoints.Count > 0)
        {
            KillPoint(0);
        }
    }

    private void KillPoint(int index)
    {
        
        if (index >= drawingPoints.Count)
        {
            return;
        }

        // add to dying points if visible
        if (drawingPoints[index].alpha >= 0.0f)
        {
            dyingPoints.Add(new DyingPoint(drawingPoints[index].position, drawingPoints[index].rotation, dyingLifeTime, drawingPoints[index].alpha * dyingLifeTime));
        }

        // remove from list
        drawingPoints.RemoveAt(index);
    }

    public void Render()
    {
        for (int i = 0; i < drawingPoints.Count; ++i)
        {
            GLRenderMatToQuad(brushMaterial, GetPathWorldPoint(i), drawingPoints[i].rotation, quadSize * scaleFactor, new Color(0.0f, 0.0f, 0.0f, Mathf.Max(drawingPoints[i].alpha, 0.0f)));
        }

        for (int i = 0; i < dyingPoints.Count; ++i)
        {
            GLRenderMatToQuad(brushMaterial, GetDyingPathWorldPoint(i), dyingPoints[i].rotation, quadSize * scaleFactor, new Color(0.0f, 0.0f, 0.0f, dyingPoints[i].Alpha));
        }
    }

    public void RenderOp()
    {
        GLRenderLoop(brushMaterial);
    }

    void GLRenderLoop(Material mat)
    {
        Color colorToUse = Color.black;
        GL.PushMatrix();

        

        for (var j = 0; j < mat.passCount; ++j)
        {
            mat.SetPass(j);
            for (int m = 0; m < drawingPoints.Count; ++m)
            {
                colorToUse.a = Mathf.Max(drawingPoints[m].alpha, 0.0f);
                GLRenderQuads(GetPathWorldPoint(m), drawingPoints[m].rotation, renderSize, colorToUse);
            }
            for (int i = 0; i < dyingPoints.Count; ++i)
            {
                colorToUse.a = dyingPoints[i].Alpha;
                GLRenderQuads(GetDyingPathWorldPoint(i), dyingPoints[i].rotation, renderSize, colorToUse);
            }
        }

        GL.PopMatrix();
    }

    void GLRenderQuads(Vector2 quadPosition, Quaternion quadRotation, Vector2 quadSize, Color color)
    {
        TR.x = quadSize.x;
        TR.y = quadSize.y;

        TL.x = -quadSize.x;
        TL.y = quadSize.y;

        BL.x = -quadSize.x;
        BL.y = -quadSize.y;

        BR.x = quadSize.x;
        BR.y = -quadSize.y;

        TR = quadRotation * TR;
        TL = quadRotation * TL;
        BL = quadRotation * BL;
        BR = quadRotation * BR;

        TR += quadPosition;
        TL += quadPosition;
        BL += quadPosition;
        BR += quadPosition;

        GL.Begin(GL.QUADS);

        GL.Color(color);
        GL.TexCoord2(0.0f, 0.0f);
        GL.Vertex3(BL.x, BL.y, 0.1f);
        GL.TexCoord2(1.0f, 0.0f);
        GL.Vertex3(BR.x, BR.y, 0.1f);
        GL.TexCoord2(1.0f, 1.0f);
        GL.Vertex3(TR.x, TR.y, 0.1f);
        GL.TexCoord2(0.0f, 1.0f);
        GL.Vertex3(TL.x, TL.y, 0.1f);

        GL.End();


    }
    
    // TODO: render list, also set color channel to handle fade on destroy
    void GLRenderMatToQuad(Material mat, Vector2 quadPosition, Quaternion quadRotation, Vector2 quadSize, Color color)
    {
        GL.PushMatrix();

        //GL.wireframe = true;

        Vector2 TR = new Vector2(quadSize.x, quadSize.y);
        Vector2 TL = new Vector2(-quadSize.x, quadSize.y);
        Vector2 BL = new Vector2(-quadSize.x, -quadSize.y);
        Vector2 BR = new Vector2(quadSize.x, -quadSize.y);

        TR = quadRotation * TR;
        TL = quadRotation * TL;
        BL = quadRotation * BL;
        BR = quadRotation * BR;

        TR += quadPosition;
        TL += quadPosition;
        BL += quadPosition;
        BR += quadPosition;

        for (var i = 0; i < mat.passCount; ++i)
        {
            mat.SetPass(i);

            GL.Begin(GL.QUADS);

            GL.Color(color);
            GL.TexCoord2(0.0f, 0.0f);
            GL.Vertex3(BL.x, BL.y, 0.1f);
            GL.TexCoord2(1.0f, 0.0f);
            GL.Vertex3(BR.x, BR.y, 0.1f);
            GL.TexCoord2(1.0f, 1.0f);
            GL.Vertex3(TR.x, TR.y, 0.1f);
            GL.TexCoord2(0.0f, 1.0f);
            GL.Vertex3(TL.x, TL.y, 0.1f);

            GL.End();
        }
        GL.PopMatrix();
    }

    public void OnCameraWrap(Vector3 wrapDiff)
    {
        transform.position += wrapDiff;

        // need to ensure collider remains where it initially started
        ((BoxCollider)collider).center = -transform.localPosition;
    }
}
