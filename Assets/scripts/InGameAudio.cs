﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InGameAudio : MonoBehaviour
{
    public List<AudioClip> pollenPotClips = new List<AudioClip>();
    public List<AudioClip> bugClips = new List<AudioClip>();

    private static List<AudioClip> pollenPotClipsStatic = new List<AudioClip>();
    private static List<AudioClip> bugClipsStatic = new List<AudioClip>();

    private static List<AudioSource> audioSourcePool = new List<AudioSource>();

	void Awake()
    {
        pollenPotClipsStatic.Clear();
        bugClipsStatic.Clear();
        audioSourcePool.Clear();

        for (int i = 0; i < 5; ++i)
        {
            GameObject sourceObject = new GameObject("InGameSource");
            sourceObject.transform.parent = transform;

            AudioSource source = sourceObject.AddComponent<AudioSource>();
            source.playOnAwake = false;
            source.loop = false;

            audioSourcePool.Add(source);
        }

        // copy the clip references into the static lists
        foreach (AudioClip clip in pollenPotClips)
        {
            pollenPotClipsStatic.Add(clip);
        }

        foreach (AudioClip clip in bugClips)
        {
            bugClipsStatic.Add(clip);
        }
	}

    private static void StartSource(AudioClip clip)
    {
        foreach (AudioSource source in audioSourcePool)
        {
            if (!source.isPlaying)
            {
                source.clip = clip;
                source.Play();

                break;
            }
        }
    }

    public static void PlayPollenPotClip(int clipNumber = -1)
    {
        if (clipNumber < 0 || clipNumber >= pollenPotClipsStatic.Count)
        {
            StartSource(pollenPotClipsStatic[Random.Range(0, pollenPotClipsStatic.Count)]);
        }
        else
        {
            StartSource(pollenPotClipsStatic[clipNumber]);
        }
    }

    public static void PlayBugClip(int clipNumber = -1)
    {
        if (clipNumber < 0 || clipNumber >= bugClipsStatic.Count)
        {
            StartSource(bugClipsStatic[Random.Range(0, bugClipsStatic.Count)]);
        }
        else
        {
            StartSource(bugClipsStatic[clipNumber]);
        }
    }
}
