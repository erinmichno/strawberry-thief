﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Camera2DS : MonoBehaviour
{

    public const int PANNEL_WIDTH = 2816;//256 + 512 + 2048
    public const int PANNEL_HEIGHT = 3072;//1024 + 2048

    public static int numPanelsWide = 1;
    public static int numPanelsHigh = 1;

    public const int BASE_WIDTH = 2048;//baseViewport
    public const int BASE_HEIGHT = 1536;

    public const int TILES_PER_PANEL = 4;

    public float zoomLevel = 1.0f;
    public float targetZoomLevel = 1.0f;
    public float ZOOM_MAX = 4.0f;
    public float ZOOM_MIN = 0.25f;
    protected const float MOVE_RATIO = 9.0f;

    private Vector2 posOfCamera = new Vector2(0, 0);
    private Vector2 targetPos = new Vector2(0, 0);

    private Vector2 halfScreen = new Vector2(BASE_WIDTH * 0.5f, BASE_HEIGHT * 0.5f);


    private Cursor cursor;
    private List<Camera> spriteCameras = new List<Camera>();
    private UIRoot uiRoot;
    private BrushPath brushPath;
    private GameObject[] animationPathObjects;

    public List<string> blendPrefix = new List<string>();
    public static List<Texture2D>[] blendTextures = new List<Texture2D>[2] { new List<Texture2D>(), new List<Texture2D>() };


    private Vector2 panelDimensions = new Vector2(PANNEL_WIDTH, PANNEL_HEIGHT);
    private static Vector2 mapDimensions = Vector2.one;

    public static Vector2 MapDimensions
    {
        get
        {
            return mapDimensions;
        }
    }

    bool followingCursor = true;
    bool snapToCursor = false;

    private enum ZoomState
    {
        NONE,
        ZOOMING_OUT,
        ZOOMING_IN
    }

    private ZoomState zoomState = ZoomState.NONE;

    private const float ZOOM_IN_START_LEVEL = 0.5f;
    private const float ZOOM_IN_TRANSITION_PERIOD = 8.0f;

    float zoomTimer = 0.0f;

    Vector2 levelEndOffset = Vector2.zero;

    float lastSpeed = 0.0f;
    float maxAcceleration = 0.05f;
    
    public float GetMapRatio() {return mapDimensions.x / mapDimensions.y;}

    static public void WrapVectors(ref Vector2 target, ref Vector2 original, Vector2 step)
    {
        Vector2 og = WrapWhile(original, step);
        original = og;
        //target = TargetWrapVector(target, original, step);
        print("og: " + original.ToString() + "Target:" + target.ToString());
    }
    static public Vector2 TargetWrapVector(Vector2 target, Vector2 original, Vector2 step)
    {
        target.x = TargetWrapFloat(target.x, original.x, step.x);
        target.y = TargetWrapFloat(target.y, original.y, step.y);
        return target;
    }
    static public float TargetWrapFloat(float target, float original, float step)
    {

        target = WrapWhile(target, step );

        while (Mathf.Abs(original - target) > step*0.5f)
        {
            if (original > step*0.5f)
            {
                target += step;
            }
            else
            {
                target -= step;
            }
        }
        return target;
    }

   // Yaw = MathUtility.WrapAngles0_360(Yaw);
     //   targetYaw = MathUtility.TargetWrapDegrees(targetYaw, Yaw);

    public Vector2 GetLevelEndWorld()
    {
        Vector2 centre = LevelManager.GetCurrentLevelData().levelEnd;

        centre.x *= uiRoot.transform.localScale.x;
        centre.y *= uiRoot.transform.localScale.y;

        centre += levelEndOffset;

        return centre;
    }


    const int numQuads = 16;
    public List<RenderQuad> quads = new List<RenderQuad>(numQuads);
    public List<RenderQuad> Make16Panels()
    {
        Vector2 tileWH = GetTileWH(panelDimensions);//2815/4


        Vector2 camWH = halfScreen * 2.0f;

        // Vector2 textureSize 

        //Vector2 start = -(posOfCamera);// + halfScreen;
        Vector2 start = -(Vector2)camera.transform.localPosition + halfScreen;

        //Vector2 seed;
        int panelIndex = 0;
        int panelIndexX = 0;
        int panelIndexY = 0;
        float npw = numPanelsWide;
        float nph = numPanelsHigh;
        Vector2 uv2min = Vector2.zero;
        Vector2 uv2max = Vector2.zero;
        Vector2 dist = Vector2.zero;
        for (int j = 0; j < TILES_PER_PANEL; ++j)
        {
            for (int i = 0; i < TILES_PER_PANEL; ++i)
            {

                int id = i + j * TILES_PER_PANEL;
                //seed = start;

                Vector2 BL = new Vector2((i * tileWH.x + start.x), (j * tileWH.y + start.y));
                Vector2 TR = new Vector2(((i + 1) * tileWH.x + start.x), ((j + 1) * tileWH.y + start.y));
                int STLx, STLy, SBRx, SBRy;
                Vector2 TLprime = WrapInBoundsV(BL, mapDimensions, out STLx, out STLy); //this wrap will need to change based on the number of panels present
                if (STLx != 0 || STLy != 0) //then we've wrapped
                {
                    BL = TLprime;
                    TR = BL + tileWH;
                    STLx = NegModWrap(STLx, numPanelsWide);
                    STLy = NegModWrap(STLy, numPanelsHigh);

                    panelIndexX = (STLx % numPanelsWide);
                    panelIndexY = (STLy % numPanelsHigh);
                }

                Vector2 TRPrime = WrapInBoundsV(TR, mapDimensions, out SBRx, out SBRy);
                if (SBRx != 0 || SBRy != 0)
                {
                    //print("TR one");
                    TR = TRPrime;
                    BL = TR - tileWH;
                    SBRx = NegModWrap(SBRx, numPanelsWide);
                    SBRy = NegModWrap(SBRy, numPanelsHigh);

                    panelIndexX = (SBRx % numPanelsWide);
                    panelIndexY = (SBRy % numPanelsHigh);
                }

                TR.x -= 4;
                TR.y -= 4;

                panelIndex = panelIndexX + panelIndexY * numPanelsWide;

                float B = (BL.y / camWH.y);
                float T = (TR.y / camWH.y);
                float L = (BL.x / camWH.x);
                float R = (TR.x / camWH.x); //into pixels
                //Could early exit here

                dist.x = (1.0f / npw) / (float)(TILES_PER_PANEL);
                dist.y = (1.0f / nph) / (float)(TILES_PER_PANEL);
                //i j is the index into the one panel
                uv2min.x = (float)(panelIndexX) / npw + (dist.x) * i;
                uv2min.y = (float)(panelIndexY) / nph + (dist.y) * j;
                uv2max.x = uv2min.x + (dist.x);
                uv2max.y = uv2min.y + (dist.y);

                quads[id].TL.pos = new Vector2(L, T);
                quads[id].TL.uv = new Vector2(0, 1);
                quads[id].TL.uv2 = new Vector2(uv2min.x, uv2max.y);

                quads[id].TR.pos = new Vector2(R, T);
                quads[id].TR.uv = new Vector2(1, 1);
                quads[id].TR.uv2 = new Vector2(uv2max.x, uv2max.y);

                quads[id].BR.pos = new Vector2(R, B);
                quads[id].BR.uv = new Vector2(1, 0);
                quads[id].BR.uv2 = new Vector2(uv2max.x, uv2min.y);


                quads[id].BL.pos = new Vector2(L, B);
                quads[id].BL.uv = new Vector2(0, 0);
                quads[id].BL.uv2 = new Vector2(uv2min.x, uv2min.y);

                quads[id].panelIndexLocal = panelIndex;
                quads[id].textureIndex = (i % TILES_PER_PANEL) + (TILES_PER_PANEL - 1 - (j % TILES_PER_PANEL)) * TILES_PER_PANEL; //weird b/c textures are numbered backwards in y
            }
        }

        return quads;


    }

    #region ALL_QUADS

    //ok so we know where the camera is so we can find out the TL for first seed
    RenderQuad CreateSingleGeometry(Vector2 cameraWidthHeight, Vector2 seed, Vector2 viewportAmount, Vector2 panelDims, out Vector2 dist)
    {
        //initial set up
        RenderQuad r = new RenderQuad();

        // which Panel does this belong to?  
        int panelIndexX = WrapInBounds(ref seed.x, panelDims.x); //can use to find panel number  each + - will be 
        int panelIndexY = WrapInBounds(ref seed.y, panelDims.y); //now seed in 1 panel!

        panelIndexX = NegModWrap(panelIndexX, numPanelsWide);
        panelIndexY = NegModWrap(panelIndexY, numPanelsHigh);

        r.panelIndexLocal = panelIndexX + panelIndexY * numPanelsWide;

        //Width and Height of  still to be divided by the text ratio
        Vector2 tileWH = GetTileWH(panelDims);
        
        //routine c find index of tile in the 1 panel
        Vector2 tileXY = Vector2.zero; //INT DIVISION
        tileXY.x = (int)seed.x / (int)tileWH.x;
        tileXY.y = (int)seed.y / (int)tileWH.y;

        //r.textureIndex = (int)tileXY.x + GetTexRatio() * (int)tileXY.y;
        r.textureIndex = (int)tileXY.x + (TILES_PER_PANEL - 1 - (int)tileXY.y) * TILES_PER_PANEL;

        //FIND BL

        //routine d find uv
        r.BL.uv.x = seed.x % tileWH.x;
        r.BL.uv.y = seed.y % tileWH.y;
        r.BL.uv.x /= tileWH.x;
        r.BL.uv.y /= tileWH.y;//float division

        //    r.TL.uv2 = ReadUVFinger();

        //this is 0,0 to 1,1   ROUTINE B
        r.BL.pos.x = 1.0f - (viewportAmount.x / cameraWidthHeight.x);
        r.BL.pos.y = 1.0f - (viewportAmount.y / cameraWidthHeight.y);

        //FIND TR
        r.TR.uv.x = r.BL.uv.x + viewportAmount.x / tileWH.x;
        r.TR.uv.x = Mathf.Min(1.0f, r.TR.uv.x);

        r.TR.uv.y = r.BL.uv.y + viewportAmount.y / tileWH.y;
        r.TR.uv.y = Mathf.Min(1.0f, r.TR.uv.y);

        r.TR.pos.x = (r.TR.uv.x - r.BL.uv.x) * tileWH.x;
        r.TR.pos.y = (r.TR.uv.y - r.BL.uv.y) * tileWH.y;
        dist = r.TR.pos; //THE DIST TO MOVE BY

        r.TR.pos.x /= cameraWidthHeight.x;
        r.TR.pos.y /= cameraWidthHeight.y;

        r.TR.pos += r.BL.pos;

        //TRpos BL  get TL and BR
        //TLpos is the x of BL  and y of TR
        //BR is the x of TR and the y of BL
        r.TL.pos.x = r.BL.pos.x;
        r.TL.uv.x = r.BL.uv.x;
        r.TL.pos.y = r.TR.pos.y;
        r.TL.uv.y = r.TR.uv.y;


        r.BR.pos.x = r.TR.pos.x;
        r.BR.uv.x = r.TR.uv.x;
        r.BR.pos.y = r.BL.pos.y;
        r.BR.uv.y = r.BL.uv.y;

        float xUV2Ratio = ((float)TILES_PER_PANEL * (float)numPanelsWide);
        float yUV2Ratio = ((float)TILES_PER_PANEL * (float)numPanelsHigh);

        float u2Start = (float)panelIndexX / numPanelsWide + tileXY.x / (TILES_PER_PANEL * numPanelsWide);
        float v2Start = (float)panelIndexY / numPanelsHigh + tileXY.y / (TILES_PER_PANEL * numPanelsHigh);

        r.BL.uv2.x = u2Start + r.BL.uv.x / xUV2Ratio;
        r.BL.uv2.y = v2Start + r.BL.uv.y / yUV2Ratio;

        r.TR.uv2.x = u2Start + r.TR.uv.x / xUV2Ratio;
        r.TR.uv2.y = v2Start + r.TR.uv.y / yUV2Ratio;

        r.BR.uv2.x = u2Start + r.BR.uv.x / xUV2Ratio;
        r.BR.uv2.y = v2Start + r.BR.uv.y / yUV2Ratio;

        r.TL.uv2.x = u2Start + r.TL.uv.x / xUV2Ratio;
        r.TL.uv2.y = v2Start + r.TL.uv.y / yUV2Ratio;
        
        return r;
    }

    public List<RenderQuad> MakeAllPannels()
    {
        Vector2 panelDims = new Vector2(PANNEL_WIDTH, PANNEL_HEIGHT);
        Vector2 camWH = new Vector2(BASE_WIDTH, BASE_HEIGHT) / zoomLevel;
        Vector2 halfScreen = camWH / 2.0f;

        float u = 0;
        float v = 0;
        Vector2 start = (Vector2)camera.transform.localPosition - halfScreen;
        Vector2 seed = start;//start in upper LH corner
        Vector2 viewPortAmount = camWH; // Camera WIDTH AND HEIGHT OT START WITH (with zoom!
        Vector2 subDistance;
        Vector2 prevSeed = seed;
        RenderQuad rQuad = null;
        List<RenderQuad> quadsToRender = new List<RenderQuad>();


        Vector2 TileWH = GetTileWH(panelDims);

        do
        {
            viewPortAmount.x = camWH.x; //reset x every time around
            seed.x = start.x;
            do
            {
                //get the first quad
                prevSeed = seed;

                rQuad = CreateSingleGeometry(camWH, seed, viewPortAmount, panelDims, out subDistance);
                quadsToRender.Add(rQuad);

                //move to next
                seed.x = prevSeed.x + subDistance.x;
                viewPortAmount.x -= subDistance.x;

                u = seed.x - start.x; //have we done full

            } while (u + 0.5f < camWH.x); //keep going until we've covered the distance, add the half to account for float errors

            seed.y = prevSeed.y + subDistance.y;
            viewPortAmount.y -= subDistance.y;

            v = seed.y - start.y;

        } while (v + 0.5f < camWH.y); //keep going until we've covered the height, add the half to account for float errors

        return quadsToRender;
    }

    void SetPanelIndex(ref Vector2 seed, Vector2 panelDims, RenderQuad r)
    {
        //WHICH Panel does this belong to?  
        int panelNumX = WrapInBounds(ref seed.x, panelDims.x); //can use to find panel number  each + - will be 
        int panelNumY = WrapInBounds(ref seed.y, panelDims.y); //now seed in 1 panel!
        panelNumX %= numPanelsWide;
        panelNumY %= numPanelsHigh; // so that we wrap eventually
        r.panelIndexLocal = panelNumX + panelNumY * numPanelsWide;
    }

    #endregion


    void LateUpdate()
    {
        // CheckInputAndTargets(camTargets);
        GetTargetValues();
        ClampTargets();
        ApplyTargets();


        halfScreen = new Vector2(BASE_WIDTH * 0.5f, BASE_HEIGHT * 0.5f) * 1.0f / zoomLevel;
        //now pos is camera location in pixels!
        // print("pos: " + pos.ToString());
        foreach (Camera spriteCamera in spriteCameras)
        {
            spriteCamera.transform.position = posOfCamera;
            spriteCamera.orthographicSize = 1 / zoomLevel;
        }
        if (camera)
        {
            camera.transform.position = posOfCamera;
            // camera.transform.localScale.Set(zoomLevel, zoomLevel, zoomLevel);
            camera.orthographicSize = 1 / zoomLevel;
        }

        Vector2 screenCentreOffset = (Vector2)camera.WorldToViewportPoint(GetLevelEndWorld()) * 2.0f - Vector2.one;
        Shader.SetGlobalVector("_ScreenCentreOffset", screenCentreOffset);
    }



    public Vector2 WriteUVFinger(Vector2 inputMouse)
    {
        Vector3 birdPos = spriteCameras[0].WorldToScreenPoint(cursor.transform.position);
        return WriteUVFinger(posOfCamera, inputMouse, panelDimensions, numPanelsWide, numPanelsHigh, zoomLevel);
    }

    //  ; //screen  (pixels 0 - 2048)
    public Vector2 WriteUVFinger(Vector2 cameraPos, Vector2 InputMousePos, Vector2 panelWH, int numPanelsWide, int numPanelsHigh, float zoom)
    {
        //have cameraPos
        InputMousePos.x /= Screen.width;
        InputMousePos.y /= Screen.height; //between 0 and 1
        // InputMousePos.x -= 0.5f;
        // InputMousePos.y -= 0.5f;
        // InputMousePos.y = 1 - InputMousePos.y;


        InputMousePos.x *= halfScreen.x * 2.0f;
        InputMousePos.y *= halfScreen.y * 2.0f; //now in 0 -0248


        Vector2 posL = Vector2.zero; //TODO: might need cam offset also affected by zoom?
        //  posL.y = InputMousePos.y - cameraPos.y;

        posL.x = camera.transform.localPosition.x - halfScreen.x + InputMousePos.x;
        posL.y = camera.transform.localPosition.y - halfScreen.y + InputMousePos.y;


        //in map space
        posL.x = ReduceWhile(posL.x, panelWH.x * numPanelsWide);
        posL.x = AddWhile(posL.x, panelWH.x * numPanelsWide);
        posL.y = ReduceWhile(posL.y, panelWH.y * numPanelsHigh);
        posL.y = AddWhile(posL.y, panelWH.y * numPanelsHigh); //todo update if width height is diff

        //    print("posL: " + posL.ToString() + "cam" + cameraPos.ToString() + "mouse: " +  InputMousePos.ToString());

        Vector2 uv = Vector2.zero;
        uv.x = posL.x / (panelWH.x * numPanelsWide);
        uv.y = posL.y / (panelWH.y * numPanelsHigh);
        //uv is the uv to write to
        return uv;

    }

    Vector2 ConvertFromIndex(int index, int width)
    {
        return new Vector2(index % width, index / width);
    }

    int ConvertToIndex(int x, int y, int width)
    {
        return x + y * width;
    }

    static Vector2 WrapWhile(Vector2 v, Vector2 steps)
    {
        v.x = WrapWhile(v.x, steps.x);
        v.y = WrapWhile(v.y, steps.y);
        return v;
    }
    static float WrapWhile(float v, float step)
    {
        v = AddWhile(v, step);
        v = ReduceWhile(v, step);
        return v;
    }

    static float AddWhile(float v, float step)
    {
        while (v < 0)
        {
            v += step;
        }
        return v;
    }

    static float ReduceWhile(float v, float step)
    {
        while (v >= step)
        {
            v -= step;
        }
        return v;
    }


    Vector2 WrapInBoundsV(Vector2 v, Vector2 step, out int numStepsX, out int numStepsY)
    {
        numStepsX = WrapInBounds(ref v.x, step.x);
        numStepsY = WrapInBounds(ref v.y, step.y);
        return v;
    }
    int WrapInBounds(ref float v, float step) //Routine A
    {
        int numSteps = 0;
        while (v < 0)
        {
            v += step;
            numSteps -= 1;
        }
        while (v >= step)
        {
            v -= step;
            numSteps += 1;
        }
        return numSteps;
    }

    int NegModWrap(int val, int step)
    {
        while (val >= step)
        {
            val -= step;
        }
        while (val < 0)
        {
            val += step;
        }

        return val;
    }

    int GetTexRatio()
    {
        int FullTextureSize = 4096; //TODO FIX THIS
        int CutTextureSize = 1024;

        int texRatio = FullTextureSize / CutTextureSize; //4 
        return texRatio;
    }

    Vector2 GetTileWH(Vector2 panelDims)
    {
        Vector2 tileWH = panelDims;

        int texRatio = GetTexRatio();
        tileWH /= (float)texRatio;

        return tileWH;
    }

    // Use this for initialization
    void Start()
    {
        cursor = GameObject.FindGameObjectWithTag("Cursor").GetComponent<Cursor>();
        GameObject[] spriteCameraObjects = GameObject.FindGameObjectsWithTag("SpriteCamera");
        foreach (GameObject cameraObject in spriteCameraObjects)
        {
            spriteCameras.Add(cameraObject.GetComponent<Camera>());
        }
        uiRoot = NGUITools.FindInParents<UIRoot>(gameObject);
        brushPath = GameObject.FindGameObjectWithTag("BrushPath").GetComponent<BrushPath>();
        animationPathObjects = GameObject.FindGameObjectsWithTag("AnimationPath");

        targetPos = transform.position;

        LevelManager.OnLevelComplete += OnLevelComplete;
        LevelManager.OnCursorCentred += OnCursorCentred;
        LevelManager.OnNewLevelStarted += OnNewLevelStarted;
        LevelManager.OnIntroFinished += OnIntroFinished;

        OnNewLevelStarted();
    }

    private void OnLevelComplete()
    {
        followingCursor = false;
    }

    private void OnCursorCentred()
    {
        zoomTimer = 0.0f;
        zoomState = ZoomState.ZOOMING_OUT;
    }

    private void OnNewLevelStarted()
    {
        LevelManager.LevelData levelData = LevelManager.GetCurrentLevelData();

        numPanelsWide = levelData.panelsWide;
        numPanelsHigh = levelData.panelsHigh;

        zoomLevel = levelData.defaultZoom;
        targetZoomLevel = zoomLevel;

        mapDimensions = panelDimensions;
        mapDimensions.x = mapDimensions.x * numPanelsWide;
        mapDimensions.y = mapDimensions.y * numPanelsHigh;

        blendTextures[0] = LevelManager.backgroundTextures;
        blendTextures[1] = LevelManager.blendTextures;

        for (int i = 0; i < 16; ++i)
        {
            quads.Add(new RenderQuad());
        }

        followingCursor = true;

        if (LevelManager.CurrentLevel == 0)
        {
            snapToCursor = true;
            zoomTimer = 0.0f;
            zoomState = ZoomState.ZOOMING_IN;
            zoomLevel = ZOOM_IN_START_LEVEL;
            targetZoomLevel = zoomLevel;
        }
        else
        {
            snapToCursor = false;
            zoomState = ZoomState.NONE;
        }

        levelEndOffset = Vector2.zero;
    }

    private void OnIntroFinished()
    {
        snapToCursor = false;
        zoomState = ZoomState.NONE;
        targetZoomLevel = LevelManager.GetCurrentLevelData().defaultZoom;
    }

    void GetTargetValues()
    {
        //TODO: zoom;
        GetTargetZoom();
        GetTargetPos();
    }

    void GetTargetZoom()
    {
        switch (zoomState)
        {
            case ZoomState.ZOOMING_OUT:
                {
                    zoomTimer += Time.deltaTime;

                    LevelManager.LevelData levelData = LevelManager.GetCurrentLevelData();

                    targetZoomLevel = Interpolate.Ease(Interpolate.EaseType.EaseInOutCubic)(levelData.defaultZoom, levelData.nextZoom - levelData.defaultZoom, zoomTimer, levelData.transitionPeriod);

                    if (zoomTimer >= levelData.transitionPeriod)
                    {
                        zoomState = ZoomState.NONE;
                        LevelManager.NotifyZoomComplete();
                    }
                }
                break;
            case ZoomState.ZOOMING_IN:
                {
                    zoomTimer += Time.deltaTime;

                    LevelManager.LevelData levelData = LevelManager.GetCurrentLevelData();

                    targetZoomLevel = Interpolate.Ease(Interpolate.EaseType.EaseInOutCubic)(ZOOM_IN_START_LEVEL, levelData.defaultZoom - ZOOM_IN_START_LEVEL, zoomTimer, ZOOM_IN_TRANSITION_PERIOD);

                    if (zoomTimer >= ZOOM_IN_TRANSITION_PERIOD)
                    {
                        zoomState = ZoomState.NONE;
                    }
                }
                break;
            case ZoomState.NONE:
                {
                    if (Input.GetKey(KeyCode.KeypadPlus) || Input.GetKey(KeyCode.Equals))
                    {
                        float z = 1 + 0.1f * Time.deltaTime * 4;
                        targetZoomLevel *= z;
                    }
                    else if (Input.GetKey(KeyCode.KeypadMinus) || Input.GetKey(KeyCode.Minus))
                    {
                        float z = 1 + 0.1f * Time.deltaTime * 4;
                        targetZoomLevel *= 1 / z;
                    }
                }
                break;
        }
    }

    void GetTargetPos()
    {
        if (snapToCursor)
        {
            targetPos = cursor.transform.position;
        }
        else
        {
            Vector2 screenLoc = spriteCameras[0].WorldToScreenPoint(cursor.transform.position); //screen  (pixels 0 - 2048)  or 0 to 511 in editor for example

            screenLoc.x /= Screen.width;
            screenLoc.y /= Screen.height; //between 0 and 1

            screenLoc -= Vector2.one / 2.0f; // between -0.5 and 0.5

            Vector2 boundaryDistance = new Vector2(0.2f, 0.2f);//0.3
            Vector2 boundaryStart = new Vector2(0.1f, 0.1f);//0.1
            if (Mathf.Abs(screenLoc.y) > boundaryStart.y)
            {
                targetPos.y = transform.position.y + (cursor.transform.position.y - transform.position.y) * Mathf.Min((Mathf.Abs(screenLoc.y) - boundaryStart.y) / boundaryDistance.y, 1.0f);
            }
            if (Mathf.Abs(screenLoc.x) > boundaryStart.x)
            {
                targetPos.x = transform.position.x + (cursor.transform.position.x - transform.position.x) * Mathf.Min((Mathf.Abs(screenLoc.x) - boundaryStart.x) / boundaryDistance.x, 1.0f);
            }
        }

        Vector2 og = WrapWhile(posOfCamera, mapDimensions * uiRoot.transform.localScale.x);
        Vector2 diff = og - posOfCamera;

        if (diff.magnitude > float.Epsilon)
        {
            targetPos += diff;

            cursor.OnCameraWrap(diff);

            if (cursor.CurrentState == Cursor.CursorState.CIRCLING)
            {
                levelEndOffset += diff;
            }

            brushPath.OnCameraWrap(diff);

            foreach (GameObject animationPathObject in animationPathObjects)
            {
                animationPathObject.transform.position += (Vector3)diff;
            }
            
            posOfCamera = og;
        }
        
        return;
    }

    void ClampTargets()
    {
        if(targetZoomLevel > ZOOM_MAX)
        {
            targetZoomLevel = ZOOM_MAX;
        }
        if(targetZoomLevel < ZOOM_MIN)
        {
            targetZoomLevel = ZOOM_MIN;
        }
    }


    public static bool RoughApproximately(float a, float b, float epsilon)
    {
        return (a >= (b - epsilon) && a <= (b + epsilon));
    }

    public static bool RoughApproximately(Vector3 a, Vector3 b, float epsilon)
    {
        //return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y) && Mathf.Approximately(a.z, b.z);
        return RoughApproximately(a.x, b.x, epsilon) && RoughApproximately(a.y, b.y, epsilon) && RoughApproximately(a.z, b.z, epsilon);
    }

    public static bool RoughApproximately(Vector2 a, Vector2 b, float epsilon)
    {
        //return Mathf.Approximately(a.x, b.x) && Mathf.Approximately(a.y, b.y) && Mathf.Approximately(a.z, b.z);
        return RoughApproximately(a.x, b.x, epsilon) && RoughApproximately(a.y, b.y, epsilon);
    }

    void ApplyTargets() //fix with http://docs.unity3d.com/ScriptReference/Vector3.MoveTowards.html
    {
        if (!RoughApproximately(zoomLevel, targetZoomLevel, 0.001f))
        {
            if (Mathf.Abs(targetZoomLevel - zoomLevel) < 0.01f) //usually would be amount change by
            {
                zoomLevel = targetZoomLevel;

            }
            else
            {
                float targetLine = targetZoomLevel - zoomLevel;
                zoomLevel += targetLine * MOVE_RATIO * 0.25f * Time.deltaTime;//add max step

            }
        }

        // handle look at Pos
        if (snapToCursor)
        {
            posOfCamera = targetPos;
        }
        else
        {
            if (!RoughApproximately(targetPos, posOfCamera, 0.0001f))
            {
                //if (Vector2.Distance(targetPos, posOfCamera) < 0.0001f) //usually would be amount change by
                //{
                //    posOfCamera = targetPos;
                //}
                //else
                {
                    Vector2 targetLine = targetPos - posOfCamera;
                    posOfCamera += targetLine * MOVE_RATIO * 0.25f * Time.deltaTime;

                    //posOfCamera = Vector3.MoveTowards(posOfCamera, targetPos, MOVE_RATIO * 0.125f * Time.deltaTime);
                }
            }
        }
    }
}
